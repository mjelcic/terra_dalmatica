<?php


/**
 * tabele koje je potrebno izvuci iz stare terradalmatice:
 * - wp_posts
 * - wp_postmeta
 * - wp_2_posts
 * - wp_2_postmeta
 * - wp_terms
 * - wp_term_taxonomy
 * - wp_term_relationship
 * /


/* njihova baza. postaviti parametre prije runanja*/
function getOldTableConnection() {
	$servername = "127.0.0.1";
	$username = "admin";
	$password = '***';
	$dbname = "terradal_old";

	// Connection for legacy table
	$conn = new mysqli($servername, $username, $password, $dbname);

    $conn->set_charset('utf8mb4');

    // Check connection
    if ($conn->connect_errno) {
        die("Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error);
    }

	return $conn;
}

function getNewTableConnection() {
    $servername = "127.0.0.1";
    $username = "admin";
    $password = '***';
    $dbname = "terradal_migration_2";

    // Connection for legacy table
    $conn = new mysqli($servername, $username, $password, $dbname);

    $conn->set_charset('utf8mb4');


    // Check connection
    if ($conn->connect_errno) {
        die("Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error);
    }

    return $conn;
}

// Connection for legacy table
$connNew = getNewTableConnection();

// Check connection
if ($connNew->connect_errno) {
    die("Failed to connect to MySQL: (" . $connNew->connect_errno . ") " . $connNew->connect_error);
}

// Connection for legacy table
$conn = getOldTableConnection();

// Check connection
if ($conn->connect_errno) {
    die("Failed to connect to MySQL: (" . $connNew->connect_errno . ") " . $connNew->connect_error);
}


// ovaj dio je za izvuci agente, ali cini mi se pogrešan. zasad koment out
/*
 $connNew->query("ALTER TABLE user ADD legacy_agent_id INTEGER DEFAULT NULL");

$agents = [];
$agentMetaParams = [
	"REAL_HOMES_mobile_number" => "phone_no",
	"REAL_HOMES_agent_email" => "email"
	];

$sql = "SELECT * FROM `wp_posts` where post_type = 'agent' and post_status = 'publish'";

$conn = getOldTableConnection();

$conn->real_query($sql);
$agentsRes = $conn->use_result();
$agentsCnt = 1;

while($rowFromPosts = $agentsRes->fetch_assoc()) {
		
	$agent = [
		"old_post_id" => $rowFromPosts["ID"],
		"name" => $rowFromPosts["post_title"],
		"phone_no" => "a",
		"email" => "notreal" .  $agentsCnt . "@mail.com",
		'date' => $rowFromPosts['post_date']
	];
	$agentsCnt++;

	$rowId = $rowFromPosts["ID"];

	echo $rowId;

	foreach($agentMetaParams as $oldKey => $newValue) {
		$sqlAgentMeta = "select meta_value as data from wp_postmeta where post_id=$rowId and meta_key = $oldKey";

		$conn->real_query($sqlAgentMeta);
		$res = $conn->use_result();
		if($res) {
			while($row = $res->fetch_assoc()) {
		
				$agent[$newValue] = $row["data"];	
			}
		}
	}	

	$insert = sprintf("INSERT INTO `user`(`username`, `password`, `email`, `is_active`, `roles`, `first_name`, `last_name`, `phone_no`, `personal_identification_number`, `company_name`, `activation_code`, `created_at`, `created_by`,`invalidated`, `legacy_agent_id`)
					VALUES(%s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %d, %s, %d, %d, %d)",
					"'" . $agent["name"] . "'",
					"'password'",
					"'" . $agent["email"] .  "'",
					1,
					"'ROLE_EMPLOYEE'",
					"'" . $agent["name"] . "'",
					"'" . $agent["name"] . "'",
					"'" . $agent["phone_no"] . "'",
					"'fake'",
					"'TerraDalmatica'",
					1,
					"'" . $agent["date"] . "'",
					1,
					0,
					$rowId);

					//echo $insert;die;

	if(!$connNew->query($insert
					
	)) {
    	echo "Failed to insert users: (" . $connNew->errno . ") " . $connNew->error; die;
	}}
*/



// idemo izvuci metapodatke neke: features, status, owner. mapirat cemo kasnije na nekretninu
// yes, natural joins are cool
$conn = getOldTableConnection();

// stari feature-i...
$featuresSqlOld = "SELECT tt.term_id id, t.name FROM `wp_term_taxonomy` tt natural join wp_terms t where taxonomy = 'property-feature' ORDER BY tt.`term_id` ASC";

if(!$conn->real_query($featuresSqlOld)) {
    var_dump($conn->error);
}
$oldFeaturesQueryResults = $conn->use_result();

$connNew = getNewTableConnection();

$newFeaturesSql = "SELECT fl.name, f.id FROM `feature_localization` fl inner join feature f on f.id = fl.feature_id where fl.language_id =1";

if(!$connNew->real_query($newFeaturesSql)) {
    var_dump($connNew->error);
}
$newFeaturesQueryResults = $connNew->use_result();

// mapa ce imati inpute $stariFeatureId => $noviIdIzNaseBaze.
$featureMap = [];
$oldFeatures = [];
$newFeatures = [];

while($row = $oldFeaturesQueryResults->fetch_assoc()) {
    $oldFeatures[$row["name"]] = $row["id"];
}

while($row = $newFeaturesQueryResults->fetch_assoc()) {
    $newFeatures[$row["name"]] =  $row["id"];
}

foreach ($oldFeatures as $name => $id) {
    if($newFeatures[$name]) {
        $featureMap[$id] =  (int) $newFeatures[$name];
    }
}

// dodavanje gradova...
// ista situacija sa featureima...
$locationsSqlOld = "SELECT tt.term_id id, t.name FROM `wp_term_taxonomy` tt natural join wp_terms t where taxonomy = 'property-city' ORDER BY tt.`term_id` ASC";

if(!$conn->real_query($locationsSqlOld)) {
    var_dump($conn->error);
}
$oldLocationsQueryResults = $conn->use_result();

$newLocationsSql = "SELECT ll.name, l.id FROM `location_localization` ll inner join location l on l.id = ll.location_id where ll.language_id =1";

if(!$connNew->real_query($newLocationsSql)) {
    var_dump($connNew->error);
}
$newLocationsQueryResults = $connNew->use_result();

$locationsMap = [];
$oldLocations = [];
$newLocations = [];

while($row = $oldLocationsQueryResults->fetch_assoc()) {
    $oldLocations[$row["name"]] = $row["id"];
}

while($row = $newLocationsQueryResults->fetch_assoc()) {
    $newLocations[$row["name"]] =  $row["id"];
}

foreach ($oldLocations as $name => $id) {
    if($newLocations[$name]) {
        $locationsMap[$id] = $newLocations[$name];
    }
}


// tipove ćemo hardkodirati jer nisu 1-na-1 plus ima ih malo.
$realEstateTypeMap = [
    431 => 1,
    432 => 1,
    428 => 3,
    427 => 3,
    430 => 4,
    429 => 2
];

// uzmimo lokalne client usere. kasnije cemo ih ubacivati u real estate

$newClientsSql = "SELECT * FROM user where roles = 'ROLE_USER'";

if(!$connNew->real_query($newClientsSql)) {
    var_dump($connNew->error);
}
$newClientsQueryResults = $connNew->use_result();

$clients = [];

while($row = $oldLocationsQueryResults->fetch_assoc()) {
    $clients[$row["username"]] = $row["id"];
}


// prelazimo na konkretno - real estates. izvlacimo iz wp_posts
// ovaj ce arraj sadrzavati sve realEstate-ove, koje cemo kasnije insertat
$realEstates = [];
// mapiranje podataka iz wp_postmeta. koristi se u loopu dole
$postMetaParams = [
    'REAL_HOMES_property_id' => 'property_id',
    'REAL_HOMES_property_address' => 'address',
    'REAL_HOMES_property_size' => 'living_area',
    'REAL_HOMES_property_size_postfix' => 'surface_area',
    'REAL_HOMES_property_bedrooms' => 'bedroom_count',
    'REAL_HOMES_property_price' => 'price',
    'REAL_HOMES_property_bathrooms' => 'bathroom_count',
    'REAL_HOMES_property_garage' => 'garage',
    'REAL_HOMES_tour_video_url' => 'tourVideo',
    'REAL_HOMES_property_owner_email' => 'client_email',
    'REAL_HOMES_property_owner_first_name' => 'client_first_name',
    'REAL_HOMES_property_owner_last_name' => 'client_last_name',
    'REAL_HOMES_property_owner_phone' => 'client_phone',
    'REAL_HOMES_property_private_note' => 'administration_note',
    '_yoast_wpseo_metadesc' => 'hr_desc_meta',
    '_yoast_wpseo_title' => 'hr_title_meta',
    'REAL_HOMES_featured' => 'featured'
];

// these are relevant values from serialized field in meta table (REAL_HOMES_additional_details). Source of all evil
$postMetaParamsAddDetailsMapper = [
    'Udaljenost do mora' => 'sea_distance',
    'Energetska učinkovitost' => 'energy_efficency',
    'Udaljenost od mora' => 'sea_distance',
    'Pogled na more' => 'sea_view',
    'Sea distance' => 'sea_distance',
    ' Energetska učinkovitost' => 'energy_efficency',
    'Novogradnja' => 'new_building',
    'Udaljenost do mora ' => 'sea_distance',
    'Udaljenost od mora ' => 'sea_distance',
];

/*selecting required data ID's and parameters from database wp_post. mozda post_status nije bitan, dunno*/
$sql = "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type='property'";
$conn->real_query($sql);
$resultsRealEstates = $conn->use_result();
    while($row = $resultsRealEstates->fetch_assoc()) {
		
		$propertyPostId = $row['ID'];
		$real_estate = [
			'post_id' => $row['ID'],
			'hr_id' => $row['ID'],
			'hr_title' => $row['post_title'],
			'hr_content' => $row['post_content'],
            'hr_slug' => $row['post_name'],
            'hr_title_meta' => '',
            'hr_desc_meta' => '',
			'en_title' => $row['post_title'], // we'll set same for start, and override if possible
			'en_content' => $row['post_content'], // we'll set same for start, and override if possible
            'en_slug' => $row['post_name'],
            'de_title' => $row['post_title'], // we'll set same for start, and override if possible
			'de_content' => $row['post_content'], // we'll set same for start, and override if possible
            'de_slug' => $row['post_name'],
            'date' => $row['post_date'],
            'lat' => null,
            'lng' => null,
            'garage' => false,
            'pool' => false,
            'first_row' => false,
            'bathroom_count' => 0,
            'bedroom_count' => 0,
            'price' => 0,
            'living_area' => 0,
            'surface_area' => 0,
            'energy_efficency' => null,
            'sea_view' => false,
            'sea_distance' => null,
            'features' => [],
            'agent_id' => 2,
            'client_id' => 3,
            'images' => [],
            'docs' => [],
            'location_id' => null,
            'type_id' => null,
            'tourVideo' => null,
            'address' => 'N/A',
            'administration_note' => '',
            'featured' => 0
		];

		$realEstates[] = $real_estate;
    }

    // nesto me zajebavalo, padalo, pa ovako
	if($resultsRealEstates) {
		$resultsRealEstates->free();
	}

	if($conn) {
		$conn->close();
	}

	// izvukli smo iz postova sve sto zelimo migrirati. sad idemo na obogacivanje
	foreach($realEstates as $real_estate) {
		$conn = getOldTableConnection();

		$realEstatePostMetaSQL = "SELECT meta_key, meta_value FROM wp_postmeta WHERE post_id=" . $real_estate["post_id"];

		if(!$conn->real_query($realEstatePostMetaSQL)) {
			var_dump($conn->error);
		}
		$realEstatePostMetaResults = $conn->use_result();

		if($realEstatePostMetaResults) {
			while($row = $realEstatePostMetaResults->fetch_assoc()) {

			    // unserializiraj specijalni slucaj
				if($row["meta_key"] == "REAL_HOMES_additional_details") {
					$unser = unserialize($row['meta_value']);

					foreach($unser as $key => $value) {
						if(array_key_exists($key, $postMetaParamsAddDetailsMapper)) {
							$newKey = $postMetaParamsAddDetailsMapper[$key];
							$real_estate[$newKey] = $value;
						}
					}
				}

                if($row["meta_key"] == "REAL_HOMES_property_location") {
                    $latLng = explode(",", $row["meta_value"]);

                    $real_estate['lat'] = $latLng[0];
                    $real_estate['lng'] = $latLng[1];
                }

				if(array_key_exists($row['meta_key'], $postMetaParams)) {
					$newKey = $postMetaParams[$row['meta_key']];
					$real_estate[$newKey] = $row['meta_value'];
				}
							
			}

			$realEstatePostMetaResults->free();
		} else {
			var_dump($conn->error);
		}
		$conn->close();

		// sad idemo sanitizirati podatke

        $real_estate["bedroom_count"] = array_key_exists("bedroom_count", $real_estate) ? sanitizeNumericInput($real_estate["bedroom_count"]) : 0;
        $real_estate["living_area"] = array_key_exists("living_area", $real_estate) ? sanitizeNumericInput($real_estate["living_area"]): 0;
        $real_estate["bathroom_count"] = array_key_exists("bathroom_count", $real_estate) ? sanitizeNumericInput($real_estate["bathroom_count"]): 0;
        $real_estate["price"] = array_key_exists("price", $real_estate) ? sanitizeNumericInput($real_estate["price"]): 0;
        $real_estate["garage"] = array_key_exists("garage", $real_estate) ? getBooleanValue($real_estate["garage"]) : 0;
        $real_estate["surface_area"] = array_key_exists("surface_area", $real_estate) ? getOkucnicaValue($real_estate["surface_area"]) : 0;
        $real_estate["sea_distance"] = array_key_exists("sea_distance", $real_estate) ? sanitizeNumericInput($real_estate["sea_distance"]) : 0;


        // dodati i za one serijalizirane, to ce biti malo guravo al da se


		// idemo po ostale lokalizaije u wp_2 i wp_3 posts
		// svi bi trebali imati al ajd
		if($real_estate['property_id']) {
			$conn = getOldTableConnection();

			// let's get english translation
			$englishLocaleSql = "SELECT * FROM  wp_2_posts WHERE ID IN (SELECT post_id FROM wp_2_postmeta WHERE meta_value=" . $real_estate["property_id"] . " AND meta_key='REAL_HOMES_property_id') AND post_status='publish'";

			if(!$conn->real_query($englishLocaleSql)) {
                var_dump($conn->error);
			}
			$englishLocale = $conn->use_result();

			if($englishLocale) {
				$row = $englishLocale->fetch_assoc();

				$enPostId = $row["ID"];
			
				$real_estate['en_title'] = $row['post_title'];
				$real_estate['en_content'] = $row['post_content'];
                $real_estate['en_slug'] = $row['post_name'];
                $real_estate['en_desc_meta'] = '';
                $real_estate['en_title_meta'] = '';

                $englishLocale->free();

                $englishMetaLocaleSql = "SELECT * FROM wp_2_postmeta WHERE post_id=" . $enPostId;

                if(!$conn->real_query($englishMetaLocaleSql)) {
                    var_dump($conn->error);
                }
                $englishMetaLocaleResults = $conn->use_result();

                if($englishMetaLocaleResults) {
                    while ($row = $englishMetaLocaleResults->fetch_assoc()) {
                        switch ($row['meta_key']) {
                            case '_yoast_wpseo_title':
                                $real_estate['en_title_meta'] = $row['meta_value'];

                                break;

                            case '_yoast_wpseo_metadesc':
                                $real_estate['en_desc_meta'] = $row['meta_value'];

                                break;
                        }
                    }
                    $englishMetaLocaleResults->free();
                }



            } else {
			    echo "EN LOCALE NOT HERE";
			}

            // let's get german translation as well

            $germanLocaleSql = "SELECT * FROM  wp_3_posts WHERE ID IN (SELECT post_id FROM wp_3_postmeta WHERE meta_value=" . $real_estate["property_id"] . " AND meta_key='REAL_HOMES_property_id') AND post_status='publish'";

			if(!$conn->real_query($germanLocaleSql)) {
                echo "GER LOCALE ERROR";

                var_dump($conn->error);
			}
			$germanLocale = $conn->use_result();

			if($germanLocale) {
				$row = $germanLocale->fetch_assoc();

                $dePostId = $row["ID"];


                $real_estate['de_title'] = $row['post_title'];
				$real_estate['de_content'] = $row['post_content'];
                $real_estate['de_slug'] = $row['post_name'];
                $real_estate['de_desc_meta'] = '';
                $real_estate['de_title_meta'] = '';


                $germanLocale->free();

                $germanLocaleMetaSql = "SELECT * FROM wp_3_postmeta WHERE post_id=" . $dePostId;

                if(!$conn->real_query($germanLocaleMetaSql)) {
                    var_dump($conn->error);
                }
                $germanLocaleMetaResults = $conn->use_result();

                if($germanLocaleMetaResults) {
                    while ($row = $germanLocaleMetaResults->fetch_assoc()) {
                        switch ($row['meta_key']) {
                            case '_yoast_wpseo_title':
                                $real_estate['de_title_meta'] = $row['meta_value'];

                                break;

                            case '_yoast_wpseo_metadesc':
                                $real_estate['de_desc_meta'] = $row['meta_value'];

                                break;
                        }
                    }
                    $germanLocaleMetaResults->free();
                }
			} else {
				var_dump($conn->error);
			}
			$conn->close();
		}

		$conn = getOldTableConnection();

		$featuresForRealEstateSql = "SELECT tr.term_taxonomy_id id from wp_term_relationships tr natural join wp_term_taxonomy tt where taxonomy = 'property-feature' and tr.object_id = " . $real_estate["post_id"];

        if(!$conn->real_query($featuresForRealEstateSql)) {
            var_dump($conn->error);
        }
        $featuresForRealEstateResults = $conn->use_result();

        while($row = $featuresForRealEstateResults->fetch_assoc()) {
            if(array_key_exists($row["id"], $featureMap)) {
                $real_estate["features"][] = $featureMap[$row["id"]];
            }

            if($row["id"] == 248 && !array_key_exists("garage", $real_estate)) {
                $real_estate["garage"] = true;
            }
            if($row["id"] == 247) {
                $real_estate["pool"] = true;
            }
            if($row["id"] == 273) {
                $real_estate["firstRowBySea"] = true;
            }
            if($row["id"] == 240) {
                $real_estate["sea_view"] = true;
            }
        }

        $locationForRealEstateSql = "SELECT tr.term_taxonomy_id id from wp_term_relationships tr natural join wp_term_taxonomy tt where taxonomy = 'property-city' and tr.object_id = " . $real_estate["post_id"];

        if(!$conn->real_query($locationForRealEstateSql)) {
            var_dump($conn->error);
        }
        $locationForRealEstateResults = $conn->use_result();

        $row = $locationForRealEstateResults->fetch_assoc();

        $real_estate["location_id"] = (int) $locationsMap[$row["id"]]; // no check, risky, YOLO

        $conn->close();

        $conn = getOldTableConnection();

        $typeForRealEstateSql = "SELECT tr.term_taxonomy_id id from wp_term_relationships tr natural join wp_term_taxonomy tt where taxonomy = 'property-type' and tr.object_id = " . $real_estate["post_id"];
        if(!$conn->real_query($typeForRealEstateSql)) {
            var_dump($conn->error);
        }
        $typeForRealEstateResults = $conn->use_result();

        try {
            $row = $typeForRealEstateResults->fetch_assoc();
            if(!array_key_exists($row["id"], $realEstateTypeMap)) {
                echo "SKIPPING " . $real_estate["property_id"] . " because type id is missing";
                continue;
            }
            $real_estate["type_id"] = (int) $realEstateTypeMap[$row["id"]];
        } catch (Throwable $t) {
            var_dump($t->getMessage());
            $real_estate["type_id"] = 1;
        }

        $conn->close();
        $conn = getOldTableConnection();


        // let's get images
        $imagesSql = "SELECT * FROM wp_posts p left join wp_postmeta pm on p.ID = pm.post_id WHERE ID IN (SELECT meta_value FROM `wp_postmeta` where meta_key = 'REAL_HOMES_property_images' AND post_id = " . $real_estate["post_id"] .")";
        if(!$conn->real_query($imagesSql)) {
            echo "IMAGES ERROR";
            var_dump($conn->error);
        }

        $imagesResults = $conn->use_result();
        
        while($row = $imagesResults->fetch_assoc()) {
            if(!array_key_exists($row["ID"], $real_estate["images"])) {
                $real_estate["images"][$row["ID"]] = [
                    "url" => $row["guid"],
                    "name" => $row["post_name"]
                ];
            }
            if($row["meta_key"] == "_wp_attached_file") {
                $real_estate["images"][$row["ID"]]["local_file"] = $row["meta_value"];
            }
            if($row["meta_key"] == "_wp_attachment_image_alt") {
                $real_estate["images"][$row["ID"]]["alt"] = $row["meta_value"];
            }
        }

        // let's get documents
        $docsSql = "SELECT * FROM wp_posts p left join wp_postmeta pm on p.ID = pm.post_id WHERE ID IN (SELECT meta_value FROM `wp_postmeta` where meta_key = 'REAL_HOMES_attachments' AND post_id = " . $real_estate["post_id"] .") and pm.meta_key = '_wp_attached_file'";
        if(!$conn->real_query($docsSql)) {
            echo "DOCS ERROR";

            var_dump($conn->error);
        }

        $docsResults = $conn->use_result();

        while($row = $docsResults->fetch_assoc()) {
            $real_estate["docs"][$row["ID"]] = [
                "url" => $row["guid"],
                "name" => $row["post_name"],
                "local_file" => $row["meta_value"],
                "title" => $row["post_title"],
                "mime_type" => $row["post_mime_type"]
            ];
        }

        if($connNew) {$connNew->close();}
        $connNew = getNewTableConnection();

        $status = ($real_estate["hr_title"] && $real_estate["en_title"] && $real_estate["de_title"]) ? 2 : 1;

        $RealEstateInsert = "
            INSERT INTO `real_estate` 
            (
             `location_id`, 
             `real_estate_type_id`, 
             `status_id`, 
             `bedroom_count`, 
             `first_row_by_the_sea`, 
             `sea_view`, 
             `new_building`, 
             `distance_from_the_sea`, 
             `under_construction`, 
             `pool`, 
             `bathroom_count`, 
             `garage`, 
             `price`, 
             `full_price`,
             `discount_price`,
             `living_area`, 
             `surface_area`, 
             `video_url`, 
             `longitude`, 
             `latitude`, 
             `energy_class`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`, 
             `agent_id`, 
             `client_id`, 
             `featured`,
             `virtual_walk_url`,
             `price_on_request`,
             `address`,
             `administration_note`,
             `client_note`,
             `postal_code`,
             `uid`
             ) 
              VALUES      
              ( ?,?,?,?,?,?, 0, ?, 0, ?,?,?,?,?, NULL, ?,?, NULL, ?,?,?,?, NULL, 99999, NULL, 0, 2, 3, ?,?, 0, ?,?, NULL, NULL, ? )
        ";

        $stmt = $connNew->prepare($RealEstateInsert);
        $stmt->bind_param(
            "iiiiiiiiiiddddssssisssi",
            $real_estate["location_id"],
            $real_estate["type_id"],
            $status,
            $real_estate["bedroom_count"],
            $real_estate["first_row"],
            $real_estate["sea_view"],
            $real_estate["sea_distance"],
            $real_estate["pool"],
            $real_estate["bathroom_count"],
            $real_estate["garage"],
            $real_estate["price"],
            $real_estate["price"],
            $real_estate["living_area"],
            $real_estate["surface_area"],
            $real_estate["lng"],
            $real_estate["lat"],
            $real_estate["energy_efficency"],
            $real_estate["date"],
            $real_estate["featured"],
            $real_estate["tourVideo"],
            $real_estate["address"],
            $real_estate["administration_note"],
            $real_estate["property_id"]);

        if ($result = $stmt->execute()){
            echo "success";
            $realEstateId = $stmt->insert_id;

            $stmt->free_result();
        } else {
            echo "REAL ESTATE INSERT ERROR error";
            continue;
        }


        $localeInsert = "
        INSERT INTO `real_estate_localization` 
            (
             `real_estate_id`, 
             `language_id`, 
             `title`, 
             `description`, 
             `slug`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`) 
            VALUES      (?, ?, ?, ?, ?, NOW(), NULL, 99999, NULL, 0) ";

        $localeMetaInsert = "
        INSERT INTO `real_estate_meta_localization` 
            (
             `real_estate_id`, 
             `language_id`, 
             `title`, 
             `description`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`) 
            VALUES      ( ?, ?, ?, ?, NOW(), NULL, 99999, NULL, 0)";

        $stmt = $connNew->prepare($localeInsert);
        $lnId = 1;
        $stmt->bind_param("iisss", $realEstateId, $lnId, $real_estate["hr_title"], $real_estate["hr_content"], $real_estate["hr_slug"]);


        if ($result = $stmt->execute()){
            $stmt->free_result();
        } else {
            echo "error";
        }

        $stmt2 = $connNew->prepare($localeMetaInsert);
        $stmt2->bind_param("iiss", $realEstateId, $lnId, $real_estate["hr_title_meta"], $real_estate["hr_desc_meta"]);


        if ($result = $stmt2->execute()){
            $stmt2->free_result();
        } else {
            echo "error";
        }

        if($real_estate["en_content"]) {

            $stmt = $connNew->prepare($localeInsert);
            $lnId = 2;
            $stmt->bind_param("iisss", $realEstateId, $lnId, $real_estate["en_title"], $real_estate["en_content"], $real_estate["en_slug"]);

            if ($result = $stmt->execute()){
                $stmt->free_result();
            } else {
                echo "error";
            }

            $stmt2 = $connNew->prepare($localeMetaInsert);
            $stmt2->bind_param("iiss", $realEstateId, $lnId, $real_estate["en_title_meta"], $real_estate["en_desc_meta"]);


            if ($result = $stmt2->execute()){
                $stmt2->free_result();
            } else {
                echo "error";
            }
        }

        if($real_estate["de_content"]) {

            $stmt = $connNew->prepare($localeInsert);
            $lnId = 3;
            $stmt->bind_param("iisss", $realEstateId, $lnId, $real_estate["de_title"], $real_estate["de_content"], $real_estate["de_slug"]);

            if ($result = $stmt->execute()){
                echo "success";
                $stmt->free_result();
            } else {
                echo "error";
            }

            $stmt2 = $connNew->prepare($localeMetaInsert);
            $stmt2->bind_param("iiss", $realEstateId, $lnId, $real_estate["de_title_meta"], $real_estate["de_desc_meta"]);

            if ($result = $stmt2->execute()){
                $stmt2->free_result();
            } else {
                echo "error";
            }
        }

        $featuresInsert = "INSERT INTO `real_estate_feature` 
            (`real_estate_id`, 
             `feature_id`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`) 
            VALUES      ( ?, ?, NOW(), NULL, 99999, NULL, 0) ";

        $stmt = $connNew->prepare($featuresInsert);

        foreach ($real_estate["features"] as $feature) {
            $stmt->bind_param('ii', $realEstateId, $feature);
            if ($result = $stmt->execute()){
                echo "success";
                $stmt->free_result();
            } else {
                echo "error features add";
            }
        }


        $imagesInsert = "
        INSERT INTO `real_estate_image` 
            ( 
             `real_estate_id`, 
             `image`, 
             `path`, 
             `cover`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`, 
             `original_file_name`, 
             `uuid`) 
                VALUES      (?, ?, ?, ?, NOW(), NULL, 99999, NULL, 0, '', ?) ";


        $imagesLocalizationInsert = "
        INSERT INTO `real_estate_image_meta_localization` 
            (
             `real_estate_image_id`, 
             `language_id`, 
             `alt_tag`, 
             `created_at`, 
             `updated_at`, 
             `created_by`, 
             `updated_by`, 
             `invalidated`) 
                VALUES      
                (?, ?, ?, NOW(), NULL, 99999, NULL, 0 )";

        $featured = 1;

        foreach ($real_estate["images"] as $id => $image) {
            if($image["url"] == null) {
                continue;
            }

            $stmt = $connNew->prepare($imagesInsert);
            $stmt->bind_param("issis", $realEstateId, $image["url"], $image["url"], $featured, $image["url"]);
            if ($result = $stmt->execute()){
                echo "success";
                $imageId = $stmt->insert_id;

                $stmt->free_result();

                $stmt2 = $connNew->prepare($imagesLocalizationInsert);
                foreach ([1,2,3] as $lnId) {
                    $stmt2->bind_param("iis", $imageId, $lnId, $image["alt"]);
                    if ($result = $stmt2->execute()){
                        echo "success";
                        $stmt2->free_result();
                    } else {
                        echo "error image meta add";
                        echo $stmt2->error;
                    }
                }
                $featured = 0;

            } else {
                echo "error image add";
                echo $stmt->error;
                var_dump( $image);
            }
        }
 }

	//var_dump($realEstates);



function sanitizeNumericInput($numericInputAsOriginalStringValue) {

    if(is_null($numericInputAsOriginalStringValue) || $numericInputAsOriginalStringValue == "") {
        return 0;
    }

    preg_match_all('!\d+!', $numericInputAsOriginalStringValue, $matches);

    if($matches[0] && $matches[0][0]) {
        return (int) $matches[0][0];
    }
}

function getBooleanValue($boolInputAsOriginalStringValue) {

    if(is_null($boolInputAsOriginalStringValue) || $boolInputAsOriginalStringValue == "") {
        return 0;
    }

    return 1;
}

//hue hue
function getOkucnicaValue($okucnicaOriginal) {
    if(is_null($okucnicaOriginal) || $okucnicaOriginal == "") {
        return 0;
    }

    if(substr($okucnicaOriginal, 0, 9) === "Okućnica ") {
        return sanitizeNumericInput($okucnicaOriginal);
    }

    return 0;
}

?>