<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/15/2020
 * Time: 3:02 PM
 */

namespace App\Services;


use App\Entity\RealEstateLocationPage;
use Doctrine\ORM\EntityManagerInterface;

class ServiceRealEstateLocationPage
{
    private $repo;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->repo = $entityManager->getRepository(RealEstateLocationPage::class);
    }
}