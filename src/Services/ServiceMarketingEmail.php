<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/13/2020
 * Time: 9:04 AM
 */

namespace App\Services;


use App\Entity\MarketingEmail;
use App\Utility\PostOfficeUtility;
use App\Utility\SwiftMailerUtil;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ServiceMarketingEmail extends AbstractBaseService
{
    private $postOffice;
    private $logger;

    function __construct(EntityManagerInterface $entityManager, PostOfficeUtility $postOfficeUtility, LoggerInterface $logger)
    {
        parent::__construct($entityManager, MarketingEmail::class);
        $this->postOffice = $postOfficeUtility;
        $this->logger = $logger;
    }

    public function resend(?int $id)
    {
        /** @var $mEmail MarketingEmail*/
        $mEmail = $this->getById($id);
        if($mEmail == null){
            throw new \Exception("Neispravan zahtjev za ponovno slanje marketinškog email-a!");
        }

        try {
            $this->entityManager->beginTransaction();
            $mEmail->setUpdatedAt(new \DateTime());
            $this->entityManager->flush();

            $success = $this->postOffice->sendMarketingEmail($mEmail);

            $success ? $this->entityManager->commit() : $this->entityManager->rollback();

            return $success;
        }
        catch (\Exception $exception){
            $this->logger->error("ERROR OCCURRED WHILE RESENDING MARKETING EMAIL: " . $exception->getMessage());
            return false;
        }
    }
}