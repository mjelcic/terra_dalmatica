<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/26/2020
 * Time: 4:45 PM
 */

namespace App\Services;


use App\Entity\ExchangeRate;
use Doctrine\ORM\EntityManagerInterface;

class ServiceExchangeRate extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, ExchangeRate::class);
    }

    public function getCurrentExchangeRate() : ?ExchangeRate
    {
        $qb = $this->repository->createBasicQueryBuilder("er");

        $qb->orderBy("er.created_at", "DESC")
            ->setMaxResults(1);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }
}