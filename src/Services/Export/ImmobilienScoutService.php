<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/21/2020
 * Time: 10:46 AM
 */

namespace App\Services\Export;


use App\DTO\ImmobilienScout24\Request\AddressDTO;
use App\DTO\ImmobilienScout24\Request\EnergyCertificateDTO;
use App\DTO\ImmobilienScout24\Request\PriceDTO;
use App\DTO\ImmobilienScout24\Request\RealEstateDTO;
use App\Entity\Enum\RealEstateTypeEnum;
use App\Entity\RealEstate;
use App\Entity\RealEstateImage;
use App\Utility\SystemConfig\ImmobilienScoutConfig;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ImmobilienScoutService
{
    /** @var \Immocaster_Sdk */
    private $immocaster;

    /** @var  ImmobilienScoutConfig */
    private $config;

    /** @var  $entityManager EntityManagerInterface */
    private $entityManager;

    /** @var  $logger LoggerInterface */
    private $logger;

    function __construct(ImmobilienScoutConfig $config, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;

        $this->logger = $logger;

        $this->config = $config;

        $this->immocaster = \Immocaster_Sdk::getInstance('is24', $this->config->getApiKey(), $this->config->getApiSecret());

        //Set data storage for access tokens
        /////////////////////////////////(array('mysql','DB-Host','DB-User','DB-Password','DB-Name'));
        if('prod' == $_ENV['APP_ENV']){
            $this->immocaster->setDataStorage(array('mysql', 'localhost', 'terradal_new', 'easytest123', 'terradal_new'));
        } else {
            $this->immocaster->setDataStorage(array('mysql', 'localhost:3306', 'root', '', 'terradalmatica'));
        }

        //By default, the PHP SDK uses the sandbox (test system) with the http. To use https, comment the following function:
        $this->immocaster->useHttps(true);

        //By default, Immocaster uses the ImmobilienScout24 sandbox, which provides test objects. In order to work with real data,
        //you need authorization for your Key and Secret from ImmobilienScout24. If you are authorized to work with live data,
        //the following code must be added to switch from sandbox mode to live mode:
        if('prod' == $_ENV['APP_ENV']) {
            $this->immocaster->setRequestUrl( 'live' );
        }

        //The SDK works with the PHP function "file_get_contents" by default.
        // Since this is deactivated for security reasons with many hosting providers, it is recommended to use the SDK with cURL.
        // To do this, cURL must of course be installed and active on the server or hosting package.
        //$this->immocaster->setReadingType( 'curl' );

        //To switch from XML to JSON as a response from the API.
        //$this->immocaster->setContentResultType( 'json' );

        //To get the results in strict mode
        //$this->immocaster->setStrictMode( true );
    }

    /**
     * Authorize this app and get access token and secret. Pass complete URL including parameters on which the script is integrated
     *
     * @param $sCertifyURL
     * @return string
     */
    public function certifyApplication($sCertifyURL)
    {
        $aParameter = array(
            'callback_url' => $sCertifyURL . '?user=' . $this->config->getUsername(),
            'verifyApplication' => true
        );
        // Recertify user
        $returnAuthentication = $this->immocaster->getAccess($aParameter);

        if ($returnAuthentication === true) {
            return "Certification was successful. Access tokens are stored in the DB.";
        } elseif (is_array($returnAuthentication) && count($returnAuthentication) > 1) {
            $message = '<div id="appVerifyInfo">Certification was successful. Here you will find Access Token and Token Secret comma-separated and url-encoded.</div>';
            $message .= '<div class="codebox"><textarea>' . implode(",", $returnAuthentication) . '</textarea></div>';
            return $message;
        } else {
            return "Something went wrong. Troubleshooting: User is already certified in the MySQL database or there is no connection to the database.";
        }
    }

    public function createDTOForExport(?int $id, ?RealEstate $realEstate = null)
    {
        if (!$realEstate) {
            $realEstate = $this->entityManager->getRepository(RealEstate::class)->find($id);
        }

        $FEATURE_BALCONY = 2;
        $FEATURE_LIFT = 3;
        $FEATURE_CELLAR = 14;
        $FEATURE_GARDEN = 23;
        $FEATURE_HANDICAPED = 30;

        $dto = new RealEstateDTO();
        $realEstateType = $realEstate->getRealEstateType()->getId();

        if ($realEstateType == RealEstateTypeEnum::BUSINESS) {
            throw new \Exception("ImmoScout24 export error: Type of real estate - International properties of type OFFICE are not accepted by ImmoScout24!");
        }

        $realEstateLocalization = $realEstate->getRealEstateLocalization("de");
        if (!$realEstateLocalization) {
            throw new \Exception("ImmoScout24 export error: This property has no localization data for german language!");
        }
        $features = array_map(function ($elem) {
            return $elem->getId();
        }, $realEstate->getFeatures());

        //Fill base info
        $dto->externalId = $realEstate->getUid();
        $dto->title = substr($realEstateLocalization->getTitle(), 0, 100);
        $dto->creationDate = $realEstate->getCreatedAt();
        $dto->lastModificationDate = $realEstate->getUpdatedAt();
        $dto->address = new AddressDTO(
             $realEstate->getPlace() ?? $realEstate->getLocation()->getLocationLocalization("de")->getName(),
            $realEstate->getAddress(),
            $realEstate->getPostalCode() ?? $realEstate->getLocation()->getPostalCode(),
            $realEstate->getLatitude(),
            $realEstate->getLongitude()
        );
        $dto->descriptionNote = strip_tags(substr($realEstateLocalization->getDescription(), 0, 3999));

        //Specific data, requires real estate type querying
        $dto->commercializationType = $realEstateType == RealEstateTypeEnum::LAND ? "BUY" : null;
        $dto->lift = $realEstateType == RealEstateTypeEnum::APARTMENT ? in_array($FEATURE_LIFT, $features) : null;
        $dto->buildingType = $realEstateType == RealEstateTypeEnum::HOUSE ? "NO_INFORMATION" : null;
        $dto->energyCertificate = $realEstateType != RealEstateTypeEnum::LAND ? new EnergyCertificateDTO($realEstate->getEnergyClass()) : null;
        $dto->cellar = $realEstateType != RealEstateTypeEnum::LAND ? (in_array($FEATURE_CELLAR, $features) ? "YES" : "NOT_APPLICABLE") : null;
        $dto->handicappedAccessible = $realEstateType != RealEstateTypeEnum::LAND ? (in_array($FEATURE_HANDICAPED, $features) ? "YES" : "NOT_APPLICABLE") : null;
        $dto->numberOfBedRooms = $realEstateType != RealEstateTypeEnum::LAND ? $realEstate->getBedroomCount() : null;
        $dto->numberOfBathRooms = $realEstateType != RealEstateTypeEnum::LAND ? $realEstate->getBathroomCount() : null;
        //price is a bit complicated
        $dto->price = null;
        if ($realEstate->getPriceOnRequest() === true) {
            //Only supported for lands
            if ($realEstateType == RealEstateTypeEnum::LAND) {
                $dto->price = new PriceDTO(0);
            }
        } else {
            $dto->price = new PriceDTO($realEstate->getDiscountPrice() ?? $realEstate->getFullPrice());
        }
        if (!$dto->price) {
            throw new \Exception("ImmoScout24 export error: It is not allowed to ommit price (price on request) for this type of property (only lands are exempted)!");
        }
        $dto->livingSpace = $realEstateType != RealEstateTypeEnum::LAND ? $realEstate->getLivingArea() : null;
        $dto->plotArea = $realEstateType != RealEstateTypeEnum::APARTMENT ? ($realEstateType == RealEstateTypeEnum::HOUSE ? $realEstate->getSurfaceArea() : $realEstate->getLivingArea()) : null;
        $dto->numberOfRooms = $realEstateType != RealEstateTypeEnum::LAND ? $realEstate->getBedroomCount() : null;
        $dto->balcony = $realEstateType == RealEstateTypeEnum::APARTMENT ? in_array($FEATURE_BALCONY, $features) : null;
        $dto->garden = $realEstateType == RealEstateTypeEnum::APARTMENT ? in_array($FEATURE_GARDEN, $features) : null;
        //courtage is set to NO for time being in the constructor


        $mainXmlElemName = "realestates";
        switch ($realEstateType) {
            case RealEstateTypeEnum::APARTMENT:
                $mainXmlElemName .= ":apartmentBuy";
                break;
            case RealEstateTypeEnum::HOUSE:
                $mainXmlElemName .= ":houseBuy";
                break;
            case RealEstateTypeEnum::LAND:
                $mainXmlElemName .= ":livingBuySite";
                break;
            default:
                break;
        }

        return array("mainXmlElemName" => $mainXmlElemName, "data" => $dto);
    }

    private function createXmlFromDto($dtoForExport)
    {
        $xml_options = array(
            "xml_root_node_name" => $dtoForExport["mainXmlElemName"],
            "remove_empty_tags" => true,
            "xml_encoding" => "utf-8",
        );
        $encoders = [new XmlEncoder($xml_options)];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter), new ArrayDenormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $xml = $serializer->serialize($dtoForExport["data"], 'xml');

        //Hack to add namespaces for root element
        $xml = str_replace(
            "<" . $dtoForExport["mainXmlElemName"] . ">",
            "<" . $dtoForExport["mainXmlElemName"] . ' xmlns:realestates="http://rest.immobilienscout24.de/schema/offer/realestates/1.0" xmlns:xlink="http://www.w3.org/1999/xlink">',
            $xml
        );

        return $xml;
    }

    public function exportRealEstate(RealEstate $realEstate, $exportType)
    {
        $dto = $this->createDTOForExport(null, $realEstate);
        $xml = $this->createXmlFromDto($dto);
        $aParameter = array (
            'username' => 'me',
            'service' => 'immobilienscout',
            'estate' => array( 'xml' => $xml )
        );

        $xmlResponse = null;
        if($exportType == "EXPORT"){
            $xmlResponse = $this->immocaster->exportObject($aParameter);
        }
        if($exportType == "CHANGE"){
            $aParameter['estate']['objectId'] = $realEstate->getUid();
            $xmlResponse = $this->immocaster->changeObject($aParameter);
        }

        $doc = new \DOMDocument('1.0');
        $doc->loadXML($xmlResponse);
        $messageCodeNode = $doc->getElementsByTagName('messageCode');
        $messageCode = $messageCodeNode[0]->nodeValue;

        if($messageCode == "MESSAGE_RESOURCE_CREATED" || $messageCode == "MESSAGE_RESOURCE_UPDATED")
        {
            if($messageCode == "MESSAGE_RESOURCE_CREATED"){
                $idNode = $doc->getElementsByTagName('id');
                $realEstate->setImmoid($idNode[0]->nodeValue);
                $realEstate->setImmoscoutExported(true);

                $this->entityManager->persist($realEstate);
                $this->entityManager->flush();
            }

            return true;
        }
        else{
            $this->logger->error("ERROR EXPORTING REAL ESTATE (ID" . $realEstate->getId() . ") TO IMMOSCOUT24: " . $xmlResponse);
            return false;
        }
    }

    /**
     * Exports images for the passed real estate entity
     *
     * @param RealEstate $realEstate
     */
    public function exportAttachmentsForRealEstate(RealEstate $realEstate, $localHostPath)
    {
/*        if($realEstate->getImmoscoutExported() == true && $realEstate->getImmoid() == null){
            return [];
        }*/

        //Get current set of images for real estate
        $images = $realEstate->getRealEstateImages();
        $aStatus = [];

        try {
            foreach ($images as $image)
            {
                if($image->getImmoExported() == null || $image->getImmoExported() == false)
                {
                    $imageLoco = $image->getRealEstateImageMetaLocalization('de');
                    $title = substr($image->getOriginalFileName(), 0, 29);
                    if($imageLoco != null && $imageLoco->getAltTag() != null && $imageLoco->getAltTag() != ""){
                        $title = substr($imageLoco->getAltTag(), 0, 29);
                    }
                    $aParameter = array(
                        'username' => 'me',
                        'estateid' => $realEstate->getImmoid() ?? 'ext-' . $realEstate->getUid(), //uid/external-id
                        'title' => $title,
                        'type' => 'Picture', // value can be one of: Picture, PDFDocument or Link
                        'file' => $localHostPath . '/public' . $image->getPath(), // from docs: file path OR URL
                        'externalId' => $image->getUuid(),
                        'titlePicture' => $image->isCover()
                    );

                    $xmlResponse = $this->immocaster->exportObjectAttachment($aParameter);
                    $doc = new \DOMDocument('1.0');
                    $doc->loadXML($xmlResponse);
                    $messageCodeNode = $doc->getElementsByTagName('messageCode');
                    $messageNode = $doc->getElementsByTagName('message');
                    if($messageCodeNode[0]->nodeValue != "MESSAGE_RESOURCE_CREATED") {
                        $this->logger->log("error", "ERROR EXPORTING IMAGE TO IMMOSCOUT24 (ID: " . $image->getId() . ") FOR REAL ESTATE (ID: " . $realEstate->getId() . "): " . $messageNode[0]->nodeValue);
                        $aStatus[$image->getId()] = false;
                    }

                    if($messageCodeNode[0]->nodeValue == "MESSAGE_RESOURCE_CREATED")
                    {
                        $idNode = $doc->getElementsByTagName("id");
                        $image->setImmoAttachmentId($idNode[0]->nodeValue);
                        $image->setImmoExported(true);
                        $this->entityManager->persist($image);
                        $this->entityManager->flush();
                        $aStatus[] = true;
                    }
                }
            }

            return $aStatus;
        }
        catch(\Exception $ex) {
            return array(false);
        }
    }

    public function deleteAttachmentsForRealEstate(RealEstate $realEstate, array $attachmentIds)
    {
        $realEstateImageRepo = $this->entityManager->getRepository(RealEstateImage::class);

        foreach ($attachmentIds as $attachmentId){
            //Check if image is truly deleted and only then issue DELETE request to ImmoScout24 API
            $image = $realEstateImageRepo->findBy(array("immoAttachmentId" => $attachmentId));
            if($image == null){
                $xmlResponse = $this->immocaster->deleteObjectAttachment(array(
                    "attachmentid" => $attachmentId,
                    "estateid" => "ext-" . $realEstate->getUid()
                ));
                $doc = new \DOMDocument('1.0');
                $doc->loadXML($xmlResponse);//
                $messageCodeNode = $doc->getElementsByTagName('messageCode');
                if($messageCodeNode[0]->nodeValue != "MESSAGE_RESOURCE_DELETED") {
                    $this->logger->log("error", "ERROR DELETING IMAGE FROM IMMOSCOUT24 (attachmentid: " . $attachmentId . ") FOR REAL ESTATE (ID: " . $realEstate->getId() . "): " . $xmlResponse);
                }
            }
        }
    }

    public function test($id)
    {
        $result = [];
        switch ($id) {
            case 1:
                $result = $this->immocaster->fullUserSearch(array(
                    "username" => "me",
                    //"pagesize" => 2
                ));
                break;
            case 2:
                $dto = $this->createDTOForExport(36, null);
                $xml = $this->createXmlFromDto($dto);
                $result = $this->immocaster->exportObject(array(
                    'username' => 'me',
                    'service' => 'immobilienscout',
                    'estate' => array(
                        'xml' => $xml)
                ));
                break;
            default:
                $result = [];
        }

        if (is_string($result) && "<?xml" === substr($result, 0, 5)) {
            $doc = new \DOMDocument('1.0');
            $doc->loadXML($result);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            $result = $doc->saveXML();
        }

        return $result;
    }

    public function exportStatusCheck()
    {
        $i = $pages = 1;
        $message = "Started process of status check&update of properties against ImmobilienScout24. \n";
        try {

            do{
                $xmlResponse = $this->immocaster->fullUserSearch(array(
                    "username" => "me",
                    "pagenumber" => $i,
                    "pagesize" => 100
                ));

                $doc = new \DomDocument('1.0');
                $doc->loadXML($xmlResponse);
                //get total pages and store in local variable
                if($pages === 1){
                    $xmlPages = $doc->getElementsByTagName('numberOfPages');
                    if(count($xmlPages) > 0){
                        $pages = $xmlPages[0]->nodeValue;
                    }
                }

                //update RealEstate entities found in response
                $uids = [];
                $externalIdNodes = $doc->getElementsByTagName('externalId');
                foreach ($externalIdNodes as $externalIdNode){
                    $uids[] = $externalIdNode->nodeValue;
                }

                $updateResult = $this->updateImmoScoutExportedInfo($uids);

                $message .= "Updated $updateResult records. \n";

                $i++;

            } while ($i <= $pages);

        } catch (\Exception $ex){
            $message .= $ex->getMessage();
        }

        $message .= "Finished.";

        return $message;
    }

    private function updateImmoScoutExportedInfo($uids)
    {
        $qb = $this->entityManager->getRepository(RealEstate::class)->createQueryBuilder("realEstate");

        $qb->update()
            ->set("realEstate.immoscout_exported", true)
            ->where("realEstate.uid IN (:uids)")
            ->setParameter("uids", $uids);

        $result = $qb->getQuery()->execute();

        $select = $this->entityManager->getRepository(RealEstate::class)->createQueryBuilder("realEstate")
            ->select("realEstate.id")
            ->where("realEstate.uid IN (:uids)");


        $qbRealEstateImage = $this->entityManager->getRepository(RealEstateImage::class)->createQueryBuilder("realEstateImage");
        $qbRealEstateImage->update()
            ->set("realEstateImage.immoExported", true)
            ->where("IDENTITY(realEstateImage.realEstate) IN (" . $select->getQuery()->getDQL() .  ")")
            ->setParameter("uids", $uids);

        $qbRealEstateImage->getQuery()->execute();


        return $result;
    }

    public function validForExport(RealEstate $realEstate)
    {
        $valid = true;
        if($realEstate->getRealEstateType()->getId() == RealEstateTypeEnum::BUSINESS){
            $valid = false;
        }

        if($realEstate->getPriceOnRequest() == true && $realEstate->getRealEstateType()->getId() != RealEstateTypeEnum::LAND){
            $valid = false;
        }

        if($realEstate->getRealEstateLocalization("de") == null){
            $valid = false;
        }

        return $valid;
    }

    public function exportDelta($localhostPath)
    {
        $qb = $this->entityManager->getRepository(RealEstate::class)->createBasicQueryBuilder("realEstate");
        $qb->select("realEstate")
            ->andWhere("IDENTITY(realEstate.status) = 2")
            ->andWhere("realEstate.immoscout_exported IS NULL");

        $realEstates = $qb->getQuery()->execute();

        $exportLog = "Exporting delta of real estates to ImmoScout24: \n";

        try {
            /** @var $realEstate RealEstate */
            foreach ($realEstates as $realEstate)
            {
                if($this->validForExport($realEstate)){
                    $success = $this->exportRealEstate($realEstate, "EXPORT");
                    if(true === $success){
                        $exportLog .= $realEstate->getId() . " - success \n";
                        $aStatus = $this->exportAttachmentsForRealEstate($realEstate, $localhostPath); //$this->getParameter('kernel.project_dir')
                        foreach ($aStatus as $key => $value){
                            $exportLog .= $realEstate->getId() . ":" . $key . " - " . ($value ? "success \n" : "failed \n");
                        }
                    } else {
                        $exportLog .= $realEstate->getId() . " - failed \n";
                    }
                }
            }
        } catch (\Exception $ex){
            $this->logger->error("ERROR EXPORTING DELTA TO IMMOSCOUT24:" . $ex->getMessage());
            return $exportLog . "\n FATAL ERROR (check log)";
        }

        return $exportLog;
    }
}
















