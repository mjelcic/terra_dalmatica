<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:16 PM
 */

namespace App\Services\Export;


use App\DTO\Facebook\Request\FacebookDTO;
use App\DTO\RealEstateCroatia\ImageDTO;
use App\Entity\Feature;
use App\Entity\RealEstate;
use App\GlobalSettings;
use App\Services\ServiceRealEstate;
use Doctrine\ORM\EntityManagerInterface;
use App\DTO\RealEstateCroatia\RealEstateDTO;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FacebookService
{

    protected $em;
    protected $urlGenerator;
    protected $client;
    protected $serializer;

    function __construct(EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, HttpClientInterface $client, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->client = $client;
        $this->serializer = $serializer;
    }


    public function post($realEstateId, $locale)
    {
        $realEstate = $this->em->getRepository(RealEstate::class)->find($realEstateId);
        $link = $this->urlGenerator->generate('real_estate_details', array('slug' => $realEstate->getRealEstateLocalization($locale)->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
        $title = $realEstate->getRealEstateLocalization($locale)->gettitle();

        $dto = new FacebookDTO($title, $link);

        if($realEstate->getFbPostId()) {
            $FacebookApiUrl = GlobalSettings::getFacebookApiUpdateUrl($realEstate->getFbPostId());
        }
        else{

            $FacebookApiUrl = GlobalSettings::getFacebookApiPostUrl();
        }

        $response = $this->client->request('POST', $FacebookApiUrl, [
            'headers' => [
                'Authorization' =>"Bearer " .  GlobalSettings::getFacebookAccessToken(),
                "Content-Type" => "application/json"
            ],
            'body'=> $this->serializer->serialize($dto, "json")
        ]);
        $facebookResponse = null;
        if($response->getStatusCode()==200){
            $facebookResponse = $this->serializer->deserialize($response->getContent(), \App\DTO\Facebook\Response\FacebookDTO::class, "json");
            $realEstate->setFbPostId($facebookResponse->id);
            $this->em->flush();
        }

        return $response;
    }
}