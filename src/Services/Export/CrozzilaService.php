<?php

namespace App\Services\Export;

use App\DTO\Crozzila\AmountDTO;
use App\DTO\Crozzila\ContactDTO;
use App\DTO\Crozzila\FeatureDTO;
use App\DTO\Crozzila\ImagesDTO;
use App\DTO\Crozzila\LocationDTO;
use App\DTO\Crozzila\NumberDTO;
use App\DTO\Crozzila\TagDTO;
use App\DTO\Crozzila\TagsDTO;
use App\DTO\Crozzila\VideoDTO;
use App\Entity\Enum\CrozzilaRealEstateTypeEnum;
use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\User;
use App\GlobalSettings;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use App\DTO\Crozzila\RealEstateDTO;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CrozzilaService
{
    protected $entityManager;
    private $router;
    private $global;

    function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $router, GlobalSettings $global)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->global = $global;
    }


    public function getRealEstates()
    {
        $query = $this->entityManager->createQuery("
        SELECT NEW App\DTO\Crozzila\RealEstateDTO(
        real_estate.uid,
        DATE_FORMAT(real_estate.created_at, '%d-%m-%Y'),
        IDENTITY(real_estate.RealEstateType),
        SUBSTRING(loc_hr.title,1,64),
        SUBSTRING(loc_hr.description,1, 2000),
        SUBSTRING(loc_en.title,1,64),
        SUBSTRING(loc_en.description,1, 2000),
        SUBSTRING(loc_de.title,1,64),
        SUBSTRING(loc_de.description,1, 2000),
        CONCAT('" . $this->global->getBaseUrl() . "','/nekretnina/', loc_hr.slug)
        )
        FROM App\Entity\RealEstate real_estate
        JOIN real_estate.realEstateLocalizations loc_hr WITH IDENTITY(loc_hr.language) = 1
        LEFT JOIN real_estate.realEstateLocalizations loc_en  WITH IDENTITY(loc_en.language) = 2
        LEFT JOIN real_estate.realEstateLocalizations loc_de WITH IDENTITY(loc_de.language) = 3
        LEFT JOIN real_estate.status status
        WHERE real_estate.invalidated = 0
        AND status.displayId = 'PUBLISHED'
        ");
        $properties = $query->execute();


        foreach ($properties as $property) {
            // Get global objects
            $entity = $this->entityManager->getRepository(RealEstate::class)->findOneBy(array("uid" => $property->getPropertyId()));
            $language = $this->entityManager->getRepository(Language::class)->findOneBy(array("code" => "hr"));
            $languageEn = $this->entityManager->getRepository(Language::class)->findOneBy(array("code" => "en"));
            // End get global objects

            // START Set location
            $location = $entity->getLocation();
            $postal_code = ($entity->getPostalCode() ?: $location->getPostalCode()) ?: 0;
            $locationLocalized = $location->getLocationLocalization($language)->getName();
            $property->location = new LocationDTO($postal_code, $entity->getPlace() ?: $locationLocalized, null, $entity->getAddress(), $entity->getLongitude(), $entity->getLatitude());
            // END set location

            // START Set contact
            if ($entity->getAgent()) {
                $agent = $entity->getAgent();
                $property->contact = new ContactDTO(null, null, $agent->getFirstName(), $agent->getLastName(), null, null, null, null, $agent->getPhoneNo(), $agent->getMobilePhoneNumber(), $agent->getFaxNumber(), $agent->getEmail());
            }
            // END set contact

            // START Set property_size
            $property->property_size = new NumberDTO($entity->getLivingArea());
            // END Set property_size

            // START Set surface_area
            $property->land_size = new NumberDTO($entity->getSurfaceArea());
            // END Set surface_area

            // START Set price
            $property->price = new AmountDTO($entity->getPrice());
            // END Set Price

            // START SET Images
            $result = $this->entityManager->createQuery("
            SELECT 
            CONCAT(:base_url , img.path) as image
            FROM App\Entity\RealEstateImage img
            WHERE IDENTITY(img.realEstate) = :real_estate_id
            ")->setParameter("real_estate_id", $entity->getId())
                ->setParameter("base_url", GlobalSettings::getBaseUrl())
                ->setMaxResults(12)->execute();
            $images = array_column($result, "image");

            if ($images) {
                $property->images = new ImagesDTO($images);
            }
            //END SET images

            // START SET video
            if ($entity->getVideoUrl()) {
                $property->video = new VideoDTO($entity->getVideoUrl());
            }
            //END SET videos

            // START SET features
            if ($entity->getNewBuilding() == true) {
                $condition = "new";
            } else {
                $condition = "used";
            }
            $property->features = new FeatureDTO(null, $entity->getBathroomCount(), $entity->getBedroomCount(), $condition, null, null);
            //END SET features


            //START Tags
            $tagsArray = array();
            if ($entity->getRealEstateType()->getId() <= 3) {   //Only first three RealEstateTypes have tags
                $tags = array(
                    30 => array(32, 83, null),  //Disabled people
                    7 => array(33, 244, 215), // Air conditioner
                    23 => array(36, 86, null), // Garden
                    2 => array(37, 252, null), // Balcony
                    3 => array(38, null, null), // Elevator
                    15 => array(187, 251, null), //floor heating
                    5 => array(187, 251, null), // Jacuzzi
                    9 => array(187, 251, 216), // Arranged
                    12 => array(604, 674, 593), // Parking,
                    21 => array(618, 665, 814), // Gas
                    19 => array(1619, 666, null), // Power
                    1 => array(833, 836, 732), // Alarm
                    22 => array(838, null, null), //Warehouse
                    25 => array(null, 574, null), // Terrace
                    14 => array(null, 676, 743), // Basement
                );

                $garageTag = array(602, 672, 594);
                $newBuildingTag = array(195, 262, null);
                $oldBuildingTag = array(196, 263, null);

                if ($entity->getGarage() > 0) {
                    if ($garageTag[$entity->getRealEstateType()->getId() - 1]) {
                        $tagsArray[] = $garageTag[$entity->getRealEstateType()->getId() - 1];
                    }
                }

                if ($entity->getNewBuilding() == true) {
                    if ($newBuildingTag[$entity->getRealEstateType()->getId() - 1]) {
                        $tagsArray[] = $newBuildingTag[$entity->getRealEstateType()->getId() - 1];
                    }
                } else {
                    if ($oldBuildingTag[$entity->getRealEstateType()->getId() - 1]) {
                        $tagsArray[] = $oldBuildingTag[$entity->getRealEstateType()->getId() - 1];
                    }
                }

                $features = $entity->getRealEstateFeatures();

                foreach ($features as $feature) {
                    if (array_key_exists($feature->getFeature()->getId(), $tags)) {
                        if ($tags[$feature->getFeature()->getId()][$entity->getRealEstateType()->getId() - 1]) {
                            $tagsArray[] = $tags[$feature->getFeature()->getId()][$entity->getRealEstateType()->getId() - 1];
                        }
                    }
                }

                $property->tags = new TagsDTO($tagsArray);
            }
            //END Tags

        }

        return $properties;
    }
}