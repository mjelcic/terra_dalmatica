<?php

namespace App\Services\Export;



use App\DTO\Realitica\ContactDTO;
use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\User;
use App\GlobalSettings;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RealiticaService
{
    protected $entityManager;
    private $router;
    private $global;

    function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $router, GlobalSettings $global)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->global = $global;
    }


    public function getRealEstates()
    {
        $query = $this->entityManager->createQuery("
        SELECT NEW App\DTO\Realitica\RealEstateDTO(
        real_estate.uid,
        IDENTITY(real_estate.RealEstateType),
        CONCAT(location_loc_hr.name,', Hrvatska') ,
        '',
        real_estate.price,
        real_estate.bedroomCount,
        real_estate.bathroomCount,
        real_estate.livingArea,
        real_estate.surfaceArea, 
        loc_hr.title,
        loc_en.title,
        loc_de.title,
        loc_hr.description,        
        loc_en.description,
        loc_de.description,
        real_estate.energyClass,  
        CONCAT('" . $this->global->getBaseUrl() . "','/nekretnina/', loc_hr.slug),
        CONCAT('" . $this->global->getBaseUrl() . "','/en/real-estate/', loc_en.slug),
        CONCAT('" . $this->global->getBaseUrl() . "','/de/immobilien/', loc_de.slug)
        )
        FROM App\Entity\RealEstate real_estate
        JOIN real_estate.realEstateLocalizations loc_hr WITH IDENTITY(loc_hr.language) = 1
        LEFT JOIN real_estate.realEstateLocalizations loc_en  WITH IDENTITY(loc_en.language) = 2
        LEFT JOIN real_estate.realEstateLocalizations loc_de WITH IDENTITY(loc_de.language) = 3
        LEFT JOIN real_estate.location location
        LEFT JOIN location.locationLocalizations location_loc_hr  WITH IDENTITY(location_loc_hr.language) = 1
        LEFT JOIN real_estate.status status
        WHERE real_estate.invalidated = 0
        AND status.displayId = 'PUBLISHED'
        ");
        $properties = $query->execute();

        foreach ($properties as $property) {

            // Get global objects
            $entity = $this->entityManager->getRepository(RealEstate::class)->findOneBy(array("uid" => $property->getListingId()));
            // End get global objects

            // FEATURES
            //NEW CONSTRUCTION
            if ($entity->getNewBuilding() == 1) {
                $property->features[] = "new construction";
            }

            //WATERFRONT
            if ($entity->getFirstRowByTheSea() == 1) {
                $property->features[] = "waterfront";
            }

            // AIR CONDITIONING
            $features = $entity->getRealEstateFeatures();
            foreach ($features as $feature) {
                if ($feature->getId() == 7) {
                    $property->features[] = "air conditioning";
                }
            }
            // END FEATURES

            // IMAGES
            $result = $this->entityManager->createQuery("
            SELECT 
            CONCAT(:base_url , img.path) as image
            FROM App\Entity\RealEstateImage img
            WHERE IDENTITY(img.realEstate) = :real_estate_id
            ")->setParameter("real_estate_id", $entity->getId())
                ->setParameter("base_url", GlobalSettings::getBaseUrl())
                ->setMaxResults(12)->execute();
            $property->photoUrl = array_column($result, "image");
            //END IMAGES

            // START Set contact
            if ($entity->getAgent()) {
                $agent = $entity->getAgent();
                $property->contact = new ContactDTO($agent->getFirstName() . " " . $agent->getLastName(), $agent->getPhoneNo(), $agent->getEmail());
            }
            // END set contact
        }

        return $properties;
    }
}
