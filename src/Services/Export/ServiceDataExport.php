<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:16 PM
 */

namespace App\Services\Export;


use App\DTO\RealEstateCroatia\ImageDTO;
use App\Entity\Feature;
use App\Entity\RealEstate;
use App\Services\ServiceRealEstate;
use Doctrine\ORM\EntityManagerInterface;
use App\DTO\RealEstateCroatia\RealEstateDTO;

class ServiceDataExport
{
    private $serviceRealEstate;
    private $entityManager;

    public function __construct(ServiceRealEstate $serviceRealEstate, EntityManagerInterface $entityManager)
    {
        $this->serviceRealEstate = $serviceRealEstate;
        $this->entityManager = $entityManager;
    }

    public function getDataForRealEstateCroatia()
    {
        $realEstateRepo = $this->entityManager->getRepository(RealEstate::class);
        $languages = ["hr", "en", "de"];

        $qb = $realEstateRepo->createBasicQueryBuilder("realEstate");

        $qb->select("new App\DTO\RealEstateCroatia\RealEstateDTO(
            realEstate.id,
            realEstate.uid,
            CASE
                WHEN realEstate.status = 5 THEN 2
                ELSE 0
            END,
            CASE
                WHEN realEstate.RealEstateType = 1 THEN 2
                WHEN realEstate.RealEstateType = 2 THEN 1
                WHEN realEstate.RealEstateType = 3 THEN 3
                WHEN realEstate.RealEstateType = 4 THEN 4
                ELSE 0
            END,
            realEstate.created_at,
            realEstate.updated_at,
            realEstate.bedroomCount,
            realEstate.bathroomCount,
            CASE
                WHEN realEstate.seaView = 1 THEN 1
                ELSE 0
            END,
            realEstate.firstRowByTheSea,
            CASE
                WHEN realEstate.energyClass = 'A+' THEN 1
                WHEN realEstate.energyClass = 'A' THEN 2
                WHEN realEstate.energyClass = 'B' THEN 3
                WHEN realEstate.energyClass = 'C' THEN 4
                WHEN realEstate.energyClass = 'D' THEN 5
                WHEN realEstate.energyClass = 'E' THEN 6
                WHEN realEstate.energyClass = 'F' THEN 7
                WHEN realEstate.energyClass = 'G' THEN 8
                ELSE 0
            END,
            realEstate.place,
            location_loco.name,
            realEstate.postal_code,
            location.postal_code,
            realEstate.price,
            realEstate.livingArea,
            realEstate.surfaceArea,
            agent.first_name,
            agent.last_name,
            realEstate.videoUrl
        )")
            ->leftJoin("realEstate.location", "location")
            ->leftJoin("App\Entity\LocationLocalization", "location_loco", "with", "location_loco.location = realEstate.location AND IDENTITY(location_loco.language) = 1") //hr version
            ->leftJoin("realEstate.agent", "agent")
            ->andWhere("realEstate.status IN (2, 4, 5)") //select only: published, in progress and completed
            ->andWhere("realEstate.price IS NOT NULL");

        $realEstates = $qb->getQuery()->getResult();

        //images
        $images = $this->serviceRealEstate->getImagesForRealEstates(array_map(
            function($element){
                return $element->id;
            }, $realEstates));

        /** @var  $realEstate RealEstateDTO */
        foreach ($realEstates as $realEstate)
        {
            //try to find if real estate has an elevator (Features query)
            $qbElevator = $realEstateRepo->createQueryBuilder("realEstate");

            $qbElevator->select("count(feature.id)")
                ->leftJoin("realEstate.realEstateFeatures", "features")
                ->leftJoin("features.feature", "feature")
                ->leftJoin("feature.featureLocalizations", "feature_loco", "with", "IDENTITY(feature_loco.language) = 1")
                ->andWhere("realEstate.id = :id")
                ->andWhere("feature_loco.name = :feature_name")
                ->setParameter("id", $realEstate->id)
                ->setParameter("feature_name", "Dizalo");

            $hasElevator = $qbElevator->getQuery()->getSingleScalarResult();

            $realEstate->lift = $hasElevator > 0 ? 1 : 0;

            //REC location id
            //$realEstate->location->rec_location_id = array_key_exists($realEstate->location->city, $recLocationIdMapper) ? $recLocationIdMapper[$realEstate->location->city] : 5679; //Šibenik by default

            //title(s) and description(s)
            $qbLocos = $realEstateRepo->createBasicQueryBuilder("realEstate");
            $qbLocos->select("language.code as lang_code, loco.title, loco.description")
                ->leftJoin("realEstate.realEstateLocalizations", "loco")
                ->leftJoin("loco.language", "language")
                ->andWhere("realEstate.id = :id")
                ->setParameter("id", $realEstate->id);

            $localizations = $qbLocos->getQuery()->getResult();
            foreach ($localizations as $localization){
                if($localization["lang_code"] == "hr"){
                    $realEstate->title->hr = $localization["title"];
                    $realEstate->description->hr = $localization["description"];
                }

                if($localization["lang_code"] == "en"){
                    $realEstate->title->en = $localization["title"];
                    $realEstate->description->en = $localization["description"];
                }

                if($localization["lang_code"] == "de"){
                    $realEstate->title->de = $localization["title"];
                    $realEstate->description->de = $localization["description"];
                }
            }

            //images
            $realEstate->images["image"] = array_values(
                array_map(function($elem) { return 'https://www.terradalmatica.hr' . $elem["path"]; },
                    array_filter($images, function($image) use ($realEstate){
                        return $image["real_estate_id"] == $realEstate->id;
                    })
                )
            );

            //trim video link so it only contains id
            $realEstate->video = $realEstate->video != null ? substr($realEstate->video, strlen("https://www.youtube.com/watch?v")) : null;
        }

        return $realEstates;
    }
}