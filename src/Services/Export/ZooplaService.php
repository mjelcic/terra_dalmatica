<?php
declare(strict_types=1);

namespace App\Services\Export;


use App\DTO\Zoopla\Request\ListingContent;
use App\DTO\Zoopla\Request\ListingDescription;
use App\DTO\Zoopla\Request\ListingLocation;
use App\DTO\Zoopla\Request\ListingLocationCoordinates;
use App\DTO\Zoopla\Request\ListingUpdateRequest;
use App\DTO\Zoopla\Request\PricingDto;
use App\DTO\Zoopla\Request\ZooplaDeleteRequest;
use App\DTO\Zoopla\Response\ListingDeleteResponse;
use App\DTO\Zoopla\Response\ListingUpdateResponse;
use App\Entity\GroundPlan;
use App\Entity\RealEstate;
use App\Entity\RealEstateFeature;
use App\Entity\RealEstateImage;
use App\Entity\RealEstateImageMetaLocalization;
use App\Entity\ZooplaListing;
use App\GlobalSettings;
use App\Repository\RealEstateRepository;
use App\Repository\ZooplaListingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZooplaService
{
    const BASE_URL = "https://realtime-listings-api.webservices.zpg.co.uk/live/v1/";

    const LISTING_UPDATE_URL = "listing/update";
    const LISTING_DELETE_URL = "listing/delete";

    const ACCESSIBILITY_FEATURE_ID = 30;
    const BASEMENT_FEATURE_ID = 14;
    const BURGLAR_ALARM_FEATURE_ID = 1;
    const FIREPLACE_FEATURE_ID = 6;

    /**
     * @var ZooplaListingRepository
     */
    private $repository;
    /**
     * @var RealEstateRepository
     */
    private $realEstateRepository;
    /**
     * @var HttpClientInterface
     */
    private $client;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ZooplaService constructor.
     * @param ZooplaListingRepository $repository
     * @param RealEstateRepository $realEstateRepository
     * @param HttpClientInterface $client
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        ZooplaListingRepository $repository,
        RealEstateRepository $realEstateRepository,
        HttpClientInterface $client,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        LoggerInterface  $logger
    )
    {
        $this->repository = $repository;
        $this->realEstateRepository = $realEstateRepository;
        $this->client = $client;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /** Find real estates with zoopla null props */
    public function registerRealEstatesToZoopla()
    {
        $qb = $this->realEstateRepository->createBasicQueryBuilder("re");

        $qb->select("re")
            ->leftJoin("App\Entity\ZooplaListing", "zl", "with", "zl.realEstate = re.id")
            ->andWhere("zl.id IS  NULL")
            ->andWhere("re.status IN (2,4)");

        $realEstates = $qb->getQuery()->getResult();

        // get all real estates that do not have zoopla listing. not all, but certain amount
        //$realEstates = $this->realEstateRepository->findBy(["invalidated" => 0, "zooplaListing" => null, "status" => [2, 4]], null, 1, 0);

        foreach ($realEstates as $realEstate) {
            $listingResponse = $this->sendCreateUpdateRequest($realEstate);

            if ($listingResponse != null) {
                $listing = new ZooplaListing(
                    $realEstate,
                    $listingResponse->listing_reference,
                    $listingResponse->listing_etag,
                    $listingResponse->url
                );

                $this->entityManager->persist($listing);
                $this->entityManager->flush();
            }
        }
    }

    /**  */
    public function resendRecentlyUpdatedRealEstates()
    {
        $qb = $this->repository->createBasicQueryBuilder("zl");

        $qb->select("zl")
            ->innerJoin("App\Entity\RealEstate", "re", "with", "zl.realEstate = re.id")
            ->andWhere("re.updated_at IS NOT NULL")
            ->andWhere("zl.updatedAt < re.updated_at");

        $result = $qb->getQuery()->getResult();

        /** @var ZooplaListing $item */
        foreach ($result as $item) {
            $re = $item->getRealEstate();

            if($re->getInvalidated() == 1 || in_array($re->getStatus()->getDisplayId(), ["CREATED", "REJECTED"])) {
                $response = $this->sendDeleteRequest($item, "withdrawn");

                if($response != null){
                    $item->setDeletedAt();
                    $item->setDeleteReason("withdrawn");
                    $this->entityManager->persist($item);
                    $this->entityManager->flush();
                }
            } else if($re->getStatus()->getDisplayId() == "COMPLETED") {
                $response = $this->sendDeleteRequest($item, "completed");
                if($response != null){
                    $item->setDeletedAt();
                    $item->setDeleteReason("completed");
                    $this->entityManager->persist($item);
                    $this->entityManager->flush();
                }
            } else {
                $response = $this->sendCreateUpdateRequest($item->getRealEstate());

                if ($response != null) {
                    $item->setETag($response->listing_etag);
                    $item->setUrl($response->url);
                    $item->setUpdatedAt();

                    $this->entityManager->persist($item);
                    $this->entityManager->flush();
                }
            }
        }
    }

    public function upsertSingleRealEstate(RealEstate $realEstate): ?string
    {
        $this->logger->info("Upsert single realestate: " . $realEstate->getId() . " to zoopla");
        /** @var ZooplaListing $zooplaListing */
        $zooplaListing = $this->repository->findOneBy(["realEstate" => $realEstate->getId()]);

        if($realEstate->getExportToZoopla() == false) {
            if($zooplaListing == null || $zooplaListing->getDeletedAt() != null) {
                return null;
            } else {
                $response = $this->sendDeleteRequest($zooplaListing, "withdrawn");

                if($response != null){
                    $zooplaListing->setDeletedAt();
                    $zooplaListing->setDeleteReason("withdrawn");
                    $zooplaListing->getRealEstate()->setZooplaId(null);
                    $this->entityManager->persist($zooplaListing);
                    $this->entityManager->flush();

                    return "removed";
                }
            }
        }

        if($zooplaListing != null) {
            if($realEstate->getInvalidated() == 1 || in_array($realEstate->getStatus()->getDisplayId(), ["CREATED", "REJECTED"])) {
                if($zooplaListing->getDeletedAt() != null) {
                    return null;
                }
                $response = $this->sendDeleteRequest($zooplaListing, "withdrawn");
                if($response != null){
                    $zooplaListing->setDeletedAt();
                    $zooplaListing->setDeleteReason("withdrawn");
                    $zooplaListing->getRealEstate()->setZooplaId(null);
                    $this->entityManager->persist($zooplaListing);
                    $this->entityManager->flush();
                    return "removed";
                }
            } else if($realEstate->getStatus()->getDisplayId() == "COMPLETED") {
                if($zooplaListing->getDeletedAt() != null) {
                    return null;
                }
                $response = $this->sendDeleteRequest($zooplaListing, "completed");
                if($response != null){
                    $zooplaListing->setDeletedAt();
                    $zooplaListing->setDeleteReason("completed");
                    $this->entityManager->persist($zooplaListing);
                    $this->entityManager->flush();
                    return "completed";
                }
            } else {
                $response = $this->sendCreateUpdateRequest($realEstate);
                if ($response != null) {
                    $zooplaListing->setETag($response->listing_etag);
                    $zooplaListing->setUrl($response->url);
                    $zooplaListing->setUpdatedAt();
                    $zooplaListing->clearDeletedAt();
                    $zooplaListing->setDeleteReason(null);
                    $zooplaListing->getRealEstate()->setZooplaId($response->listing_reference);

                    $this->entityManager->persist($zooplaListing);
                    $this->entityManager->flush();
                    return "updated";
                }
            }
        } else {
            if(in_array($realEstate->getStatus()->getDisplayId(), ["PUBLISHED", "IN_PROGRESS"]) && $realEstate->getInvalidated() == 0) {
                $response = $this->sendCreateUpdateRequest($realEstate);

                if ($response != null) {
                    $realEstate->setZooplaId($response->listing_reference);

                    $listing = new ZooplaListing(
                        $realEstate,
                        $response->listing_reference,
                        $response->listing_etag,
                        $response->url
                    );

                    $this->entityManager->persist($listing);
                    $this->entityManager->flush();
                    return "added";
                }
            }
        }
        return null;
    }

    public function deleteAllListings() {
        $qb = $this->repository->createBasicQueryBuilder("zl");

        $qb->select("zl")
            ->innerJoin("App\Entity\RealEstate", "re", "with", "zl.realEstate = re.id");

        $result = $qb->getQuery()->getResult();
        $count = 0;

        /** @var ZooplaListing $item */
        foreach ($result as $item) {
            $response = $this->sendDeleteRequest($item, "withdrawn");
            if($response != null){
                $item->setDeletedAt();
                $item->setDeleteReason("withdrawn");
                $this->entityManager->persist($item);
                $this->entityManager->flush();
            }                $count++;
        }

        $this->logger->error("Will remove " . $count . " items ");
    }

    private function sendCreateUpdateRequest(RealEstate $realEstate): ?ListingUpdateResponse
    {
        try {
            $request = $this->buildRequestFromRealEstate($realEstate);
            $checksum = $this->createETag($request);

            $response = $this->client->request('POST', self::BASE_URL. self::LISTING_UPDATE_URL, [
                'headers' => [
                    'Content-Type' => 'application/json; profile=https://realtime-listings.webservices.zpg.co.uk/docs/v1.2/schemas/listing/update.json',
                    'ZPG-Listing-ETag' => $checksum
                ],
                'body' => $this->serializer->serialize($request, "json"),
                'local_cert' => GlobalSettings::getZooplaCertFolder() . 'zoopla.crt',
                'local_pk' => GlobalSettings::getZooplaCertFolder() . 'private.pem'
            ]);

            if ($response->getStatusCode() == 200) {
                return $this->serializer->deserialize($response->getContent(), ListingUpdateResponse::class, "json");
            } else {
                $this->logger->error("Error registering estate with id " . $realEstate->getId() . " to zoopla: " . $response->getStatusCode() . " : " . $response->getContent(false));
            }
        } catch (\Throwable $throwable) {
            echo "Error registering estate with id " . $realEstate->getId() . " to zoopla: " . $throwable->getMessage();
            $this->logger->error("Error registering estate with id " . $realEstate->getId() . " to zoopla: " . $throwable->getMessage());
        }

        return null;
    }

    private function sendDeleteRequest(ZooplaListing $listing, string $reason)
    {
        try {
            $request = new ZooplaDeleteRequest($listing->getZooplaId(), $reason);
            $response = $this->client->request('POST', self::BASE_URL . self::LISTING_DELETE_URL, [
                'headers' => [
                    'Content-Type' => 'application/json; profile=https://realtime-listings.webservices.zpg.co.uk/docs/v1.2/schemas/listing/delete.json'
                ],
                'body' => $this->serializer->serialize($request, "json"),
                'local_cert' => GlobalSettings::getZooplaCertFolder() . 'zoopla.crt',
                'local_pk' => GlobalSettings::getZooplaCertFolder() . 'private.pem'
            ]);

            if ($response->getStatusCode() == 200) {
                return $this->serializer->deserialize($response->getContent(), ListingDeleteResponse::class, "json");
            }
        } catch (\Throwable $throwable) {
            $this->logger->error("Error deleting estate with id " . $listing->getRealEstate()->getId() . " to zoopla: " . $throwable->getMessage());
        }

        return null;
    }

    private function buildRequestFromRealEstate(RealEstate $realEstate) : ListingUpdateRequest{
        $request = new ListingUpdateRequest();
        $request->branch_reference = "85421";

        $request->detailed_description =
            [
                new ListingDescription(
                    trim($realEstate->getRealEstateLocalization("en")->getTitle()),
                    trim($realEstate->getRealEstateLocalization("en")->getDescription()))
            ];
        $request->listing_reference = (string) $realEstate->getUid();
        $request->location = $realEstate->getLocation()->getLocationLocalization("en")->getName();
        $request->property_type = $this->mapRealEstateTypeToZooplaPropertyType($realEstate);
        $request->life_cycle_status = $this->mapRealEstateStatusToZooplaLifeCycleStatus($realEstate);
        $request->category = $this->mapRealEstateTypeCategory($realEstate);

        $pricingDto = new PricingDto();
        $pricingDto->price = (float) $realEstate->getPrice();

        $location = new ListingLocation();
        $location->country_code = "HR";
        $location->postal_code = (string) $realEstate->getSafePostalCode();
        $location->street_name = $this->buildStreetName($realEstate);
        $location->town_or_city = $this->buildStreetName($realEstate);
        //$coords = new ListingLocationCoordinates();
        //$coords->latitude = (float) $realEstate->getLatitude();
        //$coords->longitude = (float) $realEstate->getLongitude();

        //$location->coordinates = $coords;

        $request->location = $location;

        $request->pricing = $pricingDto;
        if($request->pricing->price == 0) {
            throw new \Exception("Can not register listing; price is required");
        }

        //optionals
        $request->content = $this->buildContentArray($realEstate);
        if(count($request->content) <= 0) {
            throw new \Exception("Can not register listing; need to add some media content");
        }

        $request->accessibility = $realEstate->hasFeatureWithId(self::ACCESSIBILITY_FEATURE_ID);
        $request->total_bedrooms = $realEstate->getBedroomCountSafe();
        $request->basement = $realEstate->hasFeatureWithId(self::BASEMENT_FEATURE_ID);
        $request->bathrooms = $realEstate->getBathroomCount() == null ? 0 : $realEstate->getBathroomCount();
        $request->burglar_alarm = $realEstate->hasFeatureWithId(self::BURGLAR_ALARM_FEATURE_ID);
        $request->display_address = $this->buildDisplayAddress($realEstate);
        $features = $realEstate->getRealEstateFeatures()->map(function(RealEstateFeature $feature) {
            return $feature->getFeature()->getFeatureLocalization('en')->getName();
        })->toArray();

        if($realEstate->getPool()) {
            $features[] = "Pool";
        }

        if($realEstate->getFirstRowByTheSea()) {
            $features[] = "First row by the sea";
        }

        if($realEstate->getGarage()) {
            $features[] = "Garage";
        }

        if($realEstate->getSeaView()) {
            $features[] = "Sea view";
        }

        if(count($features) <= 0) {
            throw new \Exception("Can not register listing; need to add features");
        }

        $request->feature_list = $features;
        $request->fireplace = $realEstate->hasFeatureWithId(self::FIREPLACE_FEATURE_ID);
        $request->new_home = $realEstate->getNewBuilding() == null ? false : $realEstate->getNewBuilding();
        $request->swimming_pool = $realEstate->getPool() == null ? false : $realEstate->getPool();
        $request->waterfront = $realEstate->getFirstRowByTheSea() == null ? false : $realEstate->getFirstRowByTheSea();

        return $request;
    }

    private function buildDisplayAddress(RealEstate $realEstate): string {
        $baseAddress = $this->buildStreetName($realEstate);

        return $baseAddress . ",Hrvatska,Croatia";
    }

    // hack; do not display real address it seems
    private function buildStreetName(RealEstate $realEstate): string {
        $place = $realEstate->getPlace();

        if($place != null) {
            return $place;
        }

        return $realEstate->getLocation()->getLocationLocalization("en")->getName();
    }

    private function mapRealEstateTypeToZooplaPropertyType(RealEstate $realEstate): string
    {
        $type = $realEstate->getRealEstateType();
        $propType = "property";

        switch ($type->getId()) {
            case 1:
                $propType = "flat";
                break;
            case 2:
                $propType = "villa";
                break;
            case 3:
                $propType = "land";
                break;
            case 4:
                $propType = "office";
                break;
        }

        return $propType;
    }

    private function mapRealEstateTypeCategory(RealEstate $realEstate): string
    {
        $type = $realEstate->getRealEstateType();

        switch ($type->getId()) {
            case 1:
            case 2:
                return "residential";
            case 3:
            case 4:
                return "commercial";
        }

        return "residential";
    }

    private function mapRealEstateStatusToZooplaLifeCycleStatus(RealEstate $realEstate): string
    {
        $status = $realEstate->getStatus();
        $lss = "sold";

        switch ($status->getDisplayId()) {
            case "PUBLISHED":
                $lss = "available";
                break;
            case "IN_PROGRESS":
                $lss = "under_offer";
                break;
            case "COMPLETED":
                $lss = "sold";
                break;
        }

        return $lss;
    }

    /**
     * @param RealEstate $realEstate
     * @return ListingContent[]
     */
    private function buildContentArray(RealEstate $realEstate): array
    {
        $content = [];

        $coverImage = $realEstate->getCoverImage();

        if($coverImage != null) {
            $content[] = $this->buildContentForImage($coverImage);
        }

        foreach ($realEstate->getRealEstateImages() as $realEstateImage) {
            if($realEstateImage->getInvalidated() == 0 && !$realEstateImage->isCover()) {
                $content[] = $this->buildContentForImage($realEstateImage);
            }
        }

        foreach ($realEstate->getGroundPlans() as $groundPlan) {
            if($groundPlan->getInvalidated() == 0) {
                $content[] = $this->buildContentForGroundPlan($groundPlan);
            }
        }

        if($realEstate->getVirtualWalkUrl() != null) {
            $cntItem = new ListingContent();
            $cntItem->url = $realEstate->getVirtualWalkUrl();
            $cntItem->type = "virtual_tour";
            $cntItem->caption = "Virtual tour";

            $content[] = $cntItem;
        }

        return $content;
    }

    private function buildContentForImage(RealEstateImage $realEstateImage): ListingContent
    {
        $cntItem = new ListingContent();

        $cntItem->type = "image";
        $cntItem->url = GlobalSettings::getBaseUrl() . "/" .  $realEstateImage->getPath();

        /** @var RealEstateImageMetaLocalization $imageLocale */
        $imageLocale = $realEstateImage->getRealEstateImageMetaLocalizations()->filter(function(RealEstateImageMetaLocalization $metaLocalization) {
            return $metaLocalization->getLanguage()->getCode() == "en";
        })->first();

        if($imageLocale != null ) {
            $cntItem->caption = $imageLocale->getAltTag();
        } else {
            $cntItem->caption = $realEstateImage->getOriginalFileName();
        }

        if($cntItem->caption == null) {
            $cntItem->caption = "Placeholder";
        }

        return $cntItem;
    }

    private function buildContentForGroundPlan(GroundPlan $groundPlan): ListingContent
    {
        $cntItem = new ListingContent();

        $cntItem->type = "floor_plan";
        $cntItem->url = GlobalSettings::getBaseUrl() . "/" .  $groundPlan->getFilePath();

        $cntItem->caption = $groundPlan->getName();
        return $cntItem;
    }

    private function createETag(ListingUpdateRequest $request)
    {
        return md5(serialize($request));
    }

}
 