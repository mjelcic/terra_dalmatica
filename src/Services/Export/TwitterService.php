<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:16 PM
 */

namespace App\Services\Export;


use App\DTO\Twitter\Request\TwitterDTO;
use App\DTO\RealEstateCroatia\ImageDTO;
use App\Entity\Feature;
use App\Entity\RealEstate;
use App\Entity\RealEstateImage;
use App\GlobalSettings;
use App\Services\ServiceRealEstate;
use App\Services\Vendor\TwitterApiExchangeService;
use Doctrine\ORM\EntityManagerInterface;
use App\DTO\RealEstateCroatia\RealEstateDTO;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Serializer\SerializerInterface;

class TwitterService
{

    protected $em;
    protected $urlGenerator;
    protected $client;
    protected $serializer;

    function __construct(EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, HttpClientInterface $client, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->client = $client;
        $this->serializer = $serializer;
    }


    public function post($realEstateId, $locale)
    {

        $realEstate = $this->em->getRepository(RealEstate::class)->find($realEstateId);
        $link = $this->urlGenerator->generate('real_estate_details', array('slug' => $realEstate->getRealEstateLocalization($locale)->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
        //$link = "https://www.terradalmatica.hr/prodaja-nekretnina-hrvatska/prodaja-gradevinskih-poljoprivrednih-zemljista";
        $status = $realEstate->getRealEstateLocalization($locale)->gettitle();

        $imageEntity = $this->em->getRepository(RealEstateImage::class)->findOneBy(array("realEstate" => $realEstate, "cover" => 1));

        $twitter = new TwitterApiExchangeService(GlobalSettings::getTwitterAuth());
        $twitter->buildOauth(GlobalSettings::getTwitterApiUrl(), "POST");


        if ($imageEntity) {
            if($imageEntity->getCreatedBy()==9999) {
                $image = $imageEntity->getPath();
            }else{
                $image = GlobalSettings::getBaseUrl() . "/" .  $imageEntity->getPath();
            }
            if(@getimagesize($image)) {
                $uploadImageResponseJson = $this->uploadImage($image);
                $uploadImageResponse = $this->serializer->deserialize($uploadImageResponseJson, \App\DTO\Twitter\Response\TwitterImageDTO::class, "json");
                $mediaId = $uploadImageResponse->media_id;
                $twitter->setPostfields(array("status" => $status . "\n" . $link, "media_ids" => $mediaId));
            }
            else{
                $twitter->setPostfields(array("status" => $status . "\n" . $link));
            }
        }
        else{
            $twitter->setPostfields(array("status" => $status . "\n" . $link));
        }

        $response = $twitter->performRequest(true, array(CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0));

        if($response){
            $twitterResponse = $this->serializer->deserialize($response, \App\DTO\Twitter\Response\TwitterDTO::class, "json");
            $realEstate->setTwitterPostId($twitterResponse->id);
            $this->em->flush();
        }

        return $response;
    }

    public function uploadImage($imagePath)
    {
        $twitter = new TwitterApiExchangeService(GlobalSettings::getTwitterAuth());
        $twitter->buildOauth("https://upload.twitter.com/1.1/media/upload.json", "POST");
        $media_data = base64_encode(file_get_contents($imagePath));
        $twitter->setPostfields(array("media_data" => $media_data, "media_category" => "tweet_image"));
        $response = $twitter->performRequest(true, array(CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0));
        return $response;
    }
}