<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 4/14/2020
 * Time: 1:06 PM
 */

namespace App\Services;


use App\Entity\Location;
use App\Entity\RealEstateType;
use App\Entity\UserPreference;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ServiceUserPreference extends AbstractBaseService
{
    private $logger;

    function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        parent::__construct($entityManager, UserPreference::class);
        $this->logger = $logger;
    }

    public function getByUser(UserInterface $user)
    {
        $qb = $this->repository->createBasicQueryBuilder("up");

        $qb->andWhere("up.user = :user")
            ->setParameter("user", $user);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function modifyUserPreference(UserInterface $user, RealEstateType $realEstateType, Location $location, float $price)
    {
        if($user == null){
            $this->logger->error("Tried to modify user preference with user = null!");
            return;
        }

        $userPreference = $this->getByUser($user);
        $priceOffset = 100000; //TODO: needs to be defined

        if($userPreference == null){
            $userPreference = new UserPreference();
            $userPreference->setCreatedBy($user->getId());
            $userPreference->setUser($user);

            //If it's a first entry set price range to 100k +- of the initial visited real estate
            $userPreference->setPriceMin($price - $priceOffset);
            $userPreference->setPriceMax($price + $priceOffset);
        }

        if($userPreference != null){
            $userPreference->setUpdatedBy($user->getId());
            $userPreference->setUpdatedAt(new \DateTime());

            //prices
            if($userPreference->getPriceMin() + $priceOffset > $price){
                $userPreference->setPriceMin($price - $priceOffset);
            }

            //prices
            if($userPreference->getPriceMax() - $priceOffset < $price){
                $userPreference->setPriceMax($price + $priceOffset);
            }
        }

        $userPreference
            ->addRealEstateType($realEstateType)
            ->addLocation($location);

        try {
            $this->entityManager->persist($userPreference);
            $this->entityManager->flush();
        } catch (\Exception $ex){
            $this->logger->error($ex->getMessage());
        }
    }
}