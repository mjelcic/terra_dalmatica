<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 3:00 PM
 */

namespace App\Services;


use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class NotificationService extends AbstractBaseService
{

    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, Notification::class);
    }


    public function getNotificationsForUser(User $user){

        $qb = $this->repository->createBasicQueryBuilder("notification");
        $qb->where("notification.for_user = :user")
            ->orWhere("notification.for_role in (:roles)")
            ->andWhere(":user NOT MEMBER OF notification.seen_by")
            ->setParameter("user", $user)
            ->setParameter("roles", $user->getRoles());

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function createNotification(
        string $forRole,
        ?User $forUser,
        User $triggeredBy,
        string $verb,
        $object = null,
        $realEstateCustomPage = null,
        ?string $suffixMessage = null
    ) {
        $notification = new Notification();

        try {
            $notification
                ->setForRole($forRole)
                ->setForUser($forUser)
                ->setVerb($verb)
                ->setObject($object)
                ->setRealEstateCustomPage($realEstateCustomPage)
                ->setTriggeredBy($triggeredBy)
                ->setCreatedBy($triggeredBy->getId())
                ->setCreatedAt(new \DateTime)
                ->setSuffixMessage($suffixMessage);

            $this->entityManager->persist($notification);

            $this->entityManager->flush();
        } catch (\Exception $ex){
            //TODO: Something???
        }
    }
}