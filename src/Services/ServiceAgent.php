<?php


namespace App\Services;


use App\Entity\AgentPage;
use App\Entity\Language;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ServiceAgent extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, AgentPage::class);
    }

    public function getAgentDetailsAndPage(User $user, Language $language)
    {
        $qb = $this->repository->createBasicQueryBuilder("agentPage");

        $qb->select("agentPage.image AS image, user.first_name, user.last_name, user.phone_no AS phone_number, user.mobile_phone_number as mobile_phone_number, user.fax_number as fax_number, loco.slug, user.email ")
            ->leftJoin("agentPage.agentPageLocalizations", "loco")
            ->leftJoin("agentPage.user", "user")
            ->andWhere("agentPage.user = :user")
            ->andWhere("loco.language = :language")
            ->setParameter("user", $user)
            ->setParameter("language", $language);


        $result = $qb->getQuery()->getOneOrNullResult();

        // maybe our agent does not have page
        if($result == null) {
            /**
             * @var User $userDetails
             */
            $userDetails = $this->entityManager->getRepository(User::class)->find($user->getId());

            return [
                "first_name" => $userDetails->getFirstName(),
                "last_name" => $userDetails->getLastName(),
                "email" => $userDetails->getEmail(),
                "phone_number" => $userDetails->getPhoneNo(),
                "fax_number" => $userDetails->getFaxNumber(),
                "mobile_phone_number" => $userDetails->getMobilePhoneNumber()
            ];
        } else {
            return $result;
        }

    }

    public function getAgentPageBySlug($slug, Language $language)
    {
        $qb = $this->repository->createBasicQueryBuilder("agentPage");

        $qb->select("agentPage.id, agentPage.image AS image, user.first_name, user.last_name, user.email, user.phone_no AS phone_number, user.fax_number, user.mobile_phone_number, loco.content, loco.title, loco.slug, user.id as user_id")
            ->leftJoin("agentPage.agentPageLocalizations", "loco")
            ->leftJoin("agentPage.user", "user")
            ->andWhere("loco.language = :language")
            ->andWhere("loco.slug = :slug")
            ->setParameter("slug", $slug)
            ->setParameter("language", $language);


        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getSlugForLanguage($id, $locale)
    {
        $query = $this->entityManager->createQuery(
            "Select loc.slug
            FROM App\Entity\AgentPage ap
            JOIN ap.agentPageLocalizations loc  
            JOIN loc.language lang    
            WHERE ap.id = :apId
            AND lang.code = :locale"
        )->setParameter("apId", $id)
            ->setParameter(":locale", $locale)
            ->setMaxResults(1);
        $result = $query->execute();
        if ($result != null) {
            return $result[0]["slug"];
        } else {
            return null;
        }
    }

}