<?php
/**
 * Created by mijatra.
 * User: Administrator
 * Date: 1/27/2020
 * Time: 9:54 PM
 */

namespace App\Services;


use App\Entity\FeatureLocalization;
use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\RealEstateCustomPage;
use App\Entity\RealEstateCustomPageFilters;
use App\Entity\RealEstateFeature;
use App\Entity\RealEstateImage;
use App\Entity\RealEstateMetaLocalization;
use App\Entity\UserPreference;
use App\GlobalSettings;
use App\Utility\Utils;
use App\ViewModel\Frontend\Enum\RealEstatesSortEnum;
use App\ViewModel\Frontend\RealEstateDetailViewModel;
use App\ViewModel\Frontend\SearchModel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ServiceRealEstate extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, RealEstate::class);
    }

    public function getBySlug(?string $slug)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->andWhere("loco.slug = :slug")
            ->setParameter("slug", $slug);

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getRealEstateDetailsLocalized(RealEstate $realEstate, Language $language): RealEstateDetailViewModel
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("NEW App\ViewModel\Frontend\RealEstateDetailViewModel(realEstate.id, rel.title, rel.slug, rel.description, ll.name, retl.name,
                realEstate.firstRowByTheSea, realEstate.seaView, realEstate.bedroomCount, realEstate.bathroomCount, realEstate.distanceFromTheSea, 
                realEstate.energyClass, realEstate.garage, realEstate.longitude, realEstate.latitude, realEstate.livingArea, realEstate.surfaceArea, realEstate.pool, 
                realEstate.price, 0, realEstate.virtualWalkUrl, realEstate.videoUrl, realEstate.uid, realEstate.priceOnRequest, IDENTITY(retl.real_estate_type), realEstate.underConstruction)")
            ->leftJoin("App\Entity\RealEstateLocalization", "rel", "with", "rel.RealEstate = realEstate AND rel.language = :language")
            ->leftJoin("App\Entity\LocationLocalization", "ll", "with", "realEstate.location = ll.location AND ll.language = :language")
            ->leftJoin("App\Entity\RealEstateTypeLocalization", "retl", "with", "realEstate.RealEstateType = retl.real_estate_type AND retl.language = :language")
            ->andWhere("realEstate = :realEstate")
            ->setParameter("realEstate", $realEstate)
            ->setParameter("language", $language);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getRealEstateImageDetails(RealEstate $realEstate)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("image.path AS path, image.cover AS cover")
            ->leftJoin("realEstate.realEstateImages", "image")
            ->andWhere("realEstate = :realEstate")
            ->orderBy("image.cover", "DESC")
            ->setParameter("realEstate", $realEstate);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRealEstateImageDetailsWithMetaLocalized(RealEstate $realEstate, Language $language)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("image.path AS path, image.cover AS cover, imageMeta.alt_tag AS alt_tag")
            ->leftJoin("realEstate.realEstateImages", "image")
            ->leftJoin("image.realEstateImageMetaLocalizations", "imageMeta")
            ->andWhere("realEstate = :realEstate")
            ->andWhere("imageMeta.language = :language")
            ->andWhere("image.invalidated = 0")
            ->orderBy("image.cover", "DESC")
            ->setParameter("realEstate", $realEstate)
            ->setParameter("language", $language);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRealEstateImageDetailsWithMetaLocalizedForIds(array $imageIds, Language $language)
    {
        $qb = $this->entityManager->getRepository(RealEstateImage::class)->createBasicQueryBuilder("realEstateImage");

        $qb->select("realEstateImage.id AS id, realEstateImage.path AS path, realEstateImage.cover AS cover, imageMeta.alt_tag AS alt_tag")
            ->leftJoin("realEstateImage.realEstateImageMetaLocalizations", "imageMeta")
            ->where("realEstateImage.id IN (:imageIds)")
            ->andWhere("imageMeta.language = :language")
            ->orderBy("realEstateImage.cover", "DESC")
            ->setParameter("imageIds", $imageIds)
            ->setParameter("language", $language)
            ;
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getImagesForRealEstates(array $realEstatesIds)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("realEstate.id as real_estate_id, image.path AS path, image.cover AS cover")
            ->leftJoin("realEstate.realEstateImages", "image")
            ->andWhere("realEstate IN (:realEstatesIds)")
            ->orderBy("image.cover", "DESC")
            ->setParameter("realEstatesIds", $realEstatesIds);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRealEstateFeaturesLocalized(RealEstate $realEstate, Language $language)
    {
        $qb = $this->entityManager->getRepository(RealEstateFeature::class)->createBasicQueryBuilder("realEstateFeature");

        $qb->select("fl.name")
            ->leftJoin("App\Entity\FeatureLocalization", "fl", "with", "fl.feature = realEstateFeature.feature AND fl.language = :language")
            ->andWhere("realEstateFeature.real_estate = :realEstate")
            ->andWhere("fl.language = :language")
            ->setParameter("realEstate", $realEstate)
            ->setParameter("language", $language);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRealEstateFeaturesLocalizedForIds(array $featureIds, Language $language)
    {
        $qb = $this->entityManager->getRepository(FeatureLocalization::class)->createBasicQueryBuilder("fl");

        $qb->select("fl.name")
            ->andWhere("fl.feature IN (:featureIds)")
            ->andWhere("fl.language = :language")
            ->setParameter("featureIds", $featureIds)
            ->setParameter("language", $language);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRealEstateMetadataLocalized(RealEstate $realEstate, Language $language): ?RealEstateMetaLocalization
    {
        $qb = $this->entityManager->getRepository(RealEstateMetaLocalization::class)->createBasicQueryBuilder("realEstateMetaLoco");

        $qb->select("realEstateMetaLoco")
            ->andWhere("realEstateMetaLoco.RealEstate = :realEstate")
            ->andWhere("realEstateMetaLoco.language = :language")
            ->setParameter("realEstate", $realEstate)
            ->setParameter("language", $language);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getByCustomPageFilters(RealEstateCustomPageFilters $filters, bool $execute = true)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        if ($filters->getLocations()->count() > 0) {
            $qb->andWhere("realEstate.location IN (:locations)")
                ->setParameter("locations", $filters->getLocations()->toArray());
        }

        if ($filters->getRealEstateTypes()->count() > 0) {
            //TODO: the RealEstateType field name should be corrected on the RealEstate entity
            $qb->andWhere("realEstate.RealEstateType IN (:types)")
                ->setParameter("types", $filters->getRealEstateTypes()->toArray());
        }

        $result = null;

        $result = $execute ? $qb->getQuery()->getResult() : $qb;

        return $result;
    }

    public function getCategoryPageRealEstates(RealEstateCustomPage $categoryPage, int $page = 1, int $offset = 9, $sort = "", string $locale)
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->getByCustomPageFilters($categoryPage->mergeFilters(), false);

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("language.code = :locale")
            ->setParameter("locale", $locale);

        $this->setFrontendDisplaybleStatusCondition($qb);

        switch ($sort) {
            case RealEstatesSortEnum::dateHighest:
                $qb->addOrderBy("realEstate.created_at", "DESC");
                break;
            case RealEstatesSortEnum::dateLowest:
                $qb->addOrderBy("realEstate.created_at", "ASC");
                break;
            case RealEstatesSortEnum::priceLowest:
                $qb->addOrderBy("realEstate.priceOnRequest", "DESC");
                $qb->addOrderBy("realEstate.price", "ASC");
                break;
            case RealEstatesSortEnum::priceHighest:
                $qb->addOrderBy("realEstate.priceOnRequest", "DESC");
                $qb->addOrderBy("realEstate.price", "DESC");
                break;
            default:
                $qb->addOrderBy("realEstate.created_at", "DESC");
        }

        $qb->setMaxResults($offset)
            ->setFirstResult(($page - 1) * $offset);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getCategoryPageRealEstatesCount(RealEstateCustomPage $categoryPage, string $locale)
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->getByCustomPageFilters($categoryPage->mergeFilters(), false);

        $qb->select("COUNT(realEstate.id) as total")
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->andWhere("language.code = :locale")
            ->setParameter("locale", $locale);

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getSingleScalarResult();

        return $result;
    }

    public function getGeoLocationsForCategoryPageRealEstates(RealEstateCustomPageFilters $filters): array
    {
        //Get geolocations from all real estates that "belong" to the category page
        $qb = $this->getByCustomPageFilters($filters, false);

        $this->setFrontendDisplaybleStatusCondition($qb);

        $realEstates = $qb->getQuery()->getResult();

        $geoLocations = array();
        foreach ($realEstates as $realEstate) {
            /**
             * @var $realEstate RealEstate
             */
            if ($realEstate->getLatitude() && $realEstate->getLongitude()) {
                $geoLocations[] = [$realEstate->getLatitude() + GlobalSettings::LatLngOffset,
                    $realEstate->getLongitude() + GlobalSettings::LatLngOffset];
            }
        }

        return $geoLocations;
    }

    public function getRandomRealEstates($limit = 15)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->orderBy("RAND()")
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getRandomRealEstatesLocalized($limit = 15, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("language.code = :locale")
            ->orderBy("RAND()")
            ->setMaxResults($limit)
            ->setParameter("locale", $locale);

        $result = $qb->getQuery()->getResult();

        $this->setFrontendDisplaybleStatusCondition($qb);

        return $result;
    }

    public function getRealEstatesLocalizedForAgent($agentId, $limit = 15, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.agent", "agent")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("language.code = :locale")
            ->andWhere("agent.id = :agentId")
            ->setMaxResults($limit)
            ->setParameter("locale", $locale)
            ->setParameter("agentId", $agentId);

        $result = $qb->getQuery()->getResult();

        $this->setFrontendDisplaybleStatusCondition($qb);

        return $result;
    }

    public function getFeaturedRealEstatesLocalized($limit = 15, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("realEstate.featured = 1")
            ->andWhere("language.code = :locale")
            ->setMaxResults($limit)
            ->setParameter("locale", $locale);

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getFeaturedRealEstatesCount()
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("COUNT(realEstate)")
            ->andWhere("realEstate.featured = 1");

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getSingleScalarResult();

        return $result;
    }

    public function getRecentRealEstatesLocalized($limit = 15, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("language.code = :locale")
            ->orderBy("realEstate.created_at", "DESC")
            ->setMaxResults($limit)
            ->setParameter("locale", $locale);

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getCrossSellByRealEstate(RealEstate $realEstate, $limit = 15, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb)
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("realEstate.id <> :real_estate_id")
            ->andWhere("language.code = :locale")
            ->andWhere("realEstate.RealEstateType = :real_estate_type")
            ->andWhere("realEstate.price BETWEEN :min_price AND :max_price")
            ->orderBy("realEstate.created_at", "DESC")
            ->setMaxResults($limit)
            ->setParameter("real_estate_id", $realEstate->getId())
            ->setParameter("locale", $locale)
            ->setParameter("real_estate_type", $realEstate->getRealEstateType())
            ->setParameter("min_price", $realEstate->getPrice() - GlobalSettings::getCrossSellPriceOffset())
            ->setParameter("max_price", $realEstate->getPrice() + GlobalSettings::getCrossSellPriceOffset());

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->getQuery()->getResult();

        return $result;
    }


    public function searchRealEstates(SearchModel $filters, int $page = 1, int $offset = 9, $locale = "hr", &$count, &$availableFilters)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->andWhere("language.code = :locale")
            ->setParameter("locale", $locale);

        $this->setFrontendDisplaybleStatusCondition($qb);

        if ($filters->text) {

            //If entered value is integer then consider it as uid and filter by it
            if (Utils::is_integer($filters->text)) {
                $qb->andWhere("realEstate.uid = :uid")
                    ->setParameter("uid", $filters->text);
            }

            if (!Utils::is_integer($filters->text)) {
                //Hack for features
                $aDirectFeatures = array(
                    "hr" => array("prvi red do mora", "pogled na more", "nekretnine u izgradnji", "novogradnja", "bazen", "garaža"),
                    "en" => array("first row to the sea", "sea view", "under construction", "new construction", "swimming pool", "garage"),
                    "de" => array("erste reihe zu meer", "meerblick", "immobilien im Bau", "neubau", "pool", "garage")
                );

                $aFeaturesLoco = $this->entityManager
                    ->getRepository(FeatureLocalization::class)
                    ->createBasicQueryBuilder("fl")
                    ->select("fl.name")
                    ->leftJoin("fl.language", "language")
                    ->andWhere("language.code = :lang_code")
                    ->setParameter("lang_code", $locale)
                    ->getQuery()
                    ->getResult();

                $aFeatures = array_merge($aDirectFeatures[$locale], array_map(function ($elem) { return strtolower($elem["name"]); }, $aFeaturesLoco));

                $inputTextLower = strtolower($filters->text);
                $aFeatureQueries = [];
                foreach ($aFeatures as $feature)
                {
                    if(strpos($inputTextLower, $feature) !== false){
                        $aFeatureQueries[] = $qb->expr()->orX(
                            $qb->expr()->like("loco.features_map", $qb->expr()->literal('%' . $feature . '%')),
                            $qb->expr()->like("loco.synonyms", $qb->expr()->literal('%' . $feature . '%'))
                        );
                    }
                }

                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->andX(
                        "MATCH(loco.title, loco.description, loco.synonyms, loco.features_map) AGAINST(:text boolean) > 0",
                        ...$aFeatureQueries
                    ),
                    $qb->expr()->eq("loco.title", $qb->expr()->literal($filters->text))//hack for title
                ))
                    ->setParameter("text", $this->prepareStringForFullTextSearch($filters->text));
            }


        }

        if ($filters->realEstateType) {
            $qb->andWhere("realEstate.RealEstateType = :realEstateType")
                ->setParameter("realEstateType", $filters->realEstateType);
        }

        if ($filters->location) {
            $qb->andWhere("realEstate.location = :location")
                ->setParameter("location", $filters->location);
        }

        if(($filters->priceMin && $filters->priceMin != GlobalSettings::SearchbarPriceSlideMinValue)
            || ($filters->priceMax && $filters->priceMin != GlobalSettings::SearchbarPriceSlideMaxValue))
        {
            if ($filters->priceMin) {
                $qb->andWhere("realEstate.price >= :priceMin")
                    ->setParameter("priceMin", $filters->priceMin);
            }

            if($filters->priceMax < GlobalSettings::SearchbarPriceSlideMaxValue){
                if ($filters->priceMax) {
                    $qb->andWhere("realEstate.price <= :priceMax")
                        ->setParameter("priceMax", $filters->priceMax);
                }
            }
        }

        if ($filters->bedroomCount) {
            $qb->andWhere("realEstate.bedroomCount >= :bedroomCount")
                ->setParameter("bedroomCount", $filters->bedroomCount);
        }

        if ($filters->firstRowByTheSea) {
            $qb->andWhere("realEstate.firstRowByTheSea = 1");
        }

        if ($filters->seaView) {
            $qb->andWhere("realEstate.seaView = 1");
        }

        if ($filters->newBuilding) {
            $qb->andWhere("realEstate.newBuilding = 1");
        }

        if ($filters->underConstruction) {
            $qb->andWhere("realEstate.underConstruction = 1");
        }

        if ($filters->priceLowered) {
            $qb->andWhere($qb->expr()->andX("realEstate.discountPrice IS NOT NULL",
                "realEstate.discountPrice > 0"));
        }

        if ($filters->pool) {
            $qb->andWhere("realEstate.pool = 1");
        }

        //Set orderby based on sort field in the filters object
        switch ($filters->sort) {
            case RealEstatesSortEnum::dateHighest:
                $qb->addOrderBy("realEstate.created_at", "DESC");
                break;
            case RealEstatesSortEnum::dateLowest:
                $qb->addOrderBy("realEstate.created_at", "ASC");
                break;
            case RealEstatesSortEnum::priceLowest:
                $qb->addOrderBy("realEstate.priceOnRequest", "DESC");
                $qb->addOrderBy("realEstate.price", "ASC");
                break;
            case RealEstatesSortEnum::priceHighest:
                $qb->addOrderBy("realEstate.priceOnRequest", "DESC");
                $qb->addOrderBy("realEstate.price", "DESC");
                break;
            default:
                $qb->addOrderBy("realEstate.created_at", "DESC");
        }

        //Calc count
        $qb->select("count(realEstate.id)");

        $count = $qb->getQuery()->getSingleScalarResult();

        if($count > 0 && $page == 1){
            $availableFilters = $this->checkFiltersAvailabality($qb, $filters);
        }

        //get ids of all real estates
        $qb->select("realEstate.id");
        $ids = $qb->setMaxResults($offset)
            ->setFirstResult(($page - 1) * $offset)
            ->getQuery()
            ->getResult('ColumnHydrator');

        //save orderBy
        $orderByParts = $qb->getDQLPart("orderBy");

        //reset the query builder for final query
        $qb = $this->repository->createBasicQueryBuilder("realEstate");
        $qb->select("realEstate as data, images, loco.slug as slug, loco.title as title")
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "images")
            ->andWhere("realEstate.id IN (:ids)")
            ->andWhere("language.code = :locale")
            ->setParameter("locale", $locale)
            ->setParameter("ids", $ids);

        foreach ($orderByParts as $orderByPart)
        {
            $qb->addOrderBy($orderByPart);
        }

        $results = $qb->getQuery()->getResult();

        $this->indexResultsAccordingToPage($results, $page, GlobalSettings::getRealEstatesSearchResultsPerPage());

        return $results;
    }

    private function indexResultsAccordingToPage(&$results, $page, $pageCount)
    {
        for($i = 1; $i <= count($results); $i++)
        {
            $results[$i-1]["index"] = ($page-1) * $pageCount + $i;
        }
    }

    public function getByUserPreferenceLocalized(?UserPreference $preference, $locale = "hr")
    {
        if ($preference == null) {
            return [];
        }

        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $this->setSelectForSliderCardDisplay($qb);

        $qb->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "with", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("language.code = :locale")
            ->andWhere("realEstate.RealEstateType IN (:types)")
            ->andWhere("realEstate.location In (:locations)")
            ->andWhere("realEstate.price BETWEEN :price_min AND :price_max")
            ->setParameter("locale", $locale)
            ->setParameter("types", $preference->getRealEstateTypes()->toArray())
            ->setParameter("locations", $preference->getLocations()->toArray())
            ->setParameter("price_min", $preference->getPriceMin())
            ->setParameter("price_max", $preference->getPriceMax());

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    private function setSelectForSliderCardDisplay(QueryBuilder $qb): QueryBuilder
    {
        $qb->select("realEstate.id, realEstate.price, realEstate.priceOnRequest, loco.title AS title, loco.slug AS slug, image.path AS cover_image");

        return $qb;
    }

    private function setFrontendDisplaybleStatusCondition(QueryBuilder $qb): QueryBuilder
    {
        //2 = PUBLISHED, 4 = IN_PROGRESS
        $qb->leftJoin("realEstate.status", "status")
            ->andWhere("status.id IN (2,4)");

        return $qb;
    }

    private function prepareStringForFullTextSearch($input = "")
    {
        $words = explode(" ", $input);
        for ($i = 0; $i < count($words); $i++) {
            if(mb_strlen($words[$i], "UTF-8") > 3){
                $words[$i] = '+' . $words[$i];
            }
        }

        $result = implode(" ", $words);

        return $result;
    }

    // START logic for similar real estates
    private function searchModelToArrayOfFilters($searchModel)
    {
        $filtersArray = GlobalSettings::getFiltersByImportance();
        $filtersArray["text"] = $searchModel->text;
        $filtersArray["realEstateType"] = $searchModel->realEstateType;
        $filtersArray["location"] = $searchModel->location;
        $filtersArray["priceMin"] = $searchModel->priceMin;
        $filtersArray["priceMax"] = $searchModel->priceMax;
        $filtersArray["bedroomCount"] = $searchModel->bedroomCount;
        $filtersArray["firstRowByTheSea"] = $searchModel->firstRowByTheSea;
        $filtersArray["seaView"] = $searchModel->seaView;
        $filtersArray["newBuilding"] = $searchModel->newBuilding;
        $filtersArray["underConstruction"] = $searchModel->underConstruction;
        $filtersArray["underConstruction"] = $searchModel->underConstruction;
        $filtersArray["priceLowered"] = $searchModel->priceLowered;
        $filtersArray["pool"] = $searchModel->pool;
        $filtersArray["sort"] = $searchModel->sort;
        return $filtersArray;
    }

    private function arrayOfFiltersToSearchModel($filtersArray)
    {
        $searchModel = new SearchModel();
        $searchModel->text = $filtersArray["text"];
        $searchModel->realEstateType = $filtersArray["realEstateType"];
        $searchModel->location = $filtersArray["location"];
        $searchModel->priceMin = $filtersArray["priceMin"];
        $searchModel->priceMax = $filtersArray["priceMax"];
        $searchModel->bedroomCount = $filtersArray["bedroomCount"];
        $searchModel->firstRowByTheSea = $filtersArray["firstRowByTheSea"];
        $searchModel->seaView = $filtersArray["seaView"];
        $searchModel->newBuilding = $filtersArray["newBuilding"];
        $searchModel->underConstruction = $filtersArray["underConstruction"];
        $searchModel->priceLowered = $filtersArray["priceLowered"];
        $searchModel->pool = $filtersArray["pool"];
        $searchModel->sort = $filtersArray["sort"];


        return $searchModel;
    }

    private function removeLastFilter(array $filters)
    {
        foreach (array_reverse($filters) as $key => $value) {
            if (($value)&&($key!="sort")) {
                $filters[$key] = null;
                break;
            }
        }
        return $filters;
    }


    public function searchOrSimilar(SearchModel $filters, int $page = 1, int $offset = 9, $locale = "hr", &$count, &$similar, &$availableFilters)
    {
        return $this->searchOrSimilarRecursive($this->searchModelToArrayOfFilters($filters), $page, $offset, $locale, $count, $similar, $availableFilters);
    }

    private function containsOnlyNull($input)
    {
        return empty(array_filter($input, function ($a) {
            return $a !== null;
        }));
    }

    private function searchOrSimilarRecursive(array $filters, $page, $offset, $locale, &$count, &$similar, &$availableFilters)
    {
        $results = $this->searchRealEstates($this->arrayOfFiltersToSearchModel($filters), $page, $offset, $locale, $count, $availableFilters);
        if ((!$results)&&(!$this->containsOnlyNull($filters))) {
            $similar = 1;
            return $this->searchOrSimilarRecursive($this->removeLastFilter($filters), $page, $offset, $locale, $count, $similar, $availableFilters);
        } else {
            return $results;
        }
    }
    // END simlar REAL ESTATES

    public function checkFiltersAvailabality(QueryBuilder $qb, SearchModel $filters)
    {
        //If there are no filters "set" do nothing
        if($filters->isFiltered() == false){
            return [];
        }

        $validators = array(
            "firstRowByTheSea" => true,
            "seaView" => true,
            "newBuilding" => true,
            "underConstruction" => true,
            "priceLowered" => true,
            "pool" => true,
            "type1" => true,
            "type2" => true,
            "type3" => true,
            "type4" => true,
            "price1" => true,
            "price2" => true,
            "price3" => true,
            "price4" => true,
            "room1" => true,
            "room2" => true,
            "room3" => true,
            "room4" => true,
            "room5" => true,
        );

        $locationValidators = [];
        $qb->resetDQLPart("select"); //reset so we can use addSelect method of QueryBuilder

        $typeStatement = "SUM(CASE
                                WHEN realEstate.RealEstateType = 1 THEN 1
                                ELSE 0
                            END) as type1,
                        SUM(CASE
                                WHEN realEstate.RealEstateType = 2 THEN 1
                                ELSE 0
                            END) as type2,
                        SUM(CASE
                                WHEN realEstate.RealEstateType = 3 THEN 1
                                ELSE 0
                            END) as type3,
                        SUM(CASE
                                WHEN realEstate.RealEstateType = 4 THEN 1
                                ELSE 0
                            END) as type4";

        $priceStatement = "SUM(CASE
                                WHEN realEstate.price between 0 and 100000 THEN 1
                                ELSE 0
                            END) as price1,
                        SUM(CASE
                            WHEN realEstate.price between 100000 and 250000 THEN 1
                            ELSE 0
                        END) as price2,
                        SUM(CASE
                            WHEN realEstate.price between 250000 and 500000 THEN 1
                            ELSE 0
                        END) as price3,
                        SUM(CASE
                                WHEN realEstate.price > 500000 THEN 1
                                ELSE 0
                            END) as price4";

        $bedroomStatement = "SUM(CASE
                                WHEN realEstate.bedroomCount >= 1 THEN 1
                                ELSE 0
                            END) as room1,
                            SUM(CASE
                                    WHEN realEstate.bedroomCount >= 2 THEN 1
                                    ELSE 0
                                END) as room2,
                            SUM(CASE
                                    WHEN realEstate.bedroomCount >= 3 THEN 1
                                    ELSE 0
                                END) as room3,
                            SUM(CASE
                                    WHEN realEstate.bedroomCount >= 4 THEN 1
                                    ELSE 0
                                END) as room4,
                            SUM(CASE
                                    WHEN realEstate.bedroomCount >= 5 THEN 1
                                    ELSE 0
                                END) as room5";

        $loweredPriceStatement = "SUM(CASE
			WHEN realEstate.discountPrice IS NOT NULL AND realEstate.discountPrice > 0 THEN 1
			ELSE 0
		END) as priceLowered";


        if(!$filters->realEstateType){
            $qb->addSelect($typeStatement);
        }

        if(!$filters->priceMin || !$filters->priceMax){
            $qb->addSelect($priceStatement);
        }

        if(!$filters->bedroomCount){
            $qb->addSelect($bedroomStatement);
        }

        if(!$filters->firstRowByTheSea){
            $qb->addSelect("SUM(realEstate.firstRowByTheSea) as firstRowByTheSea");
        }

        if(!$filters->seaView){
            $qb->addSelect("SUM(realEstate.seaView) as seaView");
        }

        if(!$filters->newBuilding){
            $qb->addSelect("SUM(realEstate.newBuilding) as newBuilding");
        }

        if(!$filters->underConstruction){
            $qb->addSelect("SUM(realEstate.underConstruction) as underConstruction");
        }

        if(!$filters->pool){
            $qb->addSelect("SUM(realEstate.pool) as pool");
        }

        if(!$filters->priceLowered){
            $qb->addSelect($loweredPriceStatement);
        }

        $check = $qb->getQuery()->getOneOrNullResult();

        foreach ($validators as $key => $value)
        {
            if(array_key_exists($key, $check)){
                $validators[$key] = $check[$key] && $value;
            }
        }

        //locations are a special bunch
        if(!$filters->location){
            $qb->select("IDENTITY(realEstate.location) as locationId, count(realEstate.id) as count")
                ->groupBy("realEstate.location");

            $locationChecks = $qb->getQuery()->getResult();

            $mapArray = [];
            foreach ($locationChecks as $locationCheck)
            {
                $mapArray[] = $locationCheck["locationId"];
            }

            $validators["locations"] = $mapArray;

            //remove group by so the qb can be reused without any issues in the calling method
            $qb->resetDQLPart("groupBy");
        }

        return $validators;
    }

    public function autocompleteUid(string $term, $limit = 10)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->select("realEstate.uid")
            ->andWhere($qb->expr()->like("realEstate.uid", $qb->expr()->literal($term . "%")));

        $this->setFrontendDisplaybleStatusCondition($qb);

        $result = $qb->setMaxResults($limit)->getQuery()->getResult();

        return $result;
    }

    public function findAllUsedForSlugAndLanguage(string $slug, Language $language, $id)
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->andWhere("loco.slug = :slug")
            ->andWhere("loco.language = :language")
            ->andWhere("realEstate.invalidated = 0")
            ->setParameter("slug", $slug)
            ->setParameter("language", $language);

        $this->setFrontendDisplaybleStatusCondition($qb);

        if($id != null) {
            $qb->andWhere("realEstate.id != :id")
                ->setParameter("id", $id);
        }

        return $qb->getQuery()->getResult();
    }

    public function getInvalidatedRealEstates()
    {
        $qb = $this->repository->createBasicQueryBuilder("realEstate");

        $qb->Where("realEstate.invalidated = 1");

        return $qb->getQuery()->getResult();
    }

}