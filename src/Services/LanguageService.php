<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 3:00 PM
 */

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Translation\TranslatorInterface;


class LanguageService extends AbstractController
{
    protected $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getLanguages()
    {
        $query = $this->em->createQuery(
            "Select lang.code, lang.name
            FROM App\Entity\Language lang
            ORDER by lang.id desc"
        );
        $result = $query->execute();
        return $result;
    }
}