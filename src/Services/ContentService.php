<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 3:00 PM
 */

namespace App\Services;

use App\GlobalSettings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Enum\CmsPageTypeEnum;


class ContentService extends AbstractController
{

    protected $em;
    protected $global;

    function __construct(EntityManagerInterface $em, GlobalSettings $global)
    {
        $this->em = $em;
        $this->global = $global;

    }

    public function getPredefinedContentSlugs()
    {
        $query = $this->em->createQuery(
            "Select cnt.display_id, loc.slug, lang.code
            FROM App\Entity\CmsPage cnt
            JOIN cnt.cmsPageLocalizations loc
            JOIN loc.language lang
            WHERE cnt.page_type = :page_type"
        )->setParameter("page_type", CmsPageTypeEnum::PREDEFINED);
        return $query->execute();
    }

    public function getSingleContent($slug, $locale)
    {
        $query = $this->em->createQuery(
            "Select cnt.id, cnt.created_at, usr.username as username, loc.title as title, loc.slug as slug, loc.content as content, lang.code as code, photo.name as image, photoLoc.alt_tag as alt
            FROM App\Entity\CmsPage cnt
            JOIN cnt.cmsPageLocalizations loc
            JOIN loc.language lang
            JOIN App\Entity\User usr
            LEFT JOIN cnt.cover_photo photo      
            LEFT JOIN photo.photoMetaLocalizations photoLoc WITH photoLoc.language = loc.language
            WHERE loc.slug = :slug
            AND lang.code = :locale
            AND usr.id = cnt.created_by"
        )->setParameter("slug", $slug)
            ->setParameter("locale", $locale)
            ->setMaxResults(1);
        $result = $query->execute();
        if ($result != null) {
            return $result[0];
        } else {
            return null;
        }
    }

    public function getSingleCategoryId($slug)
    {
        $query = $this->em->createQuery(
            "Select cat.id
            FROM App\Entity\BlogCategory cat
            JOIN cat.blogCategoryLocalizations loc
            JOIN loc.language lang          
            WHERE loc.slug = :slug"
        )->setParameter("slug", $slug)
            ->setMaxResults(1);
        $result = $query->execute();
        if ($result != null) {
            return $result[0];
        } else {
            return null;
        }
    }

    public function getSingleContentSlug($cmsPageId, $locale)
    {
        $query = $this->em->createQuery(
            "Select loc.slug
            FROM App\Entity\CmsPage cnt
            JOIN cnt.cmsPageLocalizations loc  
            JOIN loc.language lang    
            WHERE cnt.id = :cmsPageId
            AND lang.code = :locale"
        )->setParameter("cmsPageId", $cmsPageId)
            ->setParameter(":locale", $locale)
            ->setMaxResults(1);
        $result = $query->execute();
        if ($result != null) {
            return $result[0]["slug"];
        } else {
            return null;
        }
    }

    public function getSingleContentMeta($cmsPageId, $locale)
    {
        $query = $this->em->createQuery(
            "Select m.title, m.description
            FROM App\Entity\CmsPageMetaLocalization m 
            JOIN m.cmsPage cnt
            JOIN m.language lang  
            WHERE cnt.id = :cmsPageId
            AND lang.code = :locale"
        )->setParameter("cmsPageId", $cmsPageId)
            ->setParameter(":locale", $locale)
            ->setMaxResults(1);
        $result = $query->execute();
        if ($result != null) {
            return $result[0];
        } else {
            return null;
        }
    }

    public function getBlogPosts($locale, $page = 1, $search = null, $category=null, $numOfPosts=null, $slider = false)
    {
        if($numOfPosts==null){
            $numOfPosts = $this->global->getBlogPostsPerPage();
        }
        $parameters = array("locale"=>$locale,"page_type" => CmsPageTypeEnum::BLOG );
        $where_addition = "";
        if($category){
            $where_addition .= " AND cat.id = :category ";
            $parameters["category"] = $category;
        }
        if ($search) {
            $where_addition = " AND (loc.title like :search OR loc.content like :search)";
            $parameters["search"] = "%" . $search . "%";
        }
        if($slider){
            $where_addition .= " AND cnt.slider = :slider ";
            $parameters["slider"] = 1;
        }
        $offset = ($page - 1) * $numOfPosts;
        $dql = "Select cnt as entity, usr.username as username, loc.title as title, loc.content as content, lang.code  as code, loc.slug
            FROM App\Entity\CmsPage cnt
            JOIN cnt.cmsPageLocalizations loc  
            JOIN App\Entity\User usr     
            JOIN loc.language lang
            LEFT JOIN cnt.category cat
            JOIN cat.blogCategoryLocalizations catLoc
            JOIN catLoc.language as catLocLang
            WHERE lang.code = :locale
            AND catLocLang.code = :locale
            AND cnt.page_type = :page_type
            AND usr.id = cnt.created_by " . $where_addition . "
            AND cnt.invalidated = 0
            GROUP BY cnt.id
            ORDER BY cnt.created_at desc";
        $query = $this->em->createQuery($dql
        )->setParameters($parameters)
            ->setFirstResult($offset)
            ->setMaxResults($numOfPosts);
        $result = $query->execute();
        return $result;
    }


    public function countBlogPosts($locale, $search = null, $category=null)
    {
        $where_addition = "";
        $parameters = array("locale"=>$locale);
        if($category){
            $where_addition .= " AND cat.id = :category ";
            $parameters["category"] = $category;
        }
        if ($search) {
            $where_addition = " AND (loc.title like :search OR loc.content like :search)";
            $parameters["search"] = "%" . $search . "%";
        }
        $dql = "Select COUNT(loc.id) as num
            FROM App\Entity\CmsPage cnt    
            JOIN cnt.cmsPageLocalizations loc   
            JOIN loc.language lang
            WHERE lang.code = :locale " . $where_addition . "
            AND cnt.invalidated = 0";
        $query = $this->em->createQuery($dql
        )->setParameters($parameters);
        $result = $query->execute()[0]["num"];
        $numOfPages = ceil($result / $this->global->getBlogPostsPerPage());

        return $numOfPages;
    }

    //id is used to exclude that value from the query. If we are checking the value that is already in the db than we have to exclude it from the query
    public function getUniqueFieldValue($entity, $field, $title, $id = null)
    {
        $new_title = $title;
        $isUnique = false;
        $pom = 1;

        $addition = "";

        if ($id) {
            $addition = " AND c.id <> " . $id;
        }

        while ($isUnique == false) {
            $query = $this->em->createQuery(
                "Select c.id " .
                "FROM " . $entity . " c " .
                "WHERE c." . $field . " = '" . $new_title . "'" . $addition
            )->setMaxResults($this->global->getBlogSliderPostsQuantity());
            $result = $query->execute();

            if ($result != null) {
                $new_title = $title . "-" . $pom;
            } else {
                $isUnique = true;
            }
        }
        return $new_title;
    }


}