<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 6/18/2020
 * Time: 8:43 AM
 */

namespace App\Services;


use App\Entity\GroundPlan;
use App\Entity\RealEstate;
use Doctrine\ORM\EntityManagerInterface;

class ServiceGroundPlan extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, GroundPlan::class);
    }

    public function getGroundPlansByRealEstate(RealEstate $realEstate)
    {
        $qb = $this->repository->createBasicQueryBuilder("gp");

        $qb->select("gp.name, gp.filePath, gp.mimeType")
            ->andWhere("gp.realEstate = :realEstate")
            ->setParameter("realEstate", $realEstate);

        $result = $qb->getQuery()->getResult();

        return $result;
    }
}