<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 3:00 PM
 */

namespace App\Services;


use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;

class ServiceLanguage extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, Language::class);
    }

    public function getByCode(string $code)
    {
        $qb = $this->repository->createBasicQueryBuilder("language");

        $qb->andWhere("language.code = :code")
            ->setParameter("code", $code);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }
}