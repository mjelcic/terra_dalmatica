<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/21/2020
 * Time: 2:33 PM
 */

namespace App\Services;


use App\Entity\SystemConfig;
use Doctrine\ORM\EntityManagerInterface;

class ServiceSystemConfig extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, SystemConfig::class);
    }

    public function getByCode(string $code)
    {
        $result = $this->repository->findBy(array(
            "code" => $code
        ));

        return $result;
    }
}