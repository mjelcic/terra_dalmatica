<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/26/2020
 * Time: 2:29 PM
 */

namespace App\Services;


use App\Entity\RealEstate;
use App\Entity\UserFavorite;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ServiceUserFavorite extends AbstractBaseService
{
    private $logger;
    function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        parent::__construct($entityManager, UserFavorite::class);
        $this->logger = $logger;
    }

    public function createAndSave(RealEstate $realEstate, UserInterface $user)
    {
        $userFavorite = new UserFavorite();
        $userFavorite
            ->setUser($user)
            ->setRealEstate($realEstate)
            ->setCreatedBy($user->getId());

        try{
            $this->entityManager->persist($userFavorite);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $ex){
            $this->logger->log("error", $ex->getMessage());
            return false;
        }
    }

    public function remove(UserFavorite $userFavorite, UserInterface $user)
    {
        $userFavorite
            ->setInvalidated(true)
            ->setUpdatedBy($user->getId())
            ->setUpdatedAt(new \DateTime());

        try{
            $this->entityManager->persist($userFavorite);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $ex){
            $this->logger->log("error", $ex->getMessage());
            return false;
        }
    }

    public function getByUserAndRealEstate(UserInterface $user, RealEstate $realEstate)
    {
        $qb = $this->repository->createBasicQueryBuilder("uf");

        $qb->select("uf")
            ->andWhere("uf.user = :user")
            ->andWhere("uf.real_estate = :realEstate")
            ->setParameter("user", $user)
            ->setParameter("realEstate", $realEstate);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getFavoriteRealEstatesByUserLocalized(UserInterface $user, $locale = "hr")
    {
        $qb = $this->repository->createBasicQueryBuilder("uf");

        $qb->select("uf.id, realEstate.id as realEstateId, realEstate.bedroomCount, realEstate.bathroomCount, realEstate.livingArea, realEstate.surfaceArea,
                            loco.title, loco.slug, reStatus.displayId as status, image.path as coverImagePath")
            ->leftJoin("uf.real_estate", "realEstate")
            ->leftJoin("realEstate.status", "reStatus")
            ->leftJoin("realEstate.realEstateLocalizations", "loco")
            ->leftJoin("loco.language", "language")
            ->leftJoin("realEstate.realEstateImages", "image", "WITH", "image.realEstate = realEstate AND image.cover = 1")
            ->andWhere("uf.user = :user")
            ->andWhere("language.code = :locale")
            ->orderBy("uf.created_at", "desc")
            ->setParameter("user", $user)
            ->setParameter("locale", $locale);

        $result = $qb->getQuery()->getResult();

        return $result;
    }
}