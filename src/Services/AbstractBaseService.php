<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/23/2020
 * Time: 3:33 PM
 */

namespace App\Services;


use App\Repository\AbstractBaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractBaseService
{
    protected $entityManager;
    /**
     * @var AbstractBaseRepository
     */
    protected $repository;

    function __construct(EntityManagerInterface $entityManager, string $fullClassName)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository($fullClassName);
    }

    public function getById(int $id)
    {
        $alias = "e";
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->repository->createBasicQueryBuilder($alias);
        $qb->andWhere($qb->expr()->eq($alias . ".id", $id));

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getBy(array $criteria)
    {
        $alias = "e";
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->repository->createBasicQueryBuilder($alias);

        foreach ($criteria as $key => $value)
        {
            $columnIdentificator = strpos($key, ".") ? $key : $alias . "." . $key;

            $qb->andWhere($qb->expr()->eq($columnIdentificator, $qb->expr()->literal($value)));
        }

        $results = $qb->getQuery()->getResult();
        return $results;
    }

    public function getAll()
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->repository->createBasicQueryBuilder("e");

        $results = $qb->getQuery()->getResult();

        return $results;
    }
}