<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 4/27/2018
 * Time: 3:13 PM
 */

namespace App\Services;


use App\GlobalSettings;
use Doctrine\ORM\EntityManagerInterface;

class CountService extends AbstractBaseService
{

    protected $em;
    protected $global;

    function __construct(EntityManagerInterface $em, GlobalSettings $global)
    {
        $this->em = $em;
        $this->global = $global;

    }

    public function countEntities($entity, $userId = null, array $conditions = [])
    {
        // If id is null then all entities are coutned, else only entities from current user are countes
        $dql = "Select count(a) as num  from " . $entity . " a WHERE a.invalidated = 0";
        if ($userId) {
            $dql .= " AND a.client = " . $userId;
        }
        foreach ($conditions as $key => $value) {
            $dql .=  " AND a." . $key . " = " . $value;
        }
        $query = $this->em->createQuery($dql);
        return $query->getResult()[0]['num'];
    }
}

