<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/24/2020
 * Time: 2:07 PM
 */

namespace App\Services;


use App\Entity\LocalLife;
use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;

class ServiceLocalLife extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, LocalLife::class);
    }

    public function getByLanguageAndLocation($locale, Location $location)
    {
        $qb = $this->repository->createBasicQueryBuilder("ll");

        $qb->select("ll")
            ->leftJoin("ll.location", "location")
            ->leftJoin("ll.language", "lang")
            ->andWhere("location = :location")
            ->andWhere("lang.code = :locale")
            ->setParameter("location", $location)
            ->setParameter("locale", $locale);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result == null ? $result : $result->getHtml();
    }
}