<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 4/27/2018
 * Time: 3:13 PM
 */

namespace App\Services;


use App\Entity\CmsPage;
use App\Entity\CmsPageLocalization;
use App\Entity\Enum\CmsPageTypeEnum;
use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\RealEstateCustomPage;
use App\Entity\Status;
use App\GlobalSettings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class SitemapService extends AbstractBaseService
{

    protected $em;
    protected $global;
    protected $urlGenerator;

    function __construct(EntityManagerInterface $em, GlobalSettings $global, UrlGeneratorInterface $urlGenerator)
    {
        $this->em = $em;
        $this->global = $global;
        $this->urlGenerator = $urlGenerator;
    }

    public function getStaticUrls($locale)
    {
        $urls = array();
        $urls[] = array('loc' => $this->urlGenerator->generate('index', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>1);
        $urls[] = array('loc' => $this->urlGenerator->generate('category_page_top', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        $urls[] = array('loc' => $this->urlGenerator->generate('category_page_top_houses', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        $urls[] = array('loc' => $this->urlGenerator->generate('category_page_top_apartments', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        $urls[] = array('loc' => $this->urlGenerator->generate('category_page_top_lands', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        $urls[] = array('loc' => $this->urlGenerator->generate('category_page_top_office_space', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        $urls[] = array('loc' => $this->urlGenerator->generate('blog', array('_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
        return $urls;
    }

    public function getPredefinedContentUrls($locale){
        $cmsPages = $this->em->getRepository(CmsPage::class)->findBy(array("page_type"=>CmsPageTypeEnum::PREDEFINED, "invalidated"=>0));
        $language = $this->em->getRepository(Language::class)->findOneBy(array("code"=>$locale));
        $urls = array();

        foreach ($cmsPages as $cmsPage){
            $cmsPageLoc = $this->em->getRepository(CmsPageLocalization::class)->findOneBy(array("cms_page"=>$cmsPage, "language"=> $language));
            if($cmsPageLoc){
                $urls[] = array('loc' => $this->urlGenerator->generate('predefined_content', array('slug'=>$cmsPageLoc->getSlug(),'_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
            }
        }

        return $urls;
    }

    public function getCategoryPagesUrls($locale){


        $serviceRealEstate = new ServiceRealEstate ($this->em);
        $categoryPages = $this->em->getRepository(RealEstateCustomPage::class)->findBy(array("parentRealEstateCustomPage"=>null, "invalidated" => 0, "type" => 1, "active" => 1));
        $urls = array();

        foreach ($categoryPages as $categoryPage){
                $urls[] = array('loc' => $this->urlGenerator->generate('category_page', array('slugCategory'=>$categoryPage->getRealEstateCustomPageLocalization($locale)->getSlug(), '_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
                $categoryPageRealEstatesCount = $serviceRealEstate->getCategoryPageRealEstatesCount($categoryPage, $locale);
                $maxPages = ceil($categoryPageRealEstatesCount / GlobalSettings::getCategoryPageRealEstatesPerPage());

                for($i=2;$i<=$maxPages;$i++){
                    $urls[] = array('loc' => $this->urlGenerator->generate('category_page', array('slugCategory'=>$categoryPage->getRealEstateCustomPageLocalization($locale)->getSlug(),'_locale'=>$locale)) . "?page=" . $i, 'changefreq'=>'daily', 'priority'=>0.9);
                }

                foreach ($categoryPage->getRealEstateCustomPages() as $child){

                    $categoryPageRealEstatesCount = $serviceRealEstate->getCategoryPageRealEstatesCount($child, $locale);
                    $maxPages = ceil($categoryPageRealEstatesCount / GlobalSettings::getCategoryPageRealEstatesPerPage());

                    if($child->getActive() == true){
                        $urls[] = array('loc' => $this->urlGenerator->generate('category_page', array('slugCategory'=>$categoryPage->getRealEstateCustomPageLocalization($locale)->getSlug(), 'slugSubCategory'=>$child->getRealEstateCustomPageLocalization($locale)->getSlug(),'_locale'=>$locale)), 'changefreq'=>'daily', 'priority'=>0.9);
                        for($i=2;$i<=$maxPages;$i++){
                            $urls[] = array('loc' => $this->urlGenerator->generate('category_page', array('slugCategory'=>$categoryPage->getRealEstateCustomPageLocalization($locale)->getSlug(), 'slugSubCategory'=>$child->getRealEstateCustomPageLocalization($locale)->getSlug(),'_locale'=>$locale)) . "?page=" . $i, 'changefreq'=>'daily', 'priority'=>0.9);
                        }
                    }
                }
        }
        return $urls;
    }

    public function getRealEstateUrls($locale){
        $publishedStatus = $this->em->getRepository(Status::class)->find(2);
        $realEstates = $this->em->getRepository(RealEstate::class)->findBy(array("invalidated" => 0, "status" => $publishedStatus));
        $urls = array();

        foreach ($realEstates as $realEstate){
            if($realEstate->getRealEstateLocalization($locale)) {
                $urls[] = array('loc' => $this->urlGenerator->generate('real_estate_details', array('slug' => $realEstate->getRealEstateLocalization($locale)->getSlug(),'_locale'=>$locale)), 'changefreq' => 'daily', 'priority' => 0.9);
            }

        }
        return $urls;
    }


}

