<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/8/2020
 * Time: 12:06 AM
 */

namespace App\Services;


use App\Entity\Notification;
use App\Entity\RealEstate;
use App\Entity\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ServiceUser extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, User::class);
    }

    public function getEmailsByRoles(array $roles)
    {
        $qb = $this->repository->createBasicQueryBuilder("user");

        $qb->select("user.email");

        $orX = $qb->expr()->orX();
        foreach ($roles as $role)
        {
            $orX->add("user.roles LIKE '%$role%'");
        }

        $qb->andWhere($orX);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function deactivateUserAccount(LoggerInterface $logger, int $id)
    {
        if(!$id){
            return false;
        }

        $user = $this->getById($id);
        if(!$user){
            return false;
        }

        //START TRANSACTION
        $this->entityManager->beginTransaction();

        try {
            //1) Get real estates owned by the user
            $userRealEstates = $this->entityManager->getRepository(RealEstate::class)->findBy(array("client" => $id));
            //2) Get and delete notifications related to the user's real estates or the user itself
            $qbNotifications = $this->entityManager->getRepository(Notification::class)
                ->createQueryBuilder("note")
                ->where("(note.object IN (:userRealEstates)) OR (note.for_user = :user) OR (note.triggeredBy = :user)")
                ->setParameter("userRealEstates", $userRealEstates)
                ->setParameter("user", $user);
            $userNotifications = $qbNotifications->getQuery()->getResult();
            foreach ($userNotifications as $notification){
                $this->entityManager->remove($notification);
            }
            //3 Delete real estates
            $filePaths = [];
            foreach ($userRealEstates as $userRealEstate){
                $this->entityManager->remove($userRealEstate);
                //Collect image paths
                $realEstateImages = $userRealEstate->getRealEstateImages();
                foreach ($realEstateImages as $realEstateImage){
                    $filePaths[] = $realEstateImage->getPath();
                }
                $groundPlans = $userRealEstate->getGroundPlans();
                foreach ($groundPlans as $groundPlan){
                    $filePaths[] = $groundPlan->getFilePath();
                }
                $documents = $userRealEstate->getDocuments();
                foreach ($documents as $document){
                    $filePaths[] = $document->getFilePath();
                }
            }
            //4) Delete the USER
            $this->entityManager->remove($user);

            //COMMIT
            $this->entityManager->flush();

            //If we managed to delete all from DB clean the images from server too
            foreach ($filePaths as $imagePath){
                $fullPath = \dirname(__FILE__, 3) . '/public' . $imagePath;
                if(file_exists($fullPath)){
                    unlink($fullPath);
                }
            }

            $this->entityManager->getConnection()->commit();
        }
        catch (\Exception $ex){
            $this->entityManager->rollback();
            $logger->error("USER ACCOUNT DEACTIVATION ERROR: " . $ex->getMessage());
            return false;
        }

        return true;
    }

    public function getForSelect2($query, $pageLimit = 10)
    {
        $qb = $this->repository->createBasicQueryBuilder("user");

        $qb->select("user.id, CONCAT(user.first_name, ' ', user.last_name) AS text ") //
            ->andWhere($qb->expr()->like("CONCAT(user.first_name, ' ', user.last_name)", $qb->expr()->literal("%" . $query . "%")))
            ->andWhere("user.roles LIKE '%ROLE_USER%'")
            ->andWhere("user.isActive = 1");

        $result = $qb->setMaxResults($pageLimit)->getQuery()->getResult();

        return $result;
    }

    public function getAllByRole(string $role, $active = true)
    {
        $qb = $this->repository->createBasicQueryBuilder("user");

        $qb->andWhere($qb->expr()->like("user.roles", $qb->expr()->literal("%" . $role . "%")))
            ->andWhere("user.isActive = :active")
            ->setParameter("active", $active);

        $result = $qb->getQuery()->getResult();

        return $result;
    }
}