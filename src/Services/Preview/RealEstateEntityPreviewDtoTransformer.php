<?php
declare(strict_types=1);

namespace App\Services\Preview;


use App\Entity\Feature;
use App\Entity\GroundPlan;
use App\Entity\Language;
use App\Entity\Location;
use App\Entity\RealEstate;
use App\Entity\RealEstateFeature;
use App\Entity\RealEstateImage;
use App\Entity\RealEstateLocalization;
use App\Entity\RealEstateMetaLocalization;
use App\Entity\RealEstateType;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class RealEstateEntityPreviewDtoTransformer
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function toDto(RealEstate $realEstate)
    {
        $dto = new RealEstatePreviewDto();
        $dto->id = $realEstate->getId();
        $dto->price = $realEstate->getPrice();
        $dto->priceOnRequest = $realEstate->getPriceOnRequest();
        $dto->fullPrice = $realEstate->getFullPrice();
        $dto->discountPrice = $realEstate->getDiscountPrice();
        $dto->pool = $realEstate->getPool();
        $dto->bathroomCount = $realEstate->getBathroomCount();
        $dto->bedroomCount = $realEstate->getBedroomCount();
        $dto->distanceFromTheSea = $realEstate->getDistanceFromTheSea();
        $dto->garage = $realEstate->getGarage();
        $dto->seaView = $realEstate->getSeaView();
        $dto->livingArea = $realEstate->getLivingArea();
        $dto->surfaceArea = $realEstate->getSurfaceArea();
        $dto->typeId = $realEstate->getRealEstateType()->getId();
        $dto->agentId = $realEstate->getAgent() ? $realEstate->getAgent()->getId() : null;
        $dto->clientId = $realEstate->getClient() ? $realEstate->getClient()->getId() : null;

        $dto->statusId = $realEstate->getStatus()->getId();
        $dto->locationId = $realEstate->getLocation()->getId();
        $dto->features = $realEstate->getRealEstateFeatures()
            ->filter(function(RealEstateFeature $f) { return $f->getInvalidated() == 0 && $f->getFeature()->getInvalidated() == 0;})
            ->map(function(RealEstateFeature $f) {return $f->getFeature()->getId();});
        $dto->images = $realEstate->getRealEstateImages()
            ->filter(function(RealEstateImage $i) { return $i->getInvalidated() == 0;})
            ->map(function(RealEstateImage $i) { return ["id" => $i->getId(), "cover" => $i->isCover()];});
        $dto->plans = $realEstate->getGroundPlans()
            ->filter(function(GroundPlan $g) { return $g->getInvalidated() == 0;})
            ->map(function(GroundPlan $i) { return ["id" => $i->getId()];});
        $dto->uid = $realEstate->getUid();
        $dto->videoUrl = $realEstate->getVideoUrl();
        $dto->virtualWalkUrl = $realEstate->getVirtualWalkUrl();
        $dto->realEstateLocalizations = $realEstate->getRealEstateLocalizations()->map(
            function(RealEstateLocalization $l) {
                $node = new RealEstateLocalizationNode();
                $node->languageId = $l->getLanguage()->getId();
                $node->locale = $l->getLanguage()->getCode();
                $node->description = $l->getDescription();
                $node->title = $l->getTitle();
                $node->slug = $l->getSlug();
                $node->synonyms = $l->getSynonyms();
                return $node;
            }
        );

        $dto->realEstateMetaLocalizations = $realEstate->getRealEstateMetaLocalizations()->map(
            function(RealEstateMetaLocalization $l) {
                $node = new RealEstateMetaLocalizationNode();

                $node->languageId = $l->getLanguage()->getId();
                $node->locale = $l->getLanguage()->getCode();
                $node->title = $l->getTitle();
                $node->description = $l->getDescription();

                return $node;
            }
        );

        $dto->address = $realEstate->getAddress();
        $dto->latitude = $realEstate->getLatitude();
        $dto->longitude = $realEstate->getLongitude();
        $dto->administrationNote = $realEstate->getAdministrationNote();
        $dto->postalCode = $realEstate->getPostalCode();
        $dto->keysInPossession = $realEstate->getKeysInPossession();
        $dto->lotDetails = $realEstate->getLotDetails();
        $dto->underConstruction = $realEstate->getUnderConstruction();

        return $dto;
    }

    public function realEstateFromDto(RealEstatePreviewDto $dto): RealEstate
    {
        if($dto->id != null) {
            $realEstate = $this->entityManager->getRepository(RealEstate::class)->find($dto->id);
        } else {
            $realEstate = new RealEstate(null);
        }

        $realEstate->setPrice($dto->price);
        $realEstate->setPriceOnRequest($dto->priceOnRequest);
        $realEstate->setFullPrice($dto->fullPrice);
        $realEstate->setDiscountPrice($dto->discountPrice);
        $realEstate->setPool($dto->pool);
        $realEstate->setFirstRowByTheSea($dto->firstRowByTheSea);
        $realEstate->setUnderConstruction($dto->underConstruction);
        $realEstate->setSeaView($dto->seaView);
        $realEstate->setGarage($dto->garage);
        $realEstate->setAddress($dto->address);
        $realEstate->setLongitude($dto->longitude);
        $realEstate->setLatitude($dto->latitude);
        $realEstate->setLivingArea($dto->livingArea);
        $realEstate->setSurfaceArea($dto->surfaceArea);
        $realEstate->setAdministrationNote($dto->administrationNote);
        $realEstate->setVideoUrl($dto->videoUrl);
        $realEstate->setVirtualWalkUrl($dto->virtualWalkUrl);
        $realEstate->setBathroomCount($dto->bathroomCount);
        $realEstate->setBedroomCount($dto->bedroomCount);
        $realEstate->setEnergyClass($dto->energyClass);
        $realEstate->setLotDetails($dto->lotDetails);
        $realEstate->setKeysInPossession($dto->keysInPossession);

        if($realEstate->getAgent() == null || $realEstate->getAgent()->getId() != $dto->agentId) {
            $agent = $this->entityManager->getRepository(User::class)->find($dto->agentId);
            $realEstate->setAgent($agent);
        }

        if($realEstate->getClient() == null || $realEstate->getClient()->getId() != $dto->clientId) {
            $client = $this->entityManager->getRepository(User::class)->find($dto->clientId);
            $realEstate->setClient($client);
        }

        if($realEstate->getRealEstateType() == null || $realEstate->getRealEstateType()->getId() != $dto->typeId) {
            $type = $this->entityManager->getRepository(RealEstateType::class)->find($dto->typeId);
            $realEstate->setRealEstateType($type);
        }

        if($realEstate->getLocation() == null || $realEstate->getLocation()->getId() != $dto->locationId) {
            $l = $this->entityManager->getRepository(Location::class)->find($dto->locationId);
            $realEstate->setLocation($l);
        }

        $features = $this->entityManager->getRepository(Feature::class)->findBy(["id" => $dto->features]);
        $realEstate->setFeatures($features);

        foreach ($dto->realEstateLocalizations as $localization) {
            $loco = $realEstate->getRealEstateLocalization($localization->locale);
            if($loco == null) {
                $loco = new RealEstateLocalization();
                $language = $this->entityManager->getRepository(Language::class)->find($localization->languageId);
                $loco->setLanguage($language);
                $loco->setRealEstate($realEstate);
            }
            $loco->setTitle($localization->title);
            $loco->setSlug($localization->slug);
            $loco->setDescription($localization->description);
            $loco->setSynonyms($localization->synonyms);

            $realEstate->addRealEstateLocalization($loco);
        }

        foreach ($dto->realEstateMetaLocalizations as $localization) {
            $loco = $realEstate->getRealEstateMetaLocalization($localization->locale);
            if($loco == null) {
                $loco = new RealEstateMetaLocalization();
                $language = $this->entityManager->getRepository(Language::class)->find($localization->languageId);
                $loco->setLanguage($language);
                $loco->setRealEstate($realEstate);
            }
            $loco->setTitle($localization->title);
            $loco->setDescription($localization->description);

            $realEstate->addRealEstateMetaLocalization($loco);
        }

        // handle images

        foreach ($realEstate->getRealEstateImages() as $image) {
            $image->setInvalidated(1);
            $image->setCover(false);
        }

        foreach ($dto->images as $dtoImage) {
            $image = $realEstate->getImageById($dtoImage["id"]);
            if($image == null) {
                $image = $this->entityManager->getRepository(RealEstateImage::class)->find($dtoImage["id"]);
                $image->setRealEstate($realEstate);
            }

            $image->setInvalidated(0);

            if($dtoImage["cover"]) {
                $image->setCover(true);
            }
        }

        foreach ($dto->plans as $dtoPlan) {
            $groundPlan = $realEstate->getGroudPlanById($dtoPlan["id"]);
            if($groundPlan == null) {
                $groundPlan = $this->entityManager->getRepository(GroundPlan::class)->find($dtoPlan["id"]);
                $groundPlan->setRealEstate($realEstate);
            }

            $groundPlan->setInvalidated(0);
        }

        return $realEstate;
    }

}
 