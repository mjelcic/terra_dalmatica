<?php
declare(strict_types=1);

namespace App\Services\Preview;


use App\Entity\Language;

class RealEstateMetaLocalizationNode
{
    public $languageId;

    public $locale;

    public $title;

    public $description;

}
 