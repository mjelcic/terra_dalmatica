<?php
declare(strict_types=1);

namespace App\Services\Preview;


use App\Entity\Language;

class RealEstateLocalizationNode
{
    public $languageId;

    public $title;

    public $description;

    public $slug;

    public $synonyms;

    public $locale;

}
 