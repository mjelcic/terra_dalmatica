<?php
declare(strict_types=1);

namespace App\Services\Preview;


class RealEstatePreviewDto
{
    public $id;

    public $price;

    public $fullPrice;

    public $discountPrice;

    public $title;
    
    public $livingArea;
    
    public $surfaceArea;
    
    public $images = [];

    public $plans = [];

    public $energyClass;

    public $locationId;

    public $typeId;

    public $statusId;

    public $bedroomCount;

    public $firstRowByTheSea;

    public $seaView;

    public $newBuilding;

    public $distanceFromTheSea;

    public $underConstruction;

    public $pool;

    public $bathroomCount;

    public $garage;

    public $videoUrl;

    public $virtualWalkUrl;

    public $features = [];

    /**
     * @var RealEstateLocalizationNode[]
     */
    public $realEstateLocalizations;
    /**
     * @var RealEstateMetaLocalizationNode[]
     */
    public $realEstateMetaLocalizations;

    public $agentId;

    public $clientId;

    public $priceOnRequest;

    public $uid;

    public $administrationNote;

    public $clientNote;

    public $postalCode;

    public $latitude;

    public $longitude;

    public $address;

    public $keysInPossession;

    public $lotDetails;
}
 