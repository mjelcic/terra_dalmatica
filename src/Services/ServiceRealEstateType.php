<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 10:08 AM
 */

namespace App\Services;


use App\Entity\RealEstateType;
use Doctrine\ORM\EntityManagerInterface;

class ServiceRealEstateType extends AbstractBaseService
{
    function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, RealEstateType::class);
    }

    public function getAllLocalized($locale)
    {
        $qb = $this->repository->createBasicQueryBuilder("ret");

        $qb->select("ret.id, loco.name as name")
            ->leftJoin("ret.realEstateTypeLocalizations", "loco")
            ->leftJoin("loco.language", "loco_lang")
            ->andWhere("loco_lang.code = :locale")
            ->setParameter("locale", $locale);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getForSelect2Localized($query, $pageLimit = 10, $locale)
    {
        $qb = $this->repository->createBasicQueryBuilder("ret");

        $qb->select("ret.id, loco.name as text")
            ->leftJoin("ret.realEstateTypeLocalizations", "loco")
            ->leftJoin("loco.language", "loco_lang")
            ->andWhere("loco_lang.code = :locale")
            ->setParameter("locale", $locale);

        if($query){
            $qb->andWhere($qb->expr()->like("loco.name", $qb->expr()->literal($query . "%")));
        }

        $result = $qb->setMaxResults($pageLimit)->getQuery()->getResult();

        return $result;
    }
}