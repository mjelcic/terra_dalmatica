<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 3:00 PM
 */

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Translation\TranslatorInterface;


class VisitService extends AbstractController
{
    protected $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getOnlineUsers()
    {
        $date = new \DateTime();
        $interval = new \DateInterval("PT1M");
        $interval->invert = 1;
        $date->add($interval);


        $query = $this->em->createQuery(
            "Select v
            FROM App\Entity\Visit v 
            WHERE v.visit_ended_at > :date"
        )->setParameter("date", $date);
        $result = $query->execute();
        return $result;
    }
}