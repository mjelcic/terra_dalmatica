<?php

namespace App;


class GlobalSettings
{
    const SearchbarPriceSlideMinValue = 0;
    const SearchbarPriceSlideMaxValue = 500001;
    const LatLngOffset = 0.002;
    const FeaturedRealEstatesForDisplayCount = 15;
    const MaxFeaturedRealEstatesCount = 15;

    public static function getFromMail()
    {
        return "terradal@terradalmatica.hr";
    }

    public static function getBaseUrl()
    {
        return "https://terradalmatica.hr";
    }

    public static function getDefaultLocale()
    {
        return "hr";
    }

    public static function getImageMaxDimensions()
    {
        return array(
            "width" => 1280,
            "height" => 720
        );
    }

    public static function getDefaultMetaData()
    {
        return array(
            "description" => "Prodaja Nekretnina uz Veliki izbor i Super Cijene! Pregledajte našu ponudu uz detaljne opise i slike nekretnina!",
            "title" => "Terra Dalmatica"
        );
    }

    public static function getBlogSliderPostsQuantity()
    {
        return 4;
    }

    public static function getBlogPostsPerPage()
    {
        return 4;
    }

    public static function getSummernoteImagePath()
    {
        return "uploads/img/content/";
    }

    public static function getRealEstateImagesPerRealEstate()
    {
        return 20;
    }

    public static function getRealEstateImagesBasePath()
    {
        return "uploads/img/real_estate/";
    }

    public static function getRealEstateGroundPlanPath()
    {
        return "uploads/docs/real_estate/plans/";
    }

    public static function getDocumentsPath()
    {
        return "uploads/docs/real_estate/other/";
    }

    public static function getBillOfSalesDocumentsPath()
    {
        return "uploads/docs/real_estate/bos/";
    }

    public static function getBillOfSalesConfidentialDocumentsPath()
    {
        return "uploads/docs/real_estate/bos_confidential/";
    }

    public static function getSummernoteImageUrl()
    {
        return "/uploads/img/content/";
    }

    public static function getZooplaCertFolder()
    {
        return "/home/terradal/opt/cert/zoopla/";
    }

    public static function getCategoryPageRealEstatesPerPage()
    {
        return 9;
    }

    public static function getRandomRealEstatesCrossSaleCount()
    {
        return 15;
    }

    public static function getCrossSellByRealEstateCount()
    {
        return 15;
    }

    public static function getExchangeRateSourceUrl()
    {
        return "http://api.hnb.hr/tecajn/v1?valuta=EUR";
    }

    public static function getRealEstatesSearchResultsPerPage()
    {
        return 9;
    }

    public static function getCrossSellPriceOffset()
    {
        return 100000; //100 thousand
    }

    public static function getFiltersByImportance()
    {
        return array(
            "text" => null,
            "realEstateType" => null,
            "location" => null,
            "priceMin" => null,
            "priceMax" => null,
            "bedroomCount" => null,
            "firstRowByTheSea" => null,
            "seaView" => null,
            "newBuilding" => null,
            "underConstruction" => null,
            "priceLowered" => null,
            "pool" => null,
            "sort" => null

        );
    }

    public static function getFacebookApiPostUrl()
    {
        return "https://graph.facebook.com/241143559317/feed";
    }

    public static function getFacebookApiUpdateUrl($post_id)
    {
        return "https://graph.facebook.com/" . $post_id;
    }

    public static function getFacebookAccessToken()
    {
        return "EAADfwJxdEaIBAO8KL2kggwMHRmN3YiejBKw2VZBDQB1JJvc8AAGmT6KSCp0QH7DSQXirS18IMG35jnZC5a5sH5wFHi49v29r7PZA459TcPPzCwKJ0EZCmZAuZBhZAwsfIrQ7BsUpCCspKZAgHMGnnCF8XxOFYmOB61lRbhtEayI1xwZDZD";
    }

    public static function getTwitterApiUrl()
    {
        return "https://api.twitter.com/1.1/statuses/update.json";
    }

    public static function getTwitterAuth()
    {
        return array(
            "oauth_access_token" => "85772502-MwRzS9vY3h7ZB0kVPTGfVRXwnjOIR6OmzwHVt8xvY",
            "oauth_access_token_secret" => "LP7qwWQ4gXJ0HC7dYkjq12EuEV9kp5xkF4eQE3SfeRMYi",
            "consumer_key" => "Vzj83ttMZMcNHKOULgoVLUG0o",
            "consumer_secret" => "enblTb3r51Vsom7x43WKkdGNhTFwHFzMMoNkEmhLhnvH8J0Ouc"

        );
    }

    public static function publishToSocial(){
        return false;
    }


}
