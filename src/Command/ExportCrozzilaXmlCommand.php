<?php


namespace App\Command;

use App\Services\Export\CrozzilaService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ExportCrozzilaXmlCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-crozzila-xml';

    private $service;
    private $fs;

    function __construct(CrozzilaService $crozzilaService, Filesystem $filesystem)
    {
        parent::__construct();
        $this->service = $crozzilaService;
        $this->fs = $filesystem;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $xml_options = array(
                "xml_root_node_name" => "properties",
                "remove_empty_tags" => true,
                "xml_encoding" => "utf-8"
            );
            $encoders = [new XmlEncoder($xml_options), new JsonEncoder()];
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
            $normalizers = [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)];


            $serializer = new Serializer($normalizers, $encoders);

            $realEstates = $this->service->getRealEstates();

            $xml = $serializer->serialize(array("property" => $realEstates), 'xml');

            $this
                ->setDescription('Created');

            $this->fs->dumpFile('new.terradalmatica.hr/public/uploads/export/crozzila.xml', $xml);


        } catch (\Exception $ex) {
            $output->writeln(sprintf("Greška prilikom kreiranja xml-a za crozzilu!: %s", $ex->getMessage()));
        }
        return 1;
    }


}