<?php
declare(strict_types=1);

namespace App\Command;


use App\Services\Export\ZooplaService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportNewRealEstatesToZoopla extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:register_new_zoopla_listings';

    private $service;
    private $fs;

    function __construct(ZooplaService $zooplaService)
    {
        parent::__construct();
        $this->service = $zooplaService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->service->registerRealEstatesToZoopla();

        } catch (\Exception $ex) {
            $output->writeln(sprintf("Greška prilikom exporta na zooplu!: %s", $ex->getMessage()));
        }
        return 1;
    }
}
 