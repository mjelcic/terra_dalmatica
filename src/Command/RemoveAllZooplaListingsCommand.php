<?php
declare(strict_types=1);

namespace App\Command;


use App\Services\Export\ZooplaService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// this command should run only once, and it should remove listings that do not match new criteria defined by client
// criteria: - house with pool
//           - house first row by sea
//           - appartments over 200.000 eur
class RemoveAllZooplaListingsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:clear_zoopla_listings';

    private $service;

    function __construct(ZooplaService $zooplaService)
    {
        parent::__construct();
        $this->service = $zooplaService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->service->deleteAllListings();

        } catch (\Exception $ex) {
            $output->writeln(sprintf("Greška prilikom uklanjanja sa zoople!: %s", $ex->getMessage()));
        }
        return 1;
    }

}
 