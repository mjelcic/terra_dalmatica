<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/8/2020
 * Time: 2:59 PM
 */

namespace App\Command;


use App\Services\Export\ServiceDataExport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ExportRealEstateCroatiaXmlCommand extends Command
{
// the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-rec-xml';

    private $serviceDataExport;
    private $fs;

    function __construct(ServiceDataExport $serviceDataExport, Filesystem $filesystem)
    {
        parent::__construct();
        $this->serviceDataExport = $serviceDataExport;
        $this->fs = $filesystem;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $xml_options = array(
                "xml_root_node_name" => "properties",
                "remove_empty_tags" => true,
                "xml_encoding" => "utf-8"
            );
            $encoders = [new XmlEncoder($xml_options), new JsonEncoder()];
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter), new ArrayDenormalizer()];

            $serializer = new Serializer($normalizers, $encoders);

            $realEstates = $this->serviceDataExport->getDataForRealEstateCroatia();

            $xml = $serializer->serialize(array("property" => $realEstates), 'xml', [AbstractNormalizer::IGNORED_ATTRIBUTES => ["id"]]);

            $this->fs->dumpFile('new.terradalmatica.hr/public/uploads/export/real-estate-croatia.xml', $xml);

        } catch (\Exception $ex) {
            $output->writeln(sprintf("Greška prilikom kreiranja xml-a za Real estate Croatia!: %s", $ex->getMessage()));
        }
        return 1;
    }
}