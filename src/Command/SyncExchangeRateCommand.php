<?php


namespace App\Command;


use App\Entity\ExchangeRate;
use App\GlobalSettings;
use App\Services\ServiceExchangeRate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class SyncExchangeRateCommand extends Command
{
    // trd as terradalmatica; let's say it's a namespace for our commands
    protected static $defaultName = 'trd:sync-exchange-rate';

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ServiceExchangeRate
     */
    private $service;

    function __construct(EntityManagerInterface $em, ServiceExchangeRate $serviceExchangeRate)
    {
        $this->service = $serviceExchangeRate;
        $this->em = $em;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $url = GlobalSettings::getExchangeRateSourceUrl();

            $httpClient = HttpClient::create();
            $response = $httpClient->request("GET", $url);

            if ($response->getStatusCode() == Response::HTTP_OK) {
                $content = $response->getContent();

                $jsonArray = json_decode($content, true);

                if (is_array($jsonArray) && count($jsonArray)) {
                    $dateValid = new \DateTime($jsonArray[0]["Datum primjene"], new \DateTimeZone("Europe/Zagreb"));
                    $exchangeRate = new ExchangeRate();

                    $exchangeRate
                        ->setCode($jsonArray[0]["Valuta"])
                        ->setRate(floatval(str_replace(",", ".", $jsonArray[0]["Srednji za devize"])))
                        ->setUnits((int)$jsonArray[0]["Jedinica"])
                        ->setDateValid($dateValid)
                        ->setCreatedBy(0);

                    $this->em->persist($exchangeRate);

                    $this->em->flush();

                }
            }
        } catch (\Exception $ex) {
            $output->writeln(sprintf("Greška prilikom preuzimanja kursne liste!: %s", $ex->getMessage()));
        }
        return 1;
    }


}