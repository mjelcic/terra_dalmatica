<?php
// src/Validator/Constraints/ContainsAlphanumeric.php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AllowedPassword extends Constraint
{
    public $message = 'Password must contain at least one uppercase letter, one lowercase letter and one number.';
}