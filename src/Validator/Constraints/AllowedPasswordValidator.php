<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AllowedPasswordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
		$containsUpperCase = false;
		$containsLowerCase = false;
		$containsNumber = false;
		
		// custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
		if (null === $value || '' === $value) {
            return;
        }
		
		// custom constraints should ignore less then 8 characters
        // other constraints take care of that
		if (strlen($value)<8) {
            return;
        }
		
		if(preg_match('/[A-Z]/', $value)){
		 $containsUpperCase = true;
		}
		
		if(preg_match('/[a-z]/', $value)){
		 $containsLowerCase = true;
		}
		
		if(preg_match('/[0-9]/', $value)){
		 $containsNumber = true;
		}
		
		if ((!$containsUpperCase)||(!$containsLowerCase)||(!$containsNumber)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}