<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200114115155 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cms_page (id INT AUTO_INCREMENT NOT NULL, display_id VARCHAR(100) NOT NULL, page_type INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cms_page_localization (id INT AUTO_INCREMENT NOT NULL, cms_page_id INT NOT NULL, language_id INT NOT NULL, slug VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_9680B4CA52AA6CF5 (cms_page_id), INDEX IDX_9680B4CA82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, real_estate_id INT NOT NULL, file_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, document_type INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_D8698A761E4EB97C (real_estate_id), INDEX IDX_D8698A7693CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feature (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feature_localization (id INT AUTO_INCREMENT NOT NULL, feature_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_20B929D460E4B879 (feature_id), INDEX IDX_20B929D482F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, code VARCHAR(2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, region_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_5E9E89CB98260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_localization (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_E97BCF2964D218E (location_id), INDEX IDX_E97BCF2982F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, real_estate_id INT NOT NULL, file_id INT NOT NULL, name VARCHAR(50) NOT NULL, main TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_14B784181E4EB97C (real_estate_id), INDEX IDX_14B7841893CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_meta_localization (id INT AUTO_INCREMENT NOT NULL, photo_id INT NOT NULL, language_id INT NOT NULL, alt_tag VARCHAR(200) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_197C3D517E9E4C8C (photo_id), INDEX IDX_197C3D5182F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, real_estate_type_id INT NOT NULL, status_id INT NOT NULL, bedroom_count INT DEFAULT NULL, first_row_by_the_sea TINYINT(1) NOT NULL, sea_view TINYINT(1) NOT NULL, new_building TINYINT(1) DEFAULT NULL, distance_from_the_sea INT NOT NULL, under_construction TINYINT(1) DEFAULT NULL, pool TINYINT(1) DEFAULT NULL, bathroom_count INT DEFAULT NULL, garage TINYINT(1) DEFAULT NULL, price NUMERIC(10, 2) DEFAULT NULL, rent_price NUMERIC(10, 2) DEFAULT NULL, living_area INT NOT NULL, surface_area INT NOT NULL, video_url VARCHAR(100) DEFAULT NULL, longitude VARCHAR(50) DEFAULT NULL, latitude VARCHAR(50) DEFAULT NULL, energy_class VARCHAR(10) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_DE4E97A864D218E (location_id), INDEX IDX_DE4E97A8E1547341 (real_estate_type_id), INDEX IDX_DE4E97A86BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_custom_page (id INT AUTO_INCREMENT NOT NULL, parent_real_estate_custom_page_id INT DEFAULT NULL, query VARCHAR(255) NOT NULL, photo VARCHAR(200) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_99A147AA5DC34FC6 (parent_real_estate_custom_page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_custom_page_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_custom_page_id INT NOT NULL, language_id INT NOT NULL, title VARCHAR(200) NOT NULL, slug VARCHAR(200) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_A27A6DFEAC4757A3 (real_estate_custom_page_id), INDEX IDX_A27A6DFE82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_feature (id INT AUTO_INCREMENT NOT NULL, real_estate_id INT NOT NULL, feature_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_3BCF8AB41E4EB97C (real_estate_id), INDEX IDX_3BCF8AB460E4B879 (feature_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_id INT NOT NULL, language_id INT NOT NULL, title VARCHAR(200) NOT NULL, description LONGTEXT DEFAULT NULL, slug VARCHAR(200) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_5631A6E11E4EB97C (real_estate_id), INDEX IDX_5631A6E182F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_location_page (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, photo VARCHAR(200) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_AB75E88464D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_location_page_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_location_page_id INT NOT NULL, language_id INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(2000) DEFAULT NULL, slug VARCHAR(255) NOT NULL, photo_alt_tag VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_3200C8EE36B58A90 (real_estate_location_page_id), INDEX IDX_3200C8EE82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_location_type_page (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, real_estate_type_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_5180AFF564D218E (location_id), INDEX IDX_5180AFF5E1547341 (real_estate_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_location_type_page_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_location_type_page_id INT NOT NULL, language_id INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_EE63E4EE1169575C (real_estate_location_type_page_id), INDEX IDX_EE63E4EE82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_meta_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_id INT NOT NULL, language_id INT NOT NULL, title VARCHAR(200) DEFAULT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_3DAA5BE21E4EB97C (real_estate_id), INDEX IDX_3DAA5BE282F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_type (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_type_localization (id INT AUTO_INCREMENT NOT NULL, real_estate_type_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_9998416CE1547341 (real_estate_type_id), INDEX IDX_9998416C82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region_localization (id INT AUTO_INCREMENT NOT NULL, region_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_FE0D88B898260155 (region_id), INDEX IDX_FE0D88B882F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status_localization (id INT AUTO_INCREMENT NOT NULL, status_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_526287B86BF700BD (status_id), INDEX IDX_526287B882F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(50) NOT NULL, is_active TINYINT(1) NOT NULL, roles VARCHAR(100) NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, phone_no VARCHAR(20) NOT NULL, personal_identification_number VARCHAR(11) DEFAULT NULL, company_name VARCHAR(100) DEFAULT NULL, activation_code VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_search (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, search_string LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, INDEX IDX_D1A2C09AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cms_page_localization ADD CONSTRAINT FK_9680B4CA52AA6CF5 FOREIGN KEY (cms_page_id) REFERENCES cms_page (id)');
        $this->addSql('ALTER TABLE cms_page_localization ADD CONSTRAINT FK_9680B4CA82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A761E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7693CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE feature_localization ADD CONSTRAINT FK_20B929D460E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id)');
        $this->addSql('ALTER TABLE feature_localization ADD CONSTRAINT FK_20B929D482F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE location_localization ADD CONSTRAINT FK_E97BCF2964D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE location_localization ADD CONSTRAINT FK_E97BCF2982F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B784181E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B7841893CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE photo_meta_localization ADD CONSTRAINT FK_197C3D517E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id)');
        $this->addSql('ALTER TABLE photo_meta_localization ADD CONSTRAINT FK_197C3D5182F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate ADD CONSTRAINT FK_DE4E97A864D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE real_estate ADD CONSTRAINT FK_DE4E97A8E1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id)');
        $this->addSql('ALTER TABLE real_estate ADD CONSTRAINT FK_DE4E97A86BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE real_estate_custom_page ADD CONSTRAINT FK_99A147AA5DC34FC6 FOREIGN KEY (parent_real_estate_custom_page_id) REFERENCES real_estate_custom_page (id)');
        $this->addSql('ALTER TABLE real_estate_custom_page_localization ADD CONSTRAINT FK_A27A6DFEAC4757A3 FOREIGN KEY (real_estate_custom_page_id) REFERENCES real_estate_custom_page (id)');
        $this->addSql('ALTER TABLE real_estate_custom_page_localization ADD CONSTRAINT FK_A27A6DFE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_feature ADD CONSTRAINT FK_3BCF8AB41E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_feature ADD CONSTRAINT FK_3BCF8AB460E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id)');
        $this->addSql('ALTER TABLE real_estate_localization ADD CONSTRAINT FK_5631A6E11E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_localization ADD CONSTRAINT FK_5631A6E182F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_location_page ADD CONSTRAINT FK_AB75E88464D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE real_estate_location_page_localization ADD CONSTRAINT FK_3200C8EE36B58A90 FOREIGN KEY (real_estate_location_page_id) REFERENCES real_estate_location_page (id)');
        $this->addSql('ALTER TABLE real_estate_location_page_localization ADD CONSTRAINT FK_3200C8EE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page ADD CONSTRAINT FK_5180AFF564D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page ADD CONSTRAINT FK_5180AFF5E1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization ADD CONSTRAINT FK_EE63E4EE1169575C FOREIGN KEY (real_estate_location_type_page_id) REFERENCES real_estate_location_type_page (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization ADD CONSTRAINT FK_EE63E4EE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_meta_localization ADD CONSTRAINT FK_3DAA5BE21E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_meta_localization ADD CONSTRAINT FK_3DAA5BE282F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_type_localization ADD CONSTRAINT FK_9998416CE1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id)');
        $this->addSql('ALTER TABLE real_estate_type_localization ADD CONSTRAINT FK_9998416C82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE region_localization ADD CONSTRAINT FK_FE0D88B898260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE region_localization ADD CONSTRAINT FK_FE0D88B882F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE status_localization ADD CONSTRAINT FK_526287B86BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE status_localization ADD CONSTRAINT FK_526287B882F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE user_search ADD CONSTRAINT FK_D1A2C09AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_page_localization DROP FOREIGN KEY FK_9680B4CA52AA6CF5');
        $this->addSql('ALTER TABLE feature_localization DROP FOREIGN KEY FK_20B929D460E4B879');
        $this->addSql('ALTER TABLE real_estate_feature DROP FOREIGN KEY FK_3BCF8AB460E4B879');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7693CB796C');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B7841893CB796C');
        $this->addSql('ALTER TABLE cms_page_localization DROP FOREIGN KEY FK_9680B4CA82F1BAF4');
        $this->addSql('ALTER TABLE feature_localization DROP FOREIGN KEY FK_20B929D482F1BAF4');
        $this->addSql('ALTER TABLE location_localization DROP FOREIGN KEY FK_E97BCF2982F1BAF4');
        $this->addSql('ALTER TABLE photo_meta_localization DROP FOREIGN KEY FK_197C3D5182F1BAF4');
        $this->addSql('ALTER TABLE real_estate_custom_page_localization DROP FOREIGN KEY FK_A27A6DFE82F1BAF4');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E182F1BAF4');
        $this->addSql('ALTER TABLE real_estate_location_page_localization DROP FOREIGN KEY FK_3200C8EE82F1BAF4');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization DROP FOREIGN KEY FK_EE63E4EE82F1BAF4');
        $this->addSql('ALTER TABLE real_estate_meta_localization DROP FOREIGN KEY FK_3DAA5BE282F1BAF4');
        $this->addSql('ALTER TABLE real_estate_type_localization DROP FOREIGN KEY FK_9998416C82F1BAF4');
        $this->addSql('ALTER TABLE region_localization DROP FOREIGN KEY FK_FE0D88B882F1BAF4');
        $this->addSql('ALTER TABLE status_localization DROP FOREIGN KEY FK_526287B882F1BAF4');
        $this->addSql('ALTER TABLE location_localization DROP FOREIGN KEY FK_E97BCF2964D218E');
        $this->addSql('ALTER TABLE real_estate DROP FOREIGN KEY FK_DE4E97A864D218E');
        $this->addSql('ALTER TABLE real_estate_location_page DROP FOREIGN KEY FK_AB75E88464D218E');
        $this->addSql('ALTER TABLE real_estate_location_type_page DROP FOREIGN KEY FK_5180AFF564D218E');
        $this->addSql('ALTER TABLE photo_meta_localization DROP FOREIGN KEY FK_197C3D517E9E4C8C');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A761E4EB97C');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B784181E4EB97C');
        $this->addSql('ALTER TABLE real_estate_feature DROP FOREIGN KEY FK_3BCF8AB41E4EB97C');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E11E4EB97C');
        $this->addSql('ALTER TABLE real_estate_meta_localization DROP FOREIGN KEY FK_3DAA5BE21E4EB97C');
        $this->addSql('ALTER TABLE real_estate_custom_page DROP FOREIGN KEY FK_99A147AA5DC34FC6');
        $this->addSql('ALTER TABLE real_estate_custom_page_localization DROP FOREIGN KEY FK_A27A6DFEAC4757A3');
        $this->addSql('ALTER TABLE real_estate_location_page_localization DROP FOREIGN KEY FK_3200C8EE36B58A90');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization DROP FOREIGN KEY FK_EE63E4EE1169575C');
        $this->addSql('ALTER TABLE real_estate DROP FOREIGN KEY FK_DE4E97A8E1547341');
        $this->addSql('ALTER TABLE real_estate_location_type_page DROP FOREIGN KEY FK_5180AFF5E1547341');
        $this->addSql('ALTER TABLE real_estate_type_localization DROP FOREIGN KEY FK_9998416CE1547341');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB98260155');
        $this->addSql('ALTER TABLE region_localization DROP FOREIGN KEY FK_FE0D88B898260155');
        $this->addSql('ALTER TABLE real_estate DROP FOREIGN KEY FK_DE4E97A86BF700BD');
        $this->addSql('ALTER TABLE status_localization DROP FOREIGN KEY FK_526287B86BF700BD');
        $this->addSql('ALTER TABLE user_search DROP FOREIGN KEY FK_D1A2C09AA76ED395');
        $this->addSql('DROP TABLE cms_page');
        $this->addSql('DROP TABLE cms_page_localization');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE feature');
        $this->addSql('DROP TABLE feature_localization');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE location_localization');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE photo_meta_localization');
        $this->addSql('DROP TABLE real_estate');
        $this->addSql('DROP TABLE real_estate_custom_page');
        $this->addSql('DROP TABLE real_estate_custom_page_localization');
        $this->addSql('DROP TABLE real_estate_feature');
        $this->addSql('DROP TABLE real_estate_localization');
        $this->addSql('DROP TABLE real_estate_location_page');
        $this->addSql('DROP TABLE real_estate_location_page_localization');
        $this->addSql('DROP TABLE real_estate_location_type_page');
        $this->addSql('DROP TABLE real_estate_location_type_page_localization');
        $this->addSql('DROP TABLE real_estate_meta_localization');
        $this->addSql('DROP TABLE real_estate_type');
        $this->addSql('DROP TABLE real_estate_type_localization');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE region_localization');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE status_localization');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_search');
    }
}
