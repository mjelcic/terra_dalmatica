<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200525124726 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE file');
        $this->addSql('ALTER TABLE real_estate_feature ADD CONSTRAINT FK_3BCF8AB41E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_feature ADD CONSTRAINT FK_3BCF8AB460E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id)');
        $this->addSql('ALTER TABLE real_estate_image ADD CONSTRAINT FK_25B0B7741E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization ADD CONSTRAINT FK_83FE44555C8E6A FOREIGN KEY (real_estate_image_id) REFERENCES real_estate_image (id)');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization ADD CONSTRAINT FK_83FE4482F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_location_page_localization ADD CONSTRAINT FK_3200C8EE36B58A90 FOREIGN KEY (real_estate_location_page_id) REFERENCES real_estate_location_page (id)');
        $this->addSql('ALTER TABLE real_estate_location_page_localization ADD CONSTRAINT FK_3200C8EE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page ADD CONSTRAINT FK_5180AFF564D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page ADD CONSTRAINT FK_5180AFF5E1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization ADD CONSTRAINT FK_EE63E4EE1169575C FOREIGN KEY (real_estate_location_type_page_id) REFERENCES real_estate_location_type_page (id)');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization ADD CONSTRAINT FK_EE63E4EE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_meta_localization ADD CONSTRAINT FK_3DAA5BE21E4EB97C FOREIGN KEY (real_estate_id) REFERENCES real_estate (id)');
        $this->addSql('ALTER TABLE real_estate_meta_localization ADD CONSTRAINT FK_3DAA5BE282F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE real_estate_type_localization ADD CONSTRAINT FK_9998416CE1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id)');
        $this->addSql('ALTER TABLE real_estate_type_localization ADD CONSTRAINT FK_9998416C82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE region_localization ADD CONSTRAINT FK_FE0D88B898260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE region_localization ADD CONSTRAINT FK_FE0D88B882F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE status_localization ADD CONSTRAINT FK_526287B86BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE status_localization ADD CONSTRAINT FK_526287B882F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE user_preference ADD CONSTRAINT FK_FA0E76BFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_preference_real_estate_type ADD CONSTRAINT FK_E2160099369E8F90 FOREIGN KEY (user_preference_id) REFERENCES user_preference (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_preference_real_estate_type ADD CONSTRAINT FK_E2160099E1547341 FOREIGN KEY (real_estate_type_id) REFERENCES real_estate_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_preference_location ADD CONSTRAINT FK_698A1AD2369E8F90 FOREIGN KEY (user_preference_id) REFERENCES user_preference (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_preference_location ADD CONSTRAINT FK_698A1AD264D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_search ADD CONSTRAINT FK_D1A2C09AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE real_estate_feature DROP FOREIGN KEY FK_3BCF8AB41E4EB97C');
        $this->addSql('ALTER TABLE real_estate_feature DROP FOREIGN KEY FK_3BCF8AB460E4B879');
        $this->addSql('ALTER TABLE real_estate_image DROP FOREIGN KEY FK_25B0B7741E4EB97C');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization DROP FOREIGN KEY FK_83FE44555C8E6A');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization DROP FOREIGN KEY FK_83FE4482F1BAF4');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E11E4EB97C');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E182F1BAF4');
        $this->addSql('ALTER TABLE real_estate_location_page_localization DROP FOREIGN KEY FK_3200C8EE36B58A90');
        $this->addSql('ALTER TABLE real_estate_location_page_localization DROP FOREIGN KEY FK_3200C8EE82F1BAF4');
        $this->addSql('ALTER TABLE real_estate_location_type_page DROP FOREIGN KEY FK_5180AFF564D218E');
        $this->addSql('ALTER TABLE real_estate_location_type_page DROP FOREIGN KEY FK_5180AFF5E1547341');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization DROP FOREIGN KEY FK_EE63E4EE1169575C');
        $this->addSql('ALTER TABLE real_estate_location_type_page_localization DROP FOREIGN KEY FK_EE63E4EE82F1BAF4');
        $this->addSql('ALTER TABLE real_estate_meta_localization DROP FOREIGN KEY FK_3DAA5BE21E4EB97C');
        $this->addSql('ALTER TABLE real_estate_meta_localization DROP FOREIGN KEY FK_3DAA5BE282F1BAF4');
        $this->addSql('ALTER TABLE real_estate_type_localization DROP FOREIGN KEY FK_9998416CE1547341');
        $this->addSql('ALTER TABLE real_estate_type_localization DROP FOREIGN KEY FK_9998416C82F1BAF4');
        $this->addSql('ALTER TABLE region_localization DROP FOREIGN KEY FK_FE0D88B898260155');
        $this->addSql('ALTER TABLE region_localization DROP FOREIGN KEY FK_FE0D88B882F1BAF4');
        $this->addSql('ALTER TABLE status_localization DROP FOREIGN KEY FK_526287B86BF700BD');
        $this->addSql('ALTER TABLE status_localization DROP FOREIGN KEY FK_526287B882F1BAF4');
        $this->addSql('ALTER TABLE user_preference DROP FOREIGN KEY FK_FA0E76BFA76ED395');
        $this->addSql('ALTER TABLE user_preference_location DROP FOREIGN KEY FK_698A1AD2369E8F90');
        $this->addSql('ALTER TABLE user_preference_location DROP FOREIGN KEY FK_698A1AD264D218E');
        $this->addSql('ALTER TABLE user_preference_real_estate_type DROP FOREIGN KEY FK_E2160099369E8F90');
        $this->addSql('ALTER TABLE user_preference_real_estate_type DROP FOREIGN KEY FK_E2160099E1547341');
        $this->addSql('ALTER TABLE user_search DROP FOREIGN KEY FK_D1A2C09AA76ED395');
    }
}
