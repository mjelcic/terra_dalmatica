<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200803064707 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE file');
        $this->addSql('ALTER TABLE call_button ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD created_by INT NOT NULL, ADD updated_by INT DEFAULT NULL, ADD invalidated TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, invalidated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE call_button DROP created_at, DROP updated_at, DROP created_by, DROP updated_by, DROP invalidated');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization DROP FOREIGN KEY FK_83FE44555C8E6A');
        $this->addSql('ALTER TABLE real_estate_image_meta_localization DROP FOREIGN KEY FK_83FE4482F1BAF4');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E11E4EB97C');
        $this->addSql('ALTER TABLE real_estate_localization DROP FOREIGN KEY FK_5631A6E182F1BAF4');
    }
}
