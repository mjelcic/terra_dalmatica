<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/19/2018
 * Time: 8:21 AM
 */

namespace App\Form\EventSubscribers;


use App\GlobalSettings;
use Gumlet\ImageResize;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageResizeSubscriber implements EventSubscriberInterface
{
    private $image_properties_names;
    private $logger;

    function __construct($image_properties_names = array(), LoggerInterface $logger)
    {
        $this->image_properties_names = $image_properties_names;
        $this->logger = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT => "onPreSubmit"
        );
    }

    public function onPreSubmit(FormEvent $event)
    {
        $request_data = $event->getData();

        foreach ($this->image_properties_names as $prop_desc)
        {
            //check if it'a complex property
            $image_prop = null;
            if($prop_desc->complex)
            {
                $properties = explode(",", $prop_desc->name);

                for ($i = 0; $i < count($properties); $i++)
                {
                    if($i == 0){
                        $image_prop = $request_data[$properties[$i]];
                    } else {
                        if(array_key_exists($properties[$i], $image_prop)){
                            $image_prop = $image_prop[$properties[$i]];
                        } else {
                            $image_prop = null;
                            break;
                        }
                    }
                }
            }
            else {
                $image_prop = $request_data[$prop_desc->name];
            }

            if($image_prop)
            {
                if($prop_desc->type == "single")
                {
                    $this->resize($image_prop);
                }
                elseif($prop_desc->type == "multiple")
                {
                    foreach ($image_prop as $image)
                    {
                        $file = $image['file'];
                        $this->resize($file);
                    }
                }
            }
        }
    }

    private function resize($file)
    {
        if($file instanceof UploadedFile)
        {
            try
            {
                $name = $file->getRealPath();
                $original_img = new ImageResize($name);

                //$prescribed_width = GlobalSettings::getImageMaxDimensions()["width"];
                $prescribed_height = GlobalSettings::getImageMaxDimensions()["height"];
                //$original_img->resizeToBestFit($prescribed_width, $prescribed_height);

                if($original_img->getSourceHeight() > $prescribed_height)
                {
                    $original_img->resizeToHeight($prescribed_height);

                    $original_img->save($name);
                }
            }
            catch (\Throwable $t)
            {
                $this->logger->error($t->getMessage());
                throw $t;
            }
            catch (\Exception $ex)
            {
                $this->logger->error($ex->getMessage());
                throw $ex;
            }
        }
    }
}

class ImagePropertyDescriptor
{
    function __construct($name, $type, $complex = false)
    {
        $this->name = $name;
        $this->type = $type;
        $this->complex = $complex;
    }

    public $complex;
    public $name;
    public $type;
}