<?php


namespace App\Form\Backend;


use App\Entity\AgentPageLocalization;
use App\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentPageLocalizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("language", EntityType::class, array(
            'class' => Language::class,
            'choice_label' => 'name',
            'label' => ''
        ))
            ->add("title", TextType::class, array(
                'required' => true
            ))
            ->add("slug", TextType::class, array(
                'required' => true
            ))
            ->add("content", TextareaType::class, array(
            'required' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AgentPageLocalization::class,
        ));
    }

}