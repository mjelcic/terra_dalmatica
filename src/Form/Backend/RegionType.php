<?php
// src/Form/UserType.php
namespace App\Form\Backend;

use App\Entity\Region;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RegionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("regionLocalizations", CollectionType::class, array(
            'entry_type' => RegionLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Region::class,
            'locale' => []
        ));
    }
}
