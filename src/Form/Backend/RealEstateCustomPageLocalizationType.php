<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 1/21/2020
 * Time: 10:38 PM
 */

namespace App\Form\Backend;


use App\Entity\Language;
use App\Entity\RealEstateCustomPageLocalization;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RealEstateCustomPageLocalizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("language", EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => ''
            ))
            ->add("title", TextType::class)
            ->add("slug", TextType::class, array(
              "disabled" => $options["disableSlugEdit"]
            ))
            ->add("description", TextareaType::class, array(
                "required" => false
            ));
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstateCustomPageLocalization::class,
            'locale' => '',
            'disableSlugEdit' => false
        ));
    }
}