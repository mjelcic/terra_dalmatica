<?php

namespace App\Form\Backend;

use App\Entity\AgentPage;
use App\Entity\User;
use App\GlobalSettings;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AgentPageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("user", EntityType::class, array(
                'class' => User::class,
                'choice_label' => function(User $user){
                    return $user->getFirstName() . " " . $user->getLastName();
                },
                'query_builder' => function (UserRepository $ur) {
                    return $ur->createQueryBuilder('u')
                        ->andWhere("u.roles LIKE '%EMPLOYEE%'");
                },
            ))
            ->add('image_file', VichImageType::class, array(
                'required' => false,
                'download_label' => '',
                'allow_delete' => true,
                'delete_label' => 'izbrisi'))
            ->add("agentPageLocalizations", CollectionType::class, array(
                'entry_type' => AgentPageLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AgentPage::class,
            'locale' => []
        ));
    }

}