<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/24/2020
 * Time: 11:05 AM
 */

namespace App\Form\Backend;

use App\Entity\Language;
use App\Entity\PhotoMetaLocalization;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PhotoMetaLocalizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("language", EntityType::class, array(
                "class" => Language::class,
                "choice_label" => "name",
                "label" => ""
            ))
            ->add("alt_tag", TextType::class, array(
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => PhotoMetaLocalization::class,
            "locale" => ""
        ));
    }
}