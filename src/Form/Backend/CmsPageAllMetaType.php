<?php
// This form is used to get all metaLocalization data for one CmsPage
namespace App\Form\Backend;

use App\Entity\CmsPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CmsPageAllMetaType extends AbstractType
{

    protected $translator;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("cover_photo", PhotoMedataType::class, array(
                "required" => false
            ))
            ->add("cmsPageMetaLocalizations", CollectionType::class, array(
            'entry_type' => CmsPageMetaLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CmsPage::class,
            'locale' => []
        ));
    }
}
