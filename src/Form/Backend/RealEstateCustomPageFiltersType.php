<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 1/21/2020
 * Time: 10:38 PM
 */

namespace App\Form\Backend;


use App\Entity\Location;
use App\Entity\LocationLocalization;
use App\Entity\RealEstateCustomPageFilters;
use App\Entity\RealEstateType;
use App\Entity\RealEstateTypeLocalization;
use App\GlobalSettings;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class RealEstateCustomPageFiltersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $builder
            ->add("real_estate_types", Select2EntityType::class, array(
                "class" => RealEstateType::class,
                "multiple" => true,
                "placeholder" => "Odaberite vrste nekretnina",
                "primary_key" => "id",
                "text_property" => "name",
                "remote_route" => "realestatetype_select2",
                "minimum_input_length" => 0,
                "cache" => true,
                "cache_timeout" => 60000,
                "required" => false
            ))
            ->add("locations", Select2EntityType::class, array(
                "class" => Location::class,
                "multiple" => true,
                "placeholder" => "Odaberite lokacije",
                "primary_key" => "id",
                "text_property" => "name",
                "remote_route" => "location_select2",
                "minimum_input_length" => 0,
                "cache" => true,
                "cache_timeout" => 60000,
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstateCustomPageFilters::class,
            'locale' => ''
        ));
    }
}