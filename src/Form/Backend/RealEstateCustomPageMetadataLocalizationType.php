<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/24/2020
 * Time: 10:28 AM
 */

namespace App\Form\Backend;


use App\Entity\Language;
use App\Entity\RealEstateCustomPageMetadataLocalization;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RealEstateCustomPageMetadataLocalizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("language", EntityType::class, array(
                "class" => Language::class,
                "choice_label" => "name",
                "label" => ""
            ))
            ->add("title", TextType::class, array(
                "required" => false
            ))
            ->add("description", TextareaType::class, array(
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => RealEstateCustomPageMetadataLocalization::class,
            "locale" => ""
        ));
    }
}