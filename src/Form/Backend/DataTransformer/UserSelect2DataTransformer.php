<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/14/2020
 * Time: 12:31 PM
 */

namespace App\Form\Backend\DataTransformer;


use Tetranz\Select2EntityBundle\Form\DataTransformer\EntitiesToPropertyTransformer;

class UserSelect2DataTransformer extends EntitiesToPropertyTransformer
{
    public function transform($entities)
    {
        if (empty($entities)) {
            return array();
        }

        $data = array();

        foreach ($entities as $entity) {
            $data[$entity->getId()] = $entity->getFirstName() . ' '. $entity->getLastName();
        }

        return $data;
    }
}