<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/23/2020
 * Time: 12:38 PM
 */

namespace App\Form\Backend;


use App\Entity\Photo;
use App\Form\EventSubscribers\ImagePropertyDescriptor;
use App\Form\EventSubscribers\ImageResizeSubscriber;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PhotoType extends AbstractType
{
    private $logger;

    function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("file", VichImageType::class, array(
                'required' => false,
                'download_label' => '',
                'allow_delete' => false,
                //'delete_label' => 'izbrisi',
                'label' => false,
            ));

        $builder->addEventSubscriber(new ImageResizeSubscriber(
                array( new ImagePropertyDescriptor("file,file", "single", "complex") ),
                $this->logger
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Photo::class,
            'locale' => ''
        ));
    }
}