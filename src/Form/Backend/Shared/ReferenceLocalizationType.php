<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/14/2020
 * Time: 12:53 PM
 */

namespace App\Form\Backend\Shared;


use App\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ReferenceLocalizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("language", EntityType::class, array(
            'class' => Language::class,
            'choice_label' => 'name',
            'label' => ''
        ))
        ->add("name", TextType::class, array(
            //'required' => false
        ));
    }
}