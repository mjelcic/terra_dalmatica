<?php


namespace App\Form\Backend\RealEstate;


use App\Entity\User;
use App\Form\Backend\RealEstateMetaLocalizationType;
use App\GlobalSettings;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminRealEstateCreateType extends CommonRealEstateType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $builder = $this->buildBaseForm($builder, $locale);

        $builder
            ->add("administrationNote", TextareaType::class, array('required'  => false))
            ->add("keysInPossession", CheckboxType::class, array('required'  => false))
            ->add("lotDetails", TextareaType::class, array('required'  => false))
            ->add("postalCode", NumberType::class, array('required'  => false))
            ->add("place", TextType::class, array('required'  => false))
            ->add("exportToZoopla", CheckboxType::class, array('required'  => false))
            ->add("client", EntityType::class, array(
                'class' => User::class,
                'required' => true,
                'placeholder' => '-',
                'choice_label' => function(User $user){
                    return $user->getFirstName() . " " . $user->getLastName();
                },
                'query_builder' => function (UserRepository $ur) {
                    return $ur->createQueryBuilder('u')
                        ->andWhere("u.roles LIKE '%ROLE_USER%'");
                },
            ))
            ->add("realEstateMetaLocalizations", CollectionType::class, array(
                'entry_type' => RealEstateMetaLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ))
            ->add("agent", EntityType::class, array(
                'class' => User::class,
                'required' => true,
                'placeholder' => '-',
                'choice_label' => function (User $user) {
                    return $user->getFirstName() . " " . $user->getLastName();
                },
                'query_builder' => function (UserRepository $ur) {
                    return $ur->createQueryBuilder('u')
                        ->andWhere("u.roles LIKE '%EMPLOYEE%'");
                },
            ))
            ->add("discountPrice", NumberType::class, array('required'  => false))
            ->add('add', SubmitType::class, ['label' => $this->translator->trans('Predaj')])
            ->add('addAndPublish', SubmitType::class, ['label' => $this->translator->trans('PredajObjavi')])
            ->add('preview', SubmitType::class, ['label' => $this->translator->trans('Predpregled')])
        ;
    }
}