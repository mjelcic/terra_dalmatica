<?php


namespace App\Form\Backend\RealEstate;

use App\Entity\User;
use App\Form\Backend\RealEstateMetaLocalizationType;
use App\GlobalSettings;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminRealEstateEditType extends CommonRealEstateType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $builder = $this->buildBaseForm($builder, $locale);

        $builder
            ->add("administrationNote", TextareaType::class, array('required'  => false))
            ->add("keysInPossession", CheckboxType::class, array('required'  => false))
            ->add("lotDetails", TextareaType::class, array('required'  => false))
            ->add("postalCode", NumberType::class, array('required'  => false))
            ->add("place", TextType::class, array('required'  => false))
            ->add("exportToZoopla", CheckboxType::class, array('required'  => false))
            ->add("client", EntityType::class, array(
                'class' => User::class,
                'choice_label' => function(User $user){
                    return $user->getFirstName() . " " . $user->getLastName();
                },
                'required' => true,
                'placeholder' => '-',
                'query_builder' => function (UserRepository $ur) {
                    return $ur->createQueryBuilder('u')
                        ->andWhere("u.roles LIKE '%ROLE_USER%'");
                },
            ))
            ->add("realEstateMetaLocalizations", CollectionType::class, array(
                'entry_type' => RealEstateMetaLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ))
            ->add("discountPrice", NumberType::class, array('required'  => false))
            ->add("agent", EntityType::class, array(
                'class' => User::class,
                'required' => true,
                'placeholder' => '-',
                'choice_label' => function (User $user) {
                    return $user->getFirstName() . " " . $user->getLastName();
                },
                'query_builder' => function (UserRepository $ur) {
                    return $ur->createQueryBuilder('u')
                        ->andWhere("u.roles LIKE '%EMPLOYEE%'");
                },
            ))
            ->add('edit', SubmitType::class, ['label' => $this->translator->trans('Uredi')])
            ->add('setRejected', SubmitType::class, ['label' => $this->translator->trans('Odbij'), 'validate' => false])
            ->add('editAndPublish', SubmitType::class, ['label' => $this->translator->trans('UrediObjavi')])
            ->add('editPublished', SubmitType::class, ['label' => $this->translator->trans('Uredi')])
            ->add('setInProgress', SubmitType::class, ['label' => $this->translator->trans('PostaviPredRealizaciju')])
            ->add('setCompleted', SubmitType::class, ['label' => $this->translator->trans('Realiziraj')])
            ->add('preview', SubmitType::class, ['label' => $this->translator->trans('Pretpregled')]);

    }
}