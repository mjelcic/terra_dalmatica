<?php


namespace App\Form\Backend\RealEstate;


use App\GlobalSettings;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientRealEstateEditType extends CommonRealEstateType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $builder = $this->buildBaseForm($builder, $locale);

        $builder->add('edit', SubmitType::class, ['label' => $this->translator->trans('Uredi')]);

    }
}