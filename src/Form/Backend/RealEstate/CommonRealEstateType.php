<?php


namespace App\Form\Backend\RealEstate;


use App\Entity\Feature;
use App\Entity\FeatureLocalization;
use App\Entity\Location;
use App\Entity\LocationLocalization;
use App\Entity\RealEstate;
use App\Entity\RealEstateType as RealEstateTypeEntity;
use App\Entity\RealEstateTypeLocalization;
use App\Form\Backend\RealEstateLocalizationType;
use App\GlobalSettings;
use Doctrine\Common\Collections\Collection;
use Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommonRealEstateType extends AbstractType
{
    protected $translator;
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildBaseForm(FormBuilderInterface $builder, $locale): FormBuilderInterface
    {
        return $builder
            ->add("realEstateType", EntityType::class, array(
                'class' => RealEstateTypeEntity::class,
                'choice_label' => function(RealEstateTypeEntity $type)  use ($locale) {
                    $typeLoco = $type->getRealEstateTypeLocalizations()->filter(function (RealEstateTypeLocalization $ret) use ($locale){
                        return $ret->getLanguage()->getCode() == $locale;
                    });

                    return $typeLoco && !$typeLoco->isEmpty() ? $typeLoco->first()->getName() : '';
                }
            ))
            ->add("location", EntityType::class, array(
                'class' => Location::class,
                'placeholder' => '-',
                'choice_label' => function(Location $location)  use ($locale) {
                    $locationLocal = $location->getLocationLocalizations()->filter(function (LocationLocalization $ret) use ($locale){
                        return $ret->getLanguage()->getCode() == $locale;
                    });

                    return $locationLocal && !$locationLocal->isEmpty() ? $locationLocal->first()->getName() : '';
                }
            ))
            ->add("firstRowByTheSea", CheckboxType::class, array('required'  => false))
            ->add("seaView", CheckboxType::class, array('required'  => false))
            ->add("newBuilding", CheckboxType::class, array('required'  => false))
            ->add("underConstruction", CheckboxType::class, array('required'  => false))
            ->add("pool", CheckboxType::class, array('required'  => false))
            ->add("garage", CheckboxType::class, array('required'  => false))
            ->add("bedroomCount", IntegerType::class, array('required'  => false))
            ->add("bathroomCount", IntegerType::class, array('required'  => false))
            ->add("livingArea", NumberType::class, array())
            ->add("surfaceArea", NumberType::class, array('required'  => false))
            ->add("distanceFromTheSea", IntegerType::class, array('required'  => false))
            ->add("fullPrice", NumberType::class, array())
            ->add("priceOnRequest", CheckboxType::class, array('required' => false))
            ->add("energyClass", TextType::class, array('required'  => false))
            ->add("features", EntityType::class, array(
                'class' => Feature::class,
                'choice_label' => function(Feature $feature)  use ($locale) {
                    /** @var Collection $featureLocal */
                    $featureLocal = $feature->getFeatureLocalizations()->filter(function (FeatureLocalization $fl) use ($locale){
                        return $fl->getLanguage()->getCode() === $locale;
                    });

                    return $featureLocal && !$featureLocal->isEmpty() ? $featureLocal->first()->getName() : '';
                },
                'multiple' => true,
                'expanded' => true,
            ))
            ->add("realEstateLocalizations", CollectionType::class, array(
                'entry_type' => RealEstateLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false,
                'required' => false
            ))
            ->add('latlng', GoogleMapType::class)
            ->add('videoUrl', TextType::class, array('required' => false))
            ->add('virtualWalkUrl', TextType::class, array('required' => false))
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstate::class,
            'locale' => []
        ));
    }

}