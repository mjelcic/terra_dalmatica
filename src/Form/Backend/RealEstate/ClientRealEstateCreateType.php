<?php


namespace App\Form\Backend\RealEstate;


use App\Form\Backend\RealEstateMetaLocalizationType;
use App\GlobalSettings;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientRealEstateCreateType extends CommonRealEstateType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $this->buildBaseForm($builder, $locale);

        $builder->add('add', SubmitType::class, ['label' => $this->translator->trans("Predaj")]);
        $builder->add('clientNote', TextareaType::class, ['required' => false]);
        $builder->add("realEstateMetaLocalizations", CollectionType::class, array(
            'entry_type' => RealEstateMetaLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ));

    }
}