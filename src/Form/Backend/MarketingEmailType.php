<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/13/2020
 * Time: 9:24 AM
 */

namespace App\Form\Backend;


use App\Entity\MarketingEmail;
use App\Entity\User;
use App\Form\Backend\DataTransformer\UserSelect2DataTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class MarketingEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("subject", TextType::class)
            ->add("content", TextareaType::class)
            ->add("sentTo", Select2EntityType::class, array(
                "class" => User::class,
                "transformer" => UserSelect2DataTransformer::class,
                "multiple" => true,
                "placeholder" => "Odaberite korisnika(e)",
                "primary_key" => "id",
                //"text_property" => "first_name",
                "remote_route" => "user_select2",
                "minimum_input_length" => 0,
                "cache" => true,
                "cache_timeout" => 60000
            ))
            ->add("sendToAll", CheckboxType::class, array(
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
           "data_class" => MarketingEmail::class,
            "locale" => ''
        ));
    }
}