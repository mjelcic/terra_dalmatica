<?php

namespace App\Form\Backend;

use App\Entity\Feature;
use App\Entity\FeatureLocalization;
use App\Entity\Location;
use App\Entity\LocationLocalization;
use App\Entity\RealEstate;
use App\Entity\RealEstateType as RealEstateTypeEntity;
use App\Entity\RealEstateTypeLocalization;
use App\Entity\User;
use App\GlobalSettings;
use App\Repository\UserRepository;
use Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Contracts\Translation\TranslatorInterface;

class RealEstateType extends AbstractType
{

    protected $translator;
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();
        $userRole = array_key_exists("user_role", $options) ? $options["user_role"] : 'ADMIN';
        $realEstateStatus = array_key_exists("real_estate_status", $options) ? $options["real_estate_status"] : 'NEW';

        // common part of form
        $builder
            ->add("realEstateType", EntityType::class, array(
                'class' => RealEstateTypeEntity::class,
                'choice_label' => function(RealEstateTypeEntity $type)  use ($locale) {
                    $typeLoco = $type->getRealEstateTypeLocalizations()->filter(function (RealEstateTypeLocalization $ret) use ($locale){
                        return $ret->getLanguage()->getCode() == $locale;
                    });

                    return $typeLoco && !$typeLoco->isEmpty() ? $typeLoco->first()->getName() : '';
                }
            ))
            ->add("location", EntityType::class, array(
                'class' => Location::class,
                'choice_label' => function(Location $location)  use ($locale) {
                    $locationLocal = $location->getLocationLocalizations()->filter(function (LocationLocalization $ret) use ($locale){
                        return $ret->getLanguage()->getCode() == $locale;
                    });

                    return $locationLocal && !$locationLocal->isEmpty() ? $locationLocal->first()->getName() : '';
                }
            ))
            ->add("firstRowByTheSea", CheckboxType::class, array('required'  => false))
            ->add("seaView", CheckboxType::class, array('required'  => false))
            ->add("newBuilding", CheckboxType::class, array('required'  => false))
            ->add("underConstruction", CheckboxType::class, array('required'  => false))
            ->add("pool", CheckboxType::class, array('required'  => false))
            ->add("garage", CheckboxType::class, array('required'  => false))
            ->add("bedroomCount", IntegerType::class, array('required'  => false))
            ->add("bathroomCount", IntegerType::class, array('required'  => false))
            ->add("livingArea", IntegerType::class, array())
            ->add("surfaceArea", IntegerType::class, array('required'  => false))
            ->add("distanceFromTheSea", IntegerType::class, array('required'  => false))
            ->add("price", NumberType::class, array())
            ->add("priceOnRequest", CheckboxType::class, array('required' => false))
            ->add("energyClass", TextType::class, array('required'  => false))
            ->add("features", EntityType::class, array(
                'class' => Feature::class,
                'choice_label' => function(Feature $feature)  use ($locale) {
                    $featureLocal = $feature->getFeatureLocalizations()->filter(function (FeatureLocalization $fl) use ($locale){
                        return $fl->getLanguage()->getCode() === $locale;
                    });

                    return $featureLocal && !$featureLocal->isEmpty() ? $featureLocal->first()->getName() : '';
                },
                'multiple' => true,
                'expanded' => true,
            ))
            ->add("realEstateLocalizations", CollectionType::class, array(
                'entry_type' => RealEstateLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false,
                'user_role' => $userRole
            ))
            ->add('latlng', GoogleMapType::class)
            ->add('videoUrl', TextType::class, array('required' => false))
            ->add('virtualWalkUrl', TextType::class, array('required' => false))
            ;


        // only admin sets meta locales
        if($userRole == "ADMIN") {
            $builder->add("realEstateMetaLocalizations", CollectionType::class, array(
                'entry_type' => RealEstateMetaLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ));
        }

        if($userRole == "ADMIN" || $userRole == "EMPLOYEE") {
            $builder
                ->add("administrationNote", TextareaType::class, array('required' => false))
                ->add("client", EntityType::class, array(
                    'class' => User::class,
                    'required' => true,
                    'placeholder' => '-',
                    'choice_label' => function (User $user) {
                        return $user->getFirstName() . " " . $user->getLastName();
                    },
                    'query_builder' => function (UserRepository $ur) {
                        return $ur->createQueryBuilder('u')
                            ->andWhere("u.roles LIKE '%ROLE_USER%'");
                    },
                ))
                ->add("agent", EntityType::class, array(
                    'class' => User::class,
                    'required' => true,
                    'placeholder' => '-',
                    'choice_label' => function (User $user) {
                        return $user->getFirstName() . " " . $user->getLastName();
                    },
                    'query_builder' => function (UserRepository $ur) {
                        return $ur->createQueryBuilder('u')
                            ->andWhere("u.roles LIKE '%EMPLOYEE%'");
                    },
                ));

            switch ($realEstateStatus) {
                case "NEW":
                    $builder
                        ->add('add', SubmitType::class, ['label' => 'Predaj oglas'])
                        ->add('addAndApprove', SubmitType::class, ['label' => 'Predaj i odobri']);
                    break;
                case "CREATED":
                    $builder
                        ->add('edit', SubmitType::class, ['label' => 'Uredi', 'validate' => false])
                        ->add('setRejected', SubmitType::class, ['label' => 'Odbij oglas', 'validate' => false])
                        ->add('editAndApprove', SubmitType::class, ['label' => 'Uredi i odobri']);
                    break;
                case "PUBLISHED":
                    $builder
                        ->add('edit', SubmitType::class, ['label' => 'Uredi'])
                        ->add('setInProgress', SubmitType::class, ['label' => 'Postavi pred realizaciju'])
                        ->add('setCompleted', SubmitType::class, ['label' => 'Postavi kao završen']);
                    break;
                case "REJECTED":
                    $builder
                        ->add('edit', SubmitType::class, ['label' => 'Uredi', 'validate' => false]);
                    break;
                case "IN_PROGRESS":
                    $builder
                        ->add('edit', SubmitType::class, ['label' => 'Uredi', 'validate' => false])
                        ->add('setCompleted', SubmitType::class, ['label' => 'Postavi kao završen']);

            }
        } else if ($userRole == "USER") {
            if($realEstateStatus == "NEW") {
                $builder->add('add', SubmitType::class, ['label' => 'Predaj oglas']);
            } else {
                $builder->add('edit', SubmitType::class, ['label' => 'Uredi']);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstate::class,
            'locale' => [],
            'user_role' => 'ADMIN',
            'real_estate_status' => 'NEW'
        ));

        $resolver->addAllowedTypes(
            'user_role', 'string'
        );
        $resolver->addAllowedTypes(
            'real_estate_status', 'string'
        );

    }
}
