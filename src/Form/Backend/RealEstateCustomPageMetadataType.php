<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/24/2020
 * Time: 11:01 AM
 */

namespace App\Form\Backend;

use App\Entity\RealEstateCustomPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RealEstateCustomPageMetadataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("photo", PhotoMedataType::class, array(
                "required" => false
            ))
            ->add("realEstateCustomPageMetadataLocalizations", CollectionType::class, array(
                "entry_type" => RealEstateCustomPageMetadataLocalizationType::class,
                "entry_options" => array(
                    "label" => false
                ),
                "by_reference" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => RealEstateCustomPage::class,
            "locale" => ""
        ));
    }
}