<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/15/2020
 * Time: 2:16 PM
 */

namespace App\Form\Backend;


use App\Entity\RealEstateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RealEstateTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("realEstateTypeLocalizations", CollectionType::class, array(
            'entry_type' => RealEstateTypeLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstateType::class,
            'locale' => array()
        ));
    }
}