<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/24/2020
 * Time: 11:02 AM
 */

namespace App\Form\Backend;

use App\Entity\Photo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhotoMedataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("photoMetaLocalizations", CollectionType::class, array(
                "entry_type" => PhotoMetaLocalizationType::class,
                "entry_options" => array(
                    "label" => false
                ),
                "by_reference" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => Photo::class,
            "locale" => ""
        ));
    }
}