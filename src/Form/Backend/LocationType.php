<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/14/2020
 * Time: 12:49 PM
 */

namespace App\Form\Backend;


use App\Entity\Location;
use App\Entity\Region;
use App\Entity\RegionLocalization;
use App\GlobalSettings;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $builder->add("region", EntityType::class, array(
                'class' => Region::class,
                'choice_label' => function(Region $region)  use ($locale) {
                    $regionLoco = $region->getRegionLocalizations()->filter(function (RegionLocalization $rl) use ($locale){
                        return $rl->getLanguage()->getCode() == $locale;
                    });

                    return $regionLoco && $regionLoco->count() ? $regionLoco->first()->getName() : '';
                }
            ))
            ->add("postal_code", NumberType::class, array(
                "required"=>true
            ))
            ->add("locationLocalizations", CollectionType::class, array(
                'entry_type' => LocationLocalizationType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ))
            ->add("localLives", CollectionType::class, array(
                'entry_type' => LocalLifeType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Location::class,
            'locale' => array()
        ));
    }
}