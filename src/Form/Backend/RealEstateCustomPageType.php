<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/21/2020
 * Time: 10:35 PM
 */

namespace App\Form\Backend;

use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use App\Entity\RealEstateCustomPage;
use App\Entity\RealEstateCustomPageLocalization;
use App\GlobalSettings;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;


class RealEstateCustomPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $entity = $builder->getData();

        $builder
            ->add("active", ChoiceType::class, array(
                "choices" => array(
                    "DA" => true,
                    "NE" => false
                )
            ))
            ->add("parentRealEstateCustomPage", EntityType::class, array(
                "class" => RealEstateCustomPage::class,
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder('recp')
                        ->where("recp.parentRealEstateCustomPage is NULL"); //offer only pages that don't have any 'children'
                },
                "choice_label" => function($recp) use ($locale){
                    $recpLoco = $recp->getRealEstateCustomPageLocalizations()->filter(function (RealEstateCustomPageLocalization $recpl) use ($locale){
                        return $recpl->getLanguage()->getCode() == $locale;
                    });

                    return $recpLoco && $recpLoco->count() ? $recpLoco->first()->getTitle() : "";
                },
                "placeholder" => "Odaberite Nadkategoriju",
                "required" => false
            ))
            ->add("photo", PhotoType::class, array(
                "required" => false,
                "error_bubbling" => false
            ))
            ->add("realEstateCustomPageLocalizations", CollectionType::class, array(
                "entry_type" => RealEstateCustomPageLocalizationType::class,
                "entry_options" => [
                    "label" => false,
                    "disableSlugEdit" => $entity->getType() != RealEstateCustomPageTypeEnum::CUSTOM
                ],
                "by_reference" => false
            ))
            ->add("realEstateCustomPageFilters", RealEstateCustomPageFiltersType::class, array(
                "locale" => $locale,
                "disabled" => $entity->getType() != RealEstateCustomPageTypeEnum::CUSTOM
            ));
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstateCustomPage::class,
            'locale' => ''
        ));
    }
}