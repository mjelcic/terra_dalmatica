<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/14/2020
 * Time: 12:52 PM
 */

namespace App\Form\Backend;


use App\Entity\LocationLocalization;
use App\Form\Backend\Shared\ReferenceLocalizationType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationLocalizationType extends ReferenceLocalizationType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => LocationLocalization::class,
        ));
    }
}