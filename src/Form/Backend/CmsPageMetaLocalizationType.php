<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/14/2020
 * Time: 12:52 PM
 */

namespace App\Form\Backend;


use App\Entity\CmsPageMetaLocalization;
use App\Entity\Language;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CmsPageMetaLocalizationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("language", EntityType::class, array(
        'class' => Language::class,
        'choice_label' => 'name',
        'label' => ''
    ))
            ->add("title", TextType::class, array(
                'required' => false
            ))
            ->add("description", TextareaType::class, array(
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CmsPageMetaLocalization::class,
        ));
    }
}