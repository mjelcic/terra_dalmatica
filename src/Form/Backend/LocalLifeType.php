<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/24/2020
 * Time: 2:03 PM
 */

namespace App\Form\Backend;


use App\Entity\Language;
use App\Entity\LocalLife;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocalLifeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
/*            ->add("location", EntityType::class, array(
                "disabled" => true
            ))*/
            ->add("language", EntityType::class, array(
                "class" => Language::class,
                "choice_label" => "name",
                "disabled" => true
            ))
            ->add("html", TextareaType::class, array(
                "required" => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => LocalLife::class,
            'locale' => array()
        ));
    }
}