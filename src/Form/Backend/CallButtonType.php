<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 9/27/2018
 * Time: 11:40 AM
 */

namespace App\Form\Backend;


use App\Entity\CallButton;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CallButtonType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("number", TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CallButton::class,
            'locale' => ''
        ));
    }
}