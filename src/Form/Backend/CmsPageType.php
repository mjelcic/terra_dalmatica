<?php

namespace App\Form\Backend;

use App\Entity\BlogCategory;
use App\Entity\BlogCategoryLocalization;
use App\Entity\Enum\CmsPageTypeEnum;
use App\Entity\CmsPage;
use App\Entity\Language;
use App\GlobalSettings;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use App\Form\EventSubscribers\ImageResizeSubscriber;
use App\Form\EventSubscribers\ImagePropertyDescriptor;

class CmsPageType extends AbstractType
{

    protected $translator;
    protected $logger;
    protected $locale;

    function __construct(TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->translator = $translator;
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();
        $builder->add("cmsPageLocalizations", CollectionType::class, array(
            'entry_type' => CmsPageLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ))
            ->add("page_type", ChoiceType::class, array(
                'choices' => CmsPageTypeEnum::getAvailableTypes(),
                'choice_label' => function ($choice) {
                    return CmsPageTypeEnum::getTypeName($choice);
                },
            ))
            ->add("category", EntityType::class, array(
                "class" => BlogCategory::class,
                "required"=> false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('b')
                        ->andWhere("b.invalidated = 0");
                },
                'multiple' => true,
                'expanded' => true,
                'choice_label' => function ($entity) use ($locale) {
                    return  $entity->getBlogCategoryLocalizations()->filter(function($entity) use ($locale){
                        return $entity->getLanguage()->getCode() == $locale;
                    })->first()->getName();
                },
                "placeholder" => "Odaberite kategoriju",
            ))
            ->add("display_id", HiddenType::class)
            ->add("slider", CheckboxType::class, array(
                "required" => false
            ))
            ->add("cover_photo", PhotoType::class, array(
                "required" => false
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CmsPage::class,
            'locale' => []
        ));
    }
}
