<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/14/2020
 * Time: 12:52 PM
 */

namespace App\Form\Backend;


use App\Entity\CmsPageLocalization;
use App\Entity\Language;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CmsPageLocalizationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("language", EntityType::class, array(
        'class' => Language::class,
        'choice_label' => 'name',
        'label' => ''
    ))
            ->add("slug", TextType::class, array(
                'required' => true
            ))
            ->add("title", TextType::class, array(
                'required' => true
            ))
            ->add("content", TextareaType::class, array(
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CmsPageLocalization::class,
        ));
    }
}