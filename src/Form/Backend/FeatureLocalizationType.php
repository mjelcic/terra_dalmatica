<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:54 PM
 */

namespace App\Form\Backend;



use App\Entity\FeatureLocalization;
use App\Form\Backend\Shared\ReferenceLocalizationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeatureLocalizationType extends ReferenceLocalizationType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => FeatureLocalization::class,
        ));
    }
}