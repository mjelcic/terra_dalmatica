<?php
// src/Form/UserType.php
namespace App\Form\Backend;

use App\Entity\Feature;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FeatureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("featureLocalizations", CollectionType::class, array(
            'entry_type' => FeatureLocalizationType::class,
            'entry_options' => [
                'label' => false
            ],
            'by_reference' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Feature::class,
            'locale' => []
        ));
    }
}
