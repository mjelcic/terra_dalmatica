<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/15/2020
 * Time: 2:23 PM
 */

namespace App\Form\Backend;


use App\Entity\RealEstateTypeLocalization;
use App\Form\Backend\Shared\ReferenceLocalizationType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RealEstateTypeLocalizationType extends ReferenceLocalizationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add("title_prefix", TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => RealEstateTypeLocalization::class,
        ));
    }
}