<?php
// src/Form/UserType.php
namespace App\Form\Backend;

use App\Entity\Enum\RoleEnum;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{
    function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
    }


    private $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**if(in_array("ROLE_ADMIN", $this->user->getRoles())) {
            $roles = RoleEnum::getAvailableRoles();
        }
        else{
            $roles =  ["ROLE_USER"];
        }*/

        $builder
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
            ->add('first_name', TextType::class)
            ->add('last_name', TextType::class)
            ->add('phone_no', TelType::class)
            ->add('mobile_phone_number', TelType::class, array(
                "required"=>false
            ))
            ->add('fax_number', TextType::class, array(
                "required"=>false
            ))
            ->add('personal_identification_number', TextType::class, array(
                "required"=>false
            ))
            ->add('company_name', TextType::class, array(
                "required"=>false
            ))
            ->add('roles', ChoiceType::class, array(
                'choices' => ["ROLE_ADMIN", "ROLE_EMPLOYEE"],
                'choice_label' => function($choice) {
                    return RoleEnum::getRoleName($choice);
                },
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'locale' => []
        ));
    }
}
