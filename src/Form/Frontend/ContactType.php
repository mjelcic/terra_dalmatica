<?php
// src/Form/UserType.php
namespace App\Form\Frontend;

use App\ViewModel\Frontend\ContactModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ContactType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('email', EmailType::class)
            ->add('name', TextType::class)
            ->add('email_to', HiddenType::class)
            ->add('real_estate_link', HiddenType::class)
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                "html5"=>true,
                'attr' => ['class' => 'js-datepicker'],
                "required"=>false
            ))
            ->add('phone_no', TelType::class, array(
                "required"=>false
            ))
            ->add('message', TextareaType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ContactModel::class,
            'locale' => []
        ));
    }
}
