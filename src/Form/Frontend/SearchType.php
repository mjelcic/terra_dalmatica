<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 3/3/2020
 * Time: 2:39 PM
 */

namespace App\Form\Frontend;


use App\Entity\Location;
use App\Entity\LocationLocalization;
use App\Entity\RealEstateType;
use App\Entity\RealEstateTypeLocalization;
use App\GlobalSettings;
use App\Repository\AbstractBaseRepository;
use App\Services\ServiceLocation;
use App\Services\ServiceRealEstateType;
use App\ViewModel\Frontend\Enum\RealEstatesSortEnum;
use App\ViewModel\Frontend\SearchModel;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchType extends AbstractType
{
    private $translator;
    private $serviceLocation;
    private $serviceRealEstateType;

    function __construct(TranslatorInterface $translator, ServiceLocation $serviceLocation, ServiceRealEstateType $serviceRealEstateType)
    {
        $this->translator = $translator;
        $this->serviceLocation = $serviceLocation;
        $this->serviceRealEstateType = $serviceRealEstateType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = array_key_exists("locale", $options) ? $options["locale"] : GlobalSettings::getDefaultLocale();

        $locations = $this->serviceLocation->getAllLocalized($locale);
        $locationChoices = [];
        foreach ($locations as $location){
            $locationChoices[$location["name"]] = $location["id"];
        }

        $rets = $this->serviceRealEstateType->getAllLocalized($locale);
        $retChoices = [];
        foreach ($rets as $ret){
            $retChoices[$ret["name"]] = $ret["id"];
        }

        $builder
            ->add("realEstateType", ChoiceType::class, array(
                "choices" => $retChoices,
                "required" => false
            ))
            ->add("location", ChoiceType::class, array(
                "choices" => $locationChoices,
                "required" => false
            ))
            ->add("text", TextType::class, array(
                "required" => false
            ))
            ->add("priceSelector", ChoiceType::class, array(
                "choices" => array(
                    "0 - 100.000 €" => "0,100000",
                    "100.000 - 250.000 €" => "100000,250000",
                    "250.000 - 500.000 €" => "250000,500000",
                    "500.000+ €" => "500000,999999999"
                ),
                //"placeholder" => "Sve cijene",
                "required" => false,
                "mapped" => false
            ))
            ->add("priceMin", HiddenType::class)
            ->add("priceMax", HiddenType::class)
/*            ->add("bedroomCount", IntegerType::class, array(
                "required" => false
            ))*/
            ->add("bedroomCount", ChoiceType::class, array(
                "choices" => array(
                    "Studio (ili više)" => 0,
                    "Jedna soba (ili više)" => 1,
                    "Dvije sobe (ili više)" => 2,
                    "Tri sobe (ili više)" => 3,
                    "Četiri sobe (ili više)" => 4,
                    "Pet soba (ili više)" => 5,
                ),
                //"placeholder" => "Broj soba",
                "required" => false
            ))
            ->add("firstRowByTheSea", CheckboxType::class, array(
                "required" => false
            ))
            ->add("seaView", CheckboxType::class, array(
                "required" => false
            ))
            ->add("newBuilding", CheckboxType::class, array(
                "required" => false
            ))
            ->add("underConstruction", CheckboxType::class, array(
                "required" => false
            ))
            ->add("priceLowered", CheckboxType::class, array(
                "required" => false
            ))
            ->add("pool", CheckboxType::class, array(
                "required" => false
            ))
            ->add("sort", ChoiceType::class, array(
                "choices" => array(
                    $this->translator->trans("Najnovije") => RealEstatesSortEnum::dateHighest,
                    $this->translator->trans("Najstarije") => RealEstatesSortEnum::dateLowest,
                    $this->translator->trans("Cijeni - najnižoj") => RealEstatesSortEnum::priceLowest,
                    $this->translator->trans("Cijeni - najvišoj") => RealEstatesSortEnum::priceHighest,
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => SearchModel::class,
            "locale" => [],
            "csrf_protection" => false
        ));
    }
}