<?php

declare(strict_types=1);

namespace App\ORM\Hydration;

use Doctrine\ORM\Internal\Hydration\ArrayHydrator;

/**
 * Returns one-dimensional scalar array from query: mixed[][] => mixed[]
 *
 * Example:
 * ArrayHydrator: [['id' => 1], ['id' => 2]]
 * ColumnHydrator: [1, 2]
 *
 * @see https://stackoverflow.com/questions/11657835/how-to-get-a-one-dimensional-scalar-array-as-a-doctrine-dql-query-result
 * @author Gabriel Ostrolucký
 * @license MIT
 */
class ColumnHydrator extends ArrayHydrator
{
    /**
     * @return mixed[]
     */
    protected function hydrateAllData(): array
    {
        if (!isset($this->_rsm->indexByMap['scalars'])) {
            return $this->_stmt->fetchAll(\PDO::FETCH_COLUMN);
        }

        if (!$result = parent::hydrateAllData()) {
            return $result;
        }

        $indexColumn = $this->_rsm->scalarMappings[$this->_rsm->indexByMap['scalars']];
        $keys = array_keys(reset($result));

        return array_column($result, isset($keys[1]) && $keys[0] === $indexColumn ? $keys[1] : $keys[0], $indexColumn);
    }
}