<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/25/2020
 * Time: 11:22 PM
 */

namespace App\Controller\Backend;


use App\Entity\ExchangeRate;
use App\GlobalSettings;
use App\Services\NotificationService;
use App\Services\ServiceExchangeRate;
use App\Utility\ExchangeRateHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExchangeRateController extends BaseController
{
    private $service;

    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService, ServiceExchangeRate $serviceExchangeRate)
    {
        parent::__construct($em, $translator, $validator, ExchangeRate::class, FormType::class, $notificationService);
        $this->service = $serviceExchangeRate;
    }

    public function syncExchangeRate()
    {
        try
        {
            $url = GlobalSettings::getExchangeRateSourceUrl();

            $httpClient = HttpClient::create();
            $response = $httpClient->request("GET", $url);

            if($response->getStatusCode() == Response::HTTP_OK)
            {
                $content = $response->getContent();

                $jsonArray = json_decode($content, true);

                if(is_array($jsonArray) && count($jsonArray))
                {
                    $dateValid = new \DateTime($jsonArray[0]["Datum primjene"], new \DateTimeZone("Europe/Zagreb"));
                    $exchangeRate = new ExchangeRate();
                    $user = $this->getUser();

                    $exchangeRate
                        ->setCode($jsonArray[0]["Valuta"])
                        ->setRate(floatval(str_replace(",",".", $jsonArray[0]["Srednji za devize"])))
                        ->setUnits((int)$jsonArray[0]["Jedinica"])
                        ->setDateValid($dateValid)
                        ->setCreatedBy($user != null ? $user->getId() : 0);

                    $this->entityManager->persist($exchangeRate);

                    $this->entityManager->flush();

                    //ExchangeRateHelper::Default()->setCurrentExchangeRate($exchangeRate);
                }
            }
        }
        catch (\Exception $ex)
        {
            return Response::create("Greška prilikom preuzimanja kursne liste!", Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return Response::create("Uspješno preuzeta kursna lista!");
    }

    public function syncManuallyExchangeRate()
    {
        /**
         * @var $response Response
         */
        $response = $this->syncExchangeRate();

        if($response->getStatusCode() == Response::HTTP_OK){
            $this->addFlash("success", $this->translator->trans("Uspješno preuzeta kursna lista!"));
        }

        if($response->getStatusCode() == Response::HTTP_INTERNAL_SERVER_ERROR){
            $this->addFlash("error", $this->translator->trans("Greška prilikom preuzimanja kursne liste! Pokušajte ponovo."));
        }

        return $this->redirectToRoute($this->getEntityListRouteName());
    }
}