<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:30 PM
 */

namespace App\Controller\Backend;


use App\Entity\Region;
use App\Form\Backend\RegionType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegionController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, Region::class, RegionType::class, $notificationService);
    }

    /**
     * @param $entity Region
     */
    protected function beforeEntitySave($entity)
    {
        if($entity->getRegionLocalizations())
        {
            foreach ($entity->getRegionLocalizations() as $regionLoco){
                if($entity->getId()){
                    $regionLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $regionLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }
    }
}