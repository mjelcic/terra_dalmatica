<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 1/21/2020
 * Time: 10:33 PM
 */

namespace App\Controller\Backend;


use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use App\Entity\PhotoMetaLocalization;
use App\Entity\RealEstateCustomPage;
use App\Entity\RealEstateCustomPageLocalization;
use App\Entity\RealEstateCustomPageMetadataLocalization;
use App\Form\Backend\RealEstateCustomPageMetadataType;
use App\Services\NotificationService;
use App\Services\ServiceLanguage;
use App\Services\ServiceRealEstateCustomPage;
use App\Utility\ActionType;
use App\Utility\EntityHelper;
use App\Utility\Factory\LocalizedEntityFactory;
use App\Utility\Slug\Slugifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\Backend\RealEstateCustomPageType;

class RealEstateCustomPageController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, RealEstateCustomPage::class, RealEstateCustomPageType::class, $notificationService);
    }

    public function create(Request $request)
    {
        $localizations = LocalizedEntityFactory::createLocalizations($this->entityManager, $this->fullClassName);

        $args = array("localizations" => $localizations);

        //DEFAULT type is CUSTOM, that is any cat. page created fully by user falls within that type
        $args["type"] = RealEstateCustomPageTypeEnum::CUSTOM;

        return $this->HandleDataSubmit(null, ActionType::create, $request, [], $args);
    }

    /**
     * @param $entity RealEstateCustomPage
     */
    protected function beforeEntitySave($entity)
    {
        if(!$entity->getId())
        {
            //If entity is being created via form then it falls in the custom category pages domain
            $entity->setType(RealEstateCustomPageTypeEnum::CUSTOM);

            //Add metadata(set title to loco title)
            if(!$entity->getRealEstateCustomPageMetadataLocalizations()->count())
            {
                foreach ($entity->getRealEstateCustomPageLocalizations() as $recpLoco)
                {
                    $recpMetaLoco = new RealEstateCustomPageMetadataLocalization();
                    $recpMetaLoco
                        ->setLanguage($recpLoco->getLanguage())
                        ->setTitle($recpLoco->getTitle())
                        ->setCreatedBy($this->getUser()->getId());

                    $entity->addRealEstateCustomPageMetadataLocalization($recpMetaLoco);
                }
            }
        }

        if($entity->getRealEstateCustomPageLocalizations())
        {
            $entityHelper = null;
            if($entity->getType() == RealEstateCustomPageTypeEnum::CUSTOM) {
                $entityHelper = new EntityHelper($this->entityManager);
            }

            foreach ($entity->getRealEstateCustomPageLocalizations() as $recpLoco)
            {
                if($entity->getId()){
                    $recpLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $recpLoco->setCreatedBy($this->getUser()->getId());
                }

                //Set unique value for slug
                if($entityHelper){
                    $recpLoco->setSlug($entityHelper->getUniqueFieldValue(
                        RealEstateCustomPageLocalization::class,
                        "slug",
                        $recpLoco->getSlug() ? $recpLoco->getSlug() : Slugifier::slugify($recpLoco->getTitle()),
                        $recpLoco->getId()
                    ));
                }
            }
        }

        if($entity->getRealEstateCustomPageFilters())
        {
            if($entity->getId()){
                $entity->getRealEstateCustomPageFilters()->setUpdatedBy($this->getUser()->getId());
            } else {
                $entity->getRealEstateCustomPageFilters()->setCreatedBy($this->getUser()->getId());
            }
        }

        if($entity->getPhoto()){
            if($entity->getPhoto()->getId()){
                $entity->getPhoto()->setUpdatedBy($this->getUser()->getId());
            }
            else {
                $entity->getPhoto()->setCreatedBy($this->getUser()->getId());
            }
        }
    }

    public function editMetadata(Request $request, int $id)
    {
        //0) Define needed services etc.
        $serviceRealEstateCustomPage = new ServiceRealEstateCustomPage($this->entityManager);
        $serviceLanguage = new ServiceLanguage($this->entityManager);
        $languages = $serviceLanguage->getAll();
        $locale = $request->getLocale();

        //1) Get recp entity
        /**
         * @var $realEstateCustomPage RealEstateCustomPage
         */
        $realEstateCustomPage = $serviceRealEstateCustomPage->getById($id);
        //If no FE was found return 404
        if (!$realEstateCustomPage) {
            throw $this->createNotFoundException($this->translator->trans("Subjekat koji tražite nije pronađen! Pokušajte ponovo!"));
        }

        //2) Check if there are any metadata for the recp - if not, create some
        if(!$realEstateCustomPage->getRealEstateCustomPageMetadataLocalizations()->count())
        {
            foreach ($languages as $language)
            {
                $recpMeta = new RealEstateCustomPageMetadataLocalization();
                $recpMeta->setLanguage($language);
                $realEstateCustomPage->addRealEstateCustomPageMetadataLocalization($recpMeta);
            }
        }

        if($realEstateCustomPage->getPhoto())
        {
            if(!$realEstateCustomPage->getPhoto()->getPhotoMetaLocalizations()->count())
            {
                foreach ($languages as $language)
                {
                    $photoMeta = new PhotoMetaLocalization();
                    $photoMeta->setLanguage($language);
                    $realEstateCustomPage->getPhoto()->addPhotoMetaLocalization($photoMeta);
                }
            }
        }

        //3) Create form and handle request
        $form = $this->createForm(RealEstateCustomPageMetadataType::class, $realEstateCustomPage);

        $form->handleRequest($request);

        //4)Check if submitted
        if($form->isSubmitted())
        {
            //4.1 ... and valid
            if($form->isValid())
            {
                //4.2).. if it is process data, save and return to list
                $this->beforeMetaSave($realEstateCustomPage);
                try {
                    $this->entityManager->persist($realEstateCustomPage);
                    $this->entityManager->flush();
                    $this->addFlash("success", $this->translator->trans("Uspješno ažurirani metapodaci kategorijske stranice."));
                } catch (\Exception $e) {
                    $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom spašavanja entiteta: " . $e->getMessage()));
                }

                return $this->redirectToRoute($this->getEntityListRouteName());
            }
        }

        $recpLoco = $realEstateCustomPage->getRealEstateCustomPageLocalization($locale);

        //4.2).. if not direct to the edit metadata form
        return $this->render("backend/edit_realestatecustompagemeta.html.twig", array(
            "form" => $form->createView(),
            "realEstateCustomPageName" => $recpLoco->getTitle()
        ));
    }

    /**
     * @param $entity RealEstateCustomPage
     */
    private function beforeMetaSave($entity)
    {
        $entity
            ->setUpdatedBy($this->getUser()->getId())
            ->setUpdatedAt(new \DateTime());

        if($entity->getRealEstateCustomPageMetadataLocalizations())
        {
            foreach ($entity->getRealEstateCustomPageMetadataLocalizations() as $recpMeta)
            {
                if($recpMeta->getId()){
                    $recpMeta->setUpdatedBy($this->getUser()->getId());
                }
                else {
                    $recpMeta->setCreatedBy($this->getUser()->getId());
                }
            }
        }

        if($entity->getPhoto())
        {
            if($entity->getPhoto()->getPhotoMetaLocalizations())
            {
                foreach ($entity->getPhoto()->getPhotoMetaLocalizations() as $photoMeta)
                {
                    if($photoMeta->getId()){
                        $photoMeta->setUpdatedBy($this->getUser()->getId());
                    }
                    else {
                        $photoMeta->setCreatedBy($this->getUser()->getId());
                    }
                }
            }
        }
    }
}