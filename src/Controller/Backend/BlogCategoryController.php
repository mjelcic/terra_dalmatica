<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:30 PM
 */

namespace App\Controller\Backend;


use App\Entity\BlogCategory;
use App\Form\Backend\BlogCategoryType;
use App\Services\NotificationService;
use App\Utility\Slug\Slugifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Services\ContentService;

class BlogCategoryController extends BaseLocalizedEntityController
{
    protected $contentService;
    protected $slugifier;

    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, ContentService $contentService, Slugifier $slugifier, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, BlogCategory::class, BlogCategoryType::class, $notificationService);
        $this->contentService = $contentService;
        $this->slugifier = $slugifier;
    }

    /**
     * @param $entity BlogCategory
     */
    protected function beforeEntitySave($entity)
    {
        if($entity->getBlogCategoryLocalizations())
        {
            foreach ($entity->getBlogCategoryLocalizations() as $blogCategoryLoco){
                if($entity->getId()){
                    $blogCategoryLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $blogCategoryLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }
    }

}