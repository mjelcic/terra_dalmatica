<?php
/**
 * Created by mjelcic
 * Date: 1/28/2020
 */

namespace App\Controller\Backend;

use App\Utility\ImageResize\ImageResizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\GlobalSettings;

class SummernoteController extends Controller
{
    protected $global;

    function __construct(GlobalSettings $global)
    {
        $this->global = $global;
    }

    public function upload(Request $request)
    {
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = $this->global->getSummernoteImagePath() . $filename; //change this directory
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                ImageResizer::resize(new UploadedFile($destination, $filename));
                return new JsonResponse(array("url" => $this->global->getSummernoteImageUrl() . $filename));
            }
            else {
                throw $this->createNotFoundException('Ooops!  Your upload triggered the following error:  ' . $_FILES['file']['error']);
            }
        }

    }



}