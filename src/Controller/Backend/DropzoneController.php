<?php

namespace App\Controller\Backend;


use App\GlobalSettings;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DropzoneController extends AbstractController
{

    protected $global;

    function __construct(GlobalSettings $global)
    {
        $this->global = $global;
    }

    public function upload(Request $request) {

        $uploadedFile = $request->files->get('reference');

        dd($uploadedFile);
    }

}