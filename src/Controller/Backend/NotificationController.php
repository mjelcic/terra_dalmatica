<?php

namespace App\Controller\Backend;

use App\Entity\Notification;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends BaseNotificationController
{

    protected $em;
    public function __construct(NotificationService $notificationService, EntityManagerInterface $em)
    {
        parent::__construct($notificationService);
        $this->em = $em;
    }

    public function notification($id)
    {
     $notification = $this->em->getRepository(Notification::class)->find($id);

        $notification->addSeenBy($this->getUser());
        $this->em->flush();
        return $this->render('backend/notification.html.twig', [
            'notification' => $notification
        ]);
    }
}
