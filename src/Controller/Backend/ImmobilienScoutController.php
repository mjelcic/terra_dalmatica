<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/21/2020
 * Time: 1:26 PM
 */

namespace App\Controller\Backend;


use App\Services\Export\ImmobilienScoutService;
use App\Services\ServiceRealEstate;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImmobilienScoutController extends AbstractController
{
    private $service;
    private $translator;

    function __construct(ImmobilienScoutService $service, TranslatorInterface $translator)
    {
        $this->service = $service;
        $this->translator = $translator;
    }

    public function certify(Request $request)
    {
/*        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()))
        {
            throw new UnauthorizedHttpException("", "Morate imati administratorska ovlaštenja da biste pokrenuli ovaj proces.");
        }*/

        $status = $this->service->certifyApplication($this->generateUrl("immobilien_certify", [], UrlGeneratorInterface::ABSOLUTE_URL));

        return new Response($status);
    }

    //FOR TESTING PURPOSE
/*    public function export(ServiceRealEstate $serviceRealEstate, ImmobilienScoutService $immobilienScoutService, ?int $id)
    {
        $realEstate = $serviceRealEstate->getById($id);
        if(!$realEstate){
            return $this->createNotFoundException("Nekretnina sa proslijeđenim id-om ne postoji");
        }

        $dtoForExport = $immobilienScoutService->createDTOForExport(null, $realEstate);

        $xml_options = array(
            "xml_root_node_name" => $dtoForExport["mainXmlElemName"],
            "remove_empty_tags" => true,
            "xml_encoding" => "utf-8",
        );
        $encoders = [new XmlEncoder($xml_options), new JsonEncoder()];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter), new ArrayDenormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $xml = $serializer->serialize($dtoForExport["data"], 'xml');

        //Hack to add namespaces for root element
        $xml = str_replace(
            "<" . $dtoForExport["mainXmlElemName"] . ">",
            "<" . $dtoForExport["mainXmlElemName"] . ' xmlns:realestates="http://rest.immobilienscout24.de/schema/offer/realestates/1.0" xmlns:xlink="http://www.w3.org/1999/xlink">',
            $xml
        );


        // Ovaj controller je napravljen samo radi testiranja, prava funkcionalnost za export je napravljena preko "Command", zato je ovaj nered ovdje dole ispod.
        // Drugim riječima ovaj controller ćemo pobrisati
        echo $xml;
        die(null);

        return $this->json($dtoForExport["data"]);
    }*/

    public function export(ServiceRealEstate $serviceRealEstate, ImmobilienScoutService $immobilienScoutService, ?int $id)
    {
        $realEstate = $serviceRealEstate->getById($id);
        if(!$realEstate){
            return $this->createNotFoundException("Nekretnina sa proslijeđenim id-om ne postoji");
        }
        $statusMessage = "";

        $success = $immobilienScoutService->exportRealEstate($realEstate, "EXPORT");
        if($success == true){
            $statusMessage = $this->translator->trans("Podaci za nekretninu uspješno izvezeni na ImmobilienScout24.");
            $aStatus = $immobilienScoutService->exportAttachmentsForRealEstate($realEstate, $this->getParameter('kernel.project_dir'));
            if(count($aStatus) > 0){
                $imgMessage = $this->translator->trans("Slike za nekretninu su uspješno izvezene.");
                $errorsCount = count(array_filter($aStatus, function ($elem){ return $elem == false;}));
                if(count($aStatus) == $errorsCount)
                {
                    $imgMessage = $this->translator->trans("Došlo je do greške kod izvoza slika za nekretninu. Provjerite log.");
                }
                if($errorsCount >= 1 && $errorsCount != count($aStatus))
                {
                    $imgMessage = $this->translator->trans("Došlo je do greške kod izvoza određenih slika za nekretninu. Provjerite log.");
                }
                $statusMessage .= " " . $imgMessage;
            }

        }

        if(!$success){
            $statusMessage = $this->translator->trans("Došlo je do pogreške kod izvoza podataka za nekretninu!");
        }

        return new Response($statusMessage, 200);
    }

    public function test(ImmobilienScoutService $service, ?int $id)
    {
        $result = null;

        if($id){
            $result = $service->test($id);
        }

        return $this->render("backend/immobilien_scout/test.html.twig", array(
           "result" => $result
        ));
    }

    public function exportStatusUpdate(ImmobilienScoutService $service)
    {
        $result = $service->exportStatusCheck();

        return $this->render("backend/immobilien_scout/test.html.twig", array(
            "result" => $result
        ));
    }

    public function exportDelta(ImmobilienScoutService $service)
    {
        $result = $service->exportDelta($this->getParameter('kernel.project_dir'));

        return $this->render("backend/immobilien_scout/test.html.twig", array(
            "result" => $result
        ));
    }
}