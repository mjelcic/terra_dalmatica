<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/26/2020
 * Time: 9:18 PM
 */

namespace App\Controller\Backend;


use App\Services\ServiceUserFavorite;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Flex\Response;

class UserFavoriteController extends AbstractController
{
    private $translator;
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function overview(Request $request, ServiceUserFavorite $serviceUserFavorite)
    {
        $user = $this->getUser();
        $locale = $request->getLocale();

        $userFavorites = $serviceUserFavorite->getFavoriteRealEstatesByUserLocalized($user, $locale);

        return $this->render("backend/userfavorite_list.html.twig", array(
            "userFavorites" => $userFavorites
        ));
    }

    public function delete(Request $request, ServiceUserFavorite $serviceUserFavorite , $id)
    {
        $uf = $serviceUserFavorite->getById($id);
        if($uf == null){
            return new Response("Greška u zahtjevu!", 404);
        }

        $success = $serviceUserFavorite->remove($uf, $this->getUser());

        $result = [];
        $result["success"] = $success;
        $result["message"] = $success ? $this->translator->trans("Uspješno ste uklonili nekretninu iz favorita.") :
            $this->translator->trans("Greška prilikom uklanjanja nekretnine iz favorita.");

        return $this->json($result);
    }
}