<?php

namespace App\Controller\Backend;


use App\Entity\AgentPage;
use App\Form\Backend\AgentPageType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AgentPageController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, AgentPage::class, AgentPageType::class, $notificationService);
    }

}