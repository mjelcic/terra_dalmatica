<?php
declare(strict_types=1);

namespace App\Controller\Backend;


use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\RealEstateImage;
use App\Entity\RealEstateImageMetaLocalization;
use App\GlobalSettings;
use App\Utility\ImageResize\ImageResizer;
use App\Utility\Watermarker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class RealEstateImageController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var GlobalSettings
     */
    private $global;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityManagerInterface $entityManager, GlobalSettings $global, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->global = $global;
        $this->translator = $translator;
    }

    public function addImage(Request $request) {

        /** @var Language[] $languages */
        $languages = $this->entityManager->getRepository(Language::class)->findAll();

        /** @var File $uploadedFile */
        $uploadedFile = $request->files->get('reference');

        $originalFileName = $uploadedFile->getClientOriginalName();

        $imageUuid = $request->get('imageUuid');

        $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();

        $fullPath = $this->global->getRealEstateImagesBasePath() . date("/Y/m/",time());

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        if ($uploadedFile->move($fullPath, $fileName)) {
            $uploadedFile = new UploadedFile($fullPath . "/" . $fileName, $fileName);
            ImageResizer::resize($uploadedFile);
            $path = Watermarker::appendWatermark($uploadedFile);

            $image = new RealEstateImage();
            $image->setUuid($imageUuid);
            $image->setCreatedBy($this->getUser()->getId());
            $image->setImage($fileName);
            $image->setPath("/" . $path);
            $image->setCover(false);
            $image->setInvalidated(0);
            $image->setCreatedAt(new \DateTime());
            $image->setOriginalFileName($originalFileName);

            foreach ($languages as $language) {
                $imageMetaLocalization = new RealEstateImageMetaLocalization();
                $imageMetaLocalization->setLanguage($language);
                $imageMetaLocalization->setCreatedBy($this->getUser()->getId());
                $imageMetaLocalization->setInvalidated(0);
                $imageMetaLocalization->setCreatedAt(new \DateTime());
                $imageMetaLocalization->setAltTag($originalFileName);
                $image->addMetaLocalization($imageMetaLocalization);
            }

            $this->entityManager->persist($image);
            $this->entityManager->flush();

            return new JsonResponse([
                "uuid" => $image->getUuid(),
                "html" => $this->render("backend/include/single_real_estate_image2.html.twig", array("rei" => $image, "isNew" => true))->getContent()

            ], 200);

        }

        return new JsonResponse([], 400);
    }

    public function overview(int $id) {
        $entity = $this->entityManager->getRepository(RealEstate::class)->find($id);

        if(!$entity instanceof RealEstate) {
            return new Response($this->translator->trans("Navedeni unos nije pronađen!"), 404);
        }

        //add eventually missing metadata
        /** @var Language[] $languages */
        $languages = $this->entityManager->getRepository(Language::class)->findAll();

        foreach ($entity->getRealEstateImages() as $image) {
            foreach ($languages as $language) {
                if($image->getRealEstateImageMetaLocalizations()->filter(
                    function(RealEstateImageMetaLocalization $loco) use ($language){
                        return $loco->getLanguage()->getCode() == $language->getCode();
                    })->count() == 0) {
                    $realEstateImageMeta = new RealEstateImageMetaLocalization();
                    $realEstateImageMeta->setInvalidated(0);
                    $realEstateImageMeta->setLanguage($language);
                    $realEstateImageMeta->setAltTag("");

                    $image->addMetaLocalization($realEstateImageMeta);
                }
            }
        }


        return $this->render("backend/real_estate_images.html.twig", ["realEstate" => $entity]);
    }

    public function setMetadataForRealEstateImages(int $realEstateId, Request $request)
    {
        $realEstate = $this->entityManager->find("App:RealEstate", $realEstateId);

        if(!$realEstate instanceof RealEstate) {
            return new JsonResponse([], 404);
        }
        /** @var Language[] $languages */
        $languages = $this->entityManager->getRepository(Language::class)->findAll();
        $parameters = json_decode($request->getContent(), true);

        foreach ($parameters["images"] as $image) {
            $realEstateImage = $realEstate->getRealEstateImages()->filter(
                function(RealEstateImage $rei) use ($image) { return $rei->getId() == $image["id"];})->first();

            if($realEstateImage instanceof RealEstateImage) {
                foreach ($image["locales"] as $localeDetails) {

                    $realEstateImageMeta = $realEstateImage->getRealEstateImageMetaLocalizations()->filter(
                        function(RealEstateImageMetaLocalization $loco) use ($localeDetails) {
                            return $loco->getLanguage()->getCode() == $localeDetails["locale"];
                        }
                    )->first();

                    if(!$realEstateImageMeta instanceof RealEstateImageMetaLocalization) {
                        $languagesFiltered = array_values(array_filter($languages, function(Language $language) use ($localeDetails) {
                            return $language->getCode() == $localeDetails["locale"];
                        }));

                        if(count($languagesFiltered) != 1) {
                            continue;
                        }

                        $realEstateImageMeta = new RealEstateImageMetaLocalization();
                        $realEstateImageMeta->setCreatedBy($this->getUser()->getId());
                        $realEstateImageMeta->setInvalidated(0);
                        $realEstateImageMeta->setCreatedAt(new \DateTime());
                        $realEstateImageMeta->setLanguage($languagesFiltered[0]);

                        $realEstateImage->addMetaLocalization($realEstateImageMeta);
                    }

                    $realEstateImageMeta->setAltTag($localeDetails["altTag"]);
                }
            }
        }

        $this->entityManager->persist($realEstate);
        $this->entityManager->flush();

        return new JsonResponse(
            [
                "html" => $this->render("backend/include/flashnotice.html.twig",
                        array(
                            "type" => "success",
                            "message" => $this->translator->trans("Metapodaci za slike su ažurirani")
                        )
                )->getContent()
            ],
            200
        );
    }

}
 