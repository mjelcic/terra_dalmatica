<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Entity\Document;
use App\Entity\Enum\DocumentTypeEnum;
use App\Entity\RealEstate;
use App\Entity\User;
use App\GlobalSettings;
use App\Repository\DocumentRepository;
use App\Repository\RealEstateRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class RealEstateDocumentController extends AbstractController
{
    /**
     * @var RealEstateRepository
     */
    private $realEstateRepository;
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var GlobalSettings
     */
    private $global;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        RealEstateRepository $realEstateRepository,
        DocumentRepository $documentRepository,
        UserRepository $userRepository,
        GlobalSettings $global,
        TranslatorInterface $translator
    ) {
        $this->realEstateRepository = $realEstateRepository;
        $this->documentRepository = $documentRepository;
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->global = $global;
        $this->entityManager = $entityManager;
    }

    public function create(int $id, Request $request)
    {
        $realEstate = $this->realEstateRepository->find($id);

        if(!$realEstate instanceof RealEstate) {
            return new Response($this->translator->trans("Navedena nekretnina nije pronađena!"), 404);
        }

        if(!$this->validateLoggedUserPermissionOnRealEstate($realEstate)) {
            return new Response($this->translator->trans("Nedozvoljena operacija!"), 403);
        }

        $docType = $request->get("docType");
        if(is_null($docType) || !DocumentTypeEnum::isSupported((int) $docType)) {
            return new Response($this->translator->trans("Nepoznat tip dokumenta!"), 400);
        }

        $docType = (int) $docType;

        $confidential = false;

        if($this->getUser()->getRoles()[0] == "ROLE_ADMIN") {
            $confidential = $request->get("confidential") == "true" ? true : false;
        }

        $basePath = $this->global->getDocumentsPath();

        if($docType == DocumentTypeEnum::BILL_OF_SALES) {
            $basePath = $confidential ?
                $this->global->getBillOfSalesConfidentialDocumentsPath() :
                $this->global->getBillOfSalesDocumentsPath();
        }

        /**
         * @var File $uploadedFile
         */
        $uploadedFile = $request->files->get('reference');

        $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();
        $mimeType = $uploadedFile->getMimeType();

        $fullPath = $basePath . $realEstate->getId();

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        if ($uploadedFile->move($fullPath, $fileName)) {
            $doc = new Document();
            $doc->setCreatedBy($this->getUser()->getId());
            $doc->setFilePath("/" . $fullPath . "/" .$fileName);
            $doc->setDocumentMimeType($mimeType);
            $doc->setName($uploadedFile->getClientOriginalName());
            $doc->setAdminExclusive((int) $confidential);
            $doc->setInvalidated(0);
            $doc->setCreatedAt(new \DateTime());
            $doc->setDocumentType($docType);

            $realEstate->addDocument($doc);

            $this->entityManager->persist($realEstate);
            $this->entityManager->flush();

            return new JsonResponse([
                "id" => $doc->getId(),
                "path" => $doc->getFilePath(),
                "name" => $doc->getName(),
                "mimeType" => $doc->getDocumentMimeType(),
                "adminExclusive" => $doc->getAdminExclusive(),
                "html" => $this->render("backend/include/real_estate_single_document.html.twig", array("doc" => $doc))->getContent()

            ], 200);

        }

        return new JsonResponse([], 400);
    }

    public function setDocumentAdminExclusive(int $documentId, int $realEstateId)
    {
        $realEstate = $this->realEstateRepository->find($realEstateId);

        if(!$realEstate instanceof RealEstate) {
            return new Response($this->translator->trans("Navedena nekretnina nije pronađena!"), 404);
        }

        if($this->getUser()->getRoles()[0] != "ROLE_ADMIN") {
            return new Response($this->translator->trans("Nedozvoljena operacija!"), 403);
        }

        $document = $this->entityManager->getRepository(Document::class)->find($documentId);

        if(!$document instanceof Document) {
            return new Response($this->translator->trans("Navedeni dokument nije pronađen!"), 404);
        }

        if($document->getRealEstate()->getId() != $realEstateId) {
            return new Response($this->translator->trans("Nepodudarni id-evi!"), 404); // should never happen but ok
        }

        if($document->getDocumentType() != DocumentTypeEnum::BILL_OF_SALES) {
            return new Response($this->translator->trans("Samo kupoprodajni dokumenti mogu biti tajni!"), 403);
        }

        $document->setAdminExclusive($document->getAdminExclusive() == 1 ? 0 : 1);

        $this->entityManager->persist($document);
        $this->entityManager->flush();

        return new JsonResponse([], 200);

    }

    public function overview(int $id) {
        $entity = $this->realEstateRepository->find($id);

        if(!$entity instanceof RealEstate) {
            return new Response($this->translator->trans("Navedeni unos nije pronađen!"), 404);
        }

        if(!$this->validateLoggedUserPermissionOnRealEstate($entity)) {
            return new Response($this->translator->trans("Nepripadajući sadržaj!"), 403);
        }

        return $this->render("backend/real_estate_documents.html.twig", ["realEstate" => $entity, "role" => $this->getUser()->getRoles()[0]]);
    }

    /**
     * @param RealEstate $realEstate
     * @return bool
     */
    private function validateLoggedUserPermissionOnRealEstate(RealEstate $realEstate): bool
    {
        $loggedUser = $this->getUser();
        /** @var User $userEntity */
        $userEntity = $this->userRepository->findOneBy(["username" => $loggedUser->getUsername()]);

        if($loggedUser->getRoles()[0] == "ROLE_USER" && $userEntity->getId() != $realEstate->getClient()->getId()) {
            return false;
        }

        return true;
    }

}
 