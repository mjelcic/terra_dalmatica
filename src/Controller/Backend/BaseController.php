<?php
/**
 * Created by mijatra
 * Date: 5/8/2018
 * Edited by mijatra
 * Date: 10/27/2018
 */

namespace App\Controller\Backend;


use App\Form\Backend\RealEstateType;
use App\Services\NotificationService;
use App\Utility\ActionType;
use App\Utility\MError;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


abstract class BaseController extends BaseNotificationController
{
    protected $entityManager;
    protected $fullClassName;
    protected $formType;
    protected $translator;
    protected $validator;


    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, $className, $formType, NotificationService $notificationService)
    {
        parent::__construct($notificationService);
        $this->entityManager = $em;
        $this->fullClassName = $className;
        $this->formType = $formType;
        $this->translator = $translator;
        $this->validator = $validator;
    }

    public function create(Request $request)
    {
        return $this->HandleDataSubmit(null, ActionType::create, $request);
    }

    public function edit(Request $request, $id)
    {
        return $this->HandleDataSubmit($id, ActionType::update, $request);
    }

    public function delete(Request $request, $id)
    {
        $id = $id ?? $request->request->get("id");

        $entity = $this->entityManager->getRepository($this->fullClassName)->find($id);

        if (!$entity) {
            return new Response($this->translator->trans("Navedeni unos nije pronađen!"), 404);
        }

        if (!$this->isOkToDelete($entity)) {
            return new Response($this->translator->trans("Brisanje unosa blokirano! Unos koji želite da obrišete se koristi!"),
                500);
        }

        if (is_callable([$entity, 'getInvalidated']))
        {
            $entity->setInvalidated(1);
            $entity->setUpdatedAt(new \DateTime());
            $this->addFlash("success", $this->translator->trans("Zapis obrisan!"));
        } else {
            $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom brisanja. Ne postoji polje invalidated!"));
        }

        try {
            $this->entityManager->flush();
        } catch (\Exception $e) {
            return new Response($this->translator->trans("Greška prilikom pokušaja brisanja!"),
                500);
        }

        if ($request->isXmlHttpRequest()) {
            return new Response($this->generateUrl($this->getEntityListRouteName(), array(),
                UrlGeneratorInterface::ABSOLUTE_URL));
        }

        return $this->redirectToRoute($this->getEntityListRouteName());
    }

    public function overview(Request $request)
    {
        $entities = $this->entityManager->getRepository($this->fullClassName)->findBy(['invalidated' => 0], ["created_at" => "DESC"]);
        $templateName = "backend/" . $this->getClassName() . "_list.html.twig";
        return $this->render($templateName, array(
            $this->getClassName() . "List" => $entities,
        ));
    }

    protected function HandleDataSubmit($id, $type, Request $request, $editTemplateOptions = array(), $args = null, $redirectRoute = null)
    {
        $r = new ReflectionClass($this->fullClassName);
        $entity = $r->newInstance($args); //maybe we should add some args in consideration for entities with such constructors???
        $flashMsg = "";

        //if it's update then get the entity from the DB
        if ($type == ActionType::update) {
            $entity = $this->entityManager->getRepository($this->fullClassName)->find($id);

            //If no FE was found return 404
            if (!$entity) {
                throw $this->createNotFoundException($this->translator->trans("Subjekat koji tražite nije pronađen! Pokušajte ponovo!"));
            }
        }

        //create the form
        $form = $this->createForm($this->formType, $entity, array("locale" => $request->getLocale()));

        //handle the request
        $form->handleRequest($request);

        //check if the form is submitted - POST
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $this->beforeEntitySave($entity);

                if ($type == ActionType::create)
                {
                    $entity->setCreatedBy($this->getUser()->getId());
                    $flashMsg = "Uspješno kreirano";
                }
                elseif ($type == ActionType::update)
                {
                    $entity->setUpdatedAt(new \DateTime());
                    $entity->setUpdatedBy($this->getUser()->getId());
                    $flashMsg = "Uspješno ažurirano";
                }

                $this->entityManager->persist($entity);
                try {
                    $this->entityManager->flush();
                    $this->afterEntitySave($entity, $type);
                    $this->addFlash("success", $this->translator->trans($flashMsg));
                } catch (\Exception $e) {
                    $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom sačuvavanja: " . $e->getMessage()));
                }

                //If the POST request is submitted via AJAX then return the link which the client js script will redirect to
                if ($request->isXmlHttpRequest()) {
                    return new Response($this->generateUrl($this->getEntityListRouteName(), array(),
                        UrlGeneratorInterface::ABSOLUTE_URL));
                }
                //if route is passed as parameter
                if($redirectRoute) {
                    return $this->redirectToRoute($redirectRoute);
                }

                //else redirect from here
                return $this->redirectToRoute($this->getEntityListRouteName());
            } else {
                //If the POST request is submitted via AJAX then return a JSON list of errors in format [{ propertyName: "", errorMessage: ""}, ...]
                if ($request->isXmlHttpRequest()) {
                    $errors = $this->validator->validate($entity);
                    $distilledErrs = array();
                    if (count($errors)) {
                        foreach ($errors as $error) {
                            $distilledErrs[] = new MError($error->getPropertyPath(), $error->getMessage());
                        }
                    }

                    return new JsonResponse(array('data' => $distilledErrs), 500);
                }
            }
        }

        //CREATE variable(s) to pass to a template
        //default values are for create form
        $templateOptions = array(
            'form' => $form->createView(),
            'isNew' => $type == ActionType::create
        );

        return $this->render($this->getEditFormTemplateName(), array_merge($templateOptions, $editTemplateOptions));
    }

    protected function getClassName()
    {
        $pieces = explode("\\", strtolower($this->fullClassName));

        return $pieces[count($pieces) - 1];
    }

    protected function getEntityListRouteName()
    {
        return $this->getClassName() . "_list";
    }

    protected function getEditFormTemplateName()
    {
        return "backend/new_" . $this->getClassName() . ".html.twig";
    }


    /**
     * In a class that inherits you should override this method with a logic that decides whether or not
     * an entity should be allowed to be deleted. By default this method returns TRUE!
     *
     * @param $entity
     * @return bool
     */
    protected function isOkToDelete($entity)
    {
        return true;
    }

    /**
     * If there's a need to set "things" on the owning side of possible relations do it here!
     *
     * @param $entity
     */
    protected function beforeEntitySave($entity)
    {

    }

    protected function afterEntitySave($entity, $action_type)
    {

    }

}
