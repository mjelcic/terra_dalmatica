<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/15/2020
 * Time: 2:25 PM
 */

namespace App\Controller\Backend;


use App\Entity\RealEstateType;
use App\Form\Backend\RealEstateTypeType;
use App\Services\NotificationService;
use App\Services\ServiceRealEstateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RealEstateTypeController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, RealEstateType::class, RealEstateTypeType::class, $notificationService);
    }

    /**
     * @param $entity RealEstateType
     */
    protected function beforeEntitySave($entity)
    {
        if($entity->getRealEstateTypeLocalizations())
        {
            foreach ($entity->getRealEstateTypeLocalizations() as $loco){
                if($entity->getId()){
                    $loco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $loco->setCreatedBy($this->getUser()->getId());
                }
            }
        }
    }

    public function select2Source(Request $request, ServiceRealEstateType $serviceRealEstateType)
    {
        $locale = $request->getLocale();
        $query = $request->query->get("q", "");
        $pageLimit = $request->query->get("page_limit", 10);

        $result = $serviceRealEstateType->getForSelect2Localized($query, $pageLimit, $locale);

        return $this->json($result);
    }
}