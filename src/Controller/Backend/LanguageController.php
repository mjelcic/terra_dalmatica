<?php

namespace App\Controller\Backend;

use App\Entity\Language;
use App\Form\Backend\LanguageType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LanguageController extends BaseController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, Language::class, LanguageType::class, $notificationService);
    }
}
