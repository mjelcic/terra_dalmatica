<?php
declare(strict_types=1);

namespace App\Controller\Backend;


use App\Entity\GroundPlan;
use App\GlobalSettings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class GroundPlanController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var GlobalSettings
     */
    private $global;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityManagerInterface $entityManager, GlobalSettings $global, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->global = $global;
        $this->translator = $translator;
    }

    public function addPlan(Request $request) {

        /** @var File $uploadedFile */
        $uploadedFile = $request->files->get('reference');

        $uuid = $request->get('uuid');

        $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();

        $fullPath = $this->global->getRealEstateGroundPlanPath();
        $mimeType = $uploadedFile->getMimeType();
        $originalFileName = $uploadedFile->getFilename();

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        if ($uploadedFile->move($fullPath, $fileName)) {
            $plan = new GroundPlan();
            $plan->setUuid($uuid);
            $plan->setCreatedBy($this->getUser()->getId());
            $plan->setFilePath($fileName);
            $plan->setFilePath("/" . $fullPath . "/" . $fileName);
            $plan->setInvalidated(0);
            $plan->setCreatedAt(new \DateTime());
            $plan->setName($originalFileName);
            $plan->setMimeType($mimeType);

            $this->entityManager->persist($plan);
            $this->entityManager->flush();

            return new JsonResponse([
                "uuid" => $plan->getUuid(),
                "html" => $this->render("backend/include/ground_plan.html.twig", array("gp" => $plan, "isNew" => true))->getContent()

            ], 200);

        }

        return new JsonResponse([], 400);
    }

}
 