<?php

namespace App\Controller\Backend;


use App\Entity\Language;
use App\Services\SitemapService;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SitemapController extends AbstractController
{

    protected $em;
    protected $sitemapService;
    protected $fs;

    function __construct(EntityManagerInterface $em, SitemapService $sitemapService, Filesystem $filesystem)
    {
        $this->em = $em;
        $this->sitemapService = $sitemapService;
        $this->fs = $filesystem;
    }

    public function createSitemap(Request $request)
    {
        $urls = array();
        $hostname = $request->getSchemeAndHttpHost();
        $languages = $this->em->getRepository(Language::class)->findAll();

        //get urls for each language
        foreach ($languages as $language) {
            $locale = $language->getCode();

            //get static urls
            $urls = array_merge($urls, $this->sitemapService->getStaticUrls($locale));

            //get predefined content
            $urls = array_merge($urls, $this->sitemapService->getPredefinedContentUrls($locale));

            //get category pages
            $urls = array_merge($urls, $this->sitemapService->getCategoryPagesUrls($locale));

            //get real estates
            $urls = array_merge($urls, $this->sitemapService->getRealEstateUrls($locale));
        }

        unlink('sitemap.xml');

        $this->fs->dumpFile('sitemap.xml', $this->renderView('sitemap/sitemap.html.twig', array('urls' => $urls,
            'hostname' => $hostname)));


        return $this->json(array("message"=>"sitemap.xml created"),200);
    }
}
