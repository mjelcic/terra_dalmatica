<?php

namespace App\Controller\Backend;

use App\Entity\Enum\RealEstateTypeEnum;
use App\Entity\Feature;
use App\Entity\GroundPlan;
use App\Entity\Language;
use App\Entity\RealEstate;
use App\Entity\RealEstateImage;
use App\Entity\RealEstateLocalization;
use App\Entity\RealEstateMetaLocalization;
use App\Entity\Status;
use App\Entity\User;
use App\Form\Backend\RealEstate\AdminRealEstateCreateType;
use App\Form\Backend\RealEstate\AdminRealEstateEditType;
use App\Form\Backend\RealEstate\ClientRealEstateCreateType;
use App\Form\Backend\RealEstate\ClientRealEstateEditType;
use App\Form\Backend\RealEstateType;
use App\GlobalSettings;
use App\Services\Export\FacebookService;
use App\Services\Export\TwitterService;
use App\Services\Export\ImmobilienScoutService;
use App\Services\Export\ZooplaService;
use App\Services\NotificationService;
use App\Services\Preview\RealEstateEntityPreviewDtoTransformer;
use App\Services\Preview\RealEstatePreviewDto;
use App\Services\ServiceLanguage;
use App\Services\ServiceRealEstate;
use App\Utility\ActionType;
use App\Utility\EntityHelper;
use App\Utility\Factory\LocalizedEntityFactory;
use App\Utility\Slug\Slugifier;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Swift_Mailer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RealEstateController extends BaseLocalizedEntityWithMetaController
{
    /**
     * @var GlobalSettings
     */
    private $global;
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Slugifier
     */
    private $slugifier;

    /**
     * @var EntityHelper
     */
    private $entityHelper;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var FacebookService
     */
    private $facebookService;
    /**
     * @var TwitterService
     */
    private $twitterService;
    /**
     * @var ImmobilienScoutService
     */
    private $immobilienScoutService;
    /*
     * @var ZooplaService
     */
    private $zooplaService;

    function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        ValidatorInterface $validator,
        GlobalSettings $global,
        NotificationService $notificationService,
        Swift_Mailer $mailer,
        Slugifier $slugifier,
        EntityHelper $entityHelper,
        SerializerInterface $serializer,
        SessionInterface $session,
        LoggerInterface $logger,
        FacebookService $facebookService,
        TwitterService $twitterService,
        ImmobilienScoutService $immobilienScoutService,
        ZooplaService $zooplaService
    )
    {
        parent::__construct($em, $translator, $validator, RealEstate::class, RealEstateType::class, $notificationService);
        $this->global = $global;
        $this->mailer = $mailer;
        $this->slugifier = $slugifier;
        $this->entityHelper = $entityHelper;
        $this->serializer = $serializer;
        $this->session = $session;
        $this->logger = $logger;
        $this->facebookService = $facebookService;
        $this->twitterService = $twitterService;
        $this->immobilienScoutService = $immobilienScoutService;
        $this->zooplaService = $zooplaService;
    }

    public function create(Request $request)
    {
        $localizations = LocalizedEntityFactory::createLocalizations($this->entityManager, RealEstate::class);
        $metaLocalizations = LocalizedEntityFactory::createMetaLocalizations($this->entityManager, RealEstate::class);

        $roles = $this->getUser()->getRoles();
        /** @var User $userEntity */
        $userEntity = $this->entityManager->getRepository(User::class)->findOneBy(["username" => $this->getUser()->getUsername()]);

        $userRole = str_replace("ROLE_", "", $roles[0]);

        $args = array("localizations" => $localizations, "metaLocalizations" => $metaLocalizations);

        $r = new ReflectionClass($this->fullClassName);
        /** @var RealEstate $entity */
        $entity = $r->newInstance($args); //maybe we should add some args in consideration for entities with such constructors???
        $flashMsg = "";

        if (in_array($roles[0], ["ROLE_ADMIN", "ROLE_EMPLOYEE"])) {
            $formType = AdminRealEstateCreateType::class;
        } else {
            $formType = ClientRealEstateCreateType::class;
        }

        if ($roles[0] == "ROLE_EMPLOYEE") {
            $entity->setAgent($userEntity);
        }

        $this->updateEntityFromSessionPreviewObject($entity, $request);

        $form = $this->createForm($formType, $entity, array("locale" => $request->getLocale()));

        $form->handleRequest($request);

        //check if the form is submitted - POST
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $this->beforeEntitySave($entity);

                $statusRepo = $this->entityManager->getRepository(Status::class);

                /** @var  Status $published */
                $published = $statusRepo->findOneBy(["displayId" => "PUBLISHED"]);
                /** @var  Status $created */
                $created = $statusRepo->findOneBy(["displayId" => "CREATED"]);

                $entity->setCreatedBy($this->getUser()->getId());

                $flashMsg = "Oglas kreiran";

                if (in_array($userRole, ["ADMIN", "EMPLOYEE"])) {
                    if ($form->get("addAndPublish")->isClicked()) {
                        $this->validateMandatoryLocalizationInputs($entity);
                        $flashMsg = "Oglas kreiran i objavljen";
                        $entity->setStatus($published);
                    } else {
                        $this->validateMandatoryLocalizationInputsForOneLanguage($entity);
                        $this->clearNonCompletedLocalizations($entity);
                        $entity->setStatus($created);
                    }
                } else {
                    $this->validateMandatoryLocalizationInputsForOneLanguage($entity);
                    $this->clearNonCompletedLocalizations($entity);
                    $entity->setStatus($created);
                    $entity->setClient($userEntity);
                }

                $this->handleImages($entity, $request);
                $this->handleGroundPlans($entity, $request);

                if ($form->has("preview") && $form->get("preview")->isClicked()) {
                    return $this->redirectToPreview($entity, $request);
                }

                $this->entityManager->persist($entity);
                $redirect = $userRole == "USER" ? "client_realestate_list" : "realestate_list";

                try {
                    $this->entityManager->flush();
                    $this->addFlash("success", $this->translator->trans($flashMsg));
                    $this->afterEntitySave($entity, ActionType::create);

                } catch (\Exception $e) {
                    $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom spašavanja oglasa: " . $e->getMessage()));
                    return $this->redirectToRoute($redirect, ["id" => $entity->getId()]);
                }

                if ($userRole == "USER") {
                    $this->sendRealEstateCreatedMailToAdmins($entity);
                    $this->notificationService->createNotification(
                        "ROLE_ADMIN",
                        null,
                        $userEntity,
                        "je kreirao oglas ",
                        $entity,
                        null,
                        "Molimo slijedite link na oglas za provjeru i potvrdu"
                    );
                }

                return $this->redirectToRoute($redirect, ["id" => $entity->getId()]);
            }
        }

        $templateOptions = array(
            'form' => $form->createView(),
            'isNew' => true,
            'userRole' => $roles[0],
            'locale' => $request->getLocale(),
            'images' => [
                'add' => $this->getImagesAddedInSession($request->get("hash")),
                'remove' => $this->session->get("image_remove_" . $request->get("hash")) ?? [],
                'cover' => $this->session->get("cover_image_" . $request->get("hash")),
            ],
            'plans' => [
                'add' => $this->getGroundPlansAddedInSession($request->get("hash")),
                'remove' => $this->session->get("plans_remove_" . $request->get("hash")) ?? []
            ]
        );

        return $this->render($this->getEditFormTemplateName(), $templateOptions);
    }

    public function edit(Request $request, $id)
    {
        /** @var RealEstate $entity */
        $entity = $this->entityManager->getRepository($this->fullClassName)->find($id);
        $userEntity = $this->entityManager->getRepository(User::class)->findOneBy(["username" => $this->getUser()->getUsername()]);

        //If no FE was found return 404
        if (!$entity) {
            throw $this->createNotFoundException($this->translator->trans("Subjekat koji tražite nije pronađen! Pokušajte ponovo!"));
        }

        if (!$this->validateLoggedUserPermissionOnRealEstate($entity)) {
            return new Response($this->translator->trans("Nedozvoljen pregled!"), 403);
        }

        $userRole = $this->getUser()->getRoles()[0];

        if (in_array($userRole, ["ROLE_ADMIN", "ROLE_EMPLOYEE"])) {
            $formType = AdminRealEstateEditType::class;
        } else {
            $formType = ClientRealEstateEditType::class;
        }
        $this->addMissingLocalizations($entity);

        if ($userRole == "ROLE_EMPLOYEE" && $entity->getAgent() == null) {
            $entity->setAgent($userEntity);
        }

        $this->updateEntityFromSessionPreviewObject($entity, $request);

        //create the form
        $form = $this->createForm($formType, $entity, array("locale" => $request->getLocale()));

        //handle the request
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $success = false;
            $flashMsg = "Podaci azuirirani";

            $this->handleImages($entity, $request);
            $this->handleGroundPlans($entity, $request);

            if ($form->get("edit")->isClicked()) {
                $success = $this->handleSimpleEdit($form, $entity);
            } else if ($form->get("editPublished")->isClicked()) {
                $success = $this->handleEditPublished($form, $entity);
            } else if ($form->get("editAndPublish")->isClicked()) {
                $success = $this->handleEditAndPublish($entity, $form);
                $flashMsg = "Oglas odobren i objavljen na stranici";
            } else if ($form->get("setRejected")->isClicked()) {
                $success = $this->handleReject($form, $entity);
                $flashMsg = "Oglas odbijen";
            } else if ($form->get("setInProgress")->isClicked()) {
                $success = $this->handleSetInProgress($form, $entity);
                $flashMsg = "Oglas postavljen pred realizaciju";
            } else if ($form->get("setCompleted")->isClicked()) {
                $success = $this->handleSetCompleted($form, $entity);
                $flashMsg = "Oglas postavljen kao realiziran";
            } else if ($form->get("preview")->isClicked()) {
                $this->clearNonCompletedLocalizations($entity);
                $this->beforeEntitySave($entity);
                return $this->redirectToPreview($entity, $request);
            }

            if ($success) {
                $this->addFlash("success", $this->translator->trans($flashMsg));
                $redirect = $userRole == "ROLE_USER" ? "client_realestate_list" : "realestate_list";
                return $this->redirectToRoute($redirect);
            }
        }

        $sessionHash = $request->get("hash");

        $templateOptions = array(
            'form' => $form->createView(),
            'isNew' => false,
            'userRole' => $userRole,
            'locale' => $request->getLocale(),
            'realEstate' => $entity,
            'images' => [
                'add' => $this->getImagesAddedInSession($sessionHash),
                'remove' => $this->session->get("image_remove_" . $sessionHash) ?? [],
                'cover' => $this->session->get("cover_image_" . $sessionHash)
            ],
            'plans' => [
                'add' => $this->getGroundPlansAddedInSession($sessionHash),
                'remove' => $this->session->get("plans_remove_" . $sessionHash) ?? []
            ]
        );

        return $this->render($this->getEditFormTemplateName(), $templateOptions);
    }

    public function overview(Request $request)
    {
        $repo = $this->entityManager->getRepository(RealEstate::class);
        $userEntity = $this->entityManager->getRepository(User::class)->findOneBy(["username" => $this->getUser()->getUsername()]);

        $templateName = "backend/realestate_list.html.twig";
        $filters = array("invalidated" => 0);
        $roles = $this->getUser()->getRoles();

        if (in_array("ROLE_USER", $roles)) {
            $filters["client"] = $userEntity;
        }

        $entities = $repo->findBy($filters, ["created_at" => "DESC"]);

        $status = $request->query->get("status");

        return $this->render($templateName, array(
            "realestateList" => $entities,
            "status" => $status
        ));
    }

    public function previewApply(string $hash, RealEstateEntityPreviewDtoTransformer $transformer, Request $request)
    {
        $serializedDto = $this->session->get("preview_" . $hash);

        if ($serializedDto == null) {
            throw new Exception("Real Estate preview not found!");
        }

        /** @var RealEstatePreviewDto $dtoObj */
        $dtoObj = $this->serializer->deserialize($serializedDto, RealEstatePreviewDto::class, "json");

        $realEstate = $transformer->realEstateFromDto($dtoObj);
        $this->beforeEntitySave($realEstate);
        $realEstate->setCreatedBy($this->getUser()->getId());

        if ($dtoObj->id) {
            $flashMsg = "Izmjene primjenjene na oglas!";
            $realEstate->setUpdatedBy($this->getUser()->getId());
        } else {
            $flashMsg = "Novi oglas dodan!";
            $realEstate->setCreatedBy($this->getUser()->getId());
        }

        try {
            $this->entityManager->flush();
            $this->addFlash("success", $this->translator->trans($flashMsg));
            $this->afterEntitySave($realEstate, ActionType::create);

        } catch (\Exception $e) {
            $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom spašavanja oglasa: " . $e->getMessage()));
            return $this->redirectToRoute("realestate_list", ["id" => $realEstate->getId()]);
        }

        return $this->redirectToRoute("realestate_list", ["id" => $realEstate->getId()]);

    }

    public function previewPublish(string $hash, RealEstateEntityPreviewDtoTransformer $transformer)
    {
        $serializedDto = $this->session->get("preview_" . $hash);

        if ($serializedDto == null) {
            throw new Exception("Real Estate preview not found!");
        }

        /** @var RealEstatePreviewDto $dtoObj */
        $dtoObj = $this->serializer->deserialize($serializedDto, RealEstatePreviewDto::class, "json");

        $realEstate = $transformer->realEstateFromDto($dtoObj);
        $this->beforeEntitySave($realEstate);
        $realEstate->setCreatedBy($this->getUser()->getId());

        $realEstate->setUpdatedBy($this->getUser()->getId());

        try {
            $this->handleEditAndPublish($realEstate);

        } catch (\Exception $e) {
            $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom obnove oglasa: " . $e->getMessage()));
            return $this->redirectToRoute("realestate_list", ["id" => $realEstate->getId()]);
        }

        $this->addFlash("success", $this->translator->trans("Izmjene su prihvaćene i oglas objavljen!"));

        return $this->redirectToRoute("realestate_list", ["id" => $realEstate->getId()]);
    }


    /**
     * @param $entity RealEstate
     */
    protected function beforeEntitySave($entity)
    {
        if ($entity->getRealEstateLocalizations()) {
            foreach ($entity->getRealEstateLocalizations() as $reLoco) {
                if (!$reLoco->getSlug()) {
                    //set unique slug
                    $reLoco->setSlug($this->entityHelper->getUniqueFieldValue(RealEstateLocalization::class, "slug", $this->slugifier->slugify($reLoco->getTitle()), $reLoco->getId(), $reLoco->getLanguage()->getCode()));
                }

                if ($reLoco->getId()) {
                    $reLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $reLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }

        if ($entity->getRealEstateMetaLocalizations()) {
            foreach ($entity->getRealEstateMetaLocalizations() as $remLoco) {
                if ($remLoco->getId()) {
                    $remLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $remLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }

        if ($entity->getRealEstateFeatures()) {
            foreach ($entity->getRealEstateFeatures() as $rlFeature) {
                if ($rlFeature->getId()) {
                    $rlFeature->setUpdatedBy($this->getUser()->getId());
                } else {
                    $rlFeature->setCreatedBy($this->getUser()->getId());
                }
            }
        }

        if ($entity->getUid() == null) {
            $entity->setUid($this->entityHelper->getNextIntegerForField(RealEstate::class, "uid"));
        }

        $entity->setCalculatedPrice();
    }

    private function validateMandatoryLocalizationInputs(RealEstate $realEstate): void
    {
        foreach ($realEstate->getRealEstateLocalizations() as $realEstateLocalization) {
            if (!$realEstateLocalization->getDescription() || !$realEstateLocalization->getTitle()) {
                throw new Exception($this->translator->trans("Molimo unesite naslov i opis za sve jezike"));
            }
        }
    }

    private function validateMandatoryLocalizationInputsForOneLanguage(RealEstate $realEstate): void
    {
        foreach ($realEstate->getRealEstateLocalizations() as $realEstateLocalization) {
            if ($realEstateLocalization->getDescription() && $realEstateLocalization->getTitle()) {
                return; // condition met
            }
        }
        throw new Exception($this->translator->trans("Molimo unesite naslov i opis za barem jedan jezik"));
    }

    private function clearNonCompletedLocalizations(RealEstate $realEstate): void
    {
        foreach ($realEstate->getRealEstateLocalizations() as $realEstateLocalization) {
            if (!$realEstateLocalization->getDescription() || !$realEstateLocalization->getTitle()) {
                $language = $realEstateLocalization->getLanguage();
                $realEstate->removeRealEstateLocalization($realEstateLocalization);
                $metaLocalization = $realEstate->getRealEstateMetaLocalizations()->filter(
                    function (RealEstateMetaLocalization $metaLocalization) use ($language) {
                        return $metaLocalization->getLanguage()->getId() == $language->getId();
                    })->first();
                $realEstate->removeRealEstateMetaLocalization($metaLocalization);
            }
        }
    }

    /**
     * @param RealEstate $realEstate
     * @throws \ReflectionException
     * Terrible piece of code, I know
     * If client adds real estate, he adds it on single language. Now, if admin/employee edits it, he needs other languages as well
     * So here we populate it. Yeah, this needs much consideration, went too far
     */
    private function addMissingLocalizations(RealEstate $realEstate)
    {
        $serviceLanguage = new ServiceLanguage($this->entityManager);
        $languages = $serviceLanguage->getAll();

        if ($realEstate->getRealEstateLocalizations()->count() < 3) {
            foreach ($languages as $language) {
                if ($realEstate->getRealEstateLocalizations()->filter(function (RealEstateLocalization $l) use ($language) {
                        return $l->getLanguage()->getCode() == $language->getCode();
                    })->count() == 0) {
                    $r = new ReflectionClass(RealEstateLocalization::class);
                    $localization = $r->newInstance();
                    $localization->setLanguage($language);
                    $realEstate->addRealEstateLocalization($localization);
                }
            }
        }

        if ($realEstate->getRealEstateMetaLocalizations()->count() < 3) {
            foreach ($languages as $language) {
                if ($realEstate->getRealEstateMetaLocalizations()->filter(function (RealEstateMetaLocalization $l) use ($language) {
                        return $l->getLanguage()->getCode() == $language->getCode();
                    })->count() == 0) {
                    $r = new ReflectionClass(RealEstateMetaLocalization::class);
                    $localization = $r->newInstance();
                    $localization->setLanguage($language);
                    $realEstate->addRealEstateMetaLocalization($localization);
                }
            }
        }
    }

    /**
     * @param RealEstate $realEstate
     * @return bool
     */
    private function validateLoggedUserPermissionOnRealEstate(RealEstate $realEstate): bool
    {
        $loggedUser = $this->getUser();
        /** @var User $userEntity */
        $userEntity = $this->entityManager->getRepository(User::class)->findOneBy(["username" => $loggedUser->getUsername()]);

        if ($loggedUser->getRoles()[0] == "ROLE_USER" && $userEntity->getId() != $realEstate->getClient()->getId()) {
            return false;
        }

        return true;
    }

    // Edit handle form part

    private function handleSimpleEdit(Form $form, RealEstate $entity)
    {
        $userRole = $this->getUser()->getRoles()[0];

        if (!$form->isValid()) {
            $this->addFlash("error", $this->translator->trans("Nevalidni podaci!"));
            return false;
        }
        $this->validateMandatoryLocalizationInputsForOneLanguage($entity);
        $this->clearNonCompletedLocalizations($entity);
        $this->beforeEntitySave($entity);

        $entity->setUpdatedAt(new \DateTime());
        $entity->setUpdatedBy($this->getUser()->getId());
        if ($userRole == "ROLE_USER") {
            $created = $this->entityManager->getRepository(Status::class)->findOneBy(["displayId" => "CREATED"]);
            $entity->setStatus($created);
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->afterEntitySave($entity, ActionType::update);

        if ($userRole == "ROLE_USER") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getClient(),
                "je uredio oglas ",
                $entity,
                null,
                "Oglas neće biti aktivan na stranici dok se izmjene ne potvrde. Slijedite link za potvrdu."
            );
            if ($entity->getAgent() != null) {
                $this->notificationService->createNotification(
                    "ROLE_EMPLOYEE",
                    $entity->getAgent(),
                    $entity->getClient(),
                    "uredio",
                    $entity,
                    null,
                    "Oglas neće biti aktivan na stranici dok se izmjene ne potvrde. Slijedite link za potvrdu."
                );
            }
            $this->sendRealEstateUpdatedMailToAdmins($entity);
        }

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                "uredio",
                $entity,
                null,
                "Izmjene su vidljive na stranici.");
        }

        return true;
    }

    private function handleEditPublished(Form $form, RealEstate $entity)
    {
        $this->validateMandatoryLocalizationInputs($entity);

        $userRole = $this->getUser()->getRoles()[0];

        if (!$form->isValid()) {
            $this->addFlash("error", $this->translator->trans("Nevalidni podaci!"));
            return false;
        }
        if ($userRole == "ROLE_USER") {
            throw new Exception("Unauthorized for action");
        }

        $this->beforeEntitySave($entity);

        $entity->setUpdatedAt(new \DateTime());
        $entity->setUpdatedBy($this->getUser()->getId());


        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->afterEntitySave($entity, ActionType::update);

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                "objavio",
                $entity
            );
        }

        return true;
    }

    private function handleEditAndPublish(RealEstate $entity, ?FormInterface $form = null)
    {
        $userRole = $this->getUser()->getRoles()[0];
        $statusRepo = $this->entityManager->getRepository(Status::class);

        $this->validateMandatoryLocalizationInputs($entity);


        if ($form instanceof FormInterface && !$form->isValid()) {
            $this->addFlash("error", $this->translator->trans("Nevalidni podaci!"));
            return false;
        }

        if ($userRole == "ROLE_USER") {
            $this->addFlash("error", $this->translator->trans("Nedozvoljena akcija!"));
            return false;
        }

        if ($entity->getStatus()->getDisplayId() != "CREATED") {
            $this->addFlash("error", $this->translator->trans("Nije moguće postaviti nekretninu kao odobrenu"));
            return false;
        }

        $this->beforeEntitySave($entity);

        $accepted = $statusRepo->findOneBy(["displayId" => "PUBLISHED"]);
        $entity->setStatus($accepted);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->afterEntitySave($entity, ActionType::update);

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                "objavio",
                $entity
            );
        }

        $this->sendRealEstateAcceptedMailToClient($entity);

        return true;
    }

    private function handleReject(Form $form, RealEstate $entity): bool
    {
        $userRole = $this->getUser()->getRoles()[0];
        $statusRepo = $this->entityManager->getRepository(Status::class);

        if ($userRole == "ROLE_USER") {
            $this->addFlash("error", $this->translator->trans("Nedozvoljena akcija!"));
            return false;
        }

        if ($entity->getStatus()->getDisplayId() != "CREATED") {
            $this->addFlash("error", $this->translator->trans("Nije moguće postaviti nekretninu kao odbijenu"));
            return false;
        }
        $this->clearNonCompletedLocalizations($entity);
        $rejected = $statusRepo->findOneBy(["displayId" => "REJECTED"]);
        $entity->setStatus($rejected);
        $this->beforeEntitySave($entity);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->sendRealEstateRejectedMailToClient($entity);

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                " odbio ",
                $entity);
        }

        $this->updateOnZoopla($entity);

        return true;
    }

    private function handleSetInProgress(Form $form, RealEstate $entity)
    {
        $userRole = $this->getUser()->getRoles()[0];
        $statusRepo = $this->entityManager->getRepository(Status::class);

        $this->validateMandatoryLocalizationInputs($entity);

        if (!$form->isValid()) {
            $this->addFlash("error", $this->translator->trans("Nevalidni podaci!"));
            return false;
        }

        if ($userRole == "ROLE_USER") {
            $this->addFlash("error", $this->translator->trans("Nedozvoljena akcija!"));
            return false;
        }

        if ($entity->getStatus()->getDisplayId() != "PUBLISHED") {
            $this->addFlash("error", $this->translator->trans("Nije moguće postaviti nekretninu kao pri realizaciji"));
            return false;
        }
        $inProgress = $statusRepo->findOneBy(["displayId" => "IN_PROGRESS"]);
        $entity->setStatus($inProgress);

        $this->beforeEntitySave($entity);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->afterEntitySave($entity, ActionType::update);

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                " postavio kao pri realizaciji ",
                $entity);
        }

        $this->sendRealEstateSetInProgressMailToClient($entity);

        return true;
    }

    private function handleSetCompleted(Form $form, RealEstate $entity)
    {
        $userRole = $this->getUser()->getRoles()[0];
        $statusRepo = $this->entityManager->getRepository(Status::class);

        $this->validateMandatoryLocalizationInputs($entity);

        if (!$form->isValid()) {
            $this->addFlash("error", $this->translator->trans("Nevalidni podaci!"));
            return false;
        }

        if ($userRole == "ROLE_USER") {
            $this->addFlash("error", $this->translator->trans("Nedozvoljena akcija!"));
            return false;
        }

        if (!in_array($entity->getStatus()->getDisplayId(), ["PUBLISHED", "IN_PROGRESS"])) {
            $this->addFlash("error", $this->translator->trans("Nije moguće postaviti nekretninu kao pri završenu"));
            return false;
        }
        $completed = $statusRepo->findOneBy(["displayId" => "COMPLETED"]);
        $entity->setStatus($completed);
        $this->beforeEntitySave($entity);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        if ($userRole == "ROLE_EMPLOYEE") {
            $this->notificationService->createNotification(
                "ROLE_ADMIN",
                null,
                $entity->getAgent(),
                " postavio kao realiziran",
                $entity
            );
        }

        $this->sendRealEstateCompletedMailToClient($entity);
        $this->updateOnZoopla($entity);

        return true;
    }

    // mailer part

    private function sendRealEstateCreatedMailToAdmins(RealEstate $realEstate)
    {
        /** @var User[] $admins */
        $admins = $this->entityManager->getRepository(User::class)->findBy(["roles" => "ROLE_ADMIN", "invalidated" => 0]);

        $adminEmails = array_map(function (User $admin) {
            return $admin->getEmail();
        }, $admins);

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($adminEmails)
            ->setBody($this->renderView(
                'email/real_estate_created_hr.html.twig',
                array(
                    'clientUsername' => $realEstate->getClient()->getUsername(),
                    'realEstateLocation' => $realEstate->getLocation()->getLocationLocalizations()->first()->getName(),
                    'realEstateId' => $realEstate->getId(),
                    'realEstateType' => $realEstate->getRealEstateType()->getRealEstateTypeLocalizations()->first()->getName(),
                    'realEstateTitle' => $realEstate->getRealEstateLocalizations()->first()->getTitle()
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    private function sendRealEstateUpdatedMailToAdmins(RealEstate $realEstate)
    {
        /** @var User[] $admins */
        $admins = $this->entityManager->getRepository(User::class)->findBy(["roles" => "ROLE_ADMIN", "invalidated" => 0]);

        $adminEmails = array_map(function (User $admin) {
            return $admin->getEmail();
        }, $admins);
        if ($realEstate->getAgent() != null) {
            $adminEmails[] = $realEstate->getAgent()->getEmail();
        }

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($adminEmails)
            ->setBody($this->renderView(
                'email/real_estate_updated_hr.html.twig',
                array(
                    'clientUsername' => $realEstate->getClient()->getUsername(),
                    'realEstateId' => $realEstate->getId(),
                    'realEstateTitle' => $realEstate->getRealEstateLocalizations()->first()->getTitle()
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    private function sendRealEstateAcceptedMailToClient(RealEstate $realEstate)
    {
        $locale = $realEstate->getClient()->getEmailLanguageCodeOrDefault();

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($realEstate->getClient()->getEmail())
            ->setBody($this->renderView(
                'email/real_estate_accepted_' . $locale . '.html.twig',
                array(
                    'locals' => $realEstate->getRealEstateLocalizations()->map(
                        function (RealEstateLocalization $localization) {
                            return [
                                'slug' => $localization->getSlug(),
                                'locale' => $localization->getLanguage()->getCode()
                            ];
                        }
                    ),
                    'realEstateId' => $realEstate->getId(),
                    'agent' => $realEstate->getAgent()
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    private function sendRealEstateRejectedMailToClient(RealEstate $realEstate)
    {
        $locale = $realEstate->getClient()->getEmailLanguageCodeOrDefault();

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($realEstate->getClient()->getEmail())
            ->setBody($this->renderView(
                'email/real_estate_rejected_' . $locale . '.html.twig',
                array(
                    'realEstateId' => $realEstate->getId(),
                    'realEstateTitle' => $realEstate->getSafeRealEstateLocalizatonTitle($locale)
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    private function sendRealEstateCompletedMailToClient(RealEstate $realEstate)
    {
        $locale = $realEstate->getClient()->getEmailLanguageCodeOrDefault();

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($realEstate->getClient()->getEmail())
            ->setBody($this->renderView(
                'email/real_estate_completed_' . $locale . '.html.twig',
                array(
                    'realEstateId' => $realEstate->getId(),
                    'realEstateTitle' => $realEstate->getSafeRealEstateLocalizatonTitle($locale)
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    private function sendRealEstateSetInProgressMailToClient(RealEstate $realEstate)
    {
        $locale = $realEstate->getClient()->getEmailLanguageCodeOrDefault();

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans("Terra dalmatica"))
            ->setFrom($this->global->getFromMail())
            ->setTo($realEstate->getClient()->getEmail())
            ->setBody($this->renderView(
                'email/real_estate_inprogress_' . $locale . '.html.twig',
                array(
                    'realEstateId' => $realEstate->getId(),
                    'realEstateTitle' => $realEstate->getSafeRealEstateLocalizatonTitle($locale)
                )),
                'text/html'
            );
        $this->mailer->send($message);
    }

    public function featuredSwitch(TranslatorInterface $translator, ServiceRealEstate $serviceRealEstate, $id)
    {
        $realEstate = $this->entityManager->getRepository(RealEstate::class)->find($id);
        if (!$realEstate) {
            return new Response($translator->trans("Nije pronađen sadržaj"), Response::HTTP_NOT_FOUND);
        }

        if ($realEstate->isFeatured() == true) {
            $realEstate->setFeatured(false);
            $this->entityManager->flush();
            return $this->json(array("value" => 0));
        }

        if ($realEstate->isFeatured() == false) {
            //If the limit of featured real estates is met then abort and return message
            $featuredCount = $serviceRealEstate->getFeaturedRealEstatesCount();
            if ($featuredCount < GlobalSettings::MaxFeaturedRealEstatesCount) {
                $realEstate->setFeatured(true);
                $this->entityManager->flush();
                return $this->json(array("value" => 1));
            }

            if ($featuredCount >= GlobalSettings::MaxFeaturedRealEstatesCount) {
                $message = $translator->trans("Dosegnut je limit od _count_ nekretnina! Ukoliko želite da izdvojite novu nekretninu morate prvo ukloniti svojstvo sa neke od već izdvojenih.");
                return new Response(str_replace("_count_", GlobalSettings::MaxFeaturedRealEstatesCount, $message), 500);
            }
        }

        return new Response("Unknown error!", 500);
    }

    private function handleImages(RealEstate $realEstate, Request $request)
    {
        $realEstateImagesRepo = $this->entityManager->getRepository(RealEstateImage::class);

        if ($request->request->get("images_add")) {
            $imageIds = $request->request->get("images_add");
            foreach ($imageIds as $imageId) {
                $image = $realEstateImagesRepo->find($imageId);
                $realEstate->addRealEstateImage($image);
            }
        }

        if ($request->request->get("images_remove")) {
            $imageIds = $request->request->get("images_remove");
            $immoAttachmentIds = [];
            foreach ($imageIds as $imageId) {
                $image = $realEstateImagesRepo->find($imageId);
                if($image->getImmoAttachmentId() != null){
                    $immoAttachmentIds[] = $image->getImmoAttachmentId();
                }
                $image->setInvalidated(true);
                $realEstate->removeRealEstateImage($image);
            }
            if(count($immoAttachmentIds) > 0){
                $this->session->set($realEstate->getId() . "_immo_delete_images", $immoAttachmentIds);
            }
        }

        foreach ($realEstate->getRealEstateImages() as $rei) {
            $rei->setCover(false);
        }

        $coverSet = false;

        if ($request->request->get("cover_image")) {
            $coverImageId = (int)$request->request->get("cover_image");
            foreach ($realEstate->getRealEstateImages() as $rei) {
                if ($rei->getId() == $coverImageId) {
                    $rei->setCover(true);
                    $coverSet = true;
                }
            }
        }

        if (!$coverSet && $realEstate->getRealEstateImages()->count() > 0) {
            $realEstate->getRealEstateImages()->first()->setCover(true);
        }
    }

    private function setImageDataToSession(Request $request, string $hash)
    {
        if ($request->request->get("images_add")) {
            $this->session->set("image_add_" . $hash, $request->request->get("images_add"));
        }

        if ($request->request->get("images_remove")) {
            $this->session->set("image_remove_" . $hash, $request->request->get("images_remove"));
        }

        if ($request->request->get("cover_image")) {
            $this->session->set("cover_image_" . $hash, $request->request->get("cover_image"));
        }
    }

    // leave it here for now, move somewhere
    function getImagesAddedInSession(?string $sessionHash)
    {
        if (is_null($sessionHash)) {
            return [];
        }
        $imageIds = $this->session->get("image_add_" . $sessionHash);

        if (!$imageIds) {
            return [];
        }

        $repo = $this->entityManager->getRepository(RealEstateImage::class);

        return $repo->findBy(["id" => $imageIds]);

    }

    private function handleGroundPlans(RealEstate $realEstate, Request $request)
    {
        $groundPlanRepo = $this->entityManager->getRepository(GroundPlan::class);

        if ($request->request->get("plans_add")) {
            $planIds = $request->request->get("plans_add");
            foreach ($planIds as $planId) {
                $plan = $groundPlanRepo->find($planId);
                $realEstate->addGroundPlan($plan);
            }
        }

        if ($request->request->get("plans_remove")) {
            $planIds = $request->request->get("plans_remove");
            foreach ($planIds as $planId) {
                $plan = $groundPlanRepo->find($planId);
                $plan->setInvalidated(true);
                $realEstate->removeGroundPlan($plan);
            }
        }
    }

    private function setGroundPlanDataToSession(Request $request, string $hash)
    {
        if ($request->request->get("plans_add")) {
            $this->session->set("plans_add_" . $hash, $request->request->get("plans_add"));
        }

        if ($request->request->get("plans_remove")) {
            $this->session->set("plans_remove_" . $hash, $request->request->get("plans_remove"));
        }
    }

    // leave it here for now, move somewhere
    function getGroundPlansAddedInSession(?string $sessionHash)
    {
        if (is_null($sessionHash)) {
            return [];
        }
        $planIds = $this->session->get("plans_add_" . $sessionHash);

        if (!$planIds) {
            return [];
        }

        $repo = $this->entityManager->getRepository(GroundPlan::class);

        return $repo->findBy(["id" => $planIds]);

    }

    private function redirectToPreview(RealEstate $realEstate, Request $request)
    {
        $dto = RealEstateEntityPreviewDtoTransformer::toDto($realEstate);
        $hash = $request->query->get('hash');
        if (!isset($hash) || is_null($hash)) {
            $hash = mt_rand(1000000, 10000000);
        }

        $slug = $realEstate->getRealEstateLocalizations()->first()->getSlug();
        $this->session->set("preview_" . $hash, $this->serializer->serialize($dto, "json"));
        $this->setImageDataToSession($request, $hash);
        $this->setGroundPlanDataToSession($request, $hash);

        return $this->redirectToRoute(
            "real_estate_preview",
            [
                "hash" => $hash,
                "slug" => $slug
            ]
        );
    }

    private function updateEntityFromSessionPreviewObject(RealEstate $realEstate, Request $request)
    {
        $hash = $request->get("hash");
        if (!isset($hash) || is_null($hash)) {
            return;
        }

        $previewSerialized = $this->session->get("preview_" . $hash);

        if (is_null($previewSerialized)) {
            return;
        }

        /** @var RealEstatePreviewDto $previewObj */
        $previewObj = $this->serializer->deserialize($previewSerialized, RealEstatePreviewDto::class, "json");
        $realEstate->setLatitude($previewObj->latitude);
        $realEstate->setLongitude($previewObj->longitude);

    }

    /**
     * @param $entity RealEstate
     * @param $action_type
     */
    protected function afterEntitySave($entity, $action_type)
    {
        try {
            $status = $entity->getStatus()->getDisplayId();
            if ($status == "PUBLISHED") {
                if (!$entity->getFbPostId()) {
                    if (GlobalSettings::publishToSocial() == true) {
                        $response = $this->facebookService->post($entity->getId(), "en");

                        if ($response->getStatusCode() == 200) {
                            $this->addFlash("success", $this->translator->trans("Nekretnina objavljena na facebook"));
                        } else {
                            $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na facebook"));
                        }
                    }
                }
                if (GlobalSettings::publishToSocial() == true) {
                    if (!$entity->getTwitterPostId()) {
                        $response = $this->twitterService->post($entity->getId(), "en");
                        if ($response) {
                            $this->addFlash("success", $this->translator->trans("Nekretnina objavljena na twitter"));
                        } else {
                            $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na twitter"));
                        }
                    }
                }
            }


            $this->updateOnZoopla($entity);

            if ($status == "CREATED" || $status == "PUBLISHED" || $status == "IN_PROGRESS") {
                $serviceLanguage = new ServiceLanguage($this->entityManager);
                $serviceRealEstate = new ServiceRealEstate($this->entityManager);
                $languages = $serviceLanguage->getAll();

                $this->transcribeFeatures($entity, $serviceRealEstate, $languages, "FEATURE_MAP");

                $this->entityManager->flush();
            }
            if ($status == "PUBLISHED") {
                if ($this->immobilienScoutService->validForExport($entity) === true) {
                    $exportType = $entity->getImmoscoutExported() !== true ? "EXPORT" : "CHANGE";
                    $exportSuccess = $this->immobilienScoutService->exportRealEstate($entity, $exportType);
                    if ($exportSuccess === true) {
                        $statusMessage = $this->translator->trans("Podaci za nekretninu su uspješno " . ($exportType == "EXPORT" ? "izvezeni" : "ažurirani") . " na ImmobilienScout24");
                        //Check if there are any images to delete
                        $imagesToDelete = $this->session->get($entity->getId() . "_immo_delete_images");
                        if($imagesToDelete != null && is_array($imagesToDelete)){
                            $this->immobilienScoutService->deleteAttachmentsForRealEstate($entity, $imagesToDelete);
                            $this->session->remove($entity->getId() . "_immo_delete_images");
                        }
                        //Export images for real estate
                        $aStatus = $this->immobilienScoutService->exportAttachmentsForRealEstate($entity, $this->getParameter('kernel.project_dir'));
                        if (count($aStatus) > 0) {
                            $imgMessage = $this->translator->trans("Slike za nekretninu su uspješno izvezene.");
                            $errorsCount = count(array_filter($aStatus, function ($elem) {
                                return $elem == false;
                            }));
                            if (count($aStatus) == $errorsCount) {
                                $imgMessage = $this->translator->trans("Došlo je do greške kod izvoza slika za nekretninu. Provjerite log.");
                            }
                            if ($errorsCount >= 1 && $errorsCount != count($aStatus)) {
                                $imgMessage = $this->translator->trans("Došlo je do greške kod izvoza određenih slika za nekretninu. Provjerite log.");
                            }
                            $statusMessage .= " " . $imgMessage;
                        }

                        $this->addFlash("success", $statusMessage);
                    } else {
                        $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom izvoza podataka na ImmobilienScout24"));
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->logger->error($ex->getMessage());
            $this->addFlash("error", $ex->getMessage());
        }
    }

    public function publishRealEstateToFacebook($id){
        $entity = $this->entityManager->getRepository(RealEstate::class)->find($id);
        $existing_id = $entity->getFbPostId();
        if ($entity->getStatus()->getDisplayId() == "PUBLISHED") {

                    $response = $this->facebookService->post($entity->getId(), "en");
                    if ($response->getStatusCode() == 200) {
                        if (!$existing_id) {
                        $this->addFlash("success", $this->translator->trans("Nekretnina objavljena na facebook"));
                        }
                        else{
                            $this->addFlash("success", $this->translator->trans("Nekretnina ažurirana na facebook-u"));
                        }
                    } else {
                        $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na facebook"));
                    }

        }
        else{
            $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na facebook"));
        }
        return $this->redirectToRoute("realestate_list");
    }

    public function publishRealEstateToTwitter($id){
        $entity = $this->entityManager->getRepository(RealEstate::class)->find($id);
        if ($entity->getStatus()->getDisplayId() == "PUBLISHED") {
            if (!$entity->getTwitterPostId()) {
                $response = $this->twitterService->post($entity->getId(), "en");
                if ($response) {
                    $this->addFlash("success", $this->translator->trans("Nekretnina objavljena na twitter"));
                } else {
                    $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na twitter"));
                }
            }
        }
        else{
            $this->addFlash("error", $this->translator->trans("Došlo je do pogreške prilikom objave na facebook"));
        }
        return $this->redirectToRoute("realestate_list");
    }

    public function publishRealEstateToZoopla($id){
        /** @var RealEstate $entity */
        $entity = $this->entityManager->getRepository(RealEstate::class)->find($id);
        $entity->setExportToZoopla($entity->getZooplaId() == null);

        if($entity->getExportToZoopla() == true) {
            if(!in_array($entity->getStatus()->getDisplayId(), ["PUBLISHED", "IN_PROGRESS"])) {
                $this->addFlash("error", $this->translator->trans("Samo objavljeni oglasi se mogu objaviti na Zoopla"));
                return $this->redirectToRoute("realestate_list");
            } else if(!$entity->getPrice()) {
                $this->addFlash("error", $this->translator->trans("Oglas mora imati postavljenu cijenu za objavu na Zoopla"));
                return $this->redirectToRoute("realestate_list");
            }
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->updateOnZoopla($entity);
        return $this->redirectToRoute("realestate_list");
    }

    public function featuresToSynonyms(Request $request, ServiceRealEstate $serviceRealEstate, ServiceLanguage $serviceLanguage, $id = null)
    {
        $realEstates = $id == null ? $serviceRealEstate->getAll() : $serviceRealEstate->getById($id);
        $languages = $serviceLanguage->getAll();

        try {
            /** @var RealEstate $realEstate */
            foreach ($realEstates as $realEstate) {
                $this->transcribeFeatures($realEstate, $serviceRealEstate, $languages, "SYNONYMS");
            }

            $this->entityManager->flush();
        } catch (\Exception $exception) {
            return new Response("Greška: " . $exception->getMessage(), 500);
        }

        return new Response($this->translator->trans("Uspješno izvršeno mapiranje značajki u polje sinonimi"));
    }

    private function transcribeFeatures(RealEstate $realEstate, ServiceRealEstate $serviceRealEstate, $languages = [], $target = "")
    {
        $serviceRealEstate = $serviceRealEstate ?? new ServiceRealEstate($this->entityManager);

        /** @var Language $language */
        foreach ($languages as $language) {
            $realEstateLoco = $realEstate->getRealEstateLocalization($language);
            if ($realEstateLoco != null) {
                $features = $serviceRealEstate->getRealEstateFeaturesLocalized($realEstate, $language);
                $props = $this->createRealEstateBooleanPropsTranscription($realEstate, $language->getCode());
                $transcription_array = array_merge(array_column($features, "name"), $props);
                if ($target == "SYNONYMS") {
                    $realEstateLoco->setSynonyms(implode(" ", $transcription_array));
                }
                if ($target == "FEATURE_MAP") {
                    $realEstateLoco->setFeaturesMap(implode(" ", $transcription_array));
                }
                $this->entityManager->persist($realEstateLoco);
            }
        }
    }

    private function createRealEstateBooleanPropsTranscription(RealEstate $realEstate, $languageCode): array
    {
        $props = [];

        if ($realEstate->getFirstRowByTheSea()) {
            $props[] = $this->translator->trans("Prvi red do mora", [], null, $languageCode);
        }

        if ($realEstate->getSeaView()) {
            $props[] = $this->translator->trans("Pogled na more", [], null, $languageCode);
        }

        if ($realEstate->getUnderConstruction()) {
            $props[] = $this->translator->trans("Nekretnine u izgradnji", [], null, $languageCode);
        }

        if ($realEstate->getNewBuilding()) {
            $props[] = $this->translator->trans("Novogradnja", [], null, $languageCode);
        }

        if ($realEstate->getPool()) {
            $props[] = $this->translator->trans("Bazen", [], null, $languageCode);
        }

        if ($realEstate->getGarage()) {
            $props[] = $this->translator->trans("Garaža", [], null, $languageCode);
        }

        return $props;
    }


    public function invalidatedRealEstates(ServiceRealEstate $serviceRealEstate)
    {
        $entities = $serviceRealEstate->getInvalidatedRealEstates();
        $templateName = "backend/invalidated_realestates.html.twig";
        return $this->render($templateName, array(
            "realestateList" => $entities,
        ));
    }

    public function restoreRealEstate($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $entity = $this->entityManager->getRepository(RealEstate::class)->find($id);
        $entity->setInvalidated(0);
        $this->entityManager->flush();
        $this->addFlash("success", $this->translator->trans("Nekretnina vraćena iz izbrisanih"));
        return $this->redirectToRoute("realestate_invalidated");
    }

    public function publishToFacebook($realEstateId, $locale)
    {
        return $this->json($this->facebookService->post($realEstateId, $locale));
    }

    private function updateOnZoopla(RealEstate $realEstate)
    {
        $action = $this->zooplaService->upsertSingleRealEstate($realEstate);

        if ($action == "added") {
            $this->addFlash("success", "Oglas postavljen na zoopla servis");
        } else if ($action == "updated") {
            $this->addFlash("success", "Oglas osvježen na zoopla servisu");
        } else if ($action == "removed") {
            $this->addFlash("success", "Oglas uklonjen sa zoopla servisa");
        } else if ($action == "completed") {
            $this->addFlash("success", "Oglas završen na zoopla servisu");
        }
    }

}