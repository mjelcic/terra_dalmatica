<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:30 PM
 */

namespace App\Controller\Backend;


use App\Entity\Status;
use App\Form\Backend\StatusType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StatusController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, Status::class, StatusType::class, $notificationService);
    }

    /**
     * @param $entity Status
     */
    protected function beforeEntitySave($entity)
    {
        if($entity->getStatusLocalizations())
        {
            foreach ($entity->getStatusLocalizations() as $statusLoco){
                if($entity->getId()){
                    $statusLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $statusLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }
    }
}