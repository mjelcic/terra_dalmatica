<?php

namespace App\Controller\Backend;

use App\Entity\Status;
use App\Services\CountService;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class IndexController extends BaseController
{


    protected $countService;

    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService, CountService $countService)
    {
        parent::__construct($em, $translator, $validator, Language::class, LanguageType::class, $notificationService);
        $this->countService = $countService;
    }


    public function index()
    {
        $realEstateStatuses = $this->entityManager->getRepository(Status::class)->findAll();
        $parameters = array();
        $parameters["status"] = $realEstateStatuses;

        // If id is null then all entities are coutned, else only entities from current user are countes
        if ((in_array("ROLE_ADMIN", $this->getUser()->getRoles())) || ((in_array("ROLE_EMPLOYEE", $this->getUser()->getRoles())))) {
            $userId = null;
            $parameters["clients"] = $this->countService->countEntities("App\Entity\User", null, array("roles" => "'ROLE_USER'"));
        } else {
            $userId = $this->getUser()->getId();
        }

        if (in_array("ROLE_ADMIN", $this->getUser()->getRoles() )){
            $parameters["employees"] = $this->countService->countEntities("App\Entity\User", null, array("roles" => "'ROLE_EMPLOYEE'"));
        }

        foreach ($realEstateStatuses as $status) {
            $parameters[$status->getDisplayId()] = $this->countService->countEntities("App\Entity\RealEstate", $userId, array("status" => $status->getId()));
        }
        $parameters["status"] = $realEstateStatuses;
        return $this->render('backend/index.html.twig', $parameters);
    }

    public function my_terra_dalmatica(){
        if((in_array("ROLE_ADMIN", $this->getUser()->getRoles()))||(in_array("ROLE_EMPLOYEE", $this->getUser()->getRoles()))){
            return $this->redirectToRoute("backend_index");
            }
            else{
            return $this->redirectToRoute("backend_client_index");
        }
    }
}
