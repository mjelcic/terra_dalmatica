<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:30 PM
 */

namespace App\Controller\Backend;


use App\Entity\CmsPage;
use App\Entity\CmsPageMetaLocalization;
use App\Entity\Enum\CmsPageTypeEnum;
use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use App\Entity\PhotoMetaLocalization;
use App\Form\Backend\CmsPageAllMetaType;
use App\Form\Backend\CmsPageType;
use App\Form\Backend\RealEstateCustomPageType;
use App\Services\NotificationService;
use App\Services\ServiceLanguage;
use App\Utility\ActionType;
use App\Utility\Factory\LocalizedEntityFactory;
use App\Utility\Slug\Slugifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Services\ContentService;
use App\Utility\EntityHelper;

class CmsPageController extends BaseLocalizedEntityController
{
    protected $contentService;
    protected $slugifier;
    protected $entityHelper;

    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, ContentService $contentService, Slugifier $slugifier, EntityHelper $entityHelper, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, CmsPage::class, CmsPageType::class, $notificationService);
        $this->contentService = $contentService;
        $this->slugifier = $slugifier;
        $this->entityHelper = $entityHelper;
    }

    /**
     * @param $entity CmsPage
     */
    protected function beforeEntitySave($entity)
    {
        $serviceLanguage = new ServiceLanguage($this->entityManager);
        $languages = $serviceLanguage->getAll();
        $entity->setLockedForEmployee(0);

        if ($entity->getCmsPageLocalizations()) {
            foreach ($entity->getCmsPageLocalizations() as $cmsLoco) {

                if ($entity->getId()) {
                    $cmsLoco->setUpdatedBy($this->getUser()->getId());
                    if ($cmsLoco->getSlug() != "") {
                        //set unique slug
                        $cmsLoco->setSlug($this->entityHelper->getUniqueFieldValue("App\Entity\CmsPageLocalization", "slug", $this->slugifier->slugify($cmsLoco->getSlug()), $cmsLoco->getId(), $cmsLoco->getLanguage()->getCode()));
                    }
                } else {
                    $cmsLoco->setCreatedBy($this->getUser()->getId());

                    $cmsPageMetaLocalization = new CmsPageMetaLocalization();
                    $cmsPageMetaLocalization->setLanguage($cmsLoco->getLanguage());
                    $cmsPageMetaLocalization->setTitle($cmsLoco->getTitle());
                    $cmsPageMetaLocalization->setDescription(substr(strip_tags($cmsLoco->getContent()), 0, 300));
                    $entity->addCmsPageMetaLocalization($cmsPageMetaLocalization);
                }
                if (($cmsLoco->getSlug() == "") && ($cmsLoco->getTitle() != "")) {
                    //set unique slug
                    $cmsLoco->setSlug($this->entityHelper->getUniqueFieldValue("App\Entity\CmsPageLocalization", "slug", $this->slugifier->slugify($cmsLoco->getTitle()), null, $cmsLoco->getLanguage()->getCode()));
                }
            }

            if ($entity->getCoverPhoto()) {
                if ($entity->getCoverPhoto()->getId()) {
                    $entity->getCoverPhoto()->setUpdatedBy($this->getUser()->getId());
                } else {
                    $entity->getCoverPhoto()->setCreatedBy($this->getUser()->getId());
                }

                if (!$entity->getCoverPhoto()->getPhotoMetaLocalizations()->count()) {
                    foreach ($languages as $language) {
                        $photoMeta = new PhotoMetaLocalization();
                        $photoMeta->setLanguage($language);
                        $photoMeta->setAltTag($entity->getCmsPageLocalization($language)->getTitle());
                        $photoMeta->setCreatedBy($this->getUser()->getId());
                        $entity->getCoverPhoto()->addPhotoMetaLocalization($photoMeta);
                    }
                }
            }


        }
    }

    public function editMetaData(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $cmsPage = $em->getRepository(CmsPage::class)->findOneBy(array('id' => $id));
        $cmsPage->setUpdatedAt(new \DateTime());
        $cmsPage->setUpdatedBy($this->getUser()->getId());
        $form = $this->createForm(CmsPageAllMetaType::class, $cmsPage);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->beforeEntitySave($cmsPage);
            $em->flush();
            $this->addFlash("success", $this->translator->trans("Entitet ažuriran"));
            if ($cmsPage->getPageType() == CmsPageTypeEnum::BLOG) {
                return $this->redirectToRoute('cmspage_list');
            } else {
                return $this->redirectToRoute('predefinedcms_list');
            }
        }

        return $this->render('backend/edit_cmspagemeta.html.twig', array('form' => $form->createView()));
    }

    public function overview(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if (in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $entities = $entityManager->getRepository(CmsPage::class)->findby(["invalidated" => 0, "page_type" => CmsPageTypeEnum::BLOG]);
        } else {
            $entities = $entityManager->getRepository(CmsPage::class)->findBy(
                ['invalidated' => 0, 'locked_for_employee' => 0, "page_type" => CmsPageTypeEnum::BLOG]
            );
        }
        return $this->render(
            'backend/cmspage_list.html.twig',
            array(
                'cmspageList' => $entities,
                'pageType' => CmsPageTypeEnum::BLOG)
        );
    }

    public function overviewPredefined(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if (in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $entities = $entityManager->getRepository(CmsPage::class)->findby(["invalidated" => 0, "page_type" => CmsPageTypeEnum::PREDEFINED]);
        } else {
            $entities = $entityManager->getRepository(CmsPage::class)->findBy(
                ['invalidated' => 0, 'locked_for_employee' => 0, "page_type" => CmsPageTypeEnum::PREDEFINED]
            );
        }
        return $this->render(
            'backend/cmspage_list.html.twig',
            array(
                'cmspageList' => $entities,
                'pageType' => CmsPageTypeEnum::PREDEFINED)
        );
    }

    public function afterEntitySave($entity, $type)
    {
        $this->addFlash("warning", "<a href='" . $this->generateUrl("cmspagemeta_edit", array("id" => $entity->getId())) . "'>" . $this->translator->trans("Uredite meta podatke") . "</a>");
    }

    public function edit(Request $request, $id)
    {
        $cmsPage = $this->entityManager->getRepository(CmsPage::class)->find($id);
        if ($cmsPage->getPageType() == CmsPageTypeEnum::BLOG) {
            $redirectRoute = 'cmspage_list';
        } else {
            $redirectRoute = 'predefinedcms_list';
        }

        if ($cmsPage->getOnlyMeta() == 0) {
            if (((in_array("ROLE_ADMIN", $this->getUser()->getRoles()))) || ($cmsPage->getLockedForEmployee() == 0)) {
                return $this->HandleDataSubmit($id, ActionType::update, $request, array(), null, $redirectRoute);
            }
        }

    }

    protected function isOkToDelete($entity)
    {
        if ($entity->getPageType() == 0) {
            return true;
        } else {
            return false;
        }

    }

}