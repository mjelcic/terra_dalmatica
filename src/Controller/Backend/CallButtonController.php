<?php

namespace App\Controller\Backend;

use App\Entity\CallButton;
use App\Entity\Language;
use App\Form\Backend\CallButtonType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CallButtonController extends BaseController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, CallButton::class, CallButtonType::class, $notificationService);
    }

    public function callbuttonSwitch(TranslatorInterface $translator, $id)
    {
        $callButton = $this->entityManager->getRepository(CallButton::class)->find($id);
        if (!$callButton) {
            return new Response($translator->trans("Nije pronađen sadržaj"), Response::HTTP_NOT_FOUND);
        }

        if ($callButton->getActive() == true) {
            $callButton->setActive(false);
            $this->entityManager->flush();
            return $this->json(array("value" => 0));
        } else {
            $callButtons = $this->entityManager->getRepository(CallButton::class)->findAll();
            foreach ($callButtons as $callButtonEntity) {
                $callButtonEntity->setActive(false);
            }
            $callButton->setActive(true);
            $this->entityManager->flush();
            return $this->json(array("value" => 1));
        }


    }


}
