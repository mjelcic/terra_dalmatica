<?php

namespace App\Controller\Backend;

use App\Entity\User;
use App\Entity\Visit;
use App\Services\VisitService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class VisitController extends AbstractController
{
    protected $entityManager;
    protected $visitService;

    function __construct(EntityManagerInterface $entityManager, VisitService $visitService)
    {
        $this->entityManager = $entityManager;
        $this->visitService = $visitService;
    }

    public function registerVisit(Request $request)
    {
        $user = $this->getUser();
        $lastVisit = $this->entityManager->getRepository(Visit::class)->findOneBy(array("user" => $user), array("visited_at" => "DESC"));
        $currentDateTime = new \DateTime();
        if ($lastVisit) {
            $diff = $currentDateTime->getTimestamp() - $lastVisit->getVisitEndedAt()->getTimestamp();
        }
        else{
            $diff = 9999;
        }
        if ($user) {
            if ($diff > 120) {
                $visit = new Visit();
                $visit->setUser($user);
                $visit->setVisitedAt($currentDateTime);
                $visit->setVisitEndedAt($currentDateTime);
                $this->entityManager->persist($visit);
                $this->entityManager->flush();
            }  else {
                $lastVisit->setVisitEndedAt($currentDateTime);
                $this->entityManager->flush();
            }
            return $this->json(array(), 201);
        } else {
            return $this->json(array(), 404);
        }
    }

    function onlineUsers(){
        $users = $this->entityManager->getRepository(User::class)->findAll();
        return $this->render("backend/online_users.html.twig", array("users"=>$users));
    }
    function userVisits($id){
        $user = $this->entityManager->getRepository(User::class)->find($id);
        return $this->render("backend/user_visits.html.twig", array("visits"=>$user->getVisits(), "user"=>$user));
    }

}
