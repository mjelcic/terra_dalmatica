<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:14 PM
 */

namespace App\Controller\Backend;


use App\Services\Export\ServiceDataExport;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RealEstateCroatiaController extends AbstractController
{
    private $serviceDataExport;

    public function __construct(ServiceDataExport $serviceDataExport)
    {
        $this->serviceDataExport = $serviceDataExport;
    }

    public function createXml()
    {
        $xml_options = array(
            "xml_root_node_name" => "properties",
            "remove_empty_tags" => true,
            "xml_encoding" => "utf-8"
        );
        $encoders = [new XmlEncoder($xml_options), new JsonEncoder()];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter), new ArrayDenormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $realEstates = $this->serviceDataExport->getDataForRealEstateCroatia();

        $xml = $serializer->serialize(array("property" => $realEstates), 'xml', [AbstractNormalizer::IGNORED_ATTRIBUTES => ["id"]]);

        // Ovaj controller je napravljen samo radi testiranja, prava funkcionalnost za export je napravljena preko "Command", zato je ovaj nered ovdje dole ispod.
        // Drugim riječima ovaj controller ćemo pobrisati
        echo $xml;
        die(null);

        return $this->json($realEstates);
    }
}