<?php
/**
 * Created by mijatra
 * Date: 5/8/2018
 * Edited by mijatra
 * Date: 10/27/2018
 */

namespace App\Controller\Backend;

use App\Services\NotificationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


abstract class BaseNotificationController extends AbstractController
{
    protected $notificationService;

    function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }
	
public function render($view, array $parameters = array(), Response $response = NULL): Response
{
    $parameters['notifications'] = $this->notificationService->getNotificationsForUser($this->getUser());

    return parent::render($view, $parameters, null);
}
}
