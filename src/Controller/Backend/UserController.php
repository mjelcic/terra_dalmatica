<?php

namespace App\Controller\Backend;

use App\Entity\Enum\RoleEnum;
use App\Form\Backend\ClientEditType;
use App\Form\Backend\ClientType;
use App\Form\Backend\UserType;
use App\Form\Backend\UserEditType;
use App\Form\Backend\UserSelfEditType;
use App\Entity\User;
use App\Services\ServiceUser;
use App\Utility\ActionType;
use App\Utility\MError;
use App\Utility\PostOfficeUtility;
use App\Utility\SwiftMailerUtil;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Services\NotificationService;
use Symfony\Contracts\Translation\TranslatorInterface;
use Ramsey\Uuid\Uuid;

class UserController extends BaseController
{
    private $postOffice;
    private $passwordEncoder;
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator,
                         UserPasswordEncoderInterface $passwordEncoder, NotificationService $notificationService, PostOfficeUtility $postOffice)
    {
        parent::__construct($em, $translator, $validator, User::class, UserType::class, $notificationService);
        $this->passwordEncoder = $passwordEncoder;
        $this->postOffice = $postOffice;
    }

    public function edit(Request $request, $id)
    {
        return $this->editUser($request, $id, UserEditType::class, 'backend/edit_user.html.twig');
    }

    public function selfEdit(Request $request, $id)
    {
        return $this->editUser($request, $id, UserSelfEditType::class, 'backend/self_edit_user.html.twig');
    }

    public function editUser($request, $id, $formType, $twig)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$id));
        $user->setUpdatedAt(new \DateTime());
        $user->setUpdatedBy($this->getUser()->getId());
        $form = $this->createForm($formType, $user);

        $form ->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->beforeEntitySave($user);
            $em->flush();
            $this->addFlash("success", $this->translator->trans("Entitet ažuriran"));
            if(in_array("ROLE_USER", $user->getRoles())){
                return $this->redirectToRoute("backend_client_index");
            }
            return $this->redirectToRoute('user_list');
        }

        return $this->render($twig, array('form'=>$form->createView()));
    }

    /**
     * @param User $entity
     */
    function beforeEntitySave($entity)
    {
		if($entity->getId()==null){
            $password = $this->passwordEncoder->encodePassword($entity, $entity->getPassword());
            $entity->setPassword($password);
		}

		if($entity->getRoles() == null) {
		    $entity->setRoles("ROLE_USER");
        }

        $entity->setActivationCode(sha1(mt_rand(10000, 99999) . time()));
    }

    /**
     * @param User $entity
     */
    protected function afterEntitySave($entity, $action_type)
    {
        if($action_type == ActionType::create)
        {
            $this->postOffice->notifyAboutNewClientRegistration($entity);
        }
    }

    public function clientOverview(Request $request)
    {
        $entities = $this->entityManager->getRepository($this->fullClassName)
            ->findBy(['invalidated' => 0, 'roles' => 'ROLE_USER'], ["created_at" => "DESC"]);
        $templateName = "backend/client_list.html.twig";
        return $this->render($templateName, array(
            "clientList" => $entities,
        ));
    }

    public function employeeOverview(Request $request)
    {
        $entities = $this->entityManager->getRepository($this->fullClassName)
            ->findBy(['invalidated' => 0, 'roles' => ['ROLE_ADMIN', 'ROLE_EMPLOYEE']], ["created_at" => "DESC"]);
        $templateName = "backend/user_list.html.twig";
        return $this->render($templateName, array(
            $this->getClassName() . "List" => $entities,
        ));
    }


    public function createClient(Request $request)
    {
        return $this->handleClientSubmit(null, ActionType::create, $request);
    }

    public function editClient(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$id));
        $user->setUpdatedAt(new \DateTime());
        $user->setUpdatedBy($this->getUser()->getId());
        $form = $this->createForm(ClientEditType::class, $user);

        $form ->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->beforeEntitySave($user);
            $em->flush();
            $this->addFlash("success", $this->translator->trans("Entitet ažuriran"));
            return $this->redirectToRoute('client_list');
        }

        return $this->render("backend/edit_client.html.twig", array('form'=>$form->createView()));
    }

    protected function handleClientSubmit($id, $type, Request $request)
    {
        /** @var User $entity */
        $entity = new User(); // no need for reflection

        if ($type == ActionType::update) {
            $entity = $this->entityManager->getRepository($this->fullClassName)->find($id);

            //If no FE was found return 404
            if (!$entity) {
                throw $this->createNotFoundException($this->translator->trans("Subjekat koji tražite nije pronađen! Pokušajte ponovo!"));
            }
        }

        //create the form
        $form = $this->createForm(ClientType::class, $entity, array("locale" => $request->getLocale()));

        //handle the request
        $form->handleRequest($request);

        //check if the form is submitted - POST
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $this->beforeEntitySave($entity);

                if ($type == ActionType::create)
                {
                    $entity->setCreatedBy($this->getUser()->getId());
                    $entity->setRoles(["ROLE_USER"]);
                    $flashMsg = "Entitet kreiran";
                }
                elseif ($type == ActionType::update)
                {
                    $entity->setUpdatedAt(new \DateTime());
                    $entity->setUpdatedBy($this->getUser()->getId());
                    $flashMsg = "Entitet ažuriran";
                }

                $this->entityManager->persist($entity);
                try {
                    $this->entityManager->flush();
                    $this->afterEntitySave($entity, $type);
                    $this->addFlash("success", $this->translator->trans($flashMsg));
                } catch (\Exception $e) {
                    $this->addFlash("error", $this->translator->trans("Došlo je do problema prilikom spašavanja entiteta: " . $e->getMessage()));
                }
                //else redirect from here
                return $this->redirectToRoute("client_list");
            } else {
                //If the POST request is submitted via AJAX then return a JSON list of errors in format [{ propertyName: "", errorMessage: ""}, ...]
                if ($request->isXmlHttpRequest()) {
                    $errors = $this->validator->validate($entity);
                    $distilledErrs = array();
                    if (count($errors)) {
                        foreach ($errors as $error) {
                            $distilledErrs[] = new MError($error->getPropertyPath(), $error->getMessage());
                        }
                    }

                    return new JsonResponse(array('data' => $distilledErrs), 500);
                }
            }
        }

        //CREATE variable(s) to pass to a template
        //default values are for create form
        $templateOptions = array(
            'form' => $form->createView(),
            'isNew' => $type == ActionType::create
        );

        return $this->render("backend/new_client.html.twig", array_merge($templateOptions, []));
    }

    protected function isOkToDelete($entity)
    {
        $entity->setEmail(Uuid::uuid4());
        return true;
    }

    public function deactivateUserAccount(Request $request, ServiceUser $serviceUser, LoggerInterface $logger, $id = null)
    {
        if($id == null){
            throw new \HttpInvalidParamException("Invalid value for parameter id.");
        }

        if($this->getUser()->getId() != $id || !in_array("ROLE_USER", $this->getUser()->getRoles())){
            throw new \Exception("You are not allowed to execute this function!");
        }

        $bSuccess = $serviceUser->deactivateUserAccount($logger, $id);

        if(!$bSuccess){
            $this->addFlash("error", $this->translator->trans("Brisanje korisničkog računa nije uspjelo! Molimo Vas da probate ponovo ili kontaktirate administratora!"));
            return $this->redirectToRoute("backend_client_index");
        }

        //If we got here then user account and all associated data has been deleted. Now add a message, clear the session, and redirect to the FE homepage.

        $this->addFlash("error", $this->translator->trans("Uspješno ste obrisali Vaš korisnički nalog."));

        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->redirectToRoute("index");
    }

    public function select2Source(Request $request, ServiceUser $serviceUser)
    {
        $query = $request->query->get("q", "");
        $pageLimit = $request->query->get("page_limit", 10);

        $result = $serviceUser->getForSelect2($query, $pageLimit);

        return $this->json($result);
    }
}