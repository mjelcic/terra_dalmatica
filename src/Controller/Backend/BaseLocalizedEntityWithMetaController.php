<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/15/2020
 * Time: 9:42 AM
 */

namespace App\Controller\Backend;


use App\Services\NotificationService;
use App\Utility\ActionType;
use App\Utility\Factory\LocalizedEntityFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class BaseLocalizedEntityWithMetaController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, $className, $formType, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, $className, $formType, $notificationService);
    }

    public function create(Request $request)
    {
        $localizations = LocalizedEntityFactory::createLocalizations($this->entityManager, $this->fullClassName);
        $metaLocalizations = LocalizedEntityFactory::createMetaLocalizations($this->entityManager, $this->fullClassName);


        $args = array("localizations" => $localizations, "metaLocalizations" => $metaLocalizations);

        return $this->HandleDataSubmit(null, ActionType::create, $request, [], $args);
    }
}