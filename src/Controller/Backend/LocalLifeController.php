<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/24/2020
 * Time: 2:12 PM
 */

namespace App\Controller\Backend;


use App\Entity\LocalLife;
use App\Entity\Location;
use App\Form\Backend\LocalLifeType;
use App\Services\NotificationService;
use App\Services\ServiceLanguage;
use App\Services\ServiceLocation;
use App\Utility\ActionType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class LocalLifeController extends BaseController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, LocalLife::class, LocalLifeType::class, $notificationService);
    }

    public function create(Request $request)
    {
        $this->addFlash("warning", $this->translator->trans("Nedozvoljena aktivnost!"));
        $this->redirectToRoute("location_list");
    }

    public function edit(Request $request, $id)
    {
        return parent::HandleDataSubmit($id, ActionType::update, $request, array(), null, "location_list");
    }

    public function overview(Request $request)
    {
        $this->redirectToRoute("location_list");
    }

    /*TODO: HELPER METHOD - TO BE REMOVED EVENTUALLY*/
    public function recreateAll(ServiceLocation $serviceLocation, ServiceLanguage $serviceLanguage, LoggerInterface $logger)
    {
        $locations = $serviceLocation->getAll();
        $languages = $serviceLanguage->getAll();

        foreach ($locations as $location)
        {
            /**
             * @var $location Location
             */
            if(count($location->getLocalLives()) === 0)
            {
                foreach ($languages as $language)
                {
                    $localLife = new LocalLife();
                    $localLife
                        ->setLanguage($language)
                        ->setCreatedBy($this->getUser()->getId());
                    $location->addLocalLife($localLife);
                }
            }
        }

        try{
            $this->entityManager->flush();
            return new Response("Successfully recreated local life entities for all locations that were them.");
        } catch (\Exception $ex){
            $logger->log("error", $ex->getMessage());
            return new Response("Error creating local life entries.", 500);
        }
    }
}