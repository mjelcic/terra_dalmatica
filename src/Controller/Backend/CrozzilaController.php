<?php

namespace App\Controller\Backend;

use App\Entity\Language;
use App\Services\Export\CrozzilaService;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Filesystem\Filesystem;


class CrozzilaController extends AbstractController
{
    private $service;
    private $fs;

    function __construct(CrozzilaService $crozzilaService, Filesystem $filesystem)
    {
        $this->service = $crozzilaService;
        $this->fs = $filesystem;
    }

    public function createXml()
    {
        $xml_options = array(
            "xml_root_node_name" => "properties",
            "remove_empty_tags"=>true,
            "xml_encoding"=>"utf-8",
            "as_collection" => false
        );
        $encoders = [new XmlEncoder($xml_options), new JsonEncoder()];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $normalizers = [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)];


        $serializer = new Serializer($normalizers, $encoders);

        $realEstates = $this->service->getRealEstates();

        $xml = $serializer->serialize(array("property"=>$realEstates), 'xml');

        // Ovaj controller je napravljen samo radi testiranja, prava funkcionalnost za export je napravljena preko "Command", zato je ovaj nered ovdje dole ispod.
        // Drugim riječima ovaj controller ćemo pobrisati
        echo $xml;
        die(null);

        return $this->json($xml);
    }
}
