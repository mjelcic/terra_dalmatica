<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 1:30 PM
 */

namespace App\Controller\Backend;


use App\Entity\Feature;
use App\Form\Backend\FeatureType;
use App\Services\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FeatureController extends BaseLocalizedEntityController
{
    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator, NotificationService $notificationService)
    {
        parent::__construct($em, $translator, $validator, Feature::class, FeatureType::class, $notificationService);
    }

    /**
     * @param $entity Feature
     */
    protected function beforeEntitySave($entity)
    {
        if($entity->getFeatureLocalizations())
        {
            foreach ($entity->getFeatureLocalizations() as $featureLoco){
                if($entity->getId()){
                    $featureLoco->setUpdatedBy($this->getUser()->getId());
                } else {
                    $featureLoco->setCreatedBy($this->getUser()->getId());
                }
            }
        }
    }
}