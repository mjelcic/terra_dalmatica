<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/17/2020
 * Time: 12:56 PM
 */

namespace App\Controller\Frontend;


use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use App\Entity\Language;
use App\Entity\Location;
use App\Entity\RealEstate;
use App\Entity\RealEstateLocalization;
use App\Entity\RealEstateType;
use App\Entity\Status;
use App\Entity\User;
use App\Form\Frontend\ContactType;
use App\Form\Frontend\SearchType;
use App\GlobalSettings;
use App\Services\Preview\RealEstateLocalizationNode;
use App\Services\Preview\RealEstatePreviewDto;
use App\Services\ServiceAgent;
use App\Services\ServiceExchangeRate;
use App\Services\ServiceGroundPlan;
use App\Services\ServiceLanguage;
use App\Services\ServiceLocalLife;
use App\Services\ServiceLocation;
use App\Services\ServiceRealEstate;
use App\Services\ServiceRealEstateCustomPage;
use App\Services\ServiceUserFavorite;
use App\Services\ServiceUserPreference;
use App\Utility\ExchangeRateHelper;
use App\ViewModel\Frontend\ContactModel;
use App\ViewModel\Frontend\RealEstateDetailViewModel;
use App\ViewModel\Frontend\SearchModel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RealEstateController extends BaseController
{
    public function index(Request $request, LoggerInterface $logger, TranslatorInterface $translator, $slug)
    {
        //define service etc.
        $service = new ServiceRealEstate($this->em);
        $agentService = new ServiceAgent($this->em);
        $serviceRealEstateCustomPage = new ServiceRealEstateCustomPage($this->em);
        $serviceLanguage = new ServiceLanguage($this->em);
        $serviceUserPreference = new ServiceUserPreference($this->em, $logger);
        $serviceLocalLife = new ServiceLocalLife($this->em);
        $serviceLocation = new ServiceLocation($this->em);
        $serviceUserFavorite = new ServiceUserFavorite($this->em, $logger);
        $serviceGroundPlan = new ServiceGroundPlan($this->em, $logger);
        $locale = $request->getLocale();
        $language = $serviceLanguage->getByCode($locale);

        //Get real estate
        /**
         * @var $realEstate RealEstate
         */
        $realEstate = $service->getBySlug($slug);
        if($realEstate == null){
            return $this->render("frontend/error.html.twig", array(
                "message" => $translator->trans("Stranica nije pronađena!"),
                "code" => "404",
                "parameters" => $this->parameters,
                "searchbarForm" => $this->prepareSearchbarForm($locale)
            ));
        }

        //Get real estate localized data for display
        $viewModel = $service->getRealEstateDetailsLocalized($realEstate, $language);
        if($viewModel == null){
            return $this->render("frontend/error.html.twig", array(
                "message" => $translator->trans("Greška prilikom dohvata podataka! Pokušajte ponovo."),
                "code" => "500",
                "parameters" => $this->parameters,
                "searchbarForm" => $this->prepareSearchbarForm($locale)
            ));
        }

        //Get real estate images for gallery display
        $viewModel->images = $service->getRealEstateImageDetailsWithMetaLocalized($realEstate, $language);
        //Read and set width and height for cover img so it can be used for OG data
        for ($i = 0; $i<count($viewModel->images); $i++)
        {
            if($viewModel->images[$i]["cover"] == true){
                try {
                    $imagePath = substr($viewModel->images[$i]["path"], 0, strlen("http")) === "http" ?
                        $viewModel->images[$i]["path"] : $this->getParameter('kernel.project_dir') . '/public' . $viewModel->images[$i]["path"];
                    list($width, $height) = getimagesize($imagePath);
                    $viewModel->images[$i]["width"] = $width;
                    $viewModel->images[$i]["height"] = $height;
                } catch (\Exception $exception){
                    $logger->log("error", $exception->getMessage());
                }
                break;
            }
        }

        //Get real estate features localized
        $viewModel->features = $service->getRealEstateFeaturesLocalized($realEstate, $language);

       //Set metadata
        $this->setRealEstatePageMetadata($realEstate, $language);

        //Cross-sell
        $viewModel->crossSell = $service->getCrossSellByRealEstate($realEstate, GlobalSettings::getCrossSellByRealEstateCount(), $locale);

        //Locations in offer
        $viewModel->setLocationCategoryPages($serviceLocation->getLocationsInOfferLocalized($locale, [RealEstateCustomPageTypeEnum::AUTOGENERATED]));

        //Agent data
        if($realEstate->getAgent() != null) {
            $viewModel->agent = $agentService->getAgentDetailsAndPage($realEstate->getAgent(), $language);
        }

        //Local Life
        $viewModel->localLife = $serviceLocalLife->getByLanguageAndLocation($locale, $realEstate->getLocation());

        //Check if it is in favorites
        if($this->getUser() != null && in_array("ROLE_USER", $this->getUser()->getRoles())){
            $viewModel->savedToFavorites = $serviceUserFavorite->getByUserAndRealEstate($this->getUser(), $realEstate) ? true : false;
        }

        //Get ground plans if any
        $viewModel->ground_plans = $serviceGroundPlan->getGroundPlansByRealEstate($realEstate);

        //For breadcrumbs
        $catPage = $serviceRealEstateCustomPage->getByLocationFilterLocalized($realEstate->getLocation(), RealEstateCustomPageTypeEnum::AUTOGENERATED, $locale);
        if($catPage)
        {
            //set second level of breadcrumb
            $this->addBreadcrumbElement($this->generateUrl("category_page", array("_locale"=>$locale, "slugCategory"=>$catPage["slug"])), $catPage["title"]);
        }

        //Set language selectors
        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach($languages as $language)
        {
            $realEstateLoco = $realEstate->getRealEstateLocalization($language);
            if($realEstateLoco != null){
                $url = $this->generateUrl("real_estate_details", array("_locale" => $language->getCode(), "slug" => $realEstateLoco->getSlug()));
                $this->setLanguageSelector($language->getCode(), $url);
            }
        }

        //Modify user preference
        $user = $this->getUser();
        if($user != null && in_array("ROLE_USER", $user->getRoles())){
            $serviceUserPreference->modifyUserPreference($this->getUser(), $realEstate->getRealEstateType(), $realEstate->getLocation(), $realEstate->getPrice() ?? 0);
        }

        return $this->render("frontend/real_estate_details.html.twig", array(
            "parameters" => $this->parameters,
            "viewModel" => $viewModel,
            "searchbarForm" => $this->prepareSearchbarForm($locale),
            'contactForm' => $this->createForm(ContactType::class, new ContactModel())->createView()
        ));
    }

    public function preview(
        Request $request,
        TranslatorInterface $translator,
        SessionInterface $session,
        LoggerInterface $logger,
        SerializerInterface $serializer,
        $hash,
        $slug
    ) {
        $serializedDto = $session->get("preview_" . $hash);
        $locale = $request->getLocale();

        if($serializedDto == null) {
            return $this->render("frontend/error.html.twig", array(
                "message" => $translator->trans("Stranica nije pronađena!"),
                "code" => "404",
                "parameters" => $this->parameters,
                "searchbarForm" => $this->prepareSearchbarForm($locale)
            ));
        }

        /** @var RealEstatePreviewDto $dtoObj */
        $dtoObj = $serializer->deserialize($serializedDto, RealEstatePreviewDto::class, "json");

        //define service etc.
        $service = new ServiceRealEstate($this->em);
        $agentService = new ServiceAgent($this->em);
        $serviceRealEstateCustomPage = new ServiceRealEstateCustomPage($this->em);
        $serviceLanguage = new ServiceLanguage($this->em);
        $language = $serviceLanguage->getByCode($locale);

        $localization = array_values(array_filter($dtoObj->realEstateLocalizations, function(RealEstateLocalizationNode $l) use ($locale){
            return $l->locale == $locale;
        }))[0];

        $location = $dtoObj->locationId != null ? $this->em->getRepository(Location::class)->find($dtoObj->locationId) : null;
        $agent = $dtoObj->agentId != null ? $this->em->getRepository(User::class)->find($dtoObj->agentId) : null;
        $status = $dtoObj->statusId != null ? $this->em->getRepository(Status::class)->find($dtoObj->statusId) : null;
        $type = $dtoObj->typeId != null ? $this->em->getRepository(RealEstateType::class)->find($dtoObj->typeId) : null;

        // we'll go risky and won't check slug :yolo:

        $viewModel = new RealEstateDetailViewModel(
            99999,
            $localization->title,
            $localization->slug,
            $localization->description,
            $location != null ? $location->getLocationLocalization($language)->getName() : null,
            $type == null ? $type->getTypeLocalization($language)->getName() : null,
            $dtoObj->firstRowByTheSea,
            $dtoObj->seaView,
            $dtoObj->bedroomCount,
            $dtoObj->bathroomCount,
            $dtoObj->distanceFromTheSea,
            $dtoObj->energyClass,
            $dtoObj->garage,
            22.22,
            33.33,
            $dtoObj->livingArea,
            $dtoObj->surfaceArea,
            $dtoObj->pool,
            $dtoObj->price,
            0,
            $dtoObj->virtualWalkUrl,
            $dtoObj->videoUrl,
            $dtoObj->uid,
            $dtoObj->priceOnRequest,
            $dtoObj->typeId,
            $dtoObj->underConstruction
        );

        //Get real estate images for gallery display
        $dbImages = $service->getRealEstateImageDetailsWithMetaLocalizedForIds($dtoObj->images, $language);

        foreach ($dbImages as $dbImage) {
            foreach ($dtoObj->images as $dtoObjImage) {
                if($dtoObjImage["id"] == $dbImage["id"]) {
                    $dbImage["cover"] = $dtoObjImage["cover"];
                    $viewModel->images[] = $dbImage;
                    break;
                }
            }
        }

        usort($viewModel->images, function($i1, $i2) { return $i1["cover"] ? -1 : 1;});

        //Read and set width and height for cover img so it can be used for OG data
        for ($i = 0; $i<count($viewModel->images); $i++)
        {
            if($viewModel->images[$i]["cover"] == true){
                try {
                    $imagePath = substr($viewModel->images[$i]["path"], 0, strlen("http")) === "http" ?
                        $viewModel->images[$i]["path"] : $this->getParameter('kernel.project_dir') . '/public' . $viewModel->images[$i]["path"];
                    list($width, $height) = getimagesize($imagePath);
                    $viewModel->images[$i]["width"] = $width;
                    $viewModel->images[$i]["height"] = $height;
                } catch (\Exception $exception){
                    $logger->log("error", $exception->getMessage());
                }
                break;
            }
        }

        //Get real estate features localized
        $viewModel->features = $service->getRealEstateFeaturesLocalizedForIds($dtoObj->features, $language);

        //TODO: Local life

        //Cross-sell

        if($dtoObj->id) { // edit case
            $realEstateForCrossSellObj = $service->getById($dtoObj->id);
        } else { //add case -> minimal object
            $realEstateForCrossSellObj = new RealEstate(null);
            $realEstateForCrossSellObj->setPrice($dtoObj->price);
            $realEstateForCrossSellObj->setRealEstateType($type);
        }

        $viewModel->crossSell = $service->getCrossSellByRealEstate($realEstateForCrossSellObj, GlobalSettings::getCrossSellByRealEstateCount(), $locale);

        //$viewModel->crossSell = [];

        //Agent data
        if($agent != null) {
            $viewModel->agent = $agentService->getAgentDetailsAndPage($agent, $language);
        }

        //For breadcrumbs
        $catPage = $serviceRealEstateCustomPage->getByLocationFilterLocalized($location, RealEstateCustomPageTypeEnum::AUTOGENERATED, $locale);
        if($catPage)
        {
            //set second level of breadcrumb
            $this->addBreadcrumbElement($this->generateUrl("category_page", array("_locale"=>$locale, "slugCategory"=>$catPage["slug"])), $catPage["title"]);
        }

        //Set language selectors
        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach($languages as $language)
        {
            $realEstateLocos = array_values(array_filter($dtoObj->realEstateLocalizations, function(RealEstateLocalizationNode $l) use ($language) {
                return $l->locale == $language->getCode();
            }));
            if(count($realEstateLocos)){
                $url = $this->generateUrl("real_estate_preview", array("_locale" => $language->getCode(), "hash" => $hash, "slug" => $realEstateLocos[0]->slug));
                $this->setLanguageSelector($language->getCode(), $url);
            }
        }

        if($dtoObj->id != null) {
            $actions = [
                ["name" => "Nastavi uređivati", "href" => $this->generateUrl("realestate_edit", ["id" => $dtoObj->id, "hash" => $hash]), "icon" => "edit"],
            ];
        } else {
            $actions = [
                ["name" => "Nastavi uređivati", "href" => $this->generateUrl("realestate_new", ["hash" => $hash]), "icon" => "edit"],
            ];
        }

        $actions[] =
            ["name" => "Primjeni izmjene", "href" => $this->generateUrl("realestate_preview_apply", ["hash" => $hash]), "icon" => "save"];

        if($status->getDisplayId() == "CREATED" && count($dtoObj->realEstateLocalizations) >= 3) {
            $actions[] = ["name" => "Primijeni izmjene i objavi oglas", "href" => $this->generateUrl("realestate_preview_publish", ["hash" => $hash]),"icon" => "upload"];

        }

        return $this->render("frontend/real_estate_details.html.twig", array(
            "parameters" => $this->parameters,
            "viewModel" => $viewModel,
            "searchbarForm" => $this->prepareSearchbarForm($locale),
            "actions" => $actions,
            'contactForm' => $this->createForm(ContactType::class, new ContactModel())->createView()
        ));
    }

    public function search(Request $request, ServiceRealEstate $service, TranslatorInterface $translator)
    {
        $locale = $request->getLocale();
        $searchModel = new SearchModel();
        $form = $this->createForm(SearchType::class, $searchModel, array("locale" => $locale, "method" => "GET"));
        $page = $request->query->get("page", 1);
        $count = 0;
        $similar = 0;
        $availableFilters = [];

        $form->handleRequest($request);

        //$realEstates = $service->searchRealEstates($searchModel, $page, GlobalSettings::getRealEstatesSearchResultsPerPage(), $locale, $count);
        $realEstates = $service->searchOrSimilar($searchModel, $page, GlobalSettings::getRealEstatesSearchResultsPerPage(), $locale, $count, $similar, $availableFilters);
        //Get images for real estates
        //$images = $service->getRealEstateImageDetailsWithMetaLocalized($realEstate, $language);


        $maxPages = ceil($count / GlobalSettings::getRealEstatesSearchResultsPerPage());
        $last_page = $page >= $maxPages || $maxPages == 0;

        if($request->isXmlHttpRequest()){

            $html = "";
            foreach ($realEstates as $realEstate) {
                $html .= $this->render("frontend/include/real_estate_single_item_v2.html.twig", array("realEstate" => $realEstate, "exchangeRate" => $this->parameters->exchangeRate))->getContent();
            }

            return new JsonResponse(array("html" => $html, "last_page" => $last_page, "total_count" => $count, "similar" => $similar, "extra" => $availableFilters));
        }

        //Set metadata for the page
        //TODO: To just add translations and leave it hardcoded?
        $this->setMetaTitle($translator->trans("Pretraga nekretnina"));
        $this->setMetaDescription($translator->trans("Pretraga nekretnina"));

        //Set language selectors
        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach($languages as $language)
        {
            $this->setLanguageSelector($language->getCode(), $this->generateUrl("real_estate_search", array("_locale" => $language->getCode())));
        }

        //Get location category pages
        $serviceLocation = new ServiceLocation($this->em);
        $locationCategoryPages = $serviceLocation->getLocationsInOfferLocalized($locale, [RealEstateCustomPageTypeEnum::AUTOGENERATED]);

        return $this->render("frontend/real_estate_search_results.html.twig", array(
            "searchbarForm" => $form->createView(),
            "searchbarFormAutoTrigger" => true,
            "searchResults" => $realEstates,
            "parameters" => $this->parameters,
            "totalCount" => $count,
            "loadMore" => $last_page == false,
            "similar" => $similar,
            "locationCategoryPages" => $locationCategoryPages
        ));
    }

    public function autocompleteUid(Request $request, ServiceRealEstate $serviceRealEstate)
    {
        $term = $request->get("term");
        $results = [];
        $jsonResult = [];

        if($term){
            $results = $serviceRealEstate->autocompleteUid($term);
        }

        foreach ($results as $result){
            $jsonResult[] = $result["uid"];
        }

        return $this->json($jsonResult);
    }

    private function setRealEstatePageMetadata(RealEstate $realEstate, Language $language)
    {
        $service = new ServiceRealEstate($this->em);
        $metadata = $service->getRealEstateMetadataLocalized($realEstate, $language);
        if($metadata != null)
        {
            $this->setMetaTitle($metadata->getTitle());
            $this->setMetaDescription($metadata->getDescription());
        }
    }
}