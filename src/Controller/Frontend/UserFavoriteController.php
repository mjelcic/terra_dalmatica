<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 5/27/2020
 * Time: 9:28 AM
 */

namespace App\Controller\Frontend;


use App\Entity\Language;
use App\Entity\RealEstateLocalization;
use App\Services\NotificationService;
use App\Services\ServiceRealEstate;
use App\Services\ServiceUserFavorite;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;

class UserFavoriteController extends AbstractController
{
    private $translator;
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function addRemoveFavoriteForClient(ServiceRealEstate $serviceRealEstate, ServiceUserFavorite $serviceUserFavorite, $id)
    {
        $realEstate = $serviceRealEstate->getById($id);
        if($realEstate == null){
            return new Response("Greška u zahtjevu!", 404);
        }

        $user = $this->getUser();
        if($user == null || !in_array("ROLE_USER", $this->getUser()->getRoles()) ){
            return new Response("Greška u zahtjevu!", 500);
        }

        $uf = $serviceUserFavorite->getByUserAndRealEstate($user, $realEstate);

        $result = [];
        if($uf == null){
            $success = $serviceUserFavorite->createAndSave($realEstate, $this->getUser());
            $result["success"] = $success;
            $result["message"] = $success ? $this->translator->trans("Uspješno ste dodali nekretninu u favorite.") :
                $this->translator->trans("Greška prilikom spašavanja nekretnine u favorite.");
        }

        if($uf != null){
            $success = $serviceUserFavorite->remove($uf, $user);
            $result["success"] = $success;
            $result["message"] = $success ? $this->translator->trans("Uspješno ste uklonili nekretninu iz favorita.") :
                $this->translator->trans("Greška prilikom uklanjanja nekretnine iz favorita.");
        }

        return $this->json($result);
    }

    public function addRemoveFavoriteForClient2(Request $request, ServiceRealEstate $serviceRealEstate, ServiceUserFavorite $serviceUserFavorite, EntityManagerInterface $entityManager, $id)
    {
        $realEstate = $serviceRealEstate->getById($id);
        $language = $entityManager->getRepository(Language::class)->findOneBy(array("code"=>$request->getLocale()));
        $realEstateLocalization = $entityManager->getRepository(RealEstateLocalization::class)->findOneBy(array("RealEstate"=>$realEstate,"language"=>$language ));
        $user = $this->getUser();
        if($this->isGranted("ROLE_USER")) {

            $uf = $serviceUserFavorite->getByUserAndRealEstate($user, $realEstate);

            $result = [];
            if ($uf == null) {
                $success = $serviceUserFavorite->createAndSave($realEstate, $this->getUser());
                $result["success"] = $success;
                $result["message"] = $success ? $this->translator->trans("Uspješno ste dodali nekretninu u favorite.") :
                    $this->translator->trans("Greška prilikom spašavanja nekretnine u favorite.");
            }

            if ($uf != null) {
                $result["message"] = $this->translator->trans("Nekretnina vam je već dodana u favorite.");
            }

            return $this->notification($result["message"], $realEstateLocalization->getSlug());
        }
        else{
            return $this->notification($this->translator->trans("Nemate potrebnu rolu da bi sačuvali nekretninu"), $realEstateLocalization->getSlug());
        }
    }

    protected function notification($text, $slug)
    {
        $html = "<div class='text-center'><h1 class='h4 text-gray-900 mb-4'>" . $this->translator->trans("Obavijest") . "!</h1></div>" . "<p>" . $text . "</p>";
        $this->addFlash("success", $html);

        return $this->redirectToRoute('real_estate_details', array("slug"=>$slug));
    }

}