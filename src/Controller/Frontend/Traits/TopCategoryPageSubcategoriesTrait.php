<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/11/2020
 * Time: 11:47 AM
 */

namespace App\Controller\Frontend\Traits;


use App\Services\ServiceRealEstateCustomPage;

trait TopCategoryPageSubcategoriesTrait
{
    private function getTopSubcategories(ServiceRealEstateCustomPage $serviceRealEstateCustomPage, $locale)
    {
        $specialPage = $serviceRealEstateCustomPage->getSpecialPage();
        $specialPageLoco = $specialPage->getRealEstateCustomPageLocalization($locale);

        //get top cat. page subcategories
        $subCategories = $serviceRealEstateCustomPage->getByParentLocalized($specialPage, $locale);
        for ($i = 0; $i < count($subCategories); $i++){
            $subCategories[$i]["parent_slug"] = $specialPageLoco->getSlug();
        }

        return $subCategories;
    }
}