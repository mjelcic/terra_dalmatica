<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/26/2020
 * Time: 1:15 PM
 */

namespace App\Controller\Frontend;


use App\Entity\Enum\RoleEnum;
use App\Entity\User;
use App\Form\Frontend\UserPasswordResetType;
use App\Services\ServiceRealEstate;
use App\Services\ServiceUserFavorite;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Flex\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;


class UserController extends AbstractController
{
    private $translator;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function isClientUserLoggedIn()
    {
        $isLogged = false;

        if (($this->getUser() != null) && in_array("ROLE_USER", $this->getUser()->getRoles())) {
            $isLogged = true;
        }

        return $this->json($isLogged);
    }

    public function userPasswordReset(Request $request, $user_id, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($this->getUser()->getId() != $user_id) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN');
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id' => $user_id));
        $user->setUpdatedAt(new \DateTime());
        $user->setCreatedBy($this->getUser()->getId());
        $form = $this->createForm(UserPasswordResetType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);


            $em->flush();
            $this->addFlash("success", "Lozinka promijenjena");

            if (in_array(RoleEnum::ADMIN, $this->getUser()->getRoles())) {
                if (in_array(RoleEnum::EMPLOYEE, $user->getRoles())) {
                    $route = "user_list";
                } else {
                    $route = "client_list";
                }
            }
            else{
                $route = "backend_client_index";
            }


            return $this->redirectToRoute($route);
        }

        return $this->render('backend/user_password_reset.html.twig', array('form' => $form->createView()));
    }
}