<?php

namespace App\Controller\Frontend;

use App\Entity\CallButton;
use App\Entity\Language;
use App\Form\Frontend\SearchType;
use App\GlobalSettings;
use App\Services\LanguageService;
use App\Services\ServiceExchangeRate;
use App\ViewModel\Frontend\BreadcrumbElementModel;
use App\ViewModel\Frontend\SearchModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Services\ContentService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


abstract class BaseController extends AbstractController
{

    protected $parameters;
    protected $contentService;
    protected $global;
    protected $em;
    protected $session;
    protected $languageService;
    protected $urlGenerator;

    function __construct(ContentService $contentService,SessionInterface $session, GlobalSettings $global, EntityManagerInterface $em, LanguageService $languageService, ServiceExchangeRate $serviceExchangeRate, UrlGeneratorInterface $urlGenerator)
    {
        $this->contentService = $contentService;
        $this->session = $session;
        $this->global = $global;
        $this->languageService = $languageService;
        $this->em = $em;
        $this->parameters = new \stdClass();
        $this->parameters->predefinedContent = $this->contentService->getPredefinedContentSlugs();
        $this->parameters->languages = $this->languageService->getLanguages();
        $this->setMetaDescription($global->getDefaultMetaData()["description"]);
        $this->setMetaTitle($global->getDefaultMetaData()["title"]);
        $this->setLanguageSelector("hr", "/");
        $this->setLanguageSelector("en", "/en");
        $this->setLanguageSelector("de", "/de");
        $this->parameters->callButton = $this->em->getRepository(CallButton::class)->findOneBy(array("active"=> true));

        //Set exchange rate value for easy access in calc of the price in kn
        $currentExchangeRate = $serviceExchangeRate->getCurrentExchangeRate();
        $this->parameters->exchangeRate = $currentExchangeRate != null ? $currentExchangeRate->getRate() : 0;

        $this->urlGenerator = $urlGenerator;
    }

    public function setMetaDescription($text)
    {
        $this->parameters->metaDescription = $text;
    }

    public function addBreadcrumbElement($url, $title)
    {
        $breadcrumb = new BreadcrumbElementModel($title, $url);
        $this->parameters->breadcrumbs[] = $breadcrumb;
    }

    public function setMetaTitle($text)
    {
        $this->parameters->metaTitle = $text;
    }

    public function setLanguageSelector($language, $route)
    {
        $this->parameters->languageSelector[$language] = $route;
    }

    protected function setShowSearchbar(boolean $bShow)
    {
        $this->parameters->showSearchbar = $bShow;
    }

    protected function prepareSearchbarForm($locale)
    {
        $searchModel = new SearchModel();
        $form = $this->createForm(
            SearchType::class,
                $searchModel,
                array(
                    "locale" => $locale,
                    "method" => "GET",
                    "action" => $this->urlGenerator->generate("real_estate_search")
                )
        );

        return $form->createView();
    }
}
