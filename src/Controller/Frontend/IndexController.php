<?php

namespace App\Controller\Frontend;

use App\Controller\Frontend\Traits\TopCategoryPageSubcategoriesTrait;
use App\Entity\CmsPage;
use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use App\Form\Frontend\SearchType;
use App\GlobalSettings;
use App\Services\ContentService;
use App\Services\LanguageService;
use App\Services\ServiceExchangeRate;
use App\Services\ServiceLocation;
use App\Services\ServiceRealEstate;
use App\Services\ServiceRealEstateCustomPage;
use App\Services\ServiceUserPreference;
use App\ViewModel\Frontend\HomePageViewModel;
use App\ViewModel\Frontend\SearchModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class IndexController extends BaseController
{
    use TopCategoryPageSubcategoriesTrait;

    protected $realEstateService;

    public function __construct(ContentService $contentService, SessionInterface $session, GlobalSettings $global, EntityManagerInterface $em,
                                LanguageService $languageService, ServiceExchangeRate $serviceExchangeRate, ServiceRealEstateCustomPage $realEstateService, UrlGeneratorInterface $urlGenerator)
    {
        parent::__construct($contentService, $session, $global, $em, $languageService, $serviceExchangeRate, $urlGenerator);
        $this->realEstateService = $realEstateService;
    }

    public function index(Request $request, ServiceLocation $serviceLocation, ServiceRealEstateCustomPage $serviceRealEstateCustomPage, ServiceRealEstate $serviceRealEstate, ServiceUserPreference $serviceUserPreference)
    {
        $locale = $request->getLocale();
        $subCategories = $this->getTopSubcategories($serviceRealEstateCustomPage, $locale);

        $locationCategoryPages = $serviceLocation->getLocationsInOfferLocalized($locale, [RealEstateCustomPageTypeEnum::AUTOGENERATED]);

        $customCategoryPages = $this->realEstateService->getCategoryPagesLocalized($locale, RealEstateCustomPageTypeEnum::CUSTOM);

        $suggestedRealEstates = [];
        if($this->getUser() != null){
            $userPreference = $serviceUserPreference->getByUser($this->getUser());
            if($userPreference != null){
                $suggestedRealEstates = $serviceRealEstate->getByUserPreferenceLocalized($userPreference, $locale);
            }
        }

        $recentRealEstates = $serviceRealEstate->getRecentRealEstatesLocalized(GlobalSettings::getRandomRealEstatesCrossSaleCount(), $locale);

        $featuredRealEstates = $serviceRealEstate->getFeaturedRealEstatesLocalized(GlobalSettings::FeaturedRealEstatesForDisplayCount, $locale);

        $viewModel = new HomePageViewModel($subCategories, $locationCategoryPages, $customCategoryPages, $recentRealEstates, $featuredRealEstates, $suggestedRealEstates);

        //set meta data
        $cmsPage = $this->em->getRepository(CmsPage::class)->findOneBy(array("display_id"=>"homepage"));
        if($cmsPage) {
            $metaData = $this->contentService->getSingleContentMeta($cmsPage->getId(), $request->getLocale());
            $this->setMetaDescription($metaData["description"]);
            $this->setMetaTitle($metaData["title"]);
        }

        return $this->render('frontend/index.html.twig', [
            'parameters' => $this->parameters,
            'viewModel' => $viewModel,
            'customCategoryPages' => $customCategoryPages,
            "form" => $this->prepareSearchbarForm($locale)
        ]);
    }

    protected function prepareSearchbarForm($locale)
    {
        $searchModel = new SearchModel();
        $form = $this->createForm(
            SearchType::class,
            $searchModel,
            array(
                "locale" => $locale,
                "method" => "GET",
                "action" => $this->urlGenerator->generate("real_estate_search")
            )
        );

        return $form->createView();
    }
}
