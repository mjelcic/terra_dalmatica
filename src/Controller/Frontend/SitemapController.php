<?php

namespace App\Controller\Frontend;

use App\Entity\CmsPage;
use App\Entity\CmsPageLocalization;
use App\Entity\CmsPageMetaLocalization;
use App\Entity\Language;
use App\Entity\RealEstateCustomPage;
use App\Services\ContentService;
use App\Services\ServiceRealEstateCustomPage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends BaseController
{

    public function sitemap(Request $request)
    {
        //set meta data
        $cmsPage = $this->em->getRepository(CmsPage::class)->findOneBy(array("display_id"=>"sitemap"));
        if($cmsPage) {
            $metaData = $this->contentService->getSingleContentMeta($cmsPage->getId(), $request->getLocale());
            $this->setMetaDescription($metaData["description"]);
            $this->setMetaTitle($metaData["title"]);
        }

        //set language selectors
        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach($languages as $language) {
            $this->setLanguageSelector($language->getCode(), $this->generateUrl("sitemap", array("_locale"=>$language->getCode()) ));
        }

        $categoryPages = $this->em->getRepository(RealEstateCustomPage::class)->findBy(array("parentRealEstateCustomPage"=>null, "invalidated" => 0, "type" => 1, "active" => 1));


        return $this->render('frontend/sitemap.html.twig', array( "parameters" => $this->parameters, 'locationCategoryPages' => $categoryPages));
    }
}
