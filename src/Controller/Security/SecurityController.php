<?php


namespace App\Controller\Security;

use App\Entity\CmsPage;
use App\Entity\Enum\RoleEnum;
use App\Form\Security\UserForgotPasswordType;
use App\Form\Security\UserResetPasswordType;
use App\Utility\PostOfficeUtility;
use App\Utility\SwiftMailerUtil;
use App\ViewModel\UserForgotPasswordViewModel;
use App\ViewModel\UserResetPasswordViewModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\Security\UserRegistrationType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\GlobalSettings;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\NotificationService;
use Symfony\Contracts\Translation\TranslatorInterface;


class SecurityController extends AbstractController
{
    private $entityManager;
    private $translator;
    private $global;
    private $mailer;
    private $notificationService;
    private $postOffice;

    function __construct(EntityManagerInterface $em, TranslatorInterface $translator, GlobalSettings $gl, \Swift_Mailer $mailer, NotificationService $notificationService, PostOfficeUtility $postOffice)
    {
        $this->global = $gl;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->entityManager = $em;
        $this->notificationService = $notificationService;
        $this->postOffice = $postOffice;
    }

    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, SwiftMailerUtil $swiftMailerUtil)
    {
        // 1) build the form
        $user = new User();
        $user->setCreatedBy(0);
        $user->setActivationCode(sha1(mt_rand(10000, 99999) . time()));
        $user->setRoles(array("ROLE_USER"));
        $user->setIsActive(0);
        $form = $this->createForm(UserRegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $message = (new \Swift_Message())
                ->setSubject('Obavijest sa TerraDalmatica')
                ->setFrom($this->global->getFromMail())
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'email/registration_activation_' . $request->getLocale()  . '.html.twig',
                        array(
                            'profile' => $user,
                            'activation' => $user->getActivationCode()
                        )
                    ),
                    'text/html'
                );

            $this->mailer->send($message);

            $this->postOffice->notifyAboutNewClientRegistration($user);

            return $this->notification("Poslali smo vam aktivacijski link na Vašu email adresu");
        }

        //personal-data-protection slug
        $personalDataProtectionSlug = "not-found";
        $cmsPage = $this->entityManager->getRepository(CmsPage::class)->findOneBy(array("display_id"=>"personal-data-protection"));
        if($cmsPage){
            $personalDataProtectionSlug = $cmsPage->getCmsPageLocalization($request->getLocale())->getSlug();
        }


        return $this->render(
            'security/registration.html.twig',
            array(
                'form' => $form->createView(),
                'action'=>'register',
                'personalDataProtectionSlug' => $personalDataProtectionSlug)
        );
    }

    public function forgotPassword(Request $request)
    {
        $errorMsg = "";
        $userForgotPassVM = new UserForgotPasswordViewModel();
        $form = $this->createForm(UserForgotPasswordType::class, $userForgotPassVM);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            //send an email with a link containing id and activation code
            /** @var $user User  */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'email' =>$userForgotPassVM->getEmail()
            ));

            if($user)
            {
                $message = (new \Swift_Message())
                    ->setSubject($this->translator->trans("Terra dalmatica"))
                    ->setFrom($this->global->getFromMail())
                    ->setTo($user->getEmail())
                    ->setBody($this->renderView('email/forgot_password_' . $request->getLocale()  . '.html.twig', array('id' => $user->getId(), 'username' => $user->getUsername(), 'activation'=>$user->getActivationCode())),
                        'text/html'
                    );
                $this->mailer->send($message);

                return $this->notification("Poslali smo vam email sa uputama za resetiranje lozinke");
            }

            if(!$user)
            {
                $errorMsg = "Korisnički račun za ovaj email ne postoji!";
            }
        }

        return $this->render('security/forgot_password.html.twig', array(
            'form' => $form->createView(),
            'error' => $errorMsg
        ));
    }

    public function resetPassword(Request $request, $id, $activation, UserPasswordEncoderInterface $passwordEncoder)
    {
        //check the id and activation by trying to get the user
        /**
         * @var $confirmUser User
         */
        $confirmUser = null;
        $userEntity = new User();
        $errorMsg = "";
        if($request->isMethod('GET'))
        {
            $confirmUser = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'id' => $id,
                'activation_code' => $activation
            ));

            if(!$confirmUser)
            {
                return $this->notification("Your request is not valid");
            }
        }

        $form = $this->createForm(UserResetPasswordType::class, $userEntity);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            /**
             * @var $user User
             */
            $user = $this->entityManager->getRepository(User::class )->find($id);

            $password = $passwordEncoder->encodePassword($user, $userEntity->getPassword());

            $user->setPassword($password);

            try{
                $this->entityManager->flush();

                return $this->notification("Vaša lozinka je promijenjena");
            }
            catch(\Exception $ex)
            {
                $errorMsg = "There was an error during this request. Try again later";
            }
        }

        return $this->render('security/reset_password.html.twig', array(
            'form' => $form->createView(),
            'error' => $errorMsg
        ));
    }

    public function activate($activation)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array("activation_code"=>$activation));
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found'
            );
        }
        $user->setIsActive(1);
        $em->flush();
        return $this->notification("Račun aktiviran");
    }

    protected function notification($text)
    {
        $html = "<div class='text-center'><h1 class='h4 text-gray-900 mb-4'>" . $this->translator->trans("Obavijest") . "!</h1></div>" . "<p>" . $text . "</p>";
        $this->addFlash("success", $html);

        return $this->redirectToRoute('index');
    }

}
