<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Facebook\Request;


class FacebookDTO
{
    private $message;
    private $link;

    function __construct(
        string $message,
        string $link
    )
    {
        $this->message = $message;
        $this->link = $link;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getLink(): string
    {
        return $this->link;
    }

}