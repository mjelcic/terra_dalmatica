<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:44 PM
 */

namespace App\DTO\RealEstateCroatia;


class PriceDTO
{
    /** @var $amount float */
    public $amount;

    /** @var $currency string */
    public $currency;

    public function __construct($amount)
    {
        $this->amount = $amount != null ? (int)$amount : null;
        $this->currency = "EUR"; //Prices are in euros
    }
}