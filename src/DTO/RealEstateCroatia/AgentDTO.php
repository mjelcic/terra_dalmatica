<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:48 PM
 */

namespace App\DTO\RealEstateCroatia;


class AgentDTO
{
    /** @var  $name string */
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}