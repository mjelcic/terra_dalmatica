<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:20 PM
 */

namespace App\DTO\RealEstateCroatia;


use App\Utility\Export\RealEstateCroatiaHelper;

class RealEstateDTO
{
    /** @var $id integer */
    public $id;

    /** @var $property_id integer */
    public $property_id;

    /** @var $status integer */
    public $status;

    /** @var $action integer */
    public $action;

    /** @var $property_type integer */
    public $property_type;

    /** @var $date_listed \DateTime */
    public $date_listed;

    /** @var $date_modified \DateTime */
    public $date_modified;

    /** @var $rooms integer */
    public $rooms;

    /** @var $bathrooms integer */
    public $bathrooms;

    /** @var $lift bool */
    public $lift;

    /** @var $view integer */
    public $view;

    /** @var $sea_proximity integer */
    public $sea_proximity;

    /** @var $energy_efficiency integer */
    public $energy_efficiency;

    /** @var $location LocationDTO */
    public $location;

    /** @var $price PriceDTO */
    public $price;

    /** @var $area AreaDTO */
    public $area;

    /** @var $agent AgentDTO */
    public $agent;

    /** @var $title TitleDTO */
    public $title;

    /** @var $description DescriptionDTO */
    public $description;

    /** @var $images array */
    public $images;

    /** @var $video string */
    public $video;

    public function __construct(
        $id,
        $property_id,
        $status,
        $property_type,
        $date_listed,
        $date_modified,
        $rooms,
        $bathrooms,
        $view,
        $sea_proximity,
        $energy_efficiency,
        $city1, //real estate
        $city2, //location
        $postal_code1, //real estate
        $postal_code2, //location
        $amount,
        $livin_area,
        $land_area,
        $agent_first_name,
        $agent_last_name,
        $video
    )
    {
        $this->id = $id;
        $this->property_id = $property_id;
        $this->status = $status;
        $this->action = 1; //ONLY SALES
        $this->property_type = $property_type;
        $this->date_listed = $date_listed;
        $this->date_modified = $date_modified;
        $this->rooms = $rooms;
        $this->bathrooms = $bathrooms;
        $this->lift = 0;
        $this->view = $view;
        $this->sea_proximity = $sea_proximity;
        $this->energy_efficiency = $energy_efficiency;
        $this->location = new LocationDTO($city1 ?? $city2,
            array_key_exists($city1, RealEstateCroatiaHelper::getRECLocationIdMapper()) ? RealEstateCroatiaHelper::getRECLocationIdMapper()[$city1]
                : (array_key_exists($city2, RealEstateCroatiaHelper::getRECLocationIdMapper()) ? RealEstateCroatiaHelper::getRECLocationIdMapper()[$city2] : 5679),
            $postal_code1 ?? $postal_code2);
        $this->price = new PriceDTO($amount);
        $this->area = new AreaDTO($livin_area, $land_area);
        $this->agent = new AgentDTO(($agent_first_name ?? '') . ' ' . ($agent_last_name ?? ''));
        $this->title = new TitleDTO();
        $this->description = new DescriptionDTO();
        $this->images = [];
        $this->video;
    }

}