<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:34 PM
 */

namespace App\DTO\RealEstateCroatia;


class LocationDTO
{
    /** @var $city string */
    public $city;

    /** @var $rec_location_id string */
    public $rec_location_id;

    /** @var $zip string */
    public $zip;

    public function __construct($city, $rec_location_id, $zip)
    {
        $this->city = $city;
        $this->rec_location_id = $rec_location_id;
        $this->zip = $zip ?? '';
    }
}