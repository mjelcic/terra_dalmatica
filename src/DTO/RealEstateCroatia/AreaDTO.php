<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:46 PM
 */

namespace App\DTO\RealEstateCroatia;


class AreaDTO
{
    /** @var $living_area integer */
    public $living_area;

    /** @var  $land_area integer */
    public $land_area;

    public function __construct($living_area, $land_area)
    {
        $this->living_area = $living_area != null ? (int)$living_area : null;
        $this->land_area = $land_area != null ? (int)$land_area : null;
    }
}