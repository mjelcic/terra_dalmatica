<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:52 PM
 */

namespace App\DTO\RealEstateCroatia;


class DescriptionDTO
{
    /** @var  $hr string */
    public $hr;

    /** @var  $hr string */
    public $en;

    /** @var  $hr string */
    public $de;

    public function __construct($hr="", $en="", $de="")
    {
        $this->hr = $hr;
        $this->en = $en;
        $this->de = $de;
    }
}