<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/7/2020
 * Time: 2:55 PM
 */

namespace App\DTO\RealEstateCroatia;


class ImageDTO
{
    /** @var $image string */
    public $image;

    //public $featured;

    public function __construct($image)
    {
        $this->image = $image;
    }
}