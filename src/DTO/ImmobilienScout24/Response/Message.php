<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/27/2020
 * Time: 8:33 AM
 */

namespace App\DTO\ImmobilienScout24\Response;


class Message
{
    /** @var $message string */
    public $messageCode;

    /** @var  $message string */
    public $message;

    /** @var $id int|null */
    public $id;
}