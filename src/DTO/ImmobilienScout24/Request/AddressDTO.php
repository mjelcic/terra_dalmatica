<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/22/2020
 * Time: 12:21 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class AddressDTO
{
    /** @var $street string|null */
    public $street;

    /** @var $street string|null */
    public $postcode;

    /** @var $city string */
    public $city;

    /** @var $internationalCountryRegion InternationalCountryRegionDTO */
    public $internationalCountryRegion;

    /** @var $wgs84Coordinate Wgs84CoordinateDTO|null */
    public $wgs84Coordinate;

    public function __construct(string $city, ?string $street, ?string $postcode, ?string $latitude, ?string $longitude)
    {
        $this->city = $city;
        $this->street = $street;
        $this->postcode = $postcode;
        $this->internationalCountryRegion = new InternationalCountryRegionDTO();

        if($latitude != null && $longitude != null){
            $this->wgs84Coordinate = new Wgs84CoordinateDTO($latitude, $longitude);
        }
    }
}