<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/22/2020
 * Time: 12:28 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class Wgs84CoordinateDTO
{
    /** @var  $latitude string */
    public $latitude;

    /** @var  $latitude string */
    public $longitude;

    public function __construct(?string $latitude, ?string $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }
}