<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/22/2020
 * Time: 1:58 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class PriceDTO
{
    /** @var  $value float */
    public $value;

    /** @var  $currency */
    public $currency;

    public function __construct(?float $value)
    {
        $this->value = $value ?? 0;
        $this->currency = "EUR";
    }
}