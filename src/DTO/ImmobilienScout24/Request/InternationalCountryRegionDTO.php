<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/22/2020
 * Time: 12:31 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class InternationalCountryRegionDTO
{
    /** @var $country string */
    public $country;

    /** @var $region string */
    public $region;

    public function __construct()
    {
        $this->country = "HRV";
        $this->region = "Dalmatien-Sibenik [Sibensko-Kninska]"; //We will set this one by default, they can change it afterwards thru the immoscout portal
    }
}