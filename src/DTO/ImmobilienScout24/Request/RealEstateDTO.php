<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2020
 * Time: 2:25 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class RealEstateDTO
{
    /** @var $externalId int BASE */
    public $externalId;

    /** @var $title string BASE  */
    public $title;

    /** @var $creationDate \DateTime BASE  */
    public $creationDate;

    /** @var $lastModificationDate \DateTime BASE  */
    public $lastModificationDate;

    /** @var $address AddressDTO BASE  */
    public $address;

    /** @var $descriptionNote string BASE - Max. 3.999 characters  */
    public $descriptionNote;

    /** @var $showAddress bool BASE */
    public $showAddress;

    /**
     * Applicable to Land
     *
     * @var $commercializationType string
     */
    public $commercializationType;

    /**
     * Applicable to Apartment
     *
     * @var $lift bool
     */
    public $lift;

    /**
     * Applicable to House
     *
     * @var $buildingType string
     */
    public $buildingType;

    /**
     * Applicable to Apartment and House
     *
     * @var $energyCertificate EnergyCertificateDTO
     */
    public $energyCertificate;

    /**
     * Applicable to Apartment and House
     * TRUE - YES; FALSE - NOT_APPLICABLE
     *
     * @var  $handicappedAccessible string
     */
    public $cellar;

    /**
     * Applicable to Apartment and House
     * TRUE - YES; FALSE - NOT_APPLICABLE
     *
     * @var  $handicappedAccessible string
     */
    public $handicappedAccessible;

    /**
     * Applicable to Apartment and House
     *
     * @var $numberOfBedRooms int
     */
    public $numberOfBedRooms;

    /**
     * Applicable to Apartment and House
     *
     * @var $numberOfBathRooms int
     */
    public $numberOfBathRooms;

    /**
     * Applicable to all
     *
     * @var  $price PriceDTO
     */
    public $price;

    /**
     * Applicable to Apartment and House
     *
     * @var $livingSpace double
     */
    public $livingSpace;

    /**
     * Applicable to House and Land
     *
     * @var $plotArea double
     */
    public $plotArea;

    /**
     * Applicable to Apartment and House
     *
     * @var $numberOfBedRooms int
     */
    public $numberOfRooms;

    /**
     * Applicable to Apartment
     *
     * @var $lift bool
     */
    public $balcony;

    /**
     * Applicable to Apartment
     *
     * @var $lift bool
     */
    public $garden;

    /**
     * Applicable to
     *
     * @var  $courtage CourtageDTO
     */
    public $courtage;

    public function __construct()
    {
        $this->showAddress = false; //I guess...
        $this->courtage = new CourtageDTO();
    }
}