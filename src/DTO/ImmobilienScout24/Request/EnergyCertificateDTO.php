<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2020
 * Time: 1:41 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class EnergyCertificateDTO
{
    /**
     * Values: NOT_APPLICABLE, A+, A, B, C, D, E, F, G, H.
     *
     * @var $energyEfficiencyClass string
     */
    public $energyEfficiencyClass;

    public function __construct(?string $energyEfficiencyClass)
    {
        $this->energyEfficiencyClass = $energyEfficiencyClass ?? "NOT_APPLICABLE";
    }
}