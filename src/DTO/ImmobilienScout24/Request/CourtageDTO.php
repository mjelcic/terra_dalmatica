<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/22/2020
 * Time: 2:31 PM
 */

namespace App\DTO\ImmobilienScout24\Request;


class CourtageDTO
{
    /** @var $hasCourtage string */
    public $hasCourtage;

    /** @var $courtage string */
    public $courtage;

    function __construct()
    {
        $this->hasCourtage = "YES";
        $this->courtage = "Provision für Käufer beträgt 3%+MwSt";
    }
}