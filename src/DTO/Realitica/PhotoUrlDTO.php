<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Realitica;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\SerializedName;

class PhotoUrlDTO
{
    private $photo_url;

    function __construct($photo_url)
    {
        $this->photo_url = $photo_url;
    }

    /**
     * @SerializedName("photo_url")
     */
    public function getImage(): string
    {
        return $this->photo_url;
    }
}