<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Realitica;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\SerializedName;

class FeaturesDTO
{
    private $feature;

    function __construct($feature)
    {
        $this->feature = $feature;
    }

    /**
     * @SerializedName("feature")
     */
    public function getFeature(): string
    {
        return $this->feature;
    }
}