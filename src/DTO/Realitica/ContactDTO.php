<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Realitica;


class ContactDTO
{
    private $contact_name;
    private $contact_phone;
    private $contact_email;

    function __construct(
        ?string $contact_name,
        ?string $contact_phone,
        ?string $contact_email
    )
    {
        $this->contact_name = $contact_name;
        $this->contact_phone = $contact_phone;
        $this->contact_email = $contact_email;
    }

    public function getContactName(): ?string
    {
        return $this->contact_name;
    }

    public function getContactPhone(): ?string
    {
        return $this->contact_phone;
    }

    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }
}