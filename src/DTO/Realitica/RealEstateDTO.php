<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Realitica;
use Symfony\Component\Serializer\Annotation\SerializedName;


class RealEstateDTO
{

    private $listing_id;

    private $listing_type;

    private $object_type;

    private $address_text;

    private $neighborhood_text;

    private $euros_price_int;

    private $bedrooms_int;

    private $bathrooms_number;

    private $living_area_m2_int;

    private $land_area_m2_int;

    private $title_text;

    private $title_text_en;

    private $title_text_de;

    private $description_text;

    private $description_text_en;

    private $description_text_de;

    /**
     * @var FeaturesDTO[]
     * @SerializedName("feature")
     */
    public $features;

    private $energy_rating; // A+ | A | B | C | D | E | F | G | exempt

    /**
     * @var PhotoUrlDTO[]
     * @SerializedName("photo_url")
     */
    public $photoUrl;

    private $listing_url; // string <![CDATA[ ... ]]>

    private $listing_url_en; // string <![CDATA[ ... ]]>

    private $listing_url_de; // string <![CDATA[ ... ]]>

    /**
     * @var ContactDTO|null
     */
    public $contact;

    function __construct(
        string $listing_id,
        string $object_type,
        ?string $address_text,
        ?string $neighborhood_text,
        ?int $euros_price_int,
        ?int $bedrooms_int,
        ?float $bathrooms_number,
        ?int $living_area_m2_int,
        ?int $land_area_m2_int,
        ?string $title_text,
        ?string $title_text_en,
        ?string $title_text_de,
        ?string $description_text,
        ?string $description_text_en,
        ?string $description_text_de,
        ?string $energy_rating,
        ?string $listing_url,
        ?string $listing_url_en,
        ?string $listing_url_de
    )
    {
        $this->listing_id = $listing_id;
        $this->listing_type = 'for sale';
        $this->object_type = $object_type;
        $this->address_text = $address_text;
        $this->neighborhood_text = $neighborhood_text;
        $this->euros_price_int = $euros_price_int;
        $this->bedrooms_int = $bedrooms_int;
        $this->bathrooms_number = $bathrooms_number;
        $this->living_area_m2_int = $living_area_m2_int;
        $this->land_area_m2_int = $land_area_m2_int;
        $this->title_text = $title_text;
        $this->title_text_en = $title_text_en;
        $this->title_text_de = $title_text_de;
        $this->description_text = $description_text;
        $this->description_text_en = $description_text_en;
        $this->description_text_de = $description_text_de;
        $this->energy_rating = $energy_rating;
        $this->listing_url = $listing_url;
        $this->listing_url_en = $listing_url_en;
        $this->listing_url_de = $listing_url_de;



    }

    /**
     * @SerializedName("listing_id")
     */
    public function getListingId(): string
    {
        return $this->listing_id;
    }

    /**
     * @SerializedName("listing_type")
     */
    public function getListingType(): string
    {
        return $this->listing_type;
    }

    /**
     * @SerializedName("address_text")
     */
    public function getAddressText(): string
    {
        return $this->address_text;
    }

    /**
     * @SerializedName("neighborhood_text")
     */
    public function getNeighborhoodText(): ?string
    {
        return $this->neighborhood_text;
    }

    /**
     * @SerializedName("euros_price_int")
     */
    public function getEurosPriceInt(): ?int
    {
        return $this->euros_price_int;
    }

    /**
     * @SerializedName("bedrooms_int")
     */
    public function getBedroomsInt(): ?string
    {
        return $this->bedrooms_int;
    }

    /**
     * @SerializedName("bathrooms_number")
     */
    public function getBathroomsNumber(): ?float
    {
        return $this->bathrooms_number;
    }

    /**
     * @SerializedName("living_area_m2_int")
     */
    public function getLivingAreaM2Int(): ?int
    {
        return $this->living_area_m2_int;
    }

    /**
     * @SerializedName("land_area_m2_int")
     */
    public function getLandAreaM2Int(): ?int
    {
        return $this->land_area_m2_int;
    }

    /**
     * @SerializedName("title_text")
     */
    public function getTitleText(): ?string
    {
        return $this->title_text;
    }

    /**
     * @SerializedName("title_text_en")
     */
    public function getTitleTextEn(): ?string
    {
        return $this->title_text_en;
    }

    /**
     * @SerializedName("title_text_de")
     */
    public function getTitleTextDe(): ?string
    {
        return $this->title_text_de;
    }

    /**
     * @SerializedName("description_text")
     */
    public function getDescriptionText(): ?string
    {
        return $this->description_text;
    }

    /**
     * @SerializedName("description_text_en")
     */
    public function getDescriptionTextEn(): ?string
    {
        return $this->description_text_en;
    }

    /**
     * @SerializedName("description_text_de")
     */
    public function getDescriptionTextDe(): ?string
    {
        return $this->description_text_de;
    }

    /**
     * @SerializedName("listing_url")
     */
    public function getListingUrl(): ?string
    {
        return $this->listing_url;
    }

    /**
     * @SerializedName("listing_url_en")
     */
    public function getListingUrlEn(): ?string
    {
        return $this->listing_url_en;
    }

    /**
     * @SerializedName("listing_url_de")
     */
    public function getListingUrlDe(): ?string
    {
        return $this->listing_url_de;
    }

    /**
     * @SerializedName("energy_rating")
     */
    public function getEnergyRating(): ?string
    {
        return $this->energy_rating;
    }

}