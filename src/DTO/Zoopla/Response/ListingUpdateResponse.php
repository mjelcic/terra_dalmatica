<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Response;


class ListingUpdateResponse
{
    public $status;

    public $listing_reference;

    public $listing_etag;

    public $url;

    public $new_listing;
}
 