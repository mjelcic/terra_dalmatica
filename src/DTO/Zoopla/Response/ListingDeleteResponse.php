<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Response;


class ListingDeleteResponse
{

    public $status;

    public $listing_reference;

}
 