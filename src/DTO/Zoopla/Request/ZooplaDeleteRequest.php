<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ZooplaDeleteRequest
{

    public $listing_reference;

    public $deletion_reason;

    /**
     * ZooplaDeleteRequest constructor.
     * @param $listing_reference
     * @param $deletion_reason
     */
    public function __construct($listing_reference, $deletion_reason)
    {
        $this->listing_reference = $listing_reference;
        $this->deletion_reason = $deletion_reason;
    }


}
 