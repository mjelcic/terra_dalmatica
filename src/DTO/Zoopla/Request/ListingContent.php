<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ListingContent
{

    public $url;

    public $type;

    public $caption;

}
 