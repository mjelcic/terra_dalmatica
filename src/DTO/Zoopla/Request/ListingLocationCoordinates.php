<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ListingLocationCoordinates
{
    public $latitude;

    public $longitude;
}
 