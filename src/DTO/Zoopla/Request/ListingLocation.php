<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ListingLocation
{
    public $street_name;

    public $town_or_city;

    public $postal_code;

    public $country_code;

    //public $coordinates;
}
 