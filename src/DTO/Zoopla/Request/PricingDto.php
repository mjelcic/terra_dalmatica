<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class PricingDto
{

    public $transaction_type = "sale";

    public $currency_code = "EUR";

    public $price;

}
 