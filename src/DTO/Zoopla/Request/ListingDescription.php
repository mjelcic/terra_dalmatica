<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ListingDescription
{

    public $heading;

    public $text;

    /**
     * ListingDescription constructor.
     * @param $heading
     * @param $text
     */
    public function __construct($heading, $text)
    {
        $this->heading = $heading;
        $this->text = $text;
    }


}
 