<?php
declare(strict_types=1);

namespace App\DTO\Zoopla\Request;


class ListingUpdateRequest
{
    // mandatory
    public $branch_reference;

    public $category;

    public $detailed_description;

    // status; 2 - available, 4 - under_offer, 5 - sold
    public $life_cycle_status;

    // uid
    public $listing_reference;

    public $location;

    // {transaction_type: sale, currency_code: EUR, price: duh}
    public $pricing;

    public $property_type;

    //non mandatory, but we have data
    public $accessibility;
    /**
     * @var int|null
     */
    public $total_bedrooms;
    /**
     * @var bool
     */
    public $basement;
    /**
     * @var int|null
     */
    public $bathrooms;
    /**
     * @var bool
     */
    public $burglar_alarm;
    /**
     * @var string|null
     */
    public $display_address;
    /**
     * @var array
     */
    public $feature_list;
    /**
     * @var bool
     */
    public $fireplace;
    /**
     * @var bool|null
     */
    public $swimming_pool;
    /**
     * @var bool
     */
    public $new_home;
    /**
     * @var bool|null
     */
    public $waterfront;
    /**
     * @var ListingContent[]
     */
    public $content;

}
 