<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Twitter\Response;


class TwitterImageDTO
{
    public $media_id;
    public $media_id_string;
    public $media_key;
    public $size;
    public $expires_after_secs;
}