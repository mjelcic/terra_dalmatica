<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Twitter\Request;


class TwitterDTO
{
    private $status;

    function __construct(
        string $status
    )
    {
        $this->status = $status;
    }

    public function getStatus(): string
    {
        return $this->status;
    }


}