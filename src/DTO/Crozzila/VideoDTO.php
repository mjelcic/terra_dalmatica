<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;

class VideoDTO
{
    //youtube video url
    private $video;

    function __construct(string $video)
    {
        $this->video = $video;
    }

    public function getVideo(): string
    {
        return $this->video;
    }
}