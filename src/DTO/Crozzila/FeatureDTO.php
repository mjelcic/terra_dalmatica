<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;


class FeatureDTO
{
    private $bedrooms;
    private $bathrooms;
    private $rooms; //not null
    private $condition; // used/not used
    private $age; // yyy
    private $floor;  //(basement/ground floor/high ground floor/1/2/3/4/… /attic/penthouse)


    function __construct(
        ?int $bedrooms,
        ?int $bathrooms,
        ?int $rooms,
        ?string $condition,
        ?int $age,
        ?string $floor
    )
    {
        $this->bedrooms = $bedrooms;
        $this->bathrooms = $bathrooms;
        $this->rooms = $rooms;
        $this->condition = $condition;
        $this->age = $age;
        $this->floor = $floor;
    }

    public function getBedroomsCode(): ?int
    {
        return $this->bedrooms;
    }

    public function getBathrooms(): ?int
    {
        return $this->bathrooms;
    }

    public function getRooms(): ?int
    {
        return $this->rooms;
    }

    public function getCondition(): ?string
    {
        return $this->condition;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function getFloor(): ?string
    {
        return $this->floor;
    }
}