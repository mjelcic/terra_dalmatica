<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;

use App\GlobalSettings;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\SerializedName;

class ImagesDTO
{
    //image url
    private $images = [];

    function __construct($images)
    {
        $this->images = $images;
    }

    /**
     * @SerializedName("image")
     */
    public function getImages(): array
    {
        return $this->images;
    }
}