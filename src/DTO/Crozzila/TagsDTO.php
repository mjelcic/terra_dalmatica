<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;
use Symfony\Component\Serializer\Annotation\SerializedName;


class TagsDTO
{
    private $tags = [];

    function __construct($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @SerializedName("tag")
     */
    public function getTags(): array
    {
        return $this->tags;
    }

}