<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;
use Symfony\Component\Serializer\Annotation\SerializedName;


class LocationDTO
{
    private $postal_code;
    private $city;

    //List of possible city-areas can be found on page 4 of xml specification
    private $city_area;

    private $address;
    private $geox;
    private $geoy;

    function __construct(
        int $postal_code,
        string $city,
        ?string $city_area,
        ?string $address,
        ?string $geox,
        ?string $geoy
    )
    {
        $this->postal_code = $postal_code;
        $this->city = $city;
        $this->city_area = $city_area;
        $this->address = $address;
        $this->geox = $geox;
        $this->geoy = $geoy;
    }

    /**
     * @SerializedName("postal-code")
     */
    public function getPostalCode(): int
    {
        return $this->postal_code;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCityArea(): ?string
    {
        return $this->city_area;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getGeox(): ?string
    {
        return $this->geox;
    }

    public function getGeoy(): ?string
    {
        return $this->geoy;
    }
}