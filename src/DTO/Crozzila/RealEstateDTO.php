<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;

use App\Entity\Enum\CrozzilaRealEstateTypeEnum;
use Symfony\Component\Serializer\Annotation\SerializedName;


class RealEstateDTO
{

    /**
     * @SerializedName("customer_name")
     */
    private $property_id;

    private $date_listed;

    private $property_type;

    private $listing_type;

    /**
     * @var LocationDTO
     */
    public $location; //not null

    /**
     * @var ContactDTO|null
     */
    public $contact;

    /**
     * @var NumberDTO
     * @SerializedName("property-size")
     */
    public $property_size;

    /**
     * @var NumberDTO|null
     * @SerializedName("land-size")
     */
    public $land_size;

    /**
     * @var AmountDTO
     */
    public $price;

    /**
     * @var PhotoUrlDTO
     */
    public $images;

    /**
     * @var VideoDTO
     */
    public $video;

    /**
     * @var FeatureDTO|null
     */
    public $features;

    /**
     * @var TagsDTO
     */
    public $tags;

    private $title;

    private $description;

    private $title_EN;

    private $description_EN;

    private $title_DE;

    private $description_DE;

    private $link;

    private $energy_efficiency;


    function __construct(
        int $property_id,
        string $date_listed,
        int $property_type,
        string $title,
        string $description,
        ?string $title_EN,
        ?string $description_EN,
        ?string $title_DE,
        ?string $description_DE,
        ?string $link
    )
    {
        $this->property_id = $property_id;
        $this->date_listed = $date_listed;
        $this->property_type = CrozzilaRealEstateTypeEnum::getTypeName($property_type);
        $this->listing_type = "sale";
        $this->title = $title;
        $this->description = $description;
        $this->title_EN = $title_EN;
        $this->description_EN = $description_EN;
        $this->title_DE = $title_DE;
        $this->description_DE = $description_DE;
        $this->link = $link;
    }

    /**
     * @SerializedName("property-id")
     */
    public function getPropertyId(): int
    {
        return $this->property_id;
    }

    /**
     * @SerializedName("date-listed")
     */
    public function getDateListed(): string
    {
        return $this->date_listed;
    }

    /**
     * @SerializedName("property-type")
     */
    public function getPropertyType(): string
    {
        return $this->property_type;
    }

    /**
     * @SerializedName("listing-type")
     */
    public function getListingType(): string
    {
        return $this->listing_type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @SerializedName("title-EN")
     */
    public function getTitleEn(): ?string
    {
        return $this->title_EN;
    }

    /**
     * @SerializedName("description-EN")
     */
    public function getDescriptionEn(): ?string
    {
        return $this->description_EN;
    }

    /**
     * @SerializedName("title-DE")
     */
    public function getTitleDe(): ?string
    {
        return $this->title_DE;
    }

    /**
     * @SerializedName("description-DE")
     */
    public function getDescriptionDe(): ?string
    {
        return $this->description_DE;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @SerializedName("energy_efficiency")
     */
    public function getEnergyEfficiency(): ?string
    {
        return $this->energy_efficiency;
    }



}