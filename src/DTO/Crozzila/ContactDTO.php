<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;


class ContactDTO
{
    // male/female
    private $title;
    private $business_title;
    private $name;
    private $surname;
    private $address;
    private $addressno;
    private $zipcode;
    private $city;
    private $phone;
    private $mobile;
    private $fax;
    private $email;

    function __construct(
        ?string $title,
        ?string $business_title,
        ?string $name,
        ?string $surname,
        ?string $address,
        ?string $addressno,
        ?string $zipcode,
        ?string $city,
        ?string $phone,
        ?string $mobile,
        ?string $fax,
        ?string $email
    )
    {
        $this->title = $title;
        $this->business_title = $business_title;
        $this->name = $name;
        $this->surname = $surname;
        $this->address = $address;
        $this->addressno = $addressno;
        $this->zipcode = $zipcode;
        $this->city = $city;
        $this->phone = $phone;
        $this->mobile = $mobile;
        $this->fax = $fax;
        $this->email = $email;
    }

    public function getTitleCode(): ?string
    {
        return $this->title;
    }

    public function getBusinessTitle(): ?string
    {
        return $this->business_title;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }
    public function getAddressno(): ?string
    {
        return $this->addressno;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }
    public function getPhone(): ?string
    {
        return $this->phone;
    }
    public function getMobile(): ?string
    {
        return $this->mobile;
    }
    public function getFax(): ?string
    {
        return $this->fax;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }
}