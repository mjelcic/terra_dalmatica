<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/28/2019
 * Time: 10:10 AM
 */

namespace App\DTO\Crozzila;


class NumberDTO
{
    private $number;

    function __construct(?float $number)
    {
        $this->number = $number;
    }

    public function getNumber(): ?float
    {
        return $this->number;
    }
}