<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 7/21/2020
 * Time: 12:57 PM
 */

namespace App\Utility\SystemConfig;


use App\Entity\SystemConfig;
use App\Services\ServiceSystemConfig;

class ImmobilienScoutConfig
{
    private $api_key_code = "immobilien-api-key";
    private $api_secret_code = "immobilien-api-secret";
    private $username_code = "immobilien-username";
    private $service;

    function __construct(ServiceSystemConfig $serviceSystemConfig)
    {
        $this->service = $serviceSystemConfig;
    }

    public function getApiKey()
    {
        return $this->getConfigValue($this->api_key_code);
    }

    public function getApiSecret()
    {
        return $this->getConfigValue($this->api_secret_code);
    }

    public function getUsername()
    {
        return $this->getConfigValue($this->username_code);
    }

    private function getConfigValue($code)
    {
        /** @var $config SystemConfig[]|null */
        $config = $this->service->getByCode($code);
        $result = $config ? $config[0]->getValue() : "";

        return $result;
    }
}