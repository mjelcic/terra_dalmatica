<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/21/2020
 * Time: 3:11 PM
 */

namespace App\Utility\SystemConfig;


use App\Services\ServiceSystemConfig;
use Doctrine\ORM\EntityManagerInterface;

class RealEstateCustomPageConfig
{
    private $code_prefix = "realestatecustompage";
    private $entityManager;
    private $service;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->service = new ServiceSystemConfig($this->entityManager);
    }

    public function getTitlePrefixLocalized($languageCode)
    {
        $config = $this->service->getByCode($this->code_prefix . "-" . "titleprefix" . "-" . $languageCode);
        $result = $config ? $config[0]->getValue() : "Prodaja nekretnina";

        return $result;
    }

}