<?php
declare(strict_types=1);

namespace App\Utility;


use Symfony\Component\HttpFoundation\File\File;

class Watermarker
{

    public static function appendWatermark(File $file): string
    {
        switch ($file->getExtension()) {
            case "jpeg":
            case "jpg":
                $image = imagecreatefromjpeg($file->getPathname());
                $newPath = $file->getPath()  . str_replace("." . $file->getExtension(), "", $file->getFilename()) . (string) (mt_rand(1,9)) . ".jpeg";
                break;
            case "png":
                $image = imagecreatefrompng($file->getPathname());
                $newPath = $file->getPath() . str_replace("." . $file->getExtension(), "", $file->getFilename()) . (string) (mt_rand(1,9)) . ".png";
                break;
            default:
                // can not watermark; return filepath as it was
                return $file->getPathname();

        }

        $imageWidth=imagesx($image);
        $imageHeight=imagesy($image);

        if($imageWidth > 942) {
            $logoImage = imagecreatefrompng("backend/img/watermark.png");
        } else if($imageWidth > 762) {
            $logoImage = imagecreatefrompng("backend/img/watermark-700.png");
        } else if($imageWidth > 634) {
            $logoImage = imagecreatefrompng("backend/img/watermark-585.png");
        } else if($imageWidth > 532) {
            $logoImage = imagecreatefrompng("backend/img/watermark-478.png");
        } else if($imageWidth > 360) { // edge case
            $logoImage = imagecreatefrompng("backend/img/watermark-354.png");
        } else { // beyond edge; return image without watermark
            return $file->getPathname();
        }
        imagealphablending($logoImage, true);

        $logoWidth=imagesx($logoImage);
        $logoHeight=imagesy($logoImage);

        imagecopy(
            $image,
            $logoImage,
            (int) (($imageWidth-$logoWidth)/2),
            (int) (($imageHeight-$logoHeight)/2),
            0,
            0,
            $logoWidth,
            $logoHeight
        );
        switch ($file->getExtension()) {
            case "jpeg":
            case "jpg":
                imagejpeg($image, $newPath);

                break;
            case "png":
                imagepng($image, $newPath);
                break;
        }

        return $newPath;
    }

}
 