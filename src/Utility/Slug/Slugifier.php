<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 8:49 AM
 */

namespace App\Utility\Slug;


class Slugifier
{
    public static function slugify($text) {
        // lowercase

        $text = mb_strtolower($text);

        //remove croatian letters
        $search = array("ć", "č", "ž", "š", "đ", "ä", "ë", "ö", "ü", "ß");
        $replacement = array("c", "c", "z", "s", "d", "a", "e", "o", "u", "s");

        $text = str_replace($search, $replacement, $text);

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}