<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 8:49 AM
 */

namespace App\Utility\Slug;


class Slugify
{
    public static function slugify($string) {
        $string = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();", $string);
        $string = preg_replace('/[-\s]+/', '-', $string);
        $string = trim($string, '-');
        return $string;
    }
}