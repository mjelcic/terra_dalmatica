<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 8:49 AM
 */

namespace App\Utility\ImageResize;
use Gumlet\ImageResize;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\GlobalSettings;


class ImageResizer
{
    private $logger;

    function __construct(LoggerInterface $logger)
    {

        $this->logger = $logger;
    }

    public static function resize($file)
    {
        if($file instanceof UploadedFile)
        {
            $name = $file->getRealPath();
            $original_img = new ImageResize($name);

            //$prescribed_width = GlobalSettings::getImageMaxDimensions()["width"];
            $prescribed_height = GlobalSettings::getImageMaxDimensions()["height"];
            //$original_img->resizeToBestFit($prescribed_width, $prescribed_height);

            if($original_img->getSourceHeight() > $prescribed_height)
            {
                $original_img->resizeToHeight($prescribed_height);

                $original_img->save($name);
            }
        }
    }
}