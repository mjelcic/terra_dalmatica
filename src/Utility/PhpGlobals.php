<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/4/2020
 * Time: 1:06 PM
 */

namespace App\Utility;


class PhpGlobals
{
    public static function getLocale()
    {
        $locale = null;
        if(isset($GLOBALS['request']) && $GLOBALS['request']) {
            $locale = $GLOBALS['request']->getLocale();
        }
        return $locale;
    }
}