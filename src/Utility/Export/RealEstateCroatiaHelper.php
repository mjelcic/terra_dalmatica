<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 10/5/2020
 * Time: 9:44 AM
 */

namespace App\Utility\Export;


class RealEstateCroatiaHelper
{
    public static function getRECLocationIdMapper()
    {
        return array(
            "Šibenik" => 5679,
            //Šibenik okolica
            "Šibenik okolica" => 5679,
            "Brodarica" => 7252,
            "Oklaj" => 4027,
            "Lozovac" => 3263,
            "Perković" => 4263,
            "Raslina" => 4867,
            "Bogdanovići" => 340,
            "Drinovci" => 1351,
            "Jadrtovac" => 2255,
            "Jadrija" => 7315,
            "Grebaštica" => 2017,
            "Dubrava" => 1391,
            "Zablaće" => 7316,
            "Zaton" => 6542,
            "Bilice" => 258,
            "Žaborić" => 6812,
            //Šibenski otoci
            "Šibenski otoci" => 5679,
            "Prvić Luka" => 4740,
            "Prvić Šepurine" => 4741,
            "Zlarin" => 6595,
            "Žirje" => 6677,
            "Kaprije" => 2490,
            "Krapanj" => 2853,
            //Zadar
            "Zadar" => 6469,
            "Sukošan" => 5536,
            "Zrmanja Vrelo" => 6621,
            "Privlaka" => 4700,
            "Prizna" => 4702,
            "Vinjerac" => 6237,
            "Kožino" => 2822,
            "Posedarje" => 4572,
            "Biograd na moru" => 266,
            "Lastovo" => 3096,
            "Omiš" => 4044,
            "Pašman" => 4209,
            //Dugi Otok
            "Dugi Otok" => 6078,
            "Sali" => 5068,
            "Pirovac" => 4323,
            "Skradin" => 5206,
            //Rogoznica
            "Rogoznica" => 4990,
            "Ražanj" => 4917,
            "Zečevo" => 6576,
            "Kanica" => 7317,
            //Murter
            "Murter Kornati" => 2745,
            "Murter" => 3817,
            //Vodice
            "Vodice" => 6289,
            "Srima" => 5358,
            //Trogir
            "Trogir" => 5926,
            "Vinišće" => 6228,
            "Marina" => 3531,
            "Sevid" => 5166,
            "Poljica" => 4536,
            "Okrug Gornji" => 4033,
            "Kaštel Štafilić" => 2523,
            "Hvar" => 2183,
            "Tribunj" => 5900,
            "Tisno" => 5823,
            "Brač" => 6813,
            "Primošten" => 4691,
            "Dubrovnik" => 1411,
            "Makarska" => 3382,
            "Šolta" => 6814,
            //Split
            "Split" => 5327,
            "Kaštel Gomilica" => 2517,
            //ostalo
            "Jezera" => 2364,
            "Betina" => 235,
            "Turanj" => 5969,
            "Pakoštane" => 4173,
            "Drage" => 1298,
            "Tkon" => 5833,
            "Nerežišća" => 3861,
            //Korčula
            "Korčula" => 2725,
            "Vela Luka" => 6062
        );
    }
}