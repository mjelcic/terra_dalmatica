<?php
namespace App\Utility;

class MError
{
    function __construct($propertyName, $message)
    {
        $this->message = $message;
        $this->propertyName = $propertyName;
    }

    public $propertyName;
    public $message;
}