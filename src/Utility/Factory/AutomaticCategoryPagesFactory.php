<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 9:21 AM
 */

namespace App\Utility\Factory;


use App\Entity\Location;
use App\Entity\RealEstateCustomPage;
use App\Entity\RealEstateCustomPageFilters;
use App\Entity\RealEstateCustomPageLocalization;
use App\Entity\RealEstateCustomPageMetadataLocalization;
use App\Services\ServiceLanguage;
use App\Services\ServiceRealEstateType;
use App\Utility\Slug\Slugifier;
use App\Utility\SystemConfig\RealEstateCustomPageConfig;
use App\ViewModel\Frontend\Traits\LocationCategoryPagesTrait;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Enum\RealEstateCustomPageTypeEnum;

class AutomaticCategoryPagesFactory
{
    public static function create(Location $location, EntityManagerInterface $entityManager)
    {
        //TODO: Predifined prefixes for titles and slugs

        $serviceLanguage = new ServiceLanguage($entityManager);
        $serviceRealEstateType = new ServiceRealEstateType($entityManager);
        $recpConfig = new RealEstateCustomPageConfig($entityManager);

        $languages = $serviceLanguage->getAll();

        $entityManager->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            //1.1) Create category page for location
            $locationCategoryPage = new RealEstateCustomPage();
            $locationCategoryPage
                ->setActive(true) //TODO: active or inactive by default???
                ->setType(RealEstateCustomPageTypeEnum::AUTOGENERATED)
                ->setCreatedBy(0); //Kojeg korisnika postaviti??? 0 - "SISTEM"???

            //1.2) Create localizations and metadata for each entry in the Language table
            foreach ($languages as $lang)
            {
                //Localizations
                $lcpLoco = new RealEstateCustomPageLocalization();
                $lcpLoco->setLanguage($lang);
                $lcpLoco->setTitle($recpConfig->getTitlePrefixLocalized($lang->getCode()) . " " . $location->getName($lang));
                $lcpLoco->setSlug(Slugifier::slugify($lcpLoco->getTitle()));
                $lcpLoco->setCreatedBy(0);

                //Add localization instance to the category page instance
                $locationCategoryPage->addRealEstateCustomPageLocalization($lcpLoco);

                //Metadata localizations
                $lcpMetadata = self::CreateMetadataLocalization($lcpLoco);

                //Add metadata localization instance to the category page instance
                $locationCategoryPage->addRealEstateCustomPageMetadataLocalization($lcpMetadata);
            }

            //1.4 Add filters
            $filters = new RealEstateCustomPageFilters();
            $filters
                ->setRealEstateCustomPage(null)
                ->addLocation($location)
                ->setCreatedBy(0);
            $locationCategoryPage->setRealEstateCustomPageFilters($filters);

            //Persist $locationCategoryPage
            $entityManager->persist($locationCategoryPage);

            //2.1) Create child category pages for each entry in the RealEstateType table
            $realEstateTypes = $serviceRealEstateType->getAll();
            foreach ($realEstateTypes as $realEstateType)
            {
                $retCategoryPage = new RealEstateCustomPage();
                $retCategoryPage
                    ->setParentRealEstateCustomPage($locationCategoryPage)
                    ->setActive(true) //TODO: active or inactive by default
                    ->setType(RealEstateCustomPageTypeEnum::AUTOGENERATED)
                    ->setCreatedBy(0);

                //2.2) Create localization instances for each entry in the Language table
                foreach ($languages as $language)
                {
                    $retCategoryPageLoco = new RealEstateCustomPageLocalization();
                    $retCategoryPageLoco
                        ->setLanguage($language)
                        ->setTitle($realEstateType->getTitlePrefix($language) . " " . $location->getName($language))
                        ->setSlug(Slugifier::slugify($realEstateType->getTitlePrefix($language))) //slugify only title prefix
                        ->setCreatedBy(0);

                    // Add RealEstateLocationTypePageLocalization instances to the RealEstateLocationTypePage instance
                    $retCategoryPage->addRealEstateCustomPageLocalization($retCategoryPageLoco);

                    //Metadata localizations
                    $retMetadata = self::CreateMetadataLocalization($retCategoryPageLoco);

                    //Add metadata localization instance to the category page instance
                    $retCategoryPage->addRealEstateCustomPageMetadataLocalization($retMetadata);
                }

                //2.4 Set filters
                $filters = new RealEstateCustomPageFilters();
                $filters
                    ->setRealEstateCustomPage($retCategoryPage)
                    ->addRealEstateType($realEstateType)
                    ->setCreatedBy(0);
                $retCategoryPage->setRealEstateCustomPageFilters($filters);

                //Persist $retCategoryPage
                $entityManager->persist($retCategoryPage);
            }

            //5) Save all
            $entityManager->flush();
            $entityManager->getConnection()->commit();

            //6) If all goes well return id from location category page
            return $locationCategoryPage;

        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            //TODO: Maybe log instead of throwing an exception?
            throw $e;
        }
    }

    private static function CreateMetadataLocalization(RealEstateCustomPageLocalization $realEstateCustomPageLocalization)
    {
        $recpMetadata = new RealEstateCustomPageMetadataLocalization();
        $recpMetadata->setLanguage($realEstateCustomPageLocalization->getLanguage());
        $recpMetadata->setTitle($realEstateCustomPageLocalization->getTitle()); //Set metadata title to the title of the category page
        $recpMetadata->setCreatedBy(0);

        return $recpMetadata;
    }
}