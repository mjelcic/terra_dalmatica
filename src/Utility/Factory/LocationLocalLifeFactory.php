<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/24/2020
 * Time: 3:56 PM
 */

namespace App\Utility\Factory;


use App\Entity\LocalLife;
use App\Entity\Location;
use App\Services\ServiceLanguage;
use Doctrine\ORM\EntityManagerInterface;

class LocationLocalLifeFactory
{
    public static function create(Location $location, EntityManagerInterface $entityManager)
    {
        $serviceLanguage = new ServiceLanguage($entityManager);
        $languages = $serviceLanguage->getAll();

        /**
         * @var $location Location
         */
        if(count($location->getLocalLives()) === 0)
        {
            foreach ($languages as $language)
            {
                $localLife = new LocalLife();
                $localLife
                    ->setLanguage($language)
                    ->setCreatedAt(new \DateTime())
                    ->setCreatedBy(0);
                $location->addLocalLife($localLife);
            }
        }

        $entityManager->flush();
    }
}