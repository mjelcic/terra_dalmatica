<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/13/2020
 * Time: 2:57 PM
 */

namespace App\Utility\Factory;


use App\Services\ServiceLanguage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;

class LocalizedEntityFactory
{
    public static function createLocalizations(EntityManagerInterface $em, string $fullClassName = "")
    {
        $result = new ArrayCollection();
        $serviceLanguage = new ServiceLanguage($em);
        $languages = $serviceLanguage->getAll();

        foreach ($languages as $language)
        {
            $r = new ReflectionClass($fullClassName . "Localization" );
            $localization = $r->newInstance();
            $localization->setLanguage($language);
            $result->add($localization);
        }

        return $result;
    }

    public static function createMetaLocalizations(EntityManagerInterface $em, string $fullClassName = "")
    {
        $result = new ArrayCollection();
        $serviceLanguage = new ServiceLanguage($em);
        $languages = $serviceLanguage->getAll();

        foreach ($languages as $language)
        {
            $r = new ReflectionClass($fullClassName . "MetaLocalization" );
            $localization = $r->newInstance();
            $localization->setLanguage($language);
            $result->add($localization);
        }

        return $result;
    }
}