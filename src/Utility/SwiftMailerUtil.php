<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 4/6/2019
 * Time: 12:30 AM
 */

namespace App\Utility;


use App\GlobalSettings;
use App\Services\ServiceUser;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use Psr\Log\LoggerInterface;

class SwiftMailerUtil
{
    private $twig;
    private $mailer;
    private $entityManager;
    private $logger;
    function __construct(\Swift_Mailer $mailer, Environment $twig, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    private function getRecepientEmailAddresses($userRoles)
    {
        $serviceUser = new ServiceUser($this->entityManager);
        $mailingList = $serviceUser->getEmailsByRoles($userRoles);

        $emailAddresses = [];
        foreach ($mailingList as $ml)
        {
            $emailAddresses[] = $ml["email"];
        }

        return $emailAddresses;
    }

    public function sendEmail(string $subject = "", array $parameters, $twigTemplate, array $userRoles, $emailAddresses = [])
    {
        try
        {
            if(!$emailAddresses){
                $emailAddresses = $this->getRecepientEmailAddresses($userRoles);
            }

            $message = (new \Swift_Message($subject))
                ->setFrom(GlobalSettings::getFromMail())
                ->setTo($emailAddresses)
                ->setBody($this->twig->render($twigTemplate, $parameters))
                ->setContentType("text/html");

            $this->mailer->send($message);

            return true;
        }
        catch (\Exception $ex)
        {
            $this->logger->error("ERROR SENDING EMAIL - $subject: " . $ex->getMessage());
            return false;
        }
    }
}