<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/12/2020
 * Time: 2:10 PM
 */

namespace App\Utility;


use App\Repository\AbstractBaseRepository;
use Doctrine\ORM\EntityManagerInterface;

class EntityHelper
{
    private $entityManager;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $entityType
     * @param $fieldName
     * @param $value
     * @param null $id id is used to exclude that value from the query. If we are checking the value that is already in the db than we have to exclude it from the query
     * @return string
     * @throws \Exception
     *
     *
     */
    public function getUniqueFieldValue($entityType, $fieldName, $value, $id = null, $locale = null)
    {
        if(!property_exists($entityType, $fieldName))
        {
            throw new \Exception("Field {$fieldName} doesn't exist in class {$entityType}");
        }

        $newValue = $value;
        $step = 1;

        /**
         * @var $repository AbstractBaseRepository
         */
        $repository = $this->entityManager->getRepository($entityType);
        $qb = $repository->createBasicQueryBuilder("entity", false);
        $qb->select("entity.id, entity.{$fieldName}")
            ->where($qb->expr()->orX(
                $qb->expr()->eq("entity.{$fieldName}", ":newValue"),
                $qb->expr()->like("entity.{$fieldName}", $qb->expr()->literal($newValue . "-%"))
            ))
            ->setParameter("newValue", $newValue)
            ->orderBy("entity.{$fieldName}", "DESC");

        if ($id)
        {
            $qb->andWhere("entity.id <> :id")
                ->setParameter("id", $id);
        }

        if($locale){
            $qb->join('entity.language', 'language');
            $qb->andWhere("language.code = :locale")
                ->setParameter("locale", $locale);
        }

        $queryResult = $qb->getQuery()->getResult();

        //If any results are found then new value is not unique
        if(count($queryResult))
        {
            $oldValue = $queryResult[0][$fieldName];
            $lastStep = substr($oldValue, strrpos($oldValue, '-') + 1);
            if(ctype_digit($lastStep)){
                $step = (int)$lastStep + 1;
            }

            $newValue = "{$newValue}-{$step}";
        }

        return $newValue;
    }

    /**
     * @param $entityType
     * @param $fieldName
     * @param int $increment
     * @return string
     * @throws \Exception
     */
    public function getNextIntegerForField($entityType, $fieldName, $increment = 1)
    {
        if(!property_exists($entityType, $fieldName))
        {
            throw new \Exception("Field {$fieldName} doesn't exist in class {$entityType}");
        }

        $query = $this->entityManager->createQuery("SELECT MAX(e.{$fieldName}) FROM {$entityType} e");
        $max = $query->getSingleScalarResult();

        if($max == null) {
            $max = 0;
        }

        return $max + $increment;
    }
}