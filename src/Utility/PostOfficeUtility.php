<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/8/2020
 * Time: 2:41 PM
 */

namespace App\Utility;


use App\Entity\Enum\RoleEnum;
use App\Entity\MarketingEmail;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PostOfficeUtility
{
    private $swiftMailerUtil;
    private $translator;
    function __construct(SwiftMailerUtil $swiftMailerUtil, TranslatorInterface $translator)
    {
        $this->swiftMailerUtil = $swiftMailerUtil;
        $this->translator = $translator;
    }

    public function notifyAboutNewClientRegistration(UserInterface $user)
    {
        //If client account is created notify all admin and agent users via email
        if(in_array(RoleEnum::USER, $user->getRoles()))
        {
            $this->swiftMailerUtil->sendEmail(
                $this->translator->trans("Obavještenje o novom klijentskom nalogu"),
                array("user" => $user),
                "email/new_client_notification.html.twig",
                array(RoleEnum::ADMIN, RoleEnum::EMPLOYEE)
            );
        }
    }

    public function sendMarketingEmail(MarketingEmail $mEmail)
    {
        $bSuccess = $this->swiftMailerUtil->sendEmail(
            $mEmail->getSubject(),
            array("content" => $mEmail->getContent()),
            "email/marketing_email.html.twig",
            $mEmail->getSendToAll() ? ["ROLE_USER"] : [],
            $mEmail->getSendToAll() ? [] : array_map(function ($user) { return $user->getEmail(); }, $mEmail->getSentTo()->toArray())
        );

        return $bSuccess;
    }
}