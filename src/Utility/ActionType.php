<?php
namespace App\Utility;

abstract class ActionType
{
    const create = 0;
    const update = 1;
}