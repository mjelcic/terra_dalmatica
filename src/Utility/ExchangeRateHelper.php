<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2/27/2020
 * Time: 11:13 PM
 */

namespace App\Utility;


use App\Entity\ExchangeRate;

class ExchangeRateHelper
{
    private static $default;
    private $exchangeRate;

    private function __construct()
    {

    }

    public static function Default()
    {
        if (self::$default == null)
        {
            self::$default = new ExchangeRateHelper();
        }

        return self::$default;
    }

    public function getCurrentExchangeRate(): ?ExchangeRate
    {
        return $this->exchangeRate;
    }

    public function setCurrentExchangeRate(ExchangeRate $exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }
}