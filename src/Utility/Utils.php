<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2/17/2019
 * Time: 10:33 PM
 */

namespace App\Utility;


class Utils
{
    /**
     * Method evaluates whether the string representation of supplied value contains all digits,
     * that is it can be parsed to an integer.
     *
     * @param $value mixed  A value to be evaluated
     * @return bool
     */
    public static function is_integer($value)
    {
        return ctype_digit(strval($value));
    }

    public static function is_float($value)
    {
        return is_numeric($value) && (float)$value;
    }
}