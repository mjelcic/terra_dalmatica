<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Oh\GoogleMapFormTypeBundle\Traits\LocationTrait;


/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateRepository")
 */
class RealEstate
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    use LocationTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="realEstates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateType", inversedBy="realEstates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $RealEstateType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="realEstates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $featured;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bedroomCount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $firstRowByTheSea;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $seaView;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $newBuilding;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $distanceFromTheSea;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $underConstruction;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $pool;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bathroomCount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $garage;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $fullPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $discountPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $livingArea;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $surfaceArea;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $videoUrl;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $virtualWalkUrl;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $energyClass;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateFeature", mappedBy="real_estate", orphanRemoval=true, cascade={"persist"})
     */
    private $realEstateFeatures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocalization", mappedBy="RealEstate", orphanRemoval=true, cascade={"persist"})
     */
    private $realEstateLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateMetaLocalization", mappedBy="RealEstate", orphanRemoval=true, cascade={"persist"})
     */
    private $realEstateMetaLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="real_estate", orphanRemoval=true, cascade={"persist"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="RealEstateImage", mappedBy="realEstate", orphanRemoval=true, cascade={"persist"})
     */
    private $realEstateImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GroundPlan", mappedBy="realEstate", orphanRemoval=true, cascade={"persist"})
     */
    private $groundPlans;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="realEstates")
     */
    private $agent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="clientRealEstates")
     */
    private $client;

    /**
     * @ORM\Column(type="boolean")
     */
    private $priceOnRequest;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $administrationNote;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $clientNote;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postal_code;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $keysInPossession;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lotDetails;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\UserFavorite::class, mappedBy="real_estate")
     */
    private $userFavorites;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fb_post_id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $twitterPostId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $immoid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $immoscout_exported;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $place;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $exportToZoopla;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $zooplaId;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->realEstateFeatures = new ArrayCollection();
        $this->realEstateLocalizations = new ArrayCollection();
        $this->realEstateMetaLocalizations = new ArrayCollection();
        $this->documents = new ArrayCollection();
        if($args !== null && is_array($args)){

            if(array_key_exists("localizations", $args)) {
                foreach ($args["localizations"] as $localization) {
                    $this->addRealEstateLocalization($localization);
                }
            }
            if(array_key_exists("metaLocalizations", $args)){
                foreach ($args["metaLocalizations"] as $metaLocalization){
                    $this->addRealEstateMetaLocalization($metaLocalization);
                }
            }
        }
        $this->realEstateImages = new ArrayCollection();
        $this->groundPlans = new ArrayCollection();
        $this->userFavorites = new ArrayCollection();
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getRealEstateType(): ?RealEstateType
    {
        return $this->RealEstateType;
    }

    public function setRealEstateType(?RealEstateType $RealEstateType): self
    {
        $this->RealEstateType = $RealEstateType;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBedroomCount(): ?int
    {
        return $this->bedroomCount;
    }

    public function getBedroomCountSafe(): int
    {
        return $this->bedroomCount == null ? 0 : $this->bedroomCount;
    }

    public function setBedroomCount(?int $bedroomCount): self
    {
        $this->bedroomCount = $bedroomCount;

        return $this;
    }

    public function getFirstRowByTheSea(): ?bool
    {
        return $this->firstRowByTheSea;
    }

    public function setFirstRowByTheSea(?bool $firstRowByTheSea): self
    {
        $this->firstRowByTheSea = $firstRowByTheSea;

        return $this;
    }

    public function getSeaView(): ?bool
    {
        return $this->seaView;
    }

    public function setSeaView(?bool $seaView): self
    {
        $this->seaView = $seaView;

        return $this;
    }

    public function getNewBuilding(): ?bool
    {
        return $this->newBuilding;
    }

    public function setNewBuilding(?bool $newBuilding): self
    {
        $this->newBuilding = $newBuilding;

        return $this;
    }

    public function getDistanceFromTheSea(): ?int
    {
        return $this->distanceFromTheSea;
    }

    public function setDistanceFromTheSea(?int $distanceFromTheSea): self
    {
        $this->distanceFromTheSea = $distanceFromTheSea;

        return $this;
    }

    public function getUnderConstruction(): ?bool
    {
        return $this->underConstruction;
    }

    public function setUnderConstruction(?bool $underConstruction): self
    {
        $this->underConstruction = $underConstruction;

        return $this;
    }

    public function getPool(): ?bool
    {
        return $this->pool;
    }

    public function setPool(?bool $pool): self
    {
        $this->pool = $pool;

        return $this;
    }

    public function getBathroomCount(): ?int
    {
        return $this->bathroomCount;
    }

    public function setBathroomCount(?int $bathroomCount): self
    {
        $this->bathroomCount = $bathroomCount;

        return $this;
    }

    public function getGarage(): ?bool
    {
        return $this->garage;
    }

    public function setGarage(?bool $garage): self
    {
        $this->garage = $garage;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLivingArea(): ?float
    {
        return $this->livingArea;
    }

    public function setLivingArea(float $livingArea): self
    {
        $this->livingArea = $livingArea;

        return $this;
    }

    public function getSurfaceArea(): ?float
    {
        return $this->surfaceArea;
    }

    public function setSurfaceArea(?float $surfaceArea): self
    {
        $this->surfaceArea = $surfaceArea;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(?string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getEnergyClass(): ?string
    {
        return $this->energyClass;
    }

    public function setEnergyClass(?string $energyClass): self
    {
        $this->energyClass = $energyClass;

        return $this;
    }

    /**
     * @return bool
     */
    public function getExportToZoopla(): bool
    {
        return $this->exportToZoopla == null ? false : $this->exportToZoopla;
    }

    /**
     * @param bool $exportToZoopla
     */
    public function setExportToZoopla(bool $exportToZoopla): void
    {
        $this->exportToZoopla = $exportToZoopla;
    }

    /**
     * @return mixed
     */
    public function getZooplaId()
    {
        return $this->zooplaId;
    }

    /**
     * @param mixed $zooplaId
     */
    public function setZooplaId($zooplaId): void
    {
        $this->zooplaId = $zooplaId;
    }

    /**
     * @return Collection|RealEstateFeature[]
     */
    public function getRealEstateFeatures(): Collection
    {
        return $this->realEstateFeatures;
    }

    public function addRealEstateFeature(RealEstateFeature $realEstateFeature): self
    {
        if (!$this->realEstateFeatures->contains($realEstateFeature)) {
            $this->realEstateFeatures[] = $realEstateFeature;
            $realEstateFeature->setRealEstate($this);
        }

        return $this;
    }

    public function removeRealEstateFeature(RealEstateFeature $realEstateFeature): self
    {
        if ($this->realEstateFeatures->contains($realEstateFeature)) {
            $this->realEstateFeatures->removeElement($realEstateFeature);
            // set the owning side to null (unless already changed)
            if ($realEstateFeature->getRealEstate() === $this) {
                $realEstateFeature->setRealEstate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateLocalization[]
     */
    public function getRealEstateLocalizations(): Collection
    {
        return $this->realEstateLocalizations;
    }

    public function getRealEstateLocalization($language): ?RealEstateLocalization
    {
        try
        {
            $localization = $this->getRealEstateLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    /**
     * Returns proper title for language, or first one from collection if required one is not found
     * @param $language
     * @return string|null
     */
    public function getSafeRealEstateLocalizatonTitle($language)
    {
        $localization = $this->getRealEstateLocalization($language);

        if($localization == null) {
            return $this->getRealEstateLocalizations()->first()->getTitle();
        }

        return $localization->getTitle();
    }

    public function getRealEstateMetaLocalization($language): ?RealEstateMetaLocalization
    {
        try {
            $localization = $this->getRealEstateMetaLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }  catch(\Exception $ex) {
            return null;
        }
    }

    public function addRealEstateLocalization(RealEstateLocalization $realEstateLocalization): self
    {
        if (!$this->realEstateLocalizations->contains($realEstateLocalization)) {
            $this->realEstateLocalizations[] = $realEstateLocalization;
            $realEstateLocalization->setRealEstate($this);
        }

        return $this;
    }

    public function removeRealEstateLocalization(RealEstateLocalization $realEstateLocalization): self
    {
        if ($this->realEstateLocalizations->contains($realEstateLocalization)) {
            $this->realEstateLocalizations->removeElement($realEstateLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocalization->getRealEstate() === $this) {
                $realEstateLocalization->setRealEstate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateMetaLocalization[]
     */
    public function getRealEstateMetaLocalizations(): Collection
    {
        return $this->realEstateMetaLocalizations;
    }

    public function addRealEstateMetaLocalization(RealEstateMetaLocalization $realEstateMetaLocalization): self
    {
        if (!$this->realEstateMetaLocalizations->contains($realEstateMetaLocalization)) {
            $this->realEstateMetaLocalizations[] = $realEstateMetaLocalization;
            $realEstateMetaLocalization->setRealEstate($this);
        }

        return $this;
    }

    public function removeRealEstateMetaLocalization(RealEstateMetaLocalization $realEstateMetaLocalization): self
    {
        if ($this->realEstateMetaLocalizations->contains($realEstateMetaLocalization)) {
            $this->realEstateMetaLocalizations->removeElement($realEstateMetaLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateMetaLocalization->getRealEstate() === $this) {
                $realEstateMetaLocalization->setRealEstate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setRealEstate($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getRealEstate() === $this) {
                $document->setRealEstate(null);
            }
        }

        return $this;
    }

    public function setFeatures(array $features)
    {
        $this->clearFeatures();

        foreach ($features as $feature) {
            $existing = $this->findRealEstateFeatureByFeatureId($feature->getId());

            if($existing) {
                $existing->setInvalidated(0);
            } else {
                $reFeature = new RealEstateFeature();
                $reFeature->setFeature($feature);
                $reFeature->setRealEstate($this);

                $this->addRealEstateFeature($reFeature);
            }
        }
    }

    private function clearFeatures() {
        foreach ($this->realEstateFeatures as $realEstateFeature) {
            $realEstateFeature->setInvalidated(1);
        }
    }

    public function getFeatures(): array
    {
        $features = [];

        foreach ($this->realEstateFeatures as $realEstateFeature) {
            if(!$realEstateFeature->getInvalidated()) {
                $features[] = $realEstateFeature->getFeature();
            }
        }

        return $features;
    }

    private function findRealEstateFeatureByFeatureId(int $id) {
        foreach ($this->realEstateFeatures as $realEstateFeature) {
            if($realEstateFeature->getFeature()->getId() === $id) {
                return $realEstateFeature;
            }
        }

        return null;
    }

    /**
     * @return Collection|RealEstateImage[]
     */
    public function getRealEstateImages(): Collection
    {
        return $this->realEstateImages;
    }

    public function addRealEstateImage(RealEstateImage $realEstateImage): self
    {
        if (!$this->realEstateImages->contains($realEstateImage)) {
            $this->realEstateImages[] = $realEstateImage;
            $realEstateImage->setRealEstate($this);
        }

        return $this;
    }

    public function removeRealEstateImage(RealEstateImage $realEstateImage): self
    {
        if ($this->realEstateImages->contains($realEstateImage)) {
            $this->realEstateImages->removeElement($realEstateImage);
            // set the owning side to null (unless already changed)
            if ($realEstateImage->getRealEstate() === $this) {
                $realEstateImage->setRealEstate(null);
            }
        }

        return $this;
    }

    public function setCoverImage(int $imageId): void
    {
        foreach ($this->realEstateImages as $realEstateImage) {
            if($realEstateImage->getId() === $imageId) {
                $realEstateImage->setCover(true);
            } else {
                $realEstateImage->setCover(false);
            }
        }
    }

    public function getCoverImage(): ?RealEstateImage
    {
        if(count($this->realEstateImages) > 0) {
            foreach ($this->realEstateImages as $realEstateImage) {
                if($realEstateImage->isCover()) {
                    return $realEstateImage;
                }
            }

            return $this->realEstateImages[0];
        }
        return null;
    }

    public function getImageById($imageId): ?RealEstateImage
    {
        if(count($this->realEstateImages) > 0) {
            foreach ($this->realEstateImages as $realEstateImage) {
                if($realEstateImage->getId() == $imageId) {
                    return $realEstateImage;
                }
            }
        }
        return null;
    }

    /**
     * @return Collection|GroundPlan[]
     */
    public function getGroundPlans(): Collection
    {
        return $this->groundPlans;
    }

    public function addGroundPlan(GroundPlan $groundPlan): self
    {
        if (!$this->groundPlans->contains($groundPlan)) {
            $this->groundPlans[] = $groundPlan;
            $groundPlan->setRealEstate($this);
        }

        return $this;
    }

    public function removeGroundPlan(GroundPlan $groundPlan): self
    {
        if ($this->groundPlans->contains($groundPlan)) {
            $this->groundPlans->removeElement($groundPlan);
            // set the owning side to null (unless already changed)
            if ($groundPlan->getRealEstate() === $this) {
                $groundPlan->setRealEstate(null);
            }
        }

        return $this;
    }

    public function getGroudPlanById($groundPlanId): ?GroundPlan
    {
        if(count($this->groundPlans) > 0) {
            foreach ($this->groundPlans as $groundPlan) {
                if($groundPlan->getId() == $groundPlanId) {
                    return $groundPlan;
                }
            }
        }
        return null;
    }

    /**
     * @return bool|null
     */
    public function isFeatured(): ?bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getPriceOnRequest(): ?bool
    {
        return $this->priceOnRequest;
    }

    public function setPriceOnRequest(bool $priceOnRequest): self
    {
        $this->priceOnRequest = $priceOnRequest;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVirtualWalkUrl()
    {
        return $this->virtualWalkUrl;
    }

    /**
     * @param mixed $virtualWalkUrl
     */
    public function setVirtualWalkUrl($virtualWalkUrl): void
    {
        $this->virtualWalkUrl = $virtualWalkUrl;
    }

    public function getUid(): ?int
    {
        return $this->uid;
    }

    public function setUid(?int $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getAdministrationNote(): ?string
    {
        return $this->administrationNote;
    }

    public function setAdministrationNote(?string $administrationNote): self
    {
        $this->administrationNote = $administrationNote;

        return $this;
    }

    public function getClientNote(): ?string
    {
        return $this->clientNote;
    }

    public function setClientNote(?string $clientNote): self
    {
        $this->clientNote = $clientNote;

        return $this;
    }

    public function getCoverImageId(): ?int
    {
        $coverImage = $this->getRealEstateImages()->filter(function(RealEstateImage $rei) { return $rei->isCover();})->first();

        if($coverImage instanceof RealEstateImage) {
            return $coverImage->getId();
        }

        return null;
    }

    public function getPostalCode(): ?int
    {
        return $this->postal_code;
    }

    public function getSafePostalCode(): int
    {
        if($this->postal_code != null) {
            return $this->postal_code;
        } else if($this->getLocation() && $this->getLocation()->getPostalCode() != null) {
            return $this->getLocation()->getPostalCode();
        }

        return 22000; // sibenik
    }

    public function setPostalCode(?int $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getFullPrice(): ?float
    {
        return $this->fullPrice;
    }

    /**
     * @param float|null $fullPrice
     */
    public function setFullPrice(?float $fullPrice): void
    {
        $this->fullPrice = $fullPrice;
    }

    /**
     * @return float|null
     */
    public function getDiscountPrice(): ?float
    {
        return $this->discountPrice;
    }

    /**
     * @param float|null $discountPrice
     */
    public function setDiscountPrice(?float $discountPrice): void
    {
        $this->discountPrice = $discountPrice;
    }

    public function setCalculatedPrice(): void
    {
        if($this->priceOnRequest) {
            $this->price = null;
        } else if($this->discountPrice) {
            $this->price = $this->discountPrice;
        } else {
            $this->price = $this->fullPrice;
        }
    }

    /**
     * @return Collection|UserFavorite[]
     */
    public function getUserFavorites(): Collection
    {
        return $this->userFavorites;
    }

    public function addUserFavorite(UserFavorite $userFavorite): self
    {
        if (!$this->userFavorites->contains($userFavorite)) {
            $this->userFavorites[] = $userFavorite;
            $userFavorite->setRealEstate($this);
        }

        return $this;
    }

    public function removeUserFavorite(UserFavorite $userFavorite): self
    {
        if ($this->userFavorites->contains($userFavorite)) {
            $this->userFavorites->removeElement($userFavorite);
            // set the owning side to null (unless already changed)
            if ($userFavorite->getRealEstate() === $this) {
                $userFavorite->setRealEstate(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeysInPossession()
    {
        return $this->keysInPossession;
    }

    /**
     * @param mixed $keysInPossession
     */
    public function setKeysInPossession($keysInPossession): void
    {
        $this->keysInPossession = $keysInPossession;
    }

    /**
     * @return string|null
     */
    public function getLotDetails(): ?string
    {
        return $this->lotDetails;
    }

    /**
     * @param mixed $lotDetails
     */
    public function setLotDetails($lotDetails): void
    {
        $this->lotDetails = $lotDetails;
    }

    public function getFbPostId(): ?string
    {
        return $this->fb_post_id;
    }

    public function setFbPostId(?string $fb_post_id): self
    {
        $this->fb_post_id = $fb_post_id;

        return $this;
    }

    public function getTwitterPostId(): ?string
    {
        return $this->twitterPostId;
    }

    public function setTwitterPostId(?string $twitterPostId): self
    {
        $this->twitterPostId = $twitterPostId;

        return $this;
    }

    public function getImmoid(): ?string
    {
        return $this->immoid;
    }

    public function setImmoid(?string $immoid): self
    {
        $this->immoid = $immoid;

        return $this;
    }

    public function getImmoscoutExported(): ?bool
    {
        return $this->immoscout_exported;
    }

    public function setImmoscoutExported(?bool $immoscout_exported): self
    {
        $this->immoscout_exported = $immoscout_exported;

        return $this;

    }

    public function hasFeatureWithId(int $featureId): bool
    {
        return $this->getRealEstateFeatures()->filter(function(RealEstateFeature $feature) use($featureId) {
            return $feature->getFeature()->getId() == $featureId;
        })->count() > 0;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }
}
