<?php

namespace App\Entity;

use App\Entity\Traits\LocalizedNameTrait;
use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    use LocalizedNameTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="locations")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Polje je obavezno!")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LocationLocalization", mappedBy="location", cascade={"all"})
     * @Assert\Valid
     */
    private $locationLocalizations;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstate", mappedBy="location", orphanRemoval=true)
     */
    private $realEstates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationTypePage", mappedBy="location")
     */
    private $realEstateLocationTypePages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserPreference", mappedBy="locations")
     */
    private $userPreferences;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postal_code;

    /**
     * @ORM\OneToMany(targetEntity=LocalLife::class, mappedBy="location", orphanRemoval=true, cascade={"persist"})
     */
    private $localLives;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RealEstateCustomPageFilters", mappedBy="locations")
     */
    private $realEstateCustomPageFilters;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->locationLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addLocationLocalization($localization);
            }
        }
        $this->realEstates = new ArrayCollection();
        $this->realEstateLocationTypePages = new ArrayCollection();
        $this->userPreferences = new ArrayCollection();
        $this->localLives = new ArrayCollection();
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getLocationLocalization($language): ?LocationLocalization
    {
        try
        {
            $localization = $this->getLocationLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    /**
     * @return Collection|LocationLocalization[]
     */
    public function getLocationLocalizations(): Collection
    {
        return $this->locationLocalizations;
    }

    public function addLocationLocalization(LocationLocalization $locationLocalization): self
    {
        if (!$this->locationLocalizations->contains($locationLocalization)) {
            $this->locationLocalizations[] = $locationLocalization;
            $locationLocalization->setLocation($this);
        }

        return $this;
    }

    public function removeLocationLocalization(LocationLocalization $locationLocalization): self
    {
        if ($this->locationLocalizations->contains($locationLocalization)) {
            $this->locationLocalizations->removeElement($locationLocalization);
            // set the owning side to null (unless already changed)
            if ($locationLocalization->getLocation() === $this) {
                $locationLocalization->setLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstate[]
     */
    public function getRealEstates(): Collection
    {
        return $this->realEstates;
    }

    public function addRealEstate(RealEstate $realEstate): self
    {
        if (!$this->realEstates->contains($realEstate)) {
            $this->realEstates[] = $realEstate;
            $realEstate->setLocation($this);
        }

        return $this;
    }

    public function removeRealEstate(RealEstate $realEstate): self
    {
        if ($this->realEstates->contains($realEstate)) {
            $this->realEstates->removeElement($realEstate);
            // set the owning side to null (unless already changed)
            if ($realEstate->getLocation() === $this) {
                $realEstate->setLocation(null);
            }
        }

        return $this;
    }



    /**
     * @return Collection|RealEstateLocationTypePage[]
     */
    public function getRealEstateLocationTypePages(): Collection
    {
        return $this->realEstateLocationTypePages;
    }

    public function addRealEstateLocationTypePage(RealEstateLocationTypePage $realEstateLocationTypePage): self
    {
        if (!$this->realEstateLocationTypePages->contains($realEstateLocationTypePage)) {
            $this->realEstateLocationTypePages[] = $realEstateLocationTypePage;
            $realEstateLocationTypePage->setLocation($this);
        }

        return $this;
    }

    public function removeRealEstateLocationTypePage(RealEstateLocationTypePage $realEstateLocationTypePage): self
    {
        if ($this->realEstateLocationTypePages->contains($realEstateLocationTypePage)) {
            $this->realEstateLocationTypePages->removeElement($realEstateLocationTypePage);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationTypePage->getLocation() === $this) {
                $realEstateLocationTypePage->setLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPreference[]
     */
    public function getUserPreferences(): Collection
    {
        return $this->userPreferences;
    }

    public function addUserPreference(UserPreference $userPreference): self
    {
        if (!$this->userPreferences->contains($userPreference)) {
            $this->userPreferences[] = $userPreference;
            $userPreference->addLocation($this);
        }

        return $this;
    }

    public function removeUserPreference(UserPreference $userPreference): self
    {
        if ($this->userPreferences->contains($userPreference)) {
            $this->userPreferences->removeElement($userPreference);
            $userPreference->removeLocation($this);
        }

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postal_code;
    }

    public function setPostalCode(?int $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * @return Collection|LocalLife[]
     */
    public function getLocalLives(): Collection
    {
        return $this->localLives;
    }

    public function addLocalLife(LocalLife $localLife): self
    {
        if (!$this->localLives->contains($localLife)) {
            $this->localLives[] = $localLife;
            $localLife->setLocation($this);
        }

        return $this;
    }

    public function removeLocalLife(LocalLife $localLife): self
    {
        if ($this->localLives->contains($localLife)) {
            $this->localLives->removeElement($localLife);
            // set the owning side to null (unless already changed)
            if ($localLife->getLocation() === $this) {
                $localLife->setLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateCustomPageFilters[]
     */
    public function getRealEstateCustomPageFilters(): Collection
    {
        return $this->realEstateCustomPageFilters;
    }
}
