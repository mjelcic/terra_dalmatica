<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/16/2020
 * Time: 1:43 PM
 */

namespace App\Entity\Traits;


use App\Entity\Language;
use App\Utility\PhpGlobals;

trait LocalizedNameTrait
{
    /**
     * @param $language string | Language
     */
    public function getName($language = "hr")
    {
        try
        {
            if(!is_object($language))
            {
                $locale = PhpGlobals::getLocale();
                $language = $locale ?? $language;
            }

            $action = "get" . (new \ReflectionClass($this))->getShortName() . "Localizations";

            $localization = $this->{$action}()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first()->getName();
            }

            return "";
        }
        catch(\Exception $ex)
        {
            return "";
        }
    }
}