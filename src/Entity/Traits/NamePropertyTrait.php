<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/13/2020
 * Time: 12:00 PM
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait NamePropertyTrait
{
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Polje je obavezno!")
     * @Assert\Length(max="100", maxMessage="Polje ne može sadržavati više od {{ limit }} karaktera");
     */
    private $name;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}