<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate", inversedBy="documents", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filePath;

    /**
     * @ORM\Column(type="integer")
     */
    private $document_type;

    /**
     * @ORM\Column(type="string")
     */
    private $document_mime_type;

    /**
     * @ORM\Column(type="boolean", nullable=false, )
     */
    private $admin_exclusive;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->admin_exclusive = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRealEstate(): ?RealEstate
    {
        return $this->real_estate;
    }

    public function setRealEstate(?RealEstate $real_estate): self
    {
        $this->real_estate = $real_estate;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDocumentType(): ?int
    {
        return $this->document_type;
    }

    public function setDocumentType(int $document_type): self
    {
        $this->document_type = $document_type;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath($filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @return mixed
     */
    public function getDocumentMimeType()
    {
        return $this->document_mime_type;
    }

    /**
     * @param mixed $document_mime_type
     */
    public function setDocumentMimeType($document_mime_type): void
    {
        $this->document_mime_type = $document_mime_type;
    }

    /**
     * @return int
     */
    public function getAdminExclusive(): int
    {
        return $this->admin_exclusive;
    }

    /**
     * @param int $admin_exclusive
     */
    public function setAdminExclusive(int $admin_exclusive): void
    {
        $this->admin_exclusive = $admin_exclusive;
    }
}
