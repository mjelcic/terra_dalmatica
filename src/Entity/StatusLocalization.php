<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/10/2020
 * Time: 1:53 PM
 */

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatusLocalizationRepository")
 */
class StatusLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="statusLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="statusLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}