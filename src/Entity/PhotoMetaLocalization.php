<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhotoMetaLocalizationRepository")
 */
class PhotoMetaLocalization
{

    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo", inversedBy="photoMetaLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $alt_tag;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="photoMetaLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;


    public function getPhoto(): ?Photo
    {
        return $this->photo;
    }

    public function setPhoto(?Photo $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getAltTag(): ?string
    {
        return $this->alt_tag;
    }

    public function setAltTag(string $alt_tag): self
    {
        $this->alt_tag = $alt_tag;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
