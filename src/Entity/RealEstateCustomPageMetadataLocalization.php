<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateCustomPageMetadataLocalizationRepository")
 */
class RealEstateCustomPageMetadataLocalization
{
    use BaseTrait;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateCustomPageMetadataLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateCustomPage", inversedBy="realEstateCustomPageMetadataLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_custom_page;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getRealEstateCustomPage(): ?RealEstateCustomPage
    {
        return $this->real_estate_custom_page;
    }

    public function setRealEstateCustomPage(?RealEstateCustomPage $real_estate_custom_page): self
    {
        $this->real_estate_custom_page = $real_estate_custom_page;

        return $this;
    }
}
