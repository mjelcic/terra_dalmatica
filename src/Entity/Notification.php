<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $triggeredBy;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $verb;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $suffixMessage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate")
     */
    private $object;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications")
     */
    private $for_user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $for_role;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $seen_by;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateCustomPage")
     */
    private $real_estate_custom_page;

    public function __construct()
    {
        $this->seen_by = new ArrayCollection();
        $this->baseConstruct();
    }

    public function getTriggeredBy(): ?User
    {
        return $this->triggeredBy;
    }

    public function setTriggeredBy(?UserInterface $triggeredBy): self
    {
        $this->triggeredBy = $triggeredBy;

        return $this;
    }

    public function getVerb(): ?string
    {
        return $this->verb;
    }

    public function setVerb(string $verb): self
    {
        $this->verb = $verb;

        return $this;
    }

    public function getObject(): ?RealEstate
    {
        return $this->object;
    }

    public function setObject(?RealEstate $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getForUser(): ?User
    {
        return $this->for_user;
    }

    public function setForUser(?User $for_user): self
    {
        $this->for_user = $for_user;

        return $this;
    }

    public function getForRole(): ?string
    {
        return $this->for_role;
    }

    public function setForRole(?string $for_role): self
    {
        $this->for_role = $for_role;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getSeenBy(): Collection
    {
        return $this->seen_by;
    }

    public function addSeenBy(User $seenBy): self
    {
        if (!$this->seen_by->contains($seenBy)) {
            $this->seen_by[] = $seenBy;
        }

        return $this;
    }

    public function removeSeenBy(User $seenBy): self
    {
        if ($this->seen_by->contains($seenBy)) {
            $this->seen_by->removeElement($seenBy);
        }

        return $this;
    }

    public function getRealEstateCustomPage(): ?RealEstateCustomPage
    {
        return $this->real_estate_custom_page;
    }

    public function setRealEstateCustomPage(?RealEstateCustomPage $real_estate_custom_page): self
    {
        $this->real_estate_custom_page = $real_estate_custom_page;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSuffixMessage(): ?string
    {
        return $this->suffixMessage;
    }

    /**
     * @param string|null $suffixMessage
     */
    public function setSuffixMessage(?string $suffixMessage): void
    {
        $this->suffixMessage = $suffixMessage;
    }


}
