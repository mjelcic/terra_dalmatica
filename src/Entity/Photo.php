<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * NOTE: This is not a ORM mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="photo", fileNameProperty="name")
     *
     * @Assert\Image(mimeTypesMessage="Datoteka koju pokušavate da postavite nije validna slika!")
     */
    private $file;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PhotoMetaLocalization", mappedBy="photo", orphanRemoval=true, cascade={"all"})
     */
    private $photoMetaLocalizations;

    public function __construct($args = null)
    {
        $this->baseConstruct();
        $this->photoMetaLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addPhotoMetaLocalization($localization);
            }
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PhotoMetaLocalization[]
     */
    public function getPhotoMetaLocalizations(): Collection
    {
        return $this->photoMetaLocalizations;
    }

    public function getPhotoMetaLocalization($language): ?PhotoMetaLocalization
    {
        try
        {
            $localization = $this->getPhotoMetaLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function addPhotoMetaLocalization(PhotoMetaLocalization $photoMetaLocalization): self
    {
        if (!$this->photoMetaLocalizations->contains($photoMetaLocalization)) {
            $this->photoMetaLocalizations[] = $photoMetaLocalization;
            $photoMetaLocalization->setPhoto($this);
        }

        return $this;
    }

    public function removePhotoMetaLocalization(PhotoMetaLocalization $photoMetaLocalization): self
    {
        if ($this->photoMetaLocalizations->contains($photoMetaLocalization)) {
            $this->photoMetaLocalizations->removeElement($photoMetaLocalization);
            // set the owning side to null (unless already changed)
            if ($photoMetaLocalization->getPhoto() === $this) {
                $photoMetaLocalization->setPhoto(null);
            }
        }

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setFile(?File $file = null): void
    {
        $this->file = $file;

        if (null !== $file)
        {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdatedAt(new \DateTimeImmutable());
        }
    }

    public function getFile(): ?File
    {
        return $this->file;
    }
}
