<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgentPageRepository")
 * @Vich\Uploadable
 */
class AgentPage
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image;

    /**
     * NOTE: This is not a ORM mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="content_image", fileNameProperty="image")
     *
     * @Assert\Image(mimeTypesMessage="Datoteka koju pokušavate da postavite nije validna slika!")
     */
    public $image_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageAltTag;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="agentPage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgentPageLocalization", mappedBy="agentPage", orphanRemoval=true, cascade={"all"})
     */
    private $agentPageLocalizations;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->agentPageLocalizations = new ArrayCollection();
        if($args !== null && is_array($args)) {

            if (array_key_exists("localizations", $args)) {
                foreach ($args["localizations"] as $localization) {
                    $this->addAgentPageLocalization($localization);
                }
            }
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $image = null)
    {
        $this->image_file = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->image_file;
    }


    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageAltTag(): ?string
    {
        return $this->imageAltTag;
    }

    public function setImageAltTag(?string $imageAltTag): self
    {
        $this->imageAltTag = $imageAltTag;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAgentPageLocalizations(): Collection
    {
        return $this->agentPageLocalizations;
    }


    public function addAgentPageLocalization(AgentPageLocalization $agentPageLocalization): self
    {
        if (!$this->agentPageLocalizations->contains($agentPageLocalization)) {
            $this->agentPageLocalizations[] = $agentPageLocalization;
            $agentPageLocalization->setAgentPage($this);
        }

        return $this;
    }

    public function removeAgentPageLocalization(AgentPageLocalization $agentPageLocalization): self
    {
        if ($this->agentPageLocalizations->contains($agentPageLocalization)) {
            $this->agentPageLocalizations->removeElement($agentPageLocalization);
            // set the owning side to null (unless already changed)
            if ($agentPageLocalization->getAgentPage() === $this) {
                $agentPageLocalization->setAgentPage(null);
            }
        }

        return $this;
    }
}
