<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateTypeLocalizationRepository")
 */
class RealEstateTypeLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateType", inversedBy="realEstateTypeLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateTypeLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $title_prefix;

    public function getRealEstateType(): ?RealEstateType
    {
        return $this->real_estate_type;
    }

    public function setRealEstateType(?RealEstateType $real_estate_type): self
    {
        $this->real_estate_type = $real_estate_type;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitlePrefix(): ?string
    {
        return $this->title_prefix;
    }

    public function setTitlePrefix(string $title_prefix): self
    {
        $this->title_prefix = $title_prefix;

        return $this;
    }
}
