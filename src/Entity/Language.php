<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RegionLocalization", mappedBy="language")
     */
    private $regionLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LocationLocalization", mappedBy="language")
     */
    private $locationLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateTypeLocalization", mappedBy="language")
     */
    private $realEstateTypeLocalizations;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FeatureLocalization", mappedBy="language")
     */
    private $featureLocalizations;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StatusLocalization", mappedBy="language")
     */
    private $statusLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $realEstateLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateMetaLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $realEstateMetaLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PhotoMetaLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $photoMetaLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateCustomPageLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $realEstateCustomPageLocalizations;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CmsPageLocalization", mappedBy="language")
     */
    private $cmsPageLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationPageLocalization", mappedBy="language")
     */
    private $realEstateLocationPageLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationTypePageLocalization", mappedBy="language")
     */
    private $realEstateLocationTypePageLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CmsPageMetaLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $cmsPageMetaLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateCustomPageMetadataLocalization", mappedBy="language")
     */
    private $realEstateCustomPageMetadataLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BlogCategoryLocalization", mappedBy="language", orphanRemoval=true)
     */
    private $blogCategoryLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateImageMetaLocalization", mappedBy="language")
     */
    private $realEstateImageMetaLocalizations;

    public function __construct()
    {
        $this->baseConstruct();
        $this->regionLocalizations = new ArrayCollection();
        $this->locationLocalizations = new ArrayCollection();
        $this->realEstateTypeLocalizations = new ArrayCollection();
        $this->featureLocalizations = new ArrayCollection();
        $this->statusLocalizations = new ArrayCollection();
        $this->realEstateLocalizations = new ArrayCollection();
        $this->realEstateMetaLocalizations = new ArrayCollection();
        $this->photoMetaLocalizations = new ArrayCollection();
        $this->realEstateCustomPageLocalizations = new ArrayCollection();
        $this->cmsPageLocalizations = new ArrayCollection();
        $this->realEstateLocationPageLocalizations = new ArrayCollection();
        $this->realEstateLocationTypePageLocalizations = new ArrayCollection();
        $this->cmsPageMetaLocalizations = new ArrayCollection();
        $this->realEstateCustomPageMetadataLocalizations = new ArrayCollection();

        $this->blogCategoryLocalizations = new ArrayCollection();

        $this->realEstateImageMetaLocalizations = new ArrayCollection();

    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RegionLocalization[]
     */
    public function getRegionLocalizations(): Collection
    {
        return $this->regionLocalizations;
    }

    public function addRegionLocalization(RegionLocalization $regionLocalization): self
    {
        if (!$this->regionLocalizations->contains($regionLocalization)) {
            $this->regionLocalizations[] = $regionLocalization;
            $regionLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRegionLocalization(RegionLocalization $regionLocalization): self
    {
        if ($this->regionLocalizations->contains($regionLocalization)) {
            $this->regionLocalizations->removeElement($regionLocalization);
            // set the owning side to null (unless already changed)
            if ($regionLocalization->getLanguage() === $this) {
                $regionLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LocationLocalization[]
     */
    public function getLocationLocalizations(): Collection
    {
        return $this->locationLocalizations;
    }

    public function addLocationLocalization(LocationLocalization $locationLocalization): self
    {
        if (!$this->locationLocalizations->contains($locationLocalization)) {
            $this->locationLocalizations[] = $locationLocalization;
            $locationLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeLocationLocalization(LocationLocalization $locationLocalization): self
    {
        if ($this->locationLocalizations->contains($locationLocalization)) {
            $this->locationLocalizations->removeElement($locationLocalization);
            // set the owning side to null (unless already changed)
            if ($locationLocalization->getLanguage() === $this) {
                $locationLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateTypeLocalization[]
     */
    public function getRealEstateTypeLocalizations(): Collection
    {
        return $this->realEstateTypeLocalizations;
    }

    public function addRealEstateTypeLocalization(RealEstateTypeLocalization $realEstateTypeLocalization): self
    {
        if (!$this->realEstateTypeLocalizations->contains($realEstateTypeLocalization)) {
            $this->realEstateTypeLocalizations[] = $realEstateTypeLocalization;
            $realEstateTypeLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateTypeLocalization(RealEstateTypeLocalization $realEstateTypeLocalization): self
    {
        if ($this->realEstateTypeLocalizations->contains($realEstateTypeLocalization)) {
            $this->realEstateTypeLocalizations->removeElement($realEstateTypeLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateTypeLocalization->getLanguage() === $this) {
                $realEstateTypeLocalization->setLanguage(null);
            }
        }

        return $this;
    }


    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return Collection|FeatureLocalization[]
     */
    public function getFeatureLocalizations(): Collection
    {
        return $this->featureLocalizations;
    }

    public function addFeatureLocalization(FeatureLocalization $featureLocalization): self
    {
        if (!$this->featureLocalizations->contains($featureLocalization)) {
            $this->featureLocalizations[] = $featureLocalization;
            $featureLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeFeatureLocalization(FeatureLocalization $featureLocalization): self
    {
        if ($this->featureLocalizations->contains($featureLocalization)) {
            $this->featureLocalizations->removeElement($featureLocalization);
            // set the owning side to null (unless already changed)
            if ($featureLocalization->getLanguage() === $this) {
                $featureLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatusLocalization[]
     */
    public function getStatusLocalizations(): Collection
    {
        return $this->statusLocalizations;
    }

    public function addStatusLocalization(StatusLocalization $statusLocalization): self
    {
        if (!$this->statusLocalizations->contains($statusLocalization)) {
            $this->statusLocalizations[] = $statusLocalization;
            $statusLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeStatusLocalization(StatusLocalization $statusLocalization): self
    {
        if ($this->statusLocalizations->contains($statusLocalization)) {
            $this->statusLocalizations->removeElement($statusLocalization);
            // set the owning side to null (unless already changed)
            if ($statusLocalization->getLanguage() === $this) {
                $statusLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateLocalization[]
     */
    public function getRealEstateLocalizations(): Collection
    {
        return $this->realEstateLocalizations;
    }

    public function addRealEstateLocalization(RealEstateLocalization $realEstateLocalization): self
    {
        if (!$this->realEstateLocalizations->contains($realEstateLocalization)) {
            $this->realEstateLocalizations[] = $realEstateLocalization;
            $realEstateLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateLocalization(RealEstateLocalization $realEstateLocalization): self
    {
        if ($this->realEstateLocalizations->contains($realEstateLocalization)) {
            $this->realEstateLocalizations->removeElement($realEstateLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocalization->getLanguage() === $this) {
                $realEstateLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateMetaLocalization[]
     */
    public function getRealEstateMetaLocalizations(): Collection
    {
        return $this->realEstateMetaLocalizations;
    }

    public function addRealEstateMetaLocalization(RealEstateMetaLocalization $realEstateMetaLocalization): self
    {
        if (!$this->realEstateMetaLocalizations->contains($realEstateMetaLocalization)) {
            $this->realEstateMetaLocalizations[] = $realEstateMetaLocalization;
            $realEstateMetaLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateMetaLocalization(RealEstateMetaLocalization $realEstateMetaLocalization): self
    {
        if ($this->realEstateMetaLocalizations->contains($realEstateMetaLocalization)) {
            $this->realEstateMetaLocalizations->removeElement($realEstateMetaLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateMetaLocalization->getLanguage() === $this) {
                $realEstateMetaLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PhotoMetaLocalization[]
     */
    public function getPhotoMetaLocalizations(): Collection
    {
        return $this->photoMetaLocalizations;
    }

    public function addPhotoMetaLocalization(PhotoMetaLocalization $photoMetaLocalization): self
    {
        if (!$this->photoMetaLocalizations->contains($photoMetaLocalization)) {
            $this->photoMetaLocalizations[] = $photoMetaLocalization;
            $photoMetaLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removePhotoMetaLocalization(PhotoMetaLocalization $photoMetaLocalization): self
    {
        if ($this->photoMetaLocalizations->contains($photoMetaLocalization)) {
            $this->photoMetaLocalizations->removeElement($photoMetaLocalization);
            // set the owning side to null (unless already changed)
            if ($photoMetaLocalization->getLanguage() === $this) {
                $photoMetaLocalization->setLanguage(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|RealEstateCustomPageLocalization[]
     */
    public function getRealEstateCustomPageLocalizations(): Collection
    {
        return $this->realEstateCustomPageLocalizations;
    }

    public function addRealEstateCustomPageLocalization(RealEstateCustomPageLocalization $realEstateCustomPageLocalization): self
    {
        if (!$this->realEstateCustomPageLocalizations->contains($realEstateCustomPageLocalization)) {
            $this->realEstateCustomPageLocalizations[] = $realEstateCustomPageLocalization;
            $realEstateCustomPageLocalization->setLanguage($this);
        }
    }

     /**
     * @return Collection|CmsPageLocalization[]
     */
    public function getCmsPageLocalizations(): Collection
    {
        return $this->cmsPageLocalizations;
    }

    public function addCmsPageLocalization(CmsPageLocalization $cmsPageLocalization): self
    {
        if (!$this->cmsPageLocalizations->contains($cmsPageLocalization)) {
            $this->cmsPageLocalizations[] = $cmsPageLocalization;
            $cmsPageLocalization->setLanguage($this);
        }

        return $this;
    }


    public function removeRealEstateCustomPageLocalization(RealEstateCustomPageLocalization $realEstateCustomPageLocalization): self
    {
        if ($this->realEstateCustomPageLocalizations->contains($realEstateCustomPageLocalization)) {
            $this->realEstateCustomPageLocalizations->removeElement($realEstateCustomPageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateCustomPageLocalization->getLanguage() === $this) {
                $realEstateCustomPageLocalization->setLanguage(null);
            }
        }
    }

    public function removeCmsPageLocalization(CmsPageLocalization $cmsPageLocalization): self
    {
        if ($this->cmsPageLocalizations->contains($cmsPageLocalization)) {
            $this->cmsPageLocalizations->removeElement($cmsPageLocalization);
            // set the owning side to null (unless already changed)
            if ($cmsPageLocalization->getLanguage() === $this) {
                $cmsPageLocalization->setLanguage(null);

            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateLocationPageLocalization[]
     */
    public function getRealEstateLocationPageLocalizations(): Collection
    {
        return $this->realEstateLocationPageLocalizations;
    }

    public function addRealEstateLocationPageLocalization(RealEstateLocationPageLocalization $realEstateLocationPageLocalization): self
    {
        if (!$this->realEstateLocationPageLocalizations->contains($realEstateLocationPageLocalization)) {
            $this->realEstateLocationPageLocalizations[] = $realEstateLocationPageLocalization;
            $realEstateLocationPageLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateLocationPageLocalization(RealEstateLocationPageLocalization $realEstateLocationPageLocalization): self
    {
        if ($this->realEstateLocationPageLocalizations->contains($realEstateLocationPageLocalization)) {
            $this->realEstateLocationPageLocalizations->removeElement($realEstateLocationPageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationPageLocalization->getLanguage() === $this) {
                $realEstateLocationPageLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateLocationTypePageLocalization[]
     */
    public function getRealEstateLocationTypePageLocalizations(): Collection
    {
        return $this->realEstateLocationTypePageLocalizations;
    }

    public function addRealEstateLocationTypePageLocalization(RealEstateLocationTypePageLocalization $realEstateLocationTypePageLocalization): self
    {
        if (!$this->realEstateLocationTypePageLocalizations->contains($realEstateLocationTypePageLocalization)) {
            $this->realEstateLocationTypePageLocalizations[] = $realEstateLocationTypePageLocalization;
            $realEstateLocationTypePageLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateLocationTypePageLocalization(RealEstateLocationTypePageLocalization $realEstateLocationTypePageLocalization): self
    {
        if ($this->realEstateLocationTypePageLocalizations->contains($realEstateLocationTypePageLocalization)) {
            $this->realEstateLocationTypePageLocalizations->removeElement($realEstateLocationTypePageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationTypePageLocalization->getLanguage() === $this) {
                $realEstateLocationTypePageLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CmsPageMetaLocalization[]
     */
    public function getCmsPageMetaLozalizations(): Collection
    {
        return $this->cmsPageMetaLozalizations;
    }

    public function addCmsPageMetaLozalization(CmsPageMetaLocalization $cmsPageMetaLozalization): self
    {
        if (!$this->cmsPageMetaLozalizations->contains($cmsPageMetaLozalization)) {
            $this->cmsPageMetaLozalizations[] = $cmsPageMetaLozalization;
            $cmsPageMetaLozalization->setLanguage($this);
        }

        return $this;
    }

    public function removeCmsPageMetaLozalization(CmsPageMetaLocalization $cmsPageMetaLozalization): self
    {
        if ($this->cmsPageMetaLozalizations->contains($cmsPageMetaLozalization)) {
            $this->cmsPageMetaLozalizations->removeElement($cmsPageMetaLozalization);
            // set the owning side to null (unless already changed)
            if ($cmsPageMetaLozalization->getLanguage() === $this) {
                $cmsPageMetaLozalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateCustomPageMetadataLocalization[]
     */
    public function getRealEstateCustomPageMetadataLocalizations(): Collection
    {
        return $this->realEstateCustomPageMetadataLocalizations;
    }

    public function addRealEstateCustomPageMetadataLocalization(RealEstateCustomPageMetadataLocalization $realEstateCustomPageMetadataLocalization): self
    {
        if (!$this->realEstateCustomPageMetadataLocalizations->contains($realEstateCustomPageMetadataLocalization)) {
            $this->realEstateCustomPageMetadataLocalizations[] = $realEstateCustomPageMetadataLocalization;
            $realEstateCustomPageMetadataLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeRealEstateCustomPageMetadataLocalization(RealEstateCustomPageMetadataLocalization $realEstateCustomPageMetadataLocalization): self
    {
        if ($this->realEstateCustomPageMetadataLocalizations->contains($realEstateCustomPageMetadataLocalization)) {
            $this->realEstateCustomPageMetadataLocalizations->removeElement($realEstateCustomPageMetadataLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateCustomPageMetadataLocalization->getLanguage() === $this) {
                $realEstateCustomPageMetadataLocalization->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogCategoryLocalization[]
     */
    public function getBlogCategoryLocalizations(): Collection
    {
        return $this->blogCategoryLocalizations;
    }

    public function addBlogCategoryLocalization(BlogCategoryLocalization $blogCategoryLocalization): self
    {
        if (!$this->blogCategoryLocalizations->contains($blogCategoryLocalization)) {
            $this->blogCategoryLocalizations[] = $blogCategoryLocalization;
            $blogCategoryLocalization->setLanguage($this);
        }

        return $this;
    }

    public function removeBlogCategoryLocalization(BlogCategoryLocalization $blogCategoryLocalization): self
    {
        if ($this->blogCategoryLocalizations->contains($blogCategoryLocalization)) {
            $this->blogCategoryLocalizations->removeElement($blogCategoryLocalization);
            // set the owning side to null (unless already changed)
            if ($blogCategoryLocalization->getLanguage() === $this) {
                $blogCategoryLocalization->setLanguage(null);
            }
        }

        return $this;
    }
}
