<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Entity\Traits\NamePropertyTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationLocalizationRepository")
 * @ORM\Table(name="location_localization", uniqueConstraints={@ORM\UniqueConstraint(name="idx_name_language_id", columns={"name", "language_id"})})
 * @UniqueEntity(
 *     fields={"name", "language"},
 *     message="Unos već postoji. Morate unijeti jedinstvenu vrijednost za navedeni jezik."
 * )
 */
class LocationLocalization
{
    use BaseTrait;
    use NamePropertyTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="locationLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="locationLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
