<?php

namespace App\Entity;

use App\Entity\Enum\RealEstateCustomPageTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateCustomPageRepository")
 */
class RealEstateCustomPage
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateCustomPage", inversedBy="realEstateCustomPages")
     */
    private $parentRealEstateCustomPage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateCustomPage", mappedBy="parentRealEstateCustomPage")
     */
    private $realEstateCustomPages;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $query;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateCustomPageLocalization", mappedBy="real_estate_custom_page", orphanRemoval=true, cascade={"all"})
     * @Assert\Valid
     */
    private $realEstateCustomPageLocalizations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RealEstateCustomPageFilters", mappedBy="real_estate_custom_page", cascade={"persist", "remove"})
     */
    private $realEstateCustomPageFilters;

    /**
     * @ORM\Column(type="boolean")
     */
    private $customizable;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Photo", cascade={"persist", "remove"})
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateCustomPageMetadataLocalization", mappedBy="real_estate_custom_page", cascade={"all"})
     */
    private $realEstateCustomPageMetadataLocalizations;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    public function __construct($args = null)
    {
        $this->baseConstruct();
        $this->realEstateCustomPages = new ArrayCollection();
        $this->realEstateCustomPageLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addRealEstateCustomPageLocalization($localization);
            }
        }
        $this->setCustomizable(true);
        $this->realEstateCustomPageMetadataLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("type", $args)){
            $this->setType($args["type"]);
        }
    }

    public function getParentRealEstateCustomPage(): ?self
    {
        return $this->parentRealEstateCustomPage;
    }

    public function setParentRealEstateCustomPage(?self $parentRealEstateCustomPage): self
    {
        $this->parentRealEstateCustomPage = $parentRealEstateCustomPage;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getRealEstateCustomPages(): Collection
    {
        return $this->realEstateCustomPages;
    }

    public function addRealEstateCustomPage(self $realEstateCustomPage): self
    {
        if (!$this->realEstateCustomPages->contains($realEstateCustomPage)) {
            $this->realEstateCustomPages[] = $realEstateCustomPage;
            $realEstateCustomPage->setParentRealEstateCustomPage($this);
        }

        return $this;
    }

    public function removeRealEstateCustomPage(self $realEstateCustomPage): self
    {
        if ($this->realEstateCustomPages->contains($realEstateCustomPage)) {
            $this->realEstateCustomPages->removeElement($realEstateCustomPage);
            // set the owning side to null (unless already changed)
            if ($realEstateCustomPage->getParentRealEstateCustomPage() === $this) {
                $realEstateCustomPage->setParentRealEstateCustomPage(null);
            }
        }

        return $this;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(string $query): self
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return Collection|RealEstateCustomPageLocalization[]
     */
    public function getRealEstateCustomPageLocalizations(): Collection
    {
        return $this->realEstateCustomPageLocalizations;
    }

    /**
     * @param $language string | Language
     */
    public function getRealEstateCustomPageLocalization($language) : ?RealEstateCustomPageLocalization
    {
        //TODO: Create a trait for all entities???
        try
        {
/*            $action = "get" . (new \ReflectionClass($this))->getShortName() . "Localizations";

            $localization = $this->{$action}()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });*/

            $localization = $this->getRealEstateCustomPageLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function addRealEstateCustomPageLocalization(RealEstateCustomPageLocalization $realEstateCustomPageLocalization): self
    {
        if (!$this->realEstateCustomPageLocalizations->contains($realEstateCustomPageLocalization)) {
            $this->realEstateCustomPageLocalizations[] = $realEstateCustomPageLocalization;
            $realEstateCustomPageLocalization->setRealEstateCustomPage($this);
        }

        return $this;
    }

    public function removeRealEstateCustomPageLocalization(RealEstateCustomPageLocalization $realEstateCustomPageLocalization): self
    {
        if ($this->realEstateCustomPageLocalizations->contains($realEstateCustomPageLocalization)) {
            $this->realEstateCustomPageLocalizations->removeElement($realEstateCustomPageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateCustomPageLocalization->getRealEstateCustomPage() === $this) {
                $realEstateCustomPageLocalization->setRealEstateCustomPage(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getRealEstateCustomPageFilters(): ?RealEstateCustomPageFilters
    {
        return $this->realEstateCustomPageFilters;
    }

    public function setRealEstateCustomPageFilters(?RealEstateCustomPageFilters $realEstateCustomPageFilters): self
    {
        $this->realEstateCustomPageFilters = $realEstateCustomPageFilters;

        // set (or unset) the owning side of the relation if necessary
        $newReal_estate_custom_page = null === $realEstateCustomPageFilters ? null : $this;
        if ($realEstateCustomPageFilters->getRealEstateCustomPage() !== $newReal_estate_custom_page) {
            $realEstateCustomPageFilters->setRealEstateCustomPage($newReal_estate_custom_page);
        }

        return $this;
    }

    public function getCustomizable(): ?bool
    {
        return $this->customizable;
    }

    public function setCustomizable(bool $customizable): self
    {
        $this->customizable = $customizable;

        return $this;
    }

    public function getPhoto(): ?Photo
    {
        return $this->photo;
    }

    public function setPhoto(?Photo $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

     /**
     * @return Collection|RealEstateCustomPageMetadataLocalization[]
     */
    public function getRealEstateCustomPageMetadataLocalizations(): Collection
    {
        return $this->realEstateCustomPageMetadataLocalizations;
    }

    /**
     * @param $language string | Language
     */
    public function getRealEstateCustomPageMetadataLocalization($language): ?RealEstateCustomPageMetadataLocalization
    {
        //TODO: Create a trait for all entities???
        try
        {
            /*
            $action = "get" . (new \ReflectionClass($this))->getShortName() . "Localizations";

            $localization = $this->{$action}()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });*/

            $metaLocalization = $this->getRealEstateCustomPageMetadataLocalizations()->filter(function($metaLoco) use ($language) {
                return is_object($language) ? $metaLoco->getLanguage() == $language : $metaLoco->getLanguage()->getCode() == $language;
            });

            if($metaLocalization->count())
            {
                return $metaLocalization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function addRealEstateCustomPageMetadataLocalization(RealEstateCustomPageMetadataLocalization $realEstateCustomPageMetadataLocalization): self
    {
        if (!$this->realEstateCustomPageMetadataLocalizations->contains($realEstateCustomPageMetadataLocalization)) {
            $this->realEstateCustomPageMetadataLocalizations[] = $realEstateCustomPageMetadataLocalization;
            $realEstateCustomPageMetadataLocalization->setRealEstateCustomPage($this);
        }

        return $this;
    }

    public function removeRealEstateCustomPageMetadataLocalization(RealEstateCustomPageMetadataLocalization $realEstateCustomPageMetadataLocalization): self
    {
        if ($this->realEstateCustomPageMetadataLocalizations->contains($realEstateCustomPageMetadataLocalization)) {
            $this->realEstateCustomPageMetadataLocalizations->removeElement($realEstateCustomPageMetadataLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateCustomPageMetadataLocalization->getRealEstateCustomPage() === $this) {
                $realEstateCustomPageMetadataLocalization->setRealEstateCustomPage(null);
            }
        }

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /***************************************************************/
    /***********---->>>>>>>> VALIDATION METHODS <<<<<<<<<<--------**/
    /***************************************************************/

    /**
     * @Assert\Callback
     */
    public function validatePhoto(ExecutionContextInterface $context)
    {
        //For categories of type AUTOGENERATED or CUSTOM
        if( $this->getType() != RealEstateCustomPageTypeEnum::SYSTEM && $this->parentRealEstateCustomPage == null && $this->photo == null)
        {
            $context
                ->buildViolation('Polje je obavezno!')
                ->atPath("photo")
                ->addViolation();
        }
    }

    /***************************************************************/
    /***********---->>>>>>>> CUSTOM METHODS <<<<<<<<<<--------**/
    /***************************************************************/

    /**
     * Method merges filter objects form the calling object and its parent and return a new instance of filters object
     * which can be used afterwards.
     */
    public function mergeFilters()
    {
        if(!$this->parentRealEstateCustomPage)
        {
            return clone $this->getRealEstateCustomPageFilters();
        }
        else
        {
            $childFilters = $this->getRealEstateCustomPageFilters();
            $parentFilters = $this->getParentRealEstateCustomPage()->getRealEstateCustomPageFilters();
            $result = new RealEstateCustomPageFilters();

            //Merge locations
            foreach ($childFilters->getLocations() as $childLocation)
            {
                if(!$parentFilters->getLocations()->contains($childLocation)){
                    $result->addLocation($childLocation);
                }
            }

            $result->setLocations(new ArrayCollection(array_merge( $result->getLocations()->toArray(), $parentFilters->getLocations()->toArray() )));

            //Merge real estate types
            foreach ($childFilters->getRealEstateTypes() as $childRealEstateType)
            {
                if(!$parentFilters->getRealEstateTypes()->contains($childRealEstateType)){
                    $result->addRealEstateType($childRealEstateType);
                }
            }

            $result->setRealEstateTypes(new ArrayCollection(array_merge( $result->getRealEstateTypes()->toArray(), (array)$parentFilters->getRealEstateTypes()->toArray() )));

            return $result;
        }
    }
}
