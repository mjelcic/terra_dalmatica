<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Entity\Traits\NamePropertyTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeatureLocalizationRepository")
 */
class FeatureLocalization
{

    use BaseTrait;
    use NamePropertyTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Feature", inversedBy="featureLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $feature;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="featureLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    public function getFeature(): ?Feature
    {
        return $this->feature;
    }

    public function setFeature(?Feature $feature): self
    {
        $this->feature = $feature;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
