<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPreferenceRepository")
 */
class UserPreference
{
    use BaseTrait {
        BaseTrait::__construct as baseContruct;
    }

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RealEstateType", inversedBy="userPreferences")
     */
    private $real_estate_types;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location", inversedBy="userPreferences")
     */
    private $locations;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="userPreference", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price_min;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price_max;

    public function __construct()
    {
        $this->baseContruct();
        $this->real_estate_types = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }

    /**
     * @return Collection|RealEstateType[]
     */
    public function getRealEstateTypes(): Collection
    {
        return $this->real_estate_types;
    }

    public function addRealEstateType(RealEstateType $realEstateType): self
    {
        if (!$this->real_estate_types->contains($realEstateType)) {
            $this->real_estate_types[] = $realEstateType;
        }

        return $this;
    }

    public function removeRealEstateType(RealEstateType $realEstateType): self
    {
        if ($this->real_estate_types->contains($realEstateType)) {
            $this->real_estate_types->removeElement($realEstateType);
        }

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPriceMin(): ?string
    {
        return $this->price_min;
    }

    public function setPriceMin(string $price_min): self
    {
        $this->price_min = $price_min;

        return $this;
    }

    public function getPriceMax(): ?string
    {
        return $this->price_max;
    }

    public function setPriceMax(string $price_max): self
    {
        $this->price_max = $price_max;

        return $this;
    }
}
