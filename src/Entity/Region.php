<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RegionRepository")
 */
class Region
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

   /**
    * @ORM\OneToMany(targetEntity="App\Entity\RegionLocalization", mappedBy="region", cascade={"all"})
    * @Assert\Valid;
    */
   private $regionLocalizations;

   /**
    * @ORM\OneToMany(targetEntity="App\Entity\Location", mappedBy="region")
    */
   private $locations;

   public function __construct($args)
   {
       $this->baseConstruct();
       $this->regionLocalizations = new ArrayCollection();
       if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
           foreach ($args["localizations"] as $localization){
               $this->addRegionLocalization($localization);
           }
       }
       $this->locations = new ArrayCollection();
   }

   /**
    * @return Collection|RegionLocalization[]
    */
   public function getRegionLocalizations(): Collection
   {
       return $this->regionLocalizations;
   }

   public function addRegionLocalization(RegionLocalization $regionLocalization): self
   {
       if (!$this->regionLocalizations->contains($regionLocalization)) {
           $this->regionLocalizations[] = $regionLocalization;
           $regionLocalization->setRegion($this);
       }

       return $this;
   }

   public function removeRegionLocalization(RegionLocalization $regionLocalization): self
   {
       if ($this->regionLocalizations->contains($regionLocalization)) {
           $this->regionLocalizations->removeElement($regionLocalization);
           // set the owning side to null (unless already changed)
           if ($regionLocalization->getRegion() === $this) {
               $regionLocalization->setRegion(null);
           }
       }

       return $this;
   }

   /**
    * @return Collection|Location[]
    */
   public function getLocations(): Collection
   {
       return $this->locations;
   }

   public function addLocation(Location $location): self
   {
       if (!$this->locations->contains($location)) {
           $this->locations[] = $location;
           $location->setRegion($this);
       }

       return $this;
   }

   public function removeLocation(Location $location): self
   {
       if ($this->locations->contains($location)) {
           $this->locations->removeElement($location);
           // set the owning side to null (unless already changed)
           if ($location->getRegion() === $this) {
               $location->setRegion(null);
           }
       }

       return $this;
   }
}
