<?php


namespace App\Entity;


use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateImageMetaLocalizationRepository")
 */
class RealEstateImageMetaLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateImage", inversedBy="realEstateImageMetaLocalizations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $realEstateImage;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $alt_tag;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateImageMetaLocalizations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @return mixed
     */
    public function getRealEstateImage()
    {
        return $this->realEstateImage;
    }

    /**
     * @param mixed $realEstateImage
     */
    public function setRealEstateImage($realEstateImage): void
    {
        $this->realEstateImage = $realEstateImage;
    }


    public function getAltTag(): ?string
    {
        return $this->alt_tag;
    }

    public function setAltTag(string $alt_tag): self
    {
        $this->alt_tag = $alt_tag;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

}