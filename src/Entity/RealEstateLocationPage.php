<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateLocationPageRepository")
 */
class RealEstateLocationPage
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Location", inversedBy="realEstateLocationPage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    //private $location;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationPageLocalization", mappedBy="real_estate_location_page", cascade={"all"})
     */
    private $realEstateLocationPageLocalizations;

    public function __construct()
    {
        $this->baseConstruct();
        $this->realEstateLocationPageLocalizations = new ArrayCollection();
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|RealEstateLocationPageLocalization[]
     */
    public function getRealEstateLocationPageLocalizations(): Collection
    {
        return $this->realEstateLocationPageLocalizations;
    }

    public function addRealEstateLocationPageLocalization(RealEstateLocationPageLocalization $realEstateLocationPageLocalization): self
    {
        if (!$this->realEstateLocationPageLocalizations->contains($realEstateLocationPageLocalization)) {
            $this->realEstateLocationPageLocalizations[] = $realEstateLocationPageLocalization;
            $realEstateLocationPageLocalization->setRealEstateLocationPage($this);
        }

        return $this;
    }

    public function removeRealEstateLocationPageLocalization(RealEstateLocationPageLocalization $realEstateLocationPageLocalization): self
    {
        if ($this->realEstateLocationPageLocalizations->contains($realEstateLocationPageLocalization)) {
            $this->realEstateLocationPageLocalizations->removeElement($realEstateLocationPageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationPageLocalization->getRealEstateLocationPage() === $this) {
                $realEstateLocationPageLocalization->setRealEstateLocationPage(null);
            }
        }

        return $this;
    }
}
