<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Entity\Traits\NamePropertyTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RegionLocalizationRepository")
 */
class RegionLocalization
{
    use BaseTrait;
    use NamePropertyTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="regionLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="regionLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;


    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
