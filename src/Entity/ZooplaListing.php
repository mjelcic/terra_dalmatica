<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZooplaListingRepository")
 */
class ZooplaListing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RealEstate", cascade={"persist", "remove"})
     */
    private $realEstate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zooplaId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eTag;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $invalidated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $deleteReason;

    /**
     * ZooplaListing constructor.
     * @param $realEstate
     * @param $zooplaId
     */
    public function __construct($realEstate, $zooplaId, $eTag, $url)
    {
        $this->realEstate = $realEstate;
        $this->zooplaId = $zooplaId;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->invalidated = 0;
        $this->eTag = $eTag;
        $this->url = $url;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRealEstate(): ?RealEstate
    {
        return $this->realEstate;
    }

    public function getZooplaId(): ?string
    {
        return $this->zooplaId;
    }

    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    public function setDeletedAt(): void
    {
        $this->deletedAt = new \DateTime();
    }

    public function clearDeletedAt(): void
    {
        $this->deletedAt = null;
    }

    /**
     * @param mixed $deleteReason
     */
    public function setDeleteReason($deleteReason): void
    {
        $this->deleteReason = $deleteReason;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getInvalidated(): int
    {
        return $this->invalidated;
    }

    public function invalidate()
    {
        $this->invalidated = 1;
    }

    /**
     * @return mixed
     */
    public function getETag()
    {
        return $this->eTag;
    }

    /**
     * @param mixed $eTag
     */
    public function setETag($eTag): void
    {
        $this->eTag = $eTag;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return mixed
     */
    public function getDeleteReason()
    {
        return $this->deleteReason;
    }

}
