<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeatureRepository")
 */
class Feature
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FeatureLocalization", mappedBy="feature", cascade={"all"})
     */
    private $featureLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateFeature", mappedBy="feature", orphanRemoval=true)
     */
    private $realEstateFeatures;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->featureLocalizations = new ArrayCollection();
        $this->realEstateFeatures = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addFeatureLocalization($localization);
            }
        }
    }

    /**
     * @return Collection|FeatureLocalization[]
     */
    public function getFeatureLocalizations(): Collection
    {
        return $this->featureLocalizations;
    }

    public function getFeatureLocalization($language): FeatureLocalization
    {
        try
        {
            $localization = $this->getFeatureLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }


    public function addFeatureLocalization(FeatureLocalization $featureLocalization): self
    {
        if (!$this->featureLocalizations->contains($featureLocalization)) {
            $this->featureLocalizations[] = $featureLocalization;
            $featureLocalization->setFeature($this);
        }

        return $this;
    }

    public function removeFeatureLocalization(FeatureLocalization $featureLocalization): self
    {
        if ($this->featureLocalizations->contains($featureLocalization)) {
            $this->featureLocalizations->removeElement($featureLocalization);
            // set the owning side to null (unless already changed)
            if ($featureLocalization->getFeature() === $this) {
                $featureLocalization->setFeature(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateFeature[]
     */
    public function getRealEstateFeatures(): Collection
    {
        return $this->realEstateFeatures;
    }

    public function addRealEstateFeature(RealEstateFeature $realEstateFeature): self
    {
        if (!$this->realEstateFeatures->contains($realEstateFeature)) {
            $this->realEstateFeatures[] = $realEstateFeature;
            $realEstateFeature->setFeature($this);
        }

        return $this;
    }

    public function removeRealEstateFeature(RealEstateFeature $realEstateFeature): self
    {
        if ($this->realEstateFeatures->contains($realEstateFeature)) {
            $this->realEstateFeatures->removeElement($realEstateFeature);
            // set the owning side to null (unless already changed)
            if ($realEstateFeature->getFeature() === $this) {
                $realEstateFeature->setFeature(null);
            }
        }

        return $this;
    }
}
