<?php

namespace App\Entity;

use App\Entity\Traits\LocalizedNameTrait;
use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateTypeRepository")
 */
class RealEstateType
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    use LocalizedNameTrait;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateTypeLocalization", mappedBy="real_estate_type", cascade={"all"}, fetch="EAGER")
     */
    private $realEstateTypeLocalizations;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstate", mappedBy="RealEstateType", orphanRemoval=true)
     */
    private $realEstates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationTypePage", mappedBy="real_estate_type")
     */
    private $realEstateLocationTypePages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserPreference", mappedBy="real_estate_types")
     */
    private $userPreferences;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->realEstateTypeLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addRealEstateTypeLocalization($localization);
            }
        }
        $this->realEstates = new ArrayCollection();
        $this->realEstateLocationTypePages = new ArrayCollection();
        $this->userPreferences = new ArrayCollection();
    }

    /**
     * @param $language string | Language
     */
    public function getTitlePrefix($language)
    {
        try
        {
            $action = "get" . (new \ReflectionClass($this))->getShortName() . "Localizations";

            $localization = $this->{$action}()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first()->getTitlePrefix();
            }

            return "";
        }
        catch(\Exception $ex)
        {
            return "";
        }
    }

    /**
     * @return Collection|RealEstateTypeLocalization[]
     */
    public function getRealEstateTypeLocalizations(): Collection
    {
        return $this->realEstateTypeLocalizations;
    }

    public function addRealEstateTypeLocalization(RealEstateTypeLocalization $realEstateTypeLocalization): self
    {
        if (!$this->realEstateTypeLocalizations->contains($realEstateTypeLocalization)) {
            $this->realEstateTypeLocalizations[] = $realEstateTypeLocalization;
            $realEstateTypeLocalization->setRealEstateType($this);
        }

        return $this;
    }

    public function removeRealEstateTypeLocalization(RealEstateTypeLocalization $realEstateTypeLocalization): self
    {
        if ($this->realEstateTypeLocalizations->contains($realEstateTypeLocalization)) {
            $this->realEstateTypeLocalizations->removeElement($realEstateTypeLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateTypeLocalization->getRealEstateType() === $this) {
                $realEstateTypeLocalization->setRealEstateType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstate[]
     */
    public function getRealEstates(): Collection
    {
        return $this->realEstates;
    }

    public function addRealEstate(RealEstate $realEstate): self
    {
        if (!$this->realEstates->contains($realEstate)) {
            $this->realEstates[] = $realEstate;
            $realEstate->setRealEstateType($this);
        }

        return $this;
    }

    public function removeRealEstate(RealEstate $realEstate): self
    {
        if ($this->realEstates->contains($realEstate)) {
            $this->realEstates->removeElement($realEstate);
            // set the owning side to null (unless already changed)
            if ($realEstate->getRealEstateType() === $this) {
                $realEstate->setRealEstateType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstateLocationTypePage[]
     */
    public function getRealEstateLocationTypePages(): Collection
    {
        return $this->realEstateLocationTypePages;
    }

    public function addRealEstateLocationTypePage(RealEstateLocationTypePage $realEstateLocationTypePage): self
    {
        if (!$this->realEstateLocationTypePages->contains($realEstateLocationTypePage)) {
            $this->realEstateLocationTypePages[] = $realEstateLocationTypePage;
            $realEstateLocationTypePage->setRealEstateType($this);
        }

        return $this;
    }

    public function removeRealEstateLocationTypePage(RealEstateLocationTypePage $realEstateLocationTypePage): self
    {
        if ($this->realEstateLocationTypePages->contains($realEstateLocationTypePage)) {
            $this->realEstateLocationTypePages->removeElement($realEstateLocationTypePage);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationTypePage->getRealEstateType() === $this) {
                $realEstateLocationTypePage->setRealEstateType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPreference[]
     */
    public function getUserPreferences(): Collection
    {
        return $this->userPreferences;
    }

    public function addUserPreference(UserPreference $userPreference): self
    {
        if (!$this->userPreferences->contains($userPreference)) {
            $this->userPreferences[] = $userPreference;
            $userPreference->addRealEstateType($this);
        }

        return $this;
    }

    public function removeUserPreference(UserPreference $userPreference): self
    {
        if ($this->userPreferences->contains($userPreference)) {
            $this->userPreferences->removeElement($userPreference);
            $userPreference->removeRealEstateType($this);
        }

        return $this;
    }

    public function getTypeLocalization($language): ?RealEstateTypeLocalization
    {
        try {
            $localization = $this->getRealEstateTypeLocalizations()->filter(
                function(RealEstateTypeLocalization $loco) use ($language) {
                    return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        } catch(\Exception $ex) {
            return null;
        }
    }
}
