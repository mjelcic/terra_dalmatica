<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateCustomPageLocalizationRepository")
 */
class RealEstateCustomPageLocalization
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateCustomPage", inversedBy="realEstateCustomPageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_custom_page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateCustomPageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank(message="Polje je obavezno!")
     * @Assert\Length(max="200", maxMessage="Polje ne može sadržavati više od {{ limit }} karaktera");
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank(message="Polje je obavezno!")
     * @Assert\Length(max="200", maxMessage="Polje ne može sadržavati više od {{ limit }} karaktera");
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;


    public function getRealEstateCustomPage(): ?RealEstateCustomPage
    {
        return $this->real_estate_custom_page;
    }

    public function setRealEstateCustomPage(?RealEstateCustomPage $real_estate_custom_page): self
    {
        $this->real_estate_custom_page = $real_estate_custom_page;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
