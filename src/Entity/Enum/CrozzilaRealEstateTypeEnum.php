<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/22/2018
 * Time: 9:28 PM
 */

namespace App\Entity\Enum;


class CrozzilaRealEstateTypeEnum
{
    const APARTMENT = 1;
    const HOUSE = 2;
    const PLOT = 3;
    const OFFICE = 4;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::APARTMENT => 'apartment',
        self::HOUSE => 'house',
        self::PLOT => 'plot',
        self::OFFICE => 'office',
    ];

    /**
     * @param  string $typeName
     * @return string
     */
    public static function getTypeName($id)
    {
        if (!isset(static::$typeName[$id])) {
            return "Unknown type ($id)";
        }
        return static::$typeName[$id];
    }

}