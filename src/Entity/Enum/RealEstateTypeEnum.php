<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 6/12/2020
 * Time: 2:27 PM
 */

namespace App\Entity\Enum;


abstract class RealEstateTypeEnum
{
    const APARTMENT = 1;
    const HOUSE = 2;
    const LAND = 3;
    const BUSINESS = 4;
}