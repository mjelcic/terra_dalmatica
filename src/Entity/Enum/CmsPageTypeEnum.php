<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/22/2018
 * Time: 9:28 PM
 */

namespace App\Entity\Enum;


class CmsPageTypeEnum
{
    const BLOG = 0;
    const PREDEFINED = 1;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::BLOG => 'Blog',
        self::PREDEFINED => 'Predefined content',
    ];

    /**
     * @param  string $typeName
     * @return string
     */
    public static function getTypeName($id)
    {
        if (!isset(static::$typeName[$id])) {
            return "Unknown type ($id)";
        }
        return static::$typeName[$id];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::BLOG,
            self::PREDEFINED,
        ];
    }
}