<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/22/2018
 * Time: 9:28 PM
 */

namespace App\Entity\Enum;


class DocumentTypeEnum
{
    const BILL_OF_SALES = 0;
    const OTHER = 1;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::BILL_OF_SALES => 'Bill of sales',
        self::OTHER => 'Other',
    ];

    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getTypeName($id)
    {
        if (!isset(static::$typeName[$id])) {
            return "Unknown type ($id)";
        }
        return static::$typeName[$id];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::BILL_OF_SALES,
            self::OTHER,
        ];
    }

    public static function isSupported(int $docType): bool
    {
        return in_array($docType, self::getAvailableTypes());
    }
}