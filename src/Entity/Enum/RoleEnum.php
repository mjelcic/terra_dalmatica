<?php

namespace App\Entity\Enum;

abstract class RoleEnum
{
    const ADMIN = "ROLE_ADMIN";
    const EMPLOYEE = "ROLE_EMPLOYEE";
    const USER = "ROLE_USER";

    /** @var array user friendly named type */
    protected static $roleName = [
        self::ADMIN => 'Administrator',
        self::EMPLOYEE => 'Zaposlenik/Agent',
        self::USER => 'Klijent'
    ];

    /**
     * @param  string $roleName
     * @return string
     */
    public static function getRoleName($key)
    {
        if (!isset(static::$roleName[$key])) {
            return "Unknown type ($key)";
        }

        return static::$roleName[$key];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableRoles()
    {
        return [
            self::ADMIN,
            self::EMPLOYEE,
            self::USER
        ];
    }
}