<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/22/2018
 * Time: 9:28 PM
 */

namespace App\Entity\Enum;


class RealiticaRealEstateTypeEnum
{
    const APARTMENT = 1;
    const HOUSE = 2;
    const LAND = 3;
    const COMMERCIAL = 4;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::APARTMENT => 'apartment',
        self::HOUSE => 'house',
        self::LAND => 'land',
        self::COMMERCIAL => 'commercial',
    ];

    /**
     * @param  string $typeName
     * @return string
     */
    public static function getTypeName($id)
    {
        if (!isset(static::$typeName[$id])) {
            return "Unknown type ($id)";
        }
        return static::$typeName[$id];
    }

}