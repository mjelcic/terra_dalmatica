<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateLocationTypePageLocalizationRepository")
 */
class RealEstateLocationTypePageLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateLocationTypePage", inversedBy="realEstateLocationTypePageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_location_type_page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateLocationTypePageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    public function getRealEstateLocationTypePage(): ?RealEstateLocationTypePage
    {
        return $this->real_estate_location_type_page;
    }

    public function setRealEstateLocationTypePage(?RealEstateLocationTypePage $real_estate_location_type_page): self
    {
        $this->real_estate_location_type_page = $real_estate_location_type_page;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
