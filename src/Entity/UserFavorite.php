<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Repository\UserFavoriteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserFavoriteRepository::class)
 */
class UserFavorite
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userFavorites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=RealEstate::class, inversedBy="userFavorites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRealEstate(): ?RealEstate
    {
        return $this->real_estate;
    }

    public function setRealEstate(?RealEstate $real_estate): self
    {
        $this->real_estate = $real_estate;

        return $this;
    }
}
