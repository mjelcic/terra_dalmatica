<?php

namespace App\Entity;

use App\Repository\VisitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VisitRepository::class)
 */
class Visit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="visits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $visited_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $visit_ended_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVisitedAt(): ?\DateTimeInterface
    {
        return $this->visited_at;
    }

    public function setVisitedAt(\DateTimeInterface $visited_at): self
    {
        $this->visited_at = $visited_at;

        return $this;
    }

    public function getVisitEndedAt(): ?\DateTimeInterface
    {
        return $this->visit_ended_at;
    }

    public function setVisitEndedAt(?\DateTimeInterface $visit_ended_at): self
    {
        $this->visit_ended_at = $visit_ended_at;

        return $this;
    }
}
