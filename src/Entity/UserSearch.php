<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserSearchRepository")
 */
class UserSearch
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userSearches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $search_string;


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSearchString(): ?string
    {
        return $this->search_string;
    }

    public function setSearchString(string $search_string): self
    {
        $this->search_string = $search_string;

        return $this;
    }
}
