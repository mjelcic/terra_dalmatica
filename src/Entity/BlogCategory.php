<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;
use Symfony\Component\Intl\Locale;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BlogCategoryRepository")
 */
class BlogCategory
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CmsPage", mappedBy="category")
     */
    private $cmsPages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BlogCategoryLocalization", mappedBy="blogCategory", orphanRemoval=true, cascade={"all"})
     */
    private $blogCategoryLocalizations;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->cmsPages = new ArrayCollection();
        $this->blogCategoryLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addBlogCategoryLocalization($localization);
            }
        }
    }


    /**
     * @return Collection|CmsPage[]
     */
    public function getCmsPages(): Collection
    {
        return $this->cmsPages;
    }

    public function addCmsPage(CmsPage $cmsPage): self
    {
        if (!$this->cmsPages->contains($cmsPage)) {
            $this->cmsPages[] = $cmsPage;
            $cmsPage->setCategory($this);
        }

        return $this;
    }

    public function removeCmsPage(CmsPage $cmsPage): self
    {
        if ($this->cmsPages->contains($cmsPage)) {
            $this->cmsPages->removeElement($cmsPage);
            // set the owning side to null (unless already changed)
            if ($cmsPage->getCategory() === $this) {
                $cmsPage->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogCategoryLocalization[]
     */
    public function getBlogCategoryLocalizations(): Collection
    {
        return $this->blogCategoryLocalizations;
    }


    public function addBlogCategoryLocalization(BlogCategoryLocalization $blogCategoryLocalization): self
    {
        if (!$this->blogCategoryLocalizations->contains($blogCategoryLocalization)) {
            $this->blogCategoryLocalizations[] = $blogCategoryLocalization;
            $blogCategoryLocalization->setBlogCategory($this);
        }

        return $this;
    }

    public function removeBlogCategoryLocalization(BlogCategoryLocalization $blogCategoryLocalization): self
    {
        if ($this->blogCategoryLocalizations->contains($blogCategoryLocalization)) {
            $this->blogCategoryLocalizations->removeElement($blogCategoryLocalization);
            // set the owning side to null (unless already changed)
            if ($blogCategoryLocalization->getBlogCategory() === $this) {
                $blogCategoryLocalization->setBlogCategory(null);
            }
        }

        return $this;
    }
}
