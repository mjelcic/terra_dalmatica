<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Repository\LocalLifeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocalLifeRepository::class)
 */
class LocalLife
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity=Location::class, inversedBy="localLives")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $html;

    function __construct()
    {
        $this->baseConstruct();
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function setHtml(?string $html): self
    {
        $this->html = $html;

        return $this;
    }
}
