<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateCustomPageFiltersRepository")
 */
class RealEstateCustomPageFilters
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RealEstateCustomPage", inversedBy="realEstateCustomPageFilters", cascade={"persist", "remove"})
     */
    private $real_estate_custom_page;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RealEstateType")
     */
    private $real_estate_types;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location")
     */
    private $locations;

    public function __construct()
    {
        $this->baseConstruct();
        $this->real_estate_types = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }

    public function getRealEstateCustomPage(): ?RealEstateCustomPage
    {
        return $this->real_estate_custom_page;
    }

    public function setRealEstateCustomPage(?RealEstateCustomPage $real_estate_custom_page): self
    {
        $this->real_estate_custom_page = $real_estate_custom_page;

        return $this;
    }

    /**
     * @return Collection|RealEstateType[]
     */
    public function getRealEstateTypes(): Collection
    {
        return $this->real_estate_types;
    }

    public function setRealEstateTypes(Collection $realEstateTypes): self
    {
        $this->real_estate_types = $realEstateTypes;

        return $this;
    }

    public function addRealEstateType(RealEstateType $realEstateType): self
    {
        if (!$this->real_estate_types->contains($realEstateType)) {
            $this->real_estate_types[] = $realEstateType;
        }

        return $this;
    }

    public function removeRealEstateType(RealEstateType $realEstateType): self
    {
        if ($this->real_estate_types->contains($realEstateType)) {
            $this->real_estate_types->removeElement($realEstateType);
        }

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function setLocations(Collection $locations): self
    {
        $this->locations = $locations;

        return $this;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
        }

        return $this;
    }
}
