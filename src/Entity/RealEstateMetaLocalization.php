<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateMetaLocalizationRepository")
 */
class RealEstateMetaLocalization
{

    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate", inversedBy="realEstateMetaLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $RealEstate;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateMetaLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    public function getRealEstate(): ?RealEstate
    {
        return $this->RealEstate;
    }

    public function setRealEstate(?RealEstate $RealEstate): self
    {
        $this->RealEstate = $RealEstate;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
