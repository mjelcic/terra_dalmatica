<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/10/2020
 * Time: 1:50 PM
 */

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatusRepository")
 */
class Status
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $displayId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StatusLocalization", mappedBy="status", cascade={"all"})
     */
    private $statusLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstate", mappedBy="status", orphanRemoval=true)
     */
    private $realEstates;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->statusLocalizations = new ArrayCollection();
        $this->realEstates = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addStatusLocalization($localization);
            }
        }
    }

    /**
     * @return Collection|StatusLocalization[]
     */
    public function getStatusLocalizations(): Collection
    {
        return $this->statusLocalizations;
    }

    public function addStatusLocalization(StatusLocalization $statusLocalization): self
    {
        if (!$this->statusLocalizations->contains($statusLocalization)) {
            $this->statusLocalizations[] = $statusLocalization;
            $statusLocalization->setStatus($this);
        }

        return $this;
    }

    public function removeStatusLocalization(StatusLocalization $statusLocalization): self
    {
        if ($this->statusLocalizations->contains($statusLocalization)) {
            $this->statusLocalizations->removeElement($statusLocalization);
            // set the owning side to null (unless already changed)
            if ($statusLocalization->getStatus() === $this) {
                $statusLocalization->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RealEstate[]
     */
    public function getRealEstates(): Collection
    {
        return $this->realEstates;
    }

    public function addRealEstate(RealEstate $realEstate): self
    {
        if (!$this->realEstates->contains($realEstate)) {
            $this->realEstates[] = $realEstate;
            $realEstate->setStatus($this);
        }

        return $this;
    }

    public function removeRealEstate(RealEstate $realEstate): self
    {
        if ($this->realEstates->contains($realEstate)) {
            $this->realEstates->removeElement($realEstate);
            // set the owning side to null (unless already changed)
            if ($realEstate->getStatus() === $this) {
                $realEstate->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayId()
    {
        return $this->displayId;
    }

    /**
     * @param mixed $displayId
     */
    public function setDisplayId($displayId): void
    {
        $this->displayId = $displayId;
    }


}