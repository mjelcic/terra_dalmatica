<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CmsPageRepository")
 * @Vich\Uploadable
 */
class CmsPage
{
   use BaseTrait {
       BaseTrait::__construct as baseConstruct;
   }

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $display_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $page_type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CmsPageLocalization", mappedBy="cms_page", cascade={"all"})
     */
    private $cmsPageLocalizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CmsPageMetaLocalization", mappedBy="cmsPage", cascade={"all"})
     */
    private $cmsPageMetaLocalizations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\BlogCategory")
     */
    private $category;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $slider;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Photo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $cover_photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $locked_for_employee;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $only_meta;

    public function __construct($args)
    {
        $this->baseConstruct();
        $this->cmsPageLocalizations = new ArrayCollection();
        if($args !== null && is_array($args) && array_key_exists("localizations", $args)){
            foreach ($args["localizations"] as $localization){
                $this->addCmsPageLocalization($localization);
            }
        }
        $this->cmsPageMetaLocalizations = new ArrayCollection();
        $this->category = new ArrayCollection();
    }

    public function getDisplayId(): ?string
    {
        return $this->display_id;
    }

    public function setDisplayId(string $display_id): self
    {
        $this->display_id = $display_id;

        return $this;
    }

    public function getPageType(): ?int
    {
        return $this->page_type;
    }

    public function setPageType(int $page_type): self
    {
        $this->page_type = $page_type;

        return $this;
    }

    /**
     * @return Collection|CmsPageLocalization[]
     */
    public function getCmsPageLocalizations(): Collection
    {
        return $this->cmsPageLocalizations;
    }

    public function getCmsPageLocalization($language): ?CmsPageLocalization
    {
        try
        {
            $localization = $this->getCmsPageLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function addCmsPageLocalization(CmsPageLocalization $cmsPageLocalization): self
    {
        if (!$this->cmsPageLocalizations->contains($cmsPageLocalization)) {
            $this->cmsPageLocalizations[] = $cmsPageLocalization;
            $cmsPageLocalization->setCmsPage($this);
        }

        return $this;
    }

    public function removeCmsPageLocalization(CmsPageLocalization $cmsPageLocalization): self
    {
        if ($this->cmsPageLocalizations->contains($cmsPageLocalization)) {
            $this->cmsPageLocalizations->removeElement($cmsPageLocalization);
            // set the owning side to null (unless already changed)
            if ($cmsPageLocalization->getCmsPage() === $this) {
                $cmsPageLocalization->setCmsPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CmsPageMetaLocalization[]
     */
    public function getCmsPageMetaLocalizations(): Collection
    {
        return $this->cmsPageMetaLocalizations;
    }

    public function addCmsPageMetaLocalization(CmsPageMetaLocalization $cmsPageMetaLocalization): self
    {
        if (!$this->cmsPageMetaLocalizations->contains($cmsPageMetaLocalization)) {
            $this->cmsPageMetaLocalizations[] = $cmsPageMetaLocalization;
            $cmsPageMetaLocalization->setCmsPage($this);
        }

        return $this;
    }

    public function removeCmsPageMetaLocalization(CmsPageMetaLocalization $cmsPageMetaLocalization): self
    {
        if ($this->cmsPageMetaLocalizations->contains($cmsPageMetaLocalization)) {
            $this->cmsPageMetaLocalizations->removeElement($cmsPageMetaLocalization);
            // set the owning side to null (unless already changed)
            if ($cmsPageMetaLocalization->getCmsPage() === $this) {
                $cmsPageMetaLocalization->setCmsPage(null);
            }
        }

        return $this;
    }

     /**
     * @return Collection|BlogCategory[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(BlogCategory $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(BlogCategory $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function getSlider(): ?bool
    {
        return $this->slider;
    }

    public function setSlider(?bool $slider): self
    {
        $this->slider = $slider;

        return $this;
    }

    public function getCoverPhoto(): ?Photo
    {
        return $this->cover_photo;
    }

    public function setCoverPhoto(?Photo $cover_photo): self
    {
        $this->cover_photo = $cover_photo;

        return $this;
    }

    public function getLockedForEmployee(): ?bool
    {
        return $this->locked_for_employee;
    }

    public function setLockedForEmployee(bool $locked_for_employee): self
    {
        $this->locked = $locked_for_employee;

        return $this;
    }

    public function getOnlyMeta(): ?bool
    {
        return $this->only_meta;
    }

    public function setOnlyMeta(bool $only_meta): self
    {
        $this->only_meta = $only_meta;

        return $this;
    }

}
