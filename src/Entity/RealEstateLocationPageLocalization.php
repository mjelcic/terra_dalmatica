<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateLocationPageLocalizationRepository")
 */
class RealEstateLocationPageLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateLocationPage", inversedBy="realEstateLocationPageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_location_page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateLocationPageLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photoAltTag;

    public function getRealEstateLocationPage(): ?RealEstateLocationPage
    {
        return $this->real_estate_location_page;
    }

    public function setRealEstateLocationPage(?RealEstateLocationPage $real_estate_location_page): self
    {
        $this->real_estate_location_page = $real_estate_location_page;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPhotoAltTag(): ?string
    {
        return $this->photoAltTag;
    }

    public function setPhotoAltTag(?string $photoAltTag): self
    {
        $this->photoAltTag = $photoAltTag;

        return $this;
    }
}
