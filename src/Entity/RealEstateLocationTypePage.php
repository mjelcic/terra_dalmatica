<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateLocationTypePageRepository")
 */
class RealEstateLocationTypePage
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="realEstateLocationTypePages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstateType", inversedBy="realEstateLocationTypePages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate_type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateLocationTypePageLocalization", mappedBy="real_estate_location_type_page", cascade={"all"})
     */
    private $realEstateLocationTypePageLocalizations;

    public function __construct()
    {
        $this->baseConstruct();
        $this->realEstateLocationTypePageLocalizations = new ArrayCollection();
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getRealEstateType(): ?RealEstateType
    {
        return $this->real_estate_type;
    }

    public function setRealEstateType(?RealEstateType $real_estate_type): self
    {
        $this->real_estate_type = $real_estate_type;

        return $this;
    }

    /**
     * @return Collection|RealEstateLocationTypePageLocalization[]
     */
    public function getRealEstateLocationTypePageLocalizations(): Collection
    {
        return $this->realEstateLocationTypePageLocalizations;
    }

    public function addRealEstateLocationTypePageLocalization(RealEstateLocationTypePageLocalization $realEstateLocationTypePageLocalization): self
    {
        if (!$this->realEstateLocationTypePageLocalizations->contains($realEstateLocationTypePageLocalization)) {
            $this->realEstateLocationTypePageLocalizations[] = $realEstateLocationTypePageLocalization;
            $realEstateLocationTypePageLocalization->setRealEstateLocationTypePage($this);
        }

        return $this;
    }

    public function removeRealEstateLocationTypePageLocalization(RealEstateLocationTypePageLocalization $realEstateLocationTypePageLocalization): self
    {
        if ($this->realEstateLocationTypePageLocalizations->contains($realEstateLocationTypePageLocalization)) {
            $this->realEstateLocationTypePageLocalizations->removeElement($realEstateLocationTypePageLocalization);
            // set the owning side to null (unless already changed)
            if ($realEstateLocationTypePageLocalization->getRealEstateLocationTypePage() === $this) {
                $realEstateLocationTypePageLocalization->setRealEstateLocationTypePage(null);
            }
        }

        return $this;
    }
}
