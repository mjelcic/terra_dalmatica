<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateFeatureRepository")
 */
class RealEstateFeature
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate", inversedBy="realEstateFeatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $real_estate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Feature", inversedBy="realEstateFeatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $feature;


    public function getRealEstate(): ?RealEstate
    {
        return $this->real_estate;
    }

    public function setRealEstate(?RealEstate $real_estate): self
    {
        $this->real_estate = $real_estate;

        return $this;
    }

    public function getFeature(): ?Feature
    {
        return $this->feature;
    }

    public function setFeature(?Feature $feature): self
    {
        $this->feature = $feature;

        return $this;
    }
}
