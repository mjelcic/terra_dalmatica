<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateLocalizationRepository")
 * @ORM\Table(name="real_estate_localization", indexes={@ORM\Index(name="idx_title_description_synonyms", columns={"title", "description", "synonyms", "features_map"}, flags={"fulltext"})})
 */
class RealEstateLocalization
{
    use BaseTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate", inversedBy="realEstateLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $RealEstate;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="realEstateLocalizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $synonyms;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $features_map;

    public function getRealEstate(): ?RealEstate
    {
        return $this->RealEstate;
    }

    public function setRealEstate(?RealEstate $RealEstate): self
    {
        $this->RealEstate = $RealEstate;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getSynonyms(): ?string
    {
        return $this->synonyms;
    }

    public function setSynonyms(?string $synonyms): self
    {
        $this->synonyms = $synonyms;

        return $this;
    }

    public function getFeaturesMap(): ?string
    {
        return $this->features_map;
    }

    public function setFeaturesMap(?string $features_map): self
    {
        $this->features_map = $features_map;

        return $this;
    }
}
