<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealEstateImageRepository")
 */
class RealEstateImage
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @ORM\Column(type="string")
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RealEstate", inversedBy="realEstatePhotos", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $realEstate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstateImageMetaLocalization", mappedBy="realEstateImage", orphanRemoval=true, cascade={"all"})
     */
    private $realEstateImageMetaLocalizations;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $cover;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $uuid;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $originalFileName;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $immoAttachmentId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $immoExported;


    public function __construct()
    {
        $this->baseConstruct();
        $this->realEstateImageMetaLocalizations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    public function getRealEstate(): ?RealEstate
    {
        return $this->realEstate;
    }

    public function setRealEstate(?RealEstate $realEstate): self
    {
        $this->realEstate = $realEstate;

        return $this;
    }

    public function addMetaLocalization(RealEstateImageMetaLocalization $metaLocalization): self
    {
        if (!$this->realEstateImageMetaLocalizations->contains($metaLocalization)) {
            $this->realEstateImageMetaLocalizations[] = $metaLocalization;
            $metaLocalization->setRealEstateImage($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isCover(): bool
    {
        return $this->cover;
    }

    /**
     * @param bool $cover
     */
    public function setCover(bool $cover): void
    {
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getOriginalFileName(): string
    {
        return $this->originalFileName;
    }

    /**
     * @param string $originalFileName
     */
    public function setOriginalFileName(string $originalFileName): void
    {
        $this->originalFileName = $originalFileName;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return Collection|RealEstateImageMetaLocalization[]
     */
    public function getRealEstateImageMetaLocalizations(): Collection
    {
        return $this->realEstateImageMetaLocalizations;
    }

    public function getRealEstateImageMetaLocalization($language): ?RealEstateImageMetaLocalization
    {
        try
        {
            $localization = $this->getRealEstateImageMetaLocalizations()->filter(function($loco) use ($language) {
                return is_object($language) ? $loco->getLanguage() == $language : $loco->getLanguage()->getCode() == $language;
            });

            if($localization->count())
            {
                return $localization->first();
            }

            return null;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function getImmoAttachmentId(): ?string
    {
        return $this->immoAttachmentId;
    }

    public function setImmoAttachmentId(?string $immoAttachmentId): self
    {
        $this->immoAttachmentId = $immoAttachmentId;

        return $this;
    }

    public function getImmoExported(): ?bool
    {
        return $this->immoExported;
    }

    public function setImmoExported(?bool $immoExported): self
    {
        $this->immoExported = $immoExported;

        return $this;
    }

}
