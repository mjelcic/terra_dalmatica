<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AcmeAssert;
use App\Entity\Traits\BaseTrait;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{

    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    public $username;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(min="8", minMessage="Lozinka mora sadržavati najmanje {{ limit }} karaktera!")
     * @AcmeAssert\AllowedPassword
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone_no;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $mobile_phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fax_number;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      minMessage = "OIB mora imati točno 11 znakova",
     *      maxMessage = "OIB mora imati točno 11 znakova"
     * )
     */
    private $personal_identification_number;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $activation_code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSearch", mappedBy="user", orphanRemoval=true)
     */
    private $userSearches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstate", mappedBy="agent")
     */
    private $realEstates;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AgentPage", mappedBy="user", cascade={"persist", "remove"})
     */
    private $agentPage;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="for_user")
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RealEstate", mappedBy="client")
     */
    private $clientRealEstates;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\UserFavorite::class, mappedBy="user", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $userFavorites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserContact", mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $userContacts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserPreference", mappedBy="user", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $userPreference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=true)
     */
    private $emailLanguage;

    /**
     * @ORM\OneToMany(targetEntity=Visit::class, mappedBy="user")
     */
    private $visits;

    /**
     * @ORM\ManyToMany(targetEntity=MarketingEmail::class, mappedBy="sentTo")
     */
    private $marketingEmails;


    public function __construct()
    {
        $this->baseConstruct();
        $this->isActive = true;
        $this->userSearches = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->clientRealEstates = new ArrayCollection();
        $this->userFavorites = new ArrayCollection();
        $this->userContacts = new ArrayCollection();
        $this->visits = new ArrayCollection();
        $this->marketingEmails = new ArrayCollection();

    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setRoles($roles)
    {
        $this->roles = implode(";", $roles);

        return $this;
    }

    public function getRoles()
    {
        return explode(";", $this->roles);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
                "fields" => "email",
                "message" => "Ovaj email se već koristi!")
        ));
        $metadata->addConstraint(new UniqueEntity(array(
                "fields" => "username",
                "message" => "Ovo korisničko ime se već koristi!")
        ));
    }


    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getPhoneNo(): ?string
    {
        return $this->phone_no;
    }

    public function setPhoneNo(string $phone_no): self
    {
        $this->phone_no = $phone_no;

        return $this;
    }

    public function getPersonalIdentificationNumber(): ?string
    {
        return $this->personal_identification_number;
    }

    public function setPersonalIdentificationNumber(?string $personal_identification_number): self
    {
        $this->personal_identification_number = $personal_identification_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobile_phone_number;
    }

    /**
     * @return mixed
     */
    public function getFaxNumber()
    {
        return $this->fax_number;
    }

    /**
     * @return mixed
     */
    public function getRealEstates()
    {
        return $this->realEstates;
    }


    public function getCompanyName(): ?string
    {
        return $this->company_name;
    }

    public function setCompanyName(string $company_name): self
    {
        $this->company_name = $company_name;

        return $this;
    }

    public function getActivationCode(): ?string
    {
        return $this->activation_code;
    }

    public function setActivationCode(string $activation_code): self
    {
        $this->activation_code = $activation_code;

        return $this;
    }

    /**
     * @return Collection|UserSearch[]
     */
    public function getUserSearches(): Collection
    {
        return $this->userSearches;
    }

    public function addUserSearch(UserSearch $userSearch): self
    {
        if (!$this->userSearches->contains($userSearch)) {
            $this->userSearches[] = $userSearch;
            $userSearch->setUser($this);
        }

        return $this;
    }

    public function removeUserSearch(UserSearch $userSearch): self
    {
        if ($this->userSearches->contains($userSearch)) {
            $this->userSearches->removeElement($userSearch);
            // set the owning side to null (unless already changed)
            if ($userSearch->getUser() === $this) {
                $userSearch->setUser(null);
            }
        }

        return $this;
    }

    public function getAgentPage(): ?AgentPage
    {
        return $this->agentPage;
    }

    public function setAgentPage(AgentPage $agentPage): self
    {
        $this->agentPage = $agentPage;

        // set the owning side of the relation if necessary
        if ($agentPage->getUser() !== $this) {
            $agentPage->setUser($this);
        }
        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setForUser($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getForUser() === $this) {
                $notification->setForUser(null);
            }
        }

        return $this;
    }

    /**
     * @param mixed $mobile_phone_number
     */
    public function setMobilePhoneNumber($mobile_phone_number): void
    {
        $this->mobile_phone_number = $mobile_phone_number;
    }

    /**
     * @param mixed $fax_number
     */
    public function setFaxNumber($fax_number): void
    {
        $this->fax_number = $fax_number;
    }

    /**
     * @return Collection|RealEstate[]
     */
    public function getClientRealEstates(): Collection
    {
        return $this->clientRealEstates;
    }

    public function addClientRealEstate(RealEstate $clientRealEstate): self
    {
        if (!$this->clientRealEstates->contains($clientRealEstate)) {
            $this->clientRealEstates[] = $clientRealEstate;
            $clientRealEstate->setClient($this);
        }

        return $this;
    }

    public function removeClientRealEstate(RealEstate $clientRealEstate): self
    {
        if ($this->clientRealEstates->contains($clientRealEstate)) {
            $this->clientRealEstates->removeElement($clientRealEstate);
            // set the owning side to null (unless already changed)
            if ($clientRealEstate->getClient() === $this) {
                $clientRealEstate->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserFavorite[]
     */
    public function getUserFavorites(): Collection
    {
        return $this->userFavorites;
    }

    public function addUserFavorite(UserFavorite $userFavorite): self
    {
        if (!$this->userFavorites->contains($userFavorite)) {
            $this->userFavorites[] = $userFavorite;
            $userFavorite->setUser($this);
        }

        return $this;
    }

    public function removeUserFavorite(UserFavorite $userFavorite): self
    {
        if ($this->userFavorites->contains($userFavorite)) {
            $this->userFavorites->removeElement($userFavorite);
            // set the owning side to null (unless already changed)
            if ($userFavorite->getUser() === $this) {
                $userFavorite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserContact[]
     */
    public function getUserContacts(): Collection
    {
        return $this->userContacts;
    }

    public function addUserContact(UserContact $userContact): self
    {
        if (!$this->userContacts->contains($userContact)) {
            $this->userContacts[] = $userContact;
            $userContact->setUser($this);
        }

        return $this;
    }

    public function removeUserContact(UserContact $userContact): self
    {
        if ($this->userContacts->contains($userContact)) {
            $this->userContacts->removeElement($userContact);
            // set the owning side to null (unless already changed)
            if ($userContact->getUser() === $this) {
                $userContact->setUser(null);
            }
        }

        return $this;

    }

    public function getEmailLanguage(): ?Language
    {
        return $this->emailLanguage;
    }

    public function getEmailLanguageCodeOrDefault(): string
    {
        return $this->getEmailLanguage() ? $this->getEmailLanguage()->getCode() : "hr";
    }

    public function setEmailLanguage(?Language $emailLanguage): self
    {
        $this->emailLanguage = $emailLanguage;

        return $this;
    }

    public function getUserPreference(): ?UserPreference
    {
        return $this->userPreference;
    }

    public function setUserPreference(?UserPreference $userPreference): self
    {
        $this->userPreference = $userPreference;
        return $this;
    }

    /**
     * @return Collection|Visit[]
     */
    public function getVisits(): Collection
    {
        return $this->visits;
    }

    public function addVisit(Visit $visit): self
    {
        if (!$this->visits->contains($visit)) {
            $this->visits[] = $visit;
            $visit->setUser($this);
        }
        return $this;
    }

    /**
     * @return Collection|MarketingEmail[]
     */
    public function getMarketingEmails(): Collection
    {
        return $this->marketingEmails;
    }

    public function addMarketingEmail(MarketingEmail $marketingEmail): self
    {
        if (!$this->marketingEmails->contains($marketingEmail)) {
            $this->marketingEmails[] = $marketingEmail;
            $marketingEmail->addSentTo($this);
        }

        return $this;
    }

    public function removeVisit(Visit $visit): self
    {
        if ($this->visits->contains($visit)) {
            $this->visits->removeElement($visit);
            // set the owning side to null (unless already changed)
            if ($visit->getUser() === $this) {
                $visit->setUser(null);
            }
        }

        return $this;
    }

    public function removeMarketingEmail(MarketingEmail $marketingEmail): self
    {
        if ($this->marketingEmails->contains($marketingEmail)) {
            $this->marketingEmails->removeElement($marketingEmail);
            $marketingEmail->removeSentTo($this);
        }

        return $this;
    }

}
