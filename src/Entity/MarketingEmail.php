<?php

namespace App\Entity;

use App\Entity\Traits\BaseTrait;
use App\Repository\MarketingEmailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MarketingEmailRepository::class)
 */
class MarketingEmail
{
    use BaseTrait {
        BaseTrait::__construct as baseConstruct;
    }

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\NotBlank(message="Polje je obavezno!")
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Polje je obavezno!")
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="marketingEmails")
     */
    private $sentTo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sendToAll;

    public function __construct($args = [])
    {
        $this->baseConstruct();
        $this->sentTo = new ArrayCollection();
        if($args != null && is_array($args))
        {
            if(array_key_exists("template", $args)){
                $template = $args["template"];
                $this->subject = $template->getSubject();
                $this->content = $template->getContent();
                $this->sendToAll = $template->getSendToAll();
                foreach ($template->getSentTo() as $receiver){
                    $this->addSentTo($receiver);
                }
            }
        }
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getSentTo(): Collection
    {
        return $this->sentTo;
    }

    public function addSentTo(User $sentTo): self
    {
        if (!$this->sentTo->contains($sentTo)) {
            $this->sentTo[] = $sentTo;
        }

        return $this;
    }

    public function removeSentTo(User $sentTo): self
    {
        if ($this->sentTo->contains($sentTo)) {
            $this->sentTo->removeElement($sentTo);
        }

        return $this;
    }

    public function getSendToAll(): ?bool
    {
        return $this->sendToAll;
    }

    public function setSendToAll(?bool $sendToAll): self
    {
        $this->sendToAll = $sendToAll;

        return $this;
    }

    /***************************************************************/
    /***********---->>>>>>>> VALIDATION METHODS <<<<<<<<<<--------**/
    /***************************************************************/

    /**
     * @Assert\Callback
     */
    public function validateReceivers(ExecutionContextInterface $context)
    {
        if( $this->sendToAll == false && $this->sentTo->isEmpty() == true )
        {
            $context
                ->buildViolation('Ukoliko niste odabrali opciju "Pošalji svim klijentima" morate odabrati klijente kojima će email biti proslijeđen!')
                ->atPath("sentTo")
                ->addViolation();
        }
    }
}
