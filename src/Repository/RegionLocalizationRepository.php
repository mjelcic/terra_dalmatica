<?php

namespace App\Repository;

use App\Entity\RegionLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RegionLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegionLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegionLocalization[]    findAll()
 * @method RegionLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegionLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegionLocalization::class);
    }

    // /**
    //  * @return RegionLocalization[] Returns an array of RegionLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegionLocalization
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
