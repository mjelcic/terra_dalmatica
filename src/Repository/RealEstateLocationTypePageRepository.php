<?php

namespace App\Repository;

use App\Entity\RealEstateLocationTypePage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateLocationTypePage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateLocationTypePage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateLocationTypePage[]    findAll()
 * @method RealEstateLocationTypePage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateLocationTypePageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateLocationTypePage::class);
    }

    // /**
    //  * @return RealEstateLocationTypePage[] Returns an array of RealEstateLocationTypePage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateLocationTypePage
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
