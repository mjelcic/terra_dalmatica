<?php

namespace App\Repository;

use App\Entity\RealEstateLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateLocalization[]    findAll()
 * @method RealEstateLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateLocalization::class);
    }

    // /**
    //  * @return RealEstateLocalization[] Returns an array of RealEstateLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateLocalization
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
