<?php

namespace App\Repository;

use App\Entity\RealEstateLocationPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateLocationPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateLocationPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateLocationPage[]    findAll()
 * @method RealEstateLocationPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateLocationPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateLocationPage::class);
    }

    // /**
    //  * @return RealEstateLocationPage[] Returns an array of RealEstateLocationPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateLocationPage
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
