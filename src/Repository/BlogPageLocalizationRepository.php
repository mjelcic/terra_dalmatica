<?php

namespace App\Repository;

use App\Entity\BlogPageLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BlogPageLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPageLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPageLocalization[]    findAll()
 * @method BlogPageLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPageLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogPageLocalization::class);
    }

    // /**
    //  * @return BlogPageLocalization[] Returns an array of BlogPageLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogPageLocalization
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
