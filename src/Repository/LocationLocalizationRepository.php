<?php

namespace App\Repository;

use App\Entity\LocationLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LocationLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocationLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocationLocalization[]    findAll()
 * @method LocationLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocationLocalization::class);
    }
}
