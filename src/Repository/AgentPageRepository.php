<?php

namespace App\Repository;

use App\Entity\AgentPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgentPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgentPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgentPage[]    findAll()
 * @method AgentPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentPageRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgentPage::class);
    }

    // /**
    //  * @return AgentPage[] Returns an array of AgentPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgentPage
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
