<?php

namespace App\Repository;

use App\Entity\CmsPageMetaLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CmsPageMetaLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method CmsPageMetaLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method CmsPageMetaLocalization[]    findAll()
 * @method CmsPageMetaLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CmsPageMetaLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CmsPageMetaLocalization::class);
    }

    // /**
    //  * @return CmsPageMetaLozalization[] Returns an array of CmsPageMetaLozalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CmsPageMetaLozalization
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
