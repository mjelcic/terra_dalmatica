<?php

namespace App\Repository;

use App\Entity\RealEstateFeature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateFeature|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateFeature|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateFeature[]    findAll()
 * @method RealEstateFeature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateFeatureRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateFeature::class);
    }
}
