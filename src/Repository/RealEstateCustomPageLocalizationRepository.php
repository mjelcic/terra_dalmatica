<?php

namespace App\Repository;

use App\Entity\RealEstateCustomPageLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateCustomPageLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateCustomPageLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateCustomPageLocalization[]    findAll()
 * @method RealEstateCustomPageLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateCustomPageLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateCustomPageLocalization::class);
    }
}
