<?php

namespace App\Repository;

use App\Entity\CmsPageLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CmsPageLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method CmsPageLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method CmsPageLocalization[]    findAll()
 * @method CmsPageLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CmsPageLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CmsPageLocalization::class);
    }

    // /**
    //  * @return CmsPageLocalization[] Returns an array of CmsPageLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CmsPageLocalization
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
