<?php

namespace App\Repository;

use App\Entity\CallButton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CallButton|null find($id, $lockMode = null, $lockVersion = null)
 * @method CallButton|null findOneBy(array $criteria, array $orderBy = null)
 * @method CallButton[]    findAll()
 * @method CallButton[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CallButtonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CallButton::class);
    }

    // /**
    //  * @return CallButton[] Returns an array of CallButton objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CallButton
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
