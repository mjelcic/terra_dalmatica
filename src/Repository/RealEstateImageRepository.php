<?php

namespace App\Repository;

use App\Entity\RealEstateImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateImage[]    findAll()
 * @method RealEstateImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateImageRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateImage::class);
    }

    // /**
    //  * @return RealEstatePhoto[] Returns an array of RealEstatePhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstatePhoto
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
