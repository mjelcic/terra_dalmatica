<?php

namespace App\Repository;

use App\Entity\RealEstateCustomPageMetadataLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateCustomPageMetadataLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateCustomPageMetadataLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateCustomPageMetadataLocalization[]    findAll()
 * @method RealEstateCustomPageMetadataLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateCustomPageMetadataLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateCustomPageMetadataLocalization::class);
    }

    // /**
    //  * @return RealEstateCustomPageMetadataLocalization[] Returns an array of RealEstateCustomPageMetadataLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateCustomPageMetadataLocalization
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
