<?php

namespace App\Repository;

use App\Entity\MarketingEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MarketingEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method MarketingEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method MarketingEmail[]    findAll()
 * @method MarketingEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketingEmailRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarketingEmail::class);
    }
}
