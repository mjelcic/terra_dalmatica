<?php

namespace App\Repository;

use App\Entity\GroundPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GroundPlan|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroundPlan|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroundPlan[]    findAll()
 * @method GroundPlan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroundPlanRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroundPlan::class);
    }
}
