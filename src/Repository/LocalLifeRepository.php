<?php

namespace App\Repository;

use App\Entity\LocalLife;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LocalLife|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocalLife|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocalLife[]    findAll()
 * @method LocalLife[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalLifeRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocalLife::class);
    }
}
