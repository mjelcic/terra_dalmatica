<?php

namespace App\Repository;

use App\Entity\RealEstateImage;
use App\Entity\RealEstateImageMetaLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateImageMetaLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateImageMetaLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateImageMetaLocalization[]    findAll()
 * @method RealEstateImageMetaLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateImageMetaLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateImage::class);
    }

    // /**
    //  * @return RealEstatePhoto[] Returns an array of RealEstatePhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstatePhoto
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
