<?php

namespace App\Repository;

use App\Entity\PhotoMetaLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PhotoMetaLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhotoMetaLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhotoMetaLocalization[]    findAll()
 * @method PhotoMetaLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoMetaLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhotoMetaLocalization::class);
    }

    // /**
    //  * @return PhotoMetaLocalization[] Returns an array of PhotoMetaLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhotoMetaLocalization
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
