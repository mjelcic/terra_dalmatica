<?php

namespace App\Repository;

use App\Entity\RealEstateCustomPage;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateCustomPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateCustomPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateCustomPage[]    findAll()
 * @method RealEstateCustomPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateCustomPageRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateCustomPage::class);
    }
}
