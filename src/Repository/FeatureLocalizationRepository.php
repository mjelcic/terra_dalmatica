<?php

namespace App\Repository;

use App\Entity\FeatureLocalization;
use App\Services\AbstractBaseService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FeatureLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeatureLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeatureLocalization[]    findAll()
 * @method FeatureLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeatureLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeatureLocalization::class);
    }

    // /**
    //  * @return FeatureLocalization[] Returns an array of FeatureLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FeatureLocalization
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
