<?php

namespace App\Repository;

use App\Entity\RealEstateCustomPageFilters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateCustomPageFilters|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateCustomPageFilters|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateCustomPageFilters[]    findAll()
 * @method RealEstateCustomPageFilters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateCustomPageFiltersRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateCustomPageFilters::class);
    }

    // /**
    //  * @return RealEstateCustomPageFilters[] Returns an array of RealEstateCustomPageFilters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateCustomPageFilters
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
