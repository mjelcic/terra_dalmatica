<?php

namespace App\Repository;

use App\Entity\BlogCategoryLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BlogCategoryLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogCategoryLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogCategoryLocalization[]    findAll()
 * @method BlogCategoryLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogCategoryLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogCategoryLocalization::class);
    }

    // /**
    //  * @return BlogCategoryLocalization[] Returns an array of BlogCategoryLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogCategoryLocalization
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
