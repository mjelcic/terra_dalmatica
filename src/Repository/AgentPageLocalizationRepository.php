<?php

namespace App\Repository;

use App\Entity\AgentPageLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgentPageLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgentPageLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgentPageLocalization[]    findAll()
 * @method AgentPageLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentPageLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgentPageLocalization::class);
    }

    // /**
    //  * @return AgentLocalization[] Returns an array of AgentLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgentLocalization
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
