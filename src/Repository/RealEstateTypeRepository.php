<?php

namespace App\Repository;

use App\Entity\RealEstateType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateType[]    findAll()
 * @method RealEstateType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateTypeRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateType::class);
    }

    // /**
    //  * @return RealEstateType[] Returns an array of RealEstateType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
