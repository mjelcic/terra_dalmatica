<?php

namespace App\Repository;

use App\Entity\RealEstateLocationTypePageLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateLocationTypePageLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateLocationTypePageLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateLocationTypePageLocalization[]    findAll()
 * @method RealEstateLocationTypePageLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateLocationTypePageLocalizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateLocationTypePageLocalization::class);
    }

    // /**
    //  * @return RealEstateLocationTypePageLocalization[] Returns an array of RealEstateLocationTypePageLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateLocationTypePageLocalization
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
