<?php

namespace App\Repository;

use App\Entity\ZooplaListing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ZooplaListing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZooplaListing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZooplaListing[]    findAll()
 * @method ZooplaListing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZooplaListingRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZooplaListing::class);
    }

    // /**
    //  * @return ZooplaListing[] Returns an array of ZooplaListing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ZooplaListing
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
