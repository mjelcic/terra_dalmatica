<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 1/28/2020
 * Time: 9:59 AM
 */

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

abstract class AbstractBaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, $entityFullClassName)
    {
        parent::__construct($registry, $entityFullClassName);
    }

    public function createBasicQueryBuilder(string $queryBuilderAlias, bool $filterInvalidatedRecords = true)
    {
        $qb = $this->createQueryBuilder($queryBuilderAlias);

        if(true === $filterInvalidatedRecords)
        {
            $qb->where($queryBuilderAlias . ".invalidated = 0");
        }

        return $qb;
    }
}