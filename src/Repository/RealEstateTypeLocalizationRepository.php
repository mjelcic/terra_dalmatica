<?php

namespace App\Repository;

use App\Entity\RealEstateTypeLocalization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RealEstateTypeLocalization|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateTypeLocalization|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateTypeLocalization[]    findAll()
 * @method RealEstateTypeLocalization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateTypeLocalizationRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateTypeLocalization::class);
    }

    // /**
    //  * @return RealEstateTypeLocalization[] Returns an array of RealEstateTypeLocalization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstateTypeLocalization
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
