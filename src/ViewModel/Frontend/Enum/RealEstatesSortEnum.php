<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/24/2020
 * Time: 8:26 AM
 */

namespace App\ViewModel\Frontend\Enum;


class RealEstatesSortEnum
{
    const priceLowest = "real.estate.price.asc";
    const priceHighest = "real.estate.price.desc";
    const dateLowest = "real.estate.date.asc";
    const dateHighest = "real.estate.date.desc";
    const realEstateType = "real.estate.type";
}