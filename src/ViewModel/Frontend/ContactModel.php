<?php

namespace App\ViewModel\Frontend;

class ContactModel
{
    public $name;
    public $email;
    public $email_to;
    public $phone_no;
    public $message;
    public $date;
    public $real_estate_link;

}