<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/11/2020
 * Time: 12:03 PM
 */

namespace App\ViewModel\Frontend\Traits;


trait LocationCategoryPagesTrait
{
    private $locationCategoryPages;

    public function getLocationCategoryPages(): array
    {
        return $this->locationCategoryPages;
    }

    public function setLocationCategoryPages(array $locationCategoryPages): self
    {
        $this->locationCategoryPages = $locationCategoryPages;
        return $this;
    }
}