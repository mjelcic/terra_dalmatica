<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/25/2020
 * Time: 3:45 PM
 */

namespace App\ViewModel\Frontend\Traits;


trait CustomCategoryPagesTrait
{
    private $customCategoryPages;

    public function getCustomCategoryPages(): array
    {
        return $this->customCategoryPages;
    }

    public function setCustomCategoryPages(array $customCategoryPages): self
    {
        $this->customCategoryPages = $customCategoryPages;
        return $this;
    }
}