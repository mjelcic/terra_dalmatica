<?php
namespace App\ViewModel\Frontend;

use App\Entity\RealEstateCustomPage;
use App\ViewModel\Frontend\Traits\LocationCategoryPagesTrait;


class CategoryPageViewModel
{
    use LocationCategoryPagesTrait;

    private $categoryPage;
    private $geoLocations;
    private $realEstates;
    private $sortUrlPrefix;
    private $selectedSort;
    private $localLife;

    function __construct(RealEstateCustomPage $categoryPage = null, string $geoLocations = "", array $realEstates = [], array $locationCategoryPages = [],
                         $sortUrlPrefix = "", $selectedSort = "", $localLife = "")
    {
        $this->categoryPage = $categoryPage;
        $this->geoLocations = $geoLocations;
        $this->realEstates = $realEstates;
        $this->locationCategoryPages = $locationCategoryPages;
        $this->sortUrlPrefix = $sortUrlPrefix;
        $this->selectedSort = $selectedSort;
        $this->localLife = $localLife;
    }

    public function getCategoryPage(): RealEstateCustomPage
    {
        return $this->categoryPage;
    }

    public function setCategoryPage(RealEstateCustomPage $categoryPage): self
    {
        $this->categoryPage = $categoryPage;
        return $this;
    }

    public function getGeoLocations(): string
    {
        return $this->geoLocations;
    }

    public function setGeoLocations(string $geoLocations): self
    {
        $this->geoLocations = $geoLocations;
        return $this;
    }

    public function getRealEstates(): array
    {
        return $this->realEstates;
    }

    public function setRealEstates(array $realEstates): self
    {
        $this->realEstates = $realEstates;
        return $this;
    }

    public function getSortUrlPrefix(): string
    {
        return $this->sortUrlPrefix;
    }

    public function setSortUrlPrefix(array $sortUrlPrefix): self
    {
        $this->realEstates = $sortUrlPrefix;
        return $this;
    }

    public function getSelectedSort(): string
    {
        return $this->selectedSort;
    }

    public function setSelectedSort(array $selectedSort): self
    {
        $this->selectedSort = $selectedSort;
        return $this;
    }

    public function getLocalLife(): ?string
    {
        return $this->localLife;
    }

    public function setLocalLife(?string $localLife): self
    {
        $this->localLife = $localLife;
        return $this;
    }
}