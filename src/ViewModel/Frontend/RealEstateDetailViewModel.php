<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2/18/2020
 * Time: 9:52 PM
 */

namespace App\ViewModel\Frontend;


use App\ViewModel\Frontend\Traits\LocationCategoryPagesTrait;

class RealEstateDetailViewModel
{
    use LocationCategoryPagesTrait;

    public $id;
    public $title;
    public $slug;
    public $description;
    public $location;
    public $type;
    public $firstRowByTheSea;
    public $seaView;
    public $bedroomCount;
    public $bathroomCount;
    public $distanceFromTheSea;
    public $energyClass;
    public $garage;
    public $longitude;
    public $latitude;
    public $livingArea;
    public $surfaceArea;
    public $pool;
    public $price;
    public $images;
    public $features;
    public $crossSell;
    public $agent;
    public $exchangeRate;
    public $virtualWalk;
    public $videoUrl;
    public $uid;
    public $localLife;
    public $savedToFavorites;
    public $priceOnRequest;
    public $real_estate_type_id;
    public $ground_plans;
    public $underConstruction;

    function __construct($id, $title, $slug, $description, $location, $type, $firstRowByTheSea, $seaView, $bedroomCount, $bathroomCount, $distanceFromTheSea
                         , $energyClass, $garage, $longitude, $latitude, $livingArea, $surfaceArea, $pool, $price, $exchangeRate = 0, $virtualWalkUrl, $videoUrl, $uid, $priceOnRequest, $real_estate_type_id, $underConstruction)
    {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->location = $location;
        $this->type = $type;
        $this->firstRowByTheSea = $firstRowByTheSea;
        $this->seaView = $seaView;
        $this->bedroomCount = $bedroomCount;
        $this->bathroomCount = $bathroomCount;
        $this->distanceFromTheSea = $distanceFromTheSea;
        $this->energyClass = $energyClass;
        $this->garage = $garage;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->livingArea = $livingArea;
        $this->surfaceArea = $surfaceArea;
        $this->pool = $pool;
        $this->price = $price;
        $this->images = array();
        $this->features = array();
        $this->locationCategoryPages = array();
        $this->crossSell = array();
        $this->agent = null;
        $this->exchangeRate = $exchangeRate;
        $this->virtualWalkUrl = $virtualWalkUrl;
        $this->videoUrl = $videoUrl;
        $this->uid = $uid;
        $this->localLife = "";
        $this->savedToFavorites = false;
        $this->priceOnRequest = $priceOnRequest;
        $this->real_estate_type_id = $real_estate_type_id;
        $this->ground_plans = array();
        $this->underConstruction = $underConstruction;
    }
}