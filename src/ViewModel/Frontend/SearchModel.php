<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 3/3/2020
 * Time: 2:31 PM
 */

namespace App\ViewModel\Frontend;


use App\GlobalSettings;

class SearchModel
{
    public $text;
    public $realEstateType;
    public $location;
    public $priceMin;
    public $priceMax;
    public $bedroomCount;
    public $firstRowByTheSea;
    public $seaView;
    public $newBuilding;
    public $underConstruction;
    public $priceLowered;
    public $pool;
    public $sort;

    public function __construct()
    {
        $this->priceMin = GlobalSettings::SearchbarPriceSlideMinValue;
        $this->priceMax = GlobalSettings::SearchbarPriceSlideMaxValue;
    }

    public function isFiltered() : bool
    {
        $filtered = false;

        if($this->text || $this->realEstateType || $this->location
            || ($this->priceMin != null && $this->priceMin != GlobalSettings::SearchbarPriceSlideMinValue)
            || ($this->priceMax != null && $this->priceMax != GlobalSettings::SearchbarPriceSlideMaxValue)
            || $this->bedroomCount || $this->firstRowByTheSea || $this->seaView || $this->newBuilding || $this->underConstruction
            || $this->priceLowered || $this->pool){
            $filtered = true;
        }

        return $filtered;
    }
}