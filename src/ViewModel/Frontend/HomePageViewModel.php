<?php
/**
 * Created by PhpStorm.
 * User: mijatra
 * Date: 2/11/2020
 * Time: 11:38 AM
 */

namespace App\ViewModel\Frontend;


use App\ViewModel\Frontend\Traits\CustomCategoryPagesTrait;
use App\ViewModel\Frontend\Traits\LocationCategoryPagesTrait;

class HomePageViewModel
{
    use LocationCategoryPagesTrait;
    use CustomCategoryPagesTrait;

    private $topSubcategoryPages;
    private $recentRealEstates;
    private $featuredRealEstates;
    private $suggestedRealEstates;

    function __construct($topSubcategoryPages = [], $locationCategoryPages = [], $customCategoryPages = [], $recentRealEstates = [], $featuredRealEstates = [], $suggestedRealEstates = [])
    {
        $this->topSubcategoryPages = $topSubcategoryPages;
        $this->locationCategoryPages = $locationCategoryPages;
        $this->recentRealEstates = $recentRealEstates;
        $this->featuredRealEstates = $featuredRealEstates;
        $this->suggestedRealEstates = $suggestedRealEstates;
        $this->customCategoryPages = $customCategoryPages;
    }

    public function getTopSubcategoryPages(): array
    {
        return $this->topSubcategoryPages;
    }

    public function setTopSubcategoryPages($topSubcategoryPages) : self
    {
        $this->topSubcategoryPages = $topSubcategoryPages;
        return $this;
    }

    public function getRecentRealEstates(): array
    {
        return $this->recentRealEstates;
    }

    public function setRecentRealEstates($recentRealEstates) : self
    {
        $this->recentRealEstates = $recentRealEstates;
        return $this;
    }

    public function getFeaturedRealEstates(): array
    {
        return $this->featuredRealEstates;
    }

    public function setFeaturedRealEstates($featuredRealEstates) : self
    {
        $this->featuredRealEstates = $featuredRealEstates;
        return $this;
    }

    public function getSuggestedRealEstates(): array
    {
        return $this->suggestedRealEstates;
    }

    public function setSuggestedRealEstates($suggestedRealEstates) : self
    {
        $this->suggestedRealEstates = $suggestedRealEstates;
        return $this;
    }
}