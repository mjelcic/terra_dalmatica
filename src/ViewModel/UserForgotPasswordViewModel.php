<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 9/27/2018
 * Time: 12:43 PM
 */

namespace App\ViewModel;


class UserForgotPasswordViewModel
{
    private $username;
    private $email;

    public function getUsername() : ?string
    {
        return $this->username;
    }

    public function setUsername($username) : self
    {
        $this->username = $username;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
}