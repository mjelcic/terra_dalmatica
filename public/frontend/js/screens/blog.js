var is_processing = false;
var last_page = false;


function addMoreElements() {
    $("#loadMoreContainer").hide();
    $("#loader").show();
    is_processing = true;
    nextPage = page + 1;
    $.ajax({
        type: "GET",
        url: "/api/content?page=" + nextPage + additionalParameters,
        success: function (data) {
            $("#loader").hide();
            page = page + 1;
            insertParam("page", page);
            if (data.html.length > 0) {
                $('#postList').append(data.html);
                //The server can answer saying it's the last page so that the browser doesn't make anymore calls
                last_page = data.last_page;
            } else {
                last_page = true;
            }
            is_processing = false;
            if (last_page != true) {
                $("#loadMoreContainer").show();
            }
        },
        error: function (data) {
            $("#loader").hide();
            $("#loadMoreContainer").show();
            is_processing = false;
        }
    });
}

/*
$(window).scroll(function () {
    loadMoreContent();
});
*/


$("#loadMoreButton").click(function () {
    if (last_page === false && is_processing === false) {
        addMoreElements();
    }
});

$("#searchButton").click(function () {
    searchTriggered();
});

$("#searchBox").on('keypress',function(e) {
    if(e.which == 13) {
        searchTriggered();
    }
});

function searchTriggered(){
    $('#postList').html("");
    insertParam("search", $("#searchBox").val());
    page = 0;
    additionalParameters = "&locale=" + locale + "&search=" + $("#searchBox").val();
    addMoreElements($("#search").val());
}

function loadMoreContent() {
    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
    //Modify this parameter to establish how far down do you want to make the ajax call
    var scrolltrigger = 0.99;
    if ((wintop / (docheight - winheight)) > scrolltrigger) {
        //I added the is_processing variable to keep the ajax asynchronous but avoiding making the same call multiple times
        if (last_page === false && is_processing === false) {
            addMoreElements();
        }
    }
}

function insertParam(key, value) {
        let url = new URL(window.document.location);
        let params = new URLSearchParams(url.search.slice(1));

        if (params.has(key)) {
            params.set(key, value);
        }else {
            params.append(key, value);
        }
    history.replaceState({page: 2}, "", "?" + params)
}

$("#showCategories span").click(function () {
    $("#showCategories").addClass("d-none");
    $("#showCategories").removeClass("d-block");

    $("#hideCategories").addClass("d-block");
    $("#hideCategories").removeClass("d-none");

    $("#categories").addClass("d-block");
    $("#categories").removeClass("d-none");
});
$("#hideCategories span").click(function () {
    $("#showCategories").addClass("d-block");
    $("#showCategories").removeClass("d-none");

    $("#hideCategories").addClass("d-none");
    $("#hideCategories").removeClass("d-block");

    $("#categories").addClass("d-none");
    $("#categories").removeClass("d-block");
});