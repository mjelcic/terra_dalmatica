$(document).ready(function () {

    function submitOnPriceChange(){
        submitSearch($("input[type='hidden'][id^='search_price']"));
    }

    function filterAvalilability(validators)
    {
        if(validators.length === 0)
        {
            $("select[name^='search'] option").prop("disabled", false);
            $("input[name^='search'][type='checkbox']").prop("disabled", false);
            $("select[name^='search']").trigger("chosen:updated");
            return false;
        }

        if(validators.firstRowByTheSea !== undefined){
            $("#search_firstRowByTheSea").prop("disabled", !validators.firstRowByTheSea);
        }

        if(validators.seaView !== undefined){
            $("#search_seaView").prop("disabled", !validators.seaView);
        }

        if(validators.newBuilding !== undefined){
            $("#search_newBuilding").prop("disabled", !validators.newBuilding);
        }

        if(validators.underConstruction !== undefined){
            $("#search_underConstruction").prop("disabled", !validators.underConstruction);
        }

        if(validators.pool !== undefined){
            $("#search_pool").prop("disabled", !validators.pool);
        }

        if(validators.priceLowered !== undefined){
            $("#search_priceLowered").prop("disabled", !validators.priceLowered);
        }

        //type
        if(validators.type1 !== undefined){
            $("#search_realEstateType option[value='1']").prop("disabled", !validators.type1);
        }

        if(validators.type2 !== undefined){
            $("#search_realEstateType option[value='2']").prop("disabled", !validators.type2);
        }

        if(validators.type3 !== undefined){
            $("#search_realEstateType option[value='3']").prop("disabled", !validators.type3);
        }

        if(validators.type4 !== undefined){
            $("#search_realEstateType option[value='4']").prop("disabled", !validators.type4);
        }

        $("#search_realEstateType").trigger("chosen:updated");

        //locations
        if(validators.locations !== undefined){
            $("#search_location option:not(:first)").each(function () {
                if(validators.locations.indexOf($(this).val()) > -1){
                    $(this).prop("disabled", false);
                } else {
                    $(this).prop("disabled", true);
                }
            });

            $("#search_location").trigger("chosen:updated");
        }

        //price
        if(validators.price1 !== undefined){
            $("#search_priceSelector option[value='0,100000']").prop("disabled", !validators.price1);
        }

        if(validators.price2 !== undefined){
            $("#search_priceSelector option[value='100000,250000']").prop("disabled", !validators.price2);
        }

        if(validators.price3 !== undefined){
            $("#search_priceSelector option[value='250000,500000']").prop("disabled", !validators.price3);
        }

        if(validators.price4 !== undefined){
            $("#search_priceSelector option[value='500000,999999999']").prop("disabled", !validators.price4);
        }

        $("#search_priceSelector").trigger("chosen:updated");

        //rooms
        if(validators.room1 !== undefined){
            $("#search_bedroomCount option[value='1']").prop("disabled", !validators.room1);
        }

        if(validators.room2 !== undefined){
            $("#search_bedroomCount option[value='2']").prop("disabled", !validators.room2);
        }

        if(validators.room3 !== undefined){
            $("#search_bedroomCount option[value='3']").prop("disabled", !validators.room3);
        }

        if(validators.room4 !== undefined){
            $("#search_bedroomCount option[value='4']").prop("disabled", !validators.room4);
        }

        if(validators.room5 !== undefined){
            $("#search_bedroomCount option[value='5']").prop("disabled", !validators.room5);
        }

        $("#search_bedroomCount").trigger("chosen:updated");
    }

    function blockUIDuringTheSearch()
    {
        $realEstatesList = $($("#realEstatesList")[0]);
        $searchbar = $($(".pxp-searchbar-header")[0]);
        $sortAndInfo = $($("#resultsSortPlusInfo")[0]);

        $searchbar.block({
            message: null,
            overlayCSS: {
                backgroundColor: '#707781',
                opacity: .05
            }
        });

        $sortAndInfo.block({
            message: null,
            overlayCSS: {
                backgroundColor: 'transparent'
            }
        });

        $realEstatesList.block({
            message: '<div class="spinner-border" role="status" style="margin:auto;"></div>',
            overlayCSS: {
                backgroundColor: 'transparent'
            },
            centerY: false,
            css: {
                top: '20px',
                border: 'none',
                background: 'transparent',
                color: '#AD3A27'
            }

        });

    }

    function scrollToTop() {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
    }

    function submitSearch(trigger){
        if(trigger){

            $trigger = $(trigger);

            $realEstatesList = $($("#realEstatesList")[0]);
            $searchbar = $($(".pxp-searchbar-header")[0]);
            $sortAndInfo = $($("#resultsSortPlusInfo")[0]);

            UrlUtil.removeParam("page");

            for(var i = 0; i < $trigger.length; i++)
            {
                if($($trigger[i]).attr("type") === "checkbox" && $($trigger[i]).prop("checked") === false){
                    UrlUtil.removeParam($($trigger[i]).attr("name"));
                } else {
                    UrlUtil.insertParam($($trigger[i]).attr("name"), $($trigger[i]).val());
                }
            }

            blockUIDuringTheSearch();

            $.ajax({
                type: "GET",
                url: window.location.href,
                cache: false
            })
            .done(function(data, textStatus, jqXHR){
                if (data.html !== undefined) {
                    $realEstatesList.html(data.html);
                    if(data.extra !== undefined){
                        filterAvalilability(data.extra);
                    }
                }

                if(data.last_page !== undefined){
                    InfiniteScroll.lastPage = data.last_page;
                }

                InfiniteScroll.lastPage === true ? $("#loadMoreContainer").hide() : $("#loadMoreContainer").show();
                InfiniteScroll.page = 1;
                InfiniteScroll.setHref();

                if(data.total_count !== undefined){
                    $("#spanTotalSearchResults").text(NumberUtil.numberWithCommas(data.total_count));
                }

                if(data.similar == 1){
                    $("#similarTitle").removeClass("d-none");
                    $("#searchTitle").addClass("d-none");
                }
                else{
                    $("#similarTitle").addClass("d-none");
                    $("#searchTitle").removeClass("d-none");
                }

            })
            .fail(function(jqXHR, textStatus, errorThrown){
                console.log(textStatus + ": " + errorThrown );
            })
            .always(function () {
                $searchbar.unblock();
                $sortAndInfo.unblock();
                $realEstatesList.unblock();
            });

            scrollToTop();
        }
    }

    $("input[name^='search']").keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            submitSearch(this);
            e.preventDefault();
        }
    });

    $("#search_priceSelector").change(function (e) {
       submitOnPriceChange();
    });

    $("select[name^='search'], input[type='checkbox'][name^='search']").not("[name*='priceSelector']").change(function (e) {
        submitSearch(this);
    });

    $("#btnSubmitSearch").click(function () {
        if($(this).data("auto-trigger") == true){
            submitSearch($("#search_text")[0]);
        }
    });

    //price range slider settings
    $("#price-range-slider").on("slideStop", function () {
        submitOnPriceChange();
    });

    $(".clear-filter-text").click(function () {
        submitSearch($($(this).data("target")));
    });

    $(".clear-select-box").click(function () {
        submitSearch($($(this).data("target")));
    });

    $(".reset-filters-button").click(function () {
        submitSearch(
            $("input[name^='search'][type='text'], input[type='checkbox'][name^='search'], input[name^='search'][type='hidden'], select[name^='search']").not("[name*='priceSelector']")
        );
    });

    //check and set filters on load
    function onLoad(){
        if(!UrlUtil.paramExists("search[priceSelector]") && (UrlUtil.paramExists("[search[priceMin]")
                || UrlUtil.paramExists("search[priceMax]"))){
            var minPrice = $("#search_priceMin").val();
            var maxPrice = $("#search_priceMax").val();
            $("#search_priceSelector option").each(function(i, e){
                var values = $(e).val().split(",");
                if(values.length === 2){
                    if(minPrice >= values[0] && maxPrice <= values[1]){
                        $(e).parent().val($(e).val()).trigger("chosen:updated");
                    }
                }
            });
        }

        UrlUtil.removeParam("search[priceSelector]");
    }

    onLoad();

    //Init Infinite Scroll
    InfiniteScroll.init("realEstatesList", "loadMoreContainer", "loadMoreButton", "loader");

    //Lazy load result images for carousel
    $("div[id^='card-carousel-']").on('slide.bs.carousel', function(e) {
        var $nextImage = $(e.relatedTarget);

        $activeItem = $('.active.carousel-item', this);

         // prevents loading of an image if it's already loaded
        var src = $nextImage.data('lazy-load-src');

        if (typeof src !== "undefined" && src !== "") {
            $nextImage.css('background-image', 'url(' + src + ')');
            $nextImage.data('lazy-load-src', '');
        }
    });
});