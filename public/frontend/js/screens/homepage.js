$(document).ready(function () {

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $("#price-range-slider").on("slide", function () {
        var fieldValue = $(this).val();
        if (fieldValue) {
            var values = fieldValue.split(",");
            if (values) {
                var sliderMinValue = $("#price-range-slider").data("slider-min");
                var sliderMaxValue = $("#price-range-slider").data("slider-max");

                $("#span-price-min").text(NumberUtil.numberWithCommas(values[0]));
                values[1] == sliderMaxValue ? $("#span-price-max").text(NumberUtil.numberWithCommas(sliderMaxValue-1).toString() + "+") : $("#span-price-max").text(NumberUtil.numberWithCommas(values[1]));


                if(values[0] == sliderMinValue && values[1] == sliderMaxValue){
                    $("#search_priceMin").val("");
                    $("#search_priceMax").val("");
                    checkAllFiltersAndHideResetButton();
                }

                if(values[0] != sliderMinValue || values[1] != sliderMaxValue){
                    $("#search_priceMin").val(values[0]);
                    $("#search_priceMax").val(values[1]);
                    showResetAllButton();
                }
            }
        }
    });

    $("#price-range-slider").slider({});

    $("#search_priceSelector").change(function(e){

        var $minPrice = $("#search_priceMin");
        var $maxPrice = $("#search_priceMax");

        var priceRange = $(this).val().split(",");

        if(priceRange.length === 2){
            $minPrice.val(priceRange[0]);
            $maxPrice.val(priceRange[1]);
        } else {
            $minPrice.val(null);
            $maxPrice.val(null);
        }
    });

    $(".pxp-hero-search select").change(function() {
        var windowWidth = $(window).width();

        if($(this).prop('selectedIndex') != 0) {
            if(windowWidth < 768){
                $(this).closest("div").find(".clear-select-box").removeClass("d-none");
            }
        }
        else{
            if(windowWidth < 768){
                $(this).closest("div").find(".clear-select-box").addClass("d-none");
            }
        }
    });

});