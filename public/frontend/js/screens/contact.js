$(document).ready(function () {
    function initMap() {
        if($("#geoLocations").length > 0){
            var geoLocations = JSON.parse('' + $("#geoLocations").val());
            $("#geoLocations").remove();

            if(Array.isArray(geoLocations) && geoLocations.length > 0){
                var map = new google.maps.Map(document.getElementById('map_canvas'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    gesturelHandling: 'none',
                    zoomControl: false,
                    minZoom: 18,
                    maxZoom: 18
                });

                var marker, i;
                var mapBounds = new google.maps.LatLngBounds();

                for (i = 0; i < geoLocations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(geoLocations[i][0]), parseFloat(geoLocations[i][1])),
                        map: map
                    });
                    mapBounds.extend(marker.getPosition());
                }

                map.fitBounds(mapBounds);
            } else {
                map = new google.maps.Map(document.getElementById('map_canvas'), {
                    center: {lat: 43.712904, lng: 15.879205},
                    zoom: 10
                });
            }
        }
    }

    initMap();

});

$('#contactForm').submit(function (e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.


    $("#btnSubmit").prop("disabled", true);

    var form = $(this);
    var url = form.attr('action');
    var formData = form.serialize();

    //$("#loader").show();


    $.ajax({
        type: "POST",
        url: url,
        data: formData, // serializes the form's elements.
        success: function (data) {
            $("#contactNotification").html(data.html);
            $("#btnSubmit").prop("disabled", false);

        }
    });
});
