$(document).ready(function () {
    function initMap() {
        if($("#geoLocations").length > 0){
            var geoLocations = JSON.parse('' + $("#geoLocations").val());
            $("#geoLocations").remove();

            if(Array.isArray(geoLocations) && geoLocations.length > 0){
                var map = new google.maps.Map(document.getElementById('map_canvas'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    gesturelHandling: 'none',
                    zoomControl: false,
                    minZoom: 10,
                    maxZoom: 10
                });

                var marker, i;
                var mapBounds = new google.maps.LatLngBounds();

                for (i = 0; i < geoLocations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(geoLocations[i][0]), parseFloat(geoLocations[i][1])),
                        map: map
                    });
                    mapBounds.extend(marker.getPosition());
                }

                map.fitBounds(mapBounds);
            } else {
                map = new google.maps.Map(document.getElementById('map_canvas'), {
                    center: {lat: 43.712904, lng: 15.879205},
                    zoom: 7
                });
            }
        }
    }

    initMap();

    //SORT START

    $("#sortRealEstates").change(function () {
        UrlUtil.removeParam("page");
        UrlUtil.insertParam("sort", $(this).val());
        window.location.reload(true);
    });

    function setSortOnLoad(){
        var sort = UrlUtil.getQueryVariable("sort");
        if(sort){
            $("#sortRealEstates").val(sort);
        }
    }

    setSortOnLoad();

    //SORT END

    InfiniteScroll.init("realEstatesList", "loadMoreContainer", "loadMoreButton", "loader", "/api/category-page-real-estates", function () {
        return {
            page: this.page,
            catPage: $("#categoryPageId").val(),
            sort: UrlUtil.getQueryVariable("sort")
        };
    }, function(dataExtra) {
        $('.lazy').lazy({
            visibleOnly: true,
            threshold: 0
        });
    });
});

