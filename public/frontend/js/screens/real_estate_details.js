$(document).ready(function () {

    function initMap() {
        if($("#map_canvas").length > 0){
            var latLng = new google.maps.LatLng(parseFloat($("#real-estate-latitude").val()), parseFloat($("#real-estate-longitude").val()));

            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                gesturelHandling: 'none',
                zoomControl: false,
                minZoom: 11,
                maxZoom: 11,
                center: latLng
            });

            var mapBounds = new google.maps.LatLngBounds();

            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });

            mapBounds.extend(marker.getPosition());
            map.fitBounds(mapBounds);

            $("#real-estate-latitude").remove();
            $("#real-estate-longitude").remove();
        }
    }

    initMap();

    $("#virtualWalkFullScreen").click( function(){
        $("#virtualWalkIFrame").addClass("fullscreen");
        $("#virtualWalkExitFullScreen").removeClass("d-none");
    });

    $("#virtualWalkFullScreen").keyup(function(e) {
        if (e.key === "Escape") {
            virtualWalkExitFullScreen();
        }
    });

    $("#virtualWalkExitFullScreen").click(function(){
        virtualWalkExitFullScreen();
    });

    function virtualWalkExitFullScreen(){
        $("#virtualWalkIFrame").removeClass("fullscreen");
        $("#virtualWalkExitFullScreen").addClass("d-none");
    }

    //social share
    function getWindowSize(){
        var e = document.body,
            t = document.documentElement, //the root element of the document
            n = window.innerHeight,
            r = window.innerWidth;
        return {
            height: n || t.clientHeight || e.clientHeight,
            width: r || t.clientWidth || e.clientWidth
        };
    }

    function popup(url) {
        var windowSize = getWindowSize(),
            o = windowSize.height,
            i = windowSize.width,
            n = Math.min(600, .6 * o),
            r = Math.min(800, .8 * i);
        return window.open(url, "", ["height=" + n, "left=" + (i - r) / 2, "top=" + (o - n) / 2, "width=" + r, "status=1", "toolbar=0"].join(","));
    }

    $(".social-share").click(function (e) {
        e.preventDefault();
        popup($(this).prop("href"))
    });


    //By default, plugin uses `data-fancybox-group` attribute to create galleries.
    $(".fancybox").jqPhotoSwipe({
        galleryOpen: function (gallery) {
            //with `gallery` object you can access all methods and properties described here http://photoswipe.com/documentation/api.html
            //console.log(gallery);
            //console.log(gallery.currItem);
            //console.log(gallery.getCurrentIndex());
            //gallery.zoomTo(1, {x:gallery.viewportSize.x/2,y:gallery.viewportSize.y/2}, 500);
            //gallery.toggleDesktopZoom();
        }
    });
    //This option forces plugin to create a single gallery and ignores `data-fancybox-group` attribute.
    $(".forcedgallery > a").jqPhotoSwipe({
        forceSingleGallery: true
    });

    $('.pxp-sp-gallery-btn').click(function() {
        $('.fancybox:first').click();
    });

    //add-remove favorite
   /* $("#btn-save").click(function (e) {
        e.preventDefault();
        var $self = $(this);

        $.ajax('/api/user-logged-in')
            .then(function (data) {
                if(data === true){
                    return $.ajax($self.data("action"));
                }

                if(data === false){
                    $('#pxp-signin-modal').modal('show');
                    return false;
                }
            })
            .then(function (data) {
                if(data !== false){
                    if(data.success === true){
                        $img = $self.find("img");

                        if($self.data("saved")){
                            $self.data("saved", 0);
                            $img.attr("src", "/frontend/img/icon/TD_icons_favorites_unselected.svg");
                        }
                        else if(!$self.data("saved")){
                            $self.data("saved", 1);
                            $img.attr("src", "/frontend/img/icon/TD_icons_favorites_selected.svg");
                        }
                    }

                    if(data.message){
                        alert(data.message);
                    }
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("Error");
                console.log(textStatus + ": " + errorThrown);
            });
    });
    */


//request tour
 $("#requestTourBtn").click(function (e) {
     $(".tour").show();
         $(".contact_agent").hide();
 }
 );

    //request tour
    $("#contactAgentBtn").click(function (e) {
        $(".tour").hide();
        $(".contact_agent").show();
        $("#contact_date").val("");
        }
    );

    //navigate ground plans
    $(".ground-plan-modal-link").click(function(e){

        e.preventDefault();

        var $groundPlanContainer = $("#ground-plan-container");
        var activeItem = $groundPlanContainer.data("activated");
        var itemToBeActivated = $(this).data("activate");

       if(activeItem !== itemToBeActivated){
           $groundPlanContainer.empty();
           var elem = $(this).data("mime").includes("image") ? "img" : "iframe";

           $groundPlan = $("<" + elem + ">");
           $groundPlan.prop("src", $(this).data("src"));
           $groundPlan.addClass("embed-responsive-item");
           $groundPlanContainer.data("activated", itemToBeActivated);

           $groundPlanContainer.append($groundPlan);

           $(this).removeAttr("href");
           $("[data-activate='" + activeItem + "']").attr("href", "#");

       }
    });
});

$( '#contactForm' ).submit(function ( e ) {
    e.preventDefault(); // avoid to execute the actual submit of the form.


    $("#btnSubmit" ).prop("disabled", true);

    var form = $(this);
    var url = form.attr('action');
    var formData =  form.serialize();

    //$("#loader").show();



    $.ajax({
        type: "POST",
        url: url,
        data: formData, // serializes the form's elements.
        success: function(data)
        {
            $("#contactNotification").html(data.html);
            $("#btnSubmit" ).prop("disabled", false);
            form.trigger("reset");

        }
    });
});


