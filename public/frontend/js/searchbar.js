$(document).ready(function () {

    //Init chosen elements
    function initChosenElements(){
        var windowWidth = $(this).width();
        var $chosenControls = $('.form-control-chosen');

        var chosenInitialized = false;
        $chosenControls.each(function (index, elem) {
            chosenInitialized = !!$(elem).data("chosen");
        });

        if(windowWidth >= 768){
            $chosenControls.each(function (index, elem) {
                $(elem).removeAttr("placeholder");
                $(elem).find("option").eq(0).text("");
            });
            if(!chosenInitialized){
                $chosenControls.chosen({
                    allow_single_deselect: true,
                    disable_search: true
                });
            }
            $(".clear-select-box").addClass("d-none");
        }

        if(windowWidth < 768){
            $chosenControls.each(function (index, elem) {
                $(elem).attr("placeholder", $(elem).data("placeholder"));
                $(elem).find("option").eq(0).text($(elem).data("placeholder"));
                if($(elem).val() !== ""){
                    $(elem).closest("div").find(".clear-select-box").removeClass("d-none");
                }
            });
            if(chosenInitialized){
                $chosenControls.chosen('destroy');
            }
        }
    }

    function windowResizeHandler() {
        initChosenElements();
    }

    windowResizeHandler();

    $(window).resize(function() {
        windowResizeHandler();
    });

    $("#btnSubmitSearch").click(function () {
        if($(this).data("auto-trigger") == false){
            $(this).closest("form").submit();
        }
    });

    $("#search_text").autoComplete({
        minLength: 2,
        events: {
            search: function(qry, callback, origJQElement){
                if(/^\d+$/.test(qry) === true){
                    $.getJSON( "/api/real-estate/autocomplete", {term: qry}, function( data, status, xhr ) {
                        callback( data );
                    });
                }
            }
        }
    });

    function resetSliderToDefaultValues(){
        var $slider = $("#price-range-slider");
        $slider.slider('setValue', [$slider.data("slider-min"), $slider.data("slider-max")], false);
        $("#span-price-min").text(NumberUtil.numberWithCommas($slider.data("slider-min")));
        $("#span-price-max").text(NumberUtil.numberWithCommas($slider.data("slider-max") - 1) + "+");
    }

    $("#price-range-slider").on("slide", function () {
        var fieldValue = $(this).val();
        if (fieldValue) {
            var values = fieldValue.split(",");
            if (values) {
                var sliderMinValue = $("#price-range-slider").data("slider-min");
                var sliderMaxValue = $("#price-range-slider").data("slider-max");

                $("#span-price-min").text(NumberUtil.numberWithCommas(values[0]));
                values[1] == sliderMaxValue ? $("#span-price-max").text(NumberUtil.numberWithCommas(sliderMaxValue-1).toString() + "+") : $("#span-price-max").text(NumberUtil.numberWithCommas(values[1]));


                if(values[0] == sliderMinValue && values[1] == sliderMaxValue){
                    $("#search_priceMin").val("");
                    $("#search_priceMax").val("");
                    checkAllFiltersAndHideResetButton();
                }

                if(values[0] != sliderMinValue || values[1] != sliderMaxValue){
                    $("#search_priceMin").val(values[0]);
                    $("#search_priceMax").val(values[1]);
                    showResetAllButton();
                }
            }
        }
    });

    $("#price-range-slider").slider({});

    $("#search_priceSelector").change(function(e){

        var $minPrice = $("#search_priceMin");
        var $maxPrice = $("#search_priceMax");

        var priceRange = $(this).val().split(",");

        if(priceRange.length === 2){
            $minPrice.val(priceRange[0]);
            $maxPrice.val(priceRange[1]);
        } else {
            $minPrice.val("");
            $maxPrice.val("");
            resetSliderToDefaultValues();
        }
    });


    $(".pxp-searchbar-header :input").keyup(function () {
        if($(this).val() != "") {
            showResetAllButton();
        }
        else{
            checkAllFiltersAndHideResetButton();
        }
    });

    $(".pxp-searchbar-header select").change(function() {
        var windowWidth = $(window).width();

        if($(this).prop('selectedIndex') != 0) {
            showResetAllButton();
            if(windowWidth < 768){
                $(this).closest("div").find(".clear-select-box").removeClass("d-none");
            }
        }
        else{
            checkAllFiltersAndHideResetButton();
            if(windowWidth < 768){
                $(this).closest("div").find(".clear-select-box").addClass("d-none");
            }
        }
    });

    $('.pxp-searchbar-header input:checkbox').click( function(){
        if($(this).is(':checked')){
            showResetAllButton();
        }
        else{
            checkAllFiltersAndHideResetButton();
        }
    });

    $(".clear-filter-text").click(function () {
        $($(this).data("target")).val("");
        checkAllFiltersAndHideResetButton();
    });

    function showResetAllButton(){
        $(".pxp-content-side-search-form-reset").show(200);
    }

    function checkAllFiltersAndHideResetButton(){
        if (areAllFiltersEmpty()==true){
            $(".pxp-content-side-search-form-reset").hide(200);
        }
    }

    function areAllFiltersEmpty(){
        var $slider = $("#price-range-slider");

        if($("#search_text").val() !== ""){
            return false;
        }
        else if($(".pxp-searchbar-header select option:selected").filter('[value!=""]').length > 0){
            return false;
        }
        else if($(".pxp-searchbar-header input:checkbox").is(':checked')){
            return false;
        }
        else if($slider.val() !== ($slider.data("slider-min") + "," + $slider.data("slider-max"))){
            return false;
        }
        return true;
    }

    $(".reset-filters-button").click(function () {
        $("#search_text").val("");
        if(!$("#reset-search-text").hasClass("d-none")){
            $("#reset-search-text").addClass("d-none");
        }
        $(".pxp-searchbar-header select").val('').trigger('chosen:updated');
        $("#search_priceSelector option").each(function(index, elem){
           $(elem).attr("selected", false);
        });
        $(".pxp-content-side-search-form-reset").hide(200);
        $('.pxp-searchbar-header input:checkbox').prop('checked', false);
        //price must be set manually
        $("#search_priceMin").val("");
        $("#search_priceMax").val("");
        //...as do slider values
        resetSliderToDefaultValues();
        //hide possible form-control X-es
        $(".clear-select-box").addClass("d-none");
    });

    function showResetFiltersIfAnyFiltersAreSetOnLoad(){
        $("input[name^='search'][type='text'], input[name^='search'][type='checkbox'], select[name^='search']").not("[id='search_sort']").each(function(index, element){
            if($(element).is("input:text") && $(element).val() != ""){
                showResetAllButton();
                //We need to manually handle text field's X
                if($("#reset-search-text").hasClass("d-none")){
                    $("#reset-search-text").removeClass("d-none");
                }
                return false;
            }

            if($(element).is("input:checkbox") && $(element).prop("checked") === true){
                showResetAllButton();
                return false;
            }

            if($(element).is("select") && $(element).val() != ""){
                showResetAllButton();
                return false;
            }
        });

        var $slider = $("#price-range-slider");
        if($slider.val() !== ($slider.data("slider-min") + "," + $slider.data("slider-max"))){
            showResetAllButton();
            return false;
        }
    }

    showResetFiltersIfAnyFiltersAreSetOnLoad();

    $(".clear-select-box").click(function(e){
        $($(this).data("target")).val("");
        $(this).addClass("d-none");
        checkAllFiltersAndHideResetButton();
    });
});
