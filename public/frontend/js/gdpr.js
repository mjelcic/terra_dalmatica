var cookieName = "terradalmatica_gdpr";
var cookieLifeTime = 999999;

$( document ).ready(function() {
    if(!getCookie(cookieName)){
        $("#cookie-msg").show();
    }
});


$(".btn-accept-gdpr").click(function(){
    $("#cookie-msg").hide();

    var cookieValues = $('#gdprForm input:checked').map(function(){
        return $(this).val();
    });
    setCookie(cookieName, JSON.stringify(cookieValues.get()), cookieLifeTime);
});

function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}