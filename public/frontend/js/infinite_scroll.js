/**
 * Created by mijatra on 3/4/2020.
 */

    /////////////////////////////////////////////////////////////
    //Real estates infinite scroll
    //dependency: UrlUtil (url_util.js)
    ////////////////////////////////////////////////////////////

var InfiniteScroll = {
    isProcessing: false,
    lastPage: false,
    page: 1,
    $loadMoreContainer: null,
    $btnLoadMore: null,
    $loader: null,
    $listContainer: null,
    ajaxUrl: null,
    extraSuccessCallback: undefined,
    init: function init(listContainerId, loadMoreContainerId, btnLoadMoreId, loaderId, ajaxUrl, collectAjaxDataCallback, extraSuccessCallback)
    {
        var self = this;

        //init variables
        this.$listContainer = $("#" + listContainerId);
        this.$loadMoreContainer = $("#" + loadMoreContainerId);
        this.$btnLoadMore = $("#" + btnLoadMoreId);
        this.$loader = $("#" + loaderId);
        this.ajaxUrl = ajaxUrl;

        if(collectAjaxDataCallback !== undefined){
            self.createAjaxCallData = collectAjaxDataCallback;
        }

        if(extraSuccessCallback !== undefined){
            self.extraSuccessCallback = extraSuccessCallback;
        }

        self.initPage();
        if(self.$btnLoadMore.length > 0){
            self.setHref();
        }

        this.$btnLoadMore.click(function (e) {
            e.preventDefault();
            if (self.lastPage === false && self.isProcessing === false) {
                self.addMoreElements();
            }
        });
    },
    loadMoreContent: function loadMoreContent()
    {
        var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        //Modify this parameter to establish how far down do you want to make the ajax call
        var scrolltrigger = 0.99;
        if ((wintop / (docheight - winheight)) > scrolltrigger) {
            //I added the is_processing variable to keep the ajax asynchronous but avoiding making the same call multiple times
            if (this.lastPage === false && this.isProcessing === false) {
                this.addMoreElements();
            }
        }
    },
    initPage: function initPage()
    {
        var url = new URL(window.document.location);
        var params = new URLSearchParams(url.search);

        if (params.has("page")) {
            this.page = parseInt(params.get("page"));
        }else {
            this.page = 1;
        }
    },
    setHref: function setHref()
    {
        var self = this;
        var url = new URL(window.document.location);
        var params = new URLSearchParams(url.search);

        if (params.has("page")) {
            params.set("page", parseInt(params.get("page")) + 1);
        }else {
            params.append("page", 2);
        }

        self.$btnLoadMore.prop("href", url.href.split("?")[0] + "?" + params.toString());

    },
    addMoreElements: function addMoreElements()
    {
        var self = this;

        this.$loadMoreContainer.hide();
        this.$loader.show();
        this.isProcessing = true;
        this.page += 1;

        var ajaxData = this.createAjaxCallData === undefined ? { page: this.page } : this.createAjaxCallData();

        $.ajax({
            type: "GET",
            data: ajaxData,
            url: self.ajaxUrl ? self.ajaxUrl : window.location.href,
            cache: false
        })
        .done(function(data){
            self.$loader.hide();

            if (data.html.length > 0) {
                self.$listContainer.append(data.html);
                if(self.extraSuccessCallback !== undefined && typeof self.extraSuccessCallback === "function"){
                    self.extraSuccessCallback(data.extra);
                }
            }

            self.lastPage = data.last_page !== undefined ? data.last_page : false;

            self.lastPage === true ? self.$loadMoreContainer.hide() : self.$loadMoreContainer.show();

            UrlUtil.insertParam("page", self.page);

            if(self.$btnLoadMore.length > 0){
                self.setHref();
            }

            self.isProcessing = false;
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            self.$loader.hide();
            self.$loadMoreContainer.show();
            self.isProcessing = false;
            console.log(textStatus + ": " + errorThrown);
        });
    },
    createAjaxCallData: undefined
};

