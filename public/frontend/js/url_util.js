/**
 * Created by mijatra on 3/4/2020.
 */

var UrlUtil = new function () {
    var self = this;

    self.removeParam = function removeParam(key) {
        var url = new URL(window.document.location); //return current URL as a string
        var params = new URLSearchParams(url.search.slice(1));

        if(params.has(key)){
            params.delete(key);
        }

        history.replaceState({page: 2}, "", "?" + params);
    };

    self.insertParam = function insertParam(key, value) {
        var url = new URL(window.document.location); //return current URL as a string
        var params = new URLSearchParams(url.search.slice(1));

        if (params.has(key)) {
            params.set(key, value);
        }else {
            params.append(key, value);
        }
        history.replaceState({page: 2}, "", "?" + params);
    };

    self.getQueryVariable =  function getQueryVariable(variable)
    {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){return pair[1];}
        }
        return(false);
    };

    self.paramExists = function paramExists(key) {
        var url = new URL(window.document.location); //return current URL as a string
        var params = new URLSearchParams(url.search.slice(1));

        return params.has(key);
    }
};
