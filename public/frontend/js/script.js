$(document).ready(function () {
    var windowWidth = $(window).width();

    $('.notificationModal').modal('show');

    $(".clear-filter-text").click(function () {
        $($(this).data("target")).val("");
        $(this).addClass("d-none");
    });

    $("input[type=text]").keyup(function () {

        if ($(this).data('reset'))
            if ($(this).val() != "") {
                $($(this).data('reset')).removeClass("d-none");
            }
            else {
                $($(this).data('reset')).addClass("d-none");
            }
    })

});

var lastScrollTop = 0;
function onContentScrollHideHeader() {
    st = $(this).scrollTop();
    if (window.pageYOffset > 93) {
        $('.pxp-header').addClass('pxp-is-sticky');
        $('.pxp-searchbar-header').addClass('pxp-is-sticky');
        if(st < lastScrollTop) {
            $('.pxp-searchbar-header').removeClass('searchbar-without-header');
            $('.pxp-header').removeClass('pxp-is-hidden');

        }
        else {
            $('.pxp-searchbar-header').addClass('searchbar-without-header');
            $('.pxp-header').addClass('pxp-is-hidden');

        }

    } else {
        $('.pxp-header').removeClass('pxp-is-sticky');
        $('.pxp-searchbar-header').removeClass('pxp-is-sticky');
        $('.pxp-searchbar-header').removeClass('searchbar-without-header');
        $('.pxp-header').removeClass('pxp-is-hidden');

    }
    lastScrollTop = st;
}

window.onscroll = function() {
    onContentScrollHideHeader();
};

var lastScrollTop = 0;
function onContentScrollHideHeader() {
    st = $(this).scrollTop();
    if (window.pageYOffset > 93) {
        $('.pxp-header').addClass('pxp-is-sticky');
        $('.pxp-searchbar-header').addClass('pxp-is-sticky');
        if(st < lastScrollTop) {
            $('.pxp-searchbar-header').removeClass('searchbar-without-header');
            $('.pxp-header').removeClass('pxp-is-hidden');

        }
        else {
            $('.pxp-searchbar-header').addClass('searchbar-without-header');
            $('.pxp-header').addClass('pxp-is-hidden');

        }

    } else {
        $('.pxp-header').removeClass('pxp-is-sticky');
        $('.pxp-searchbar-header').removeClass('pxp-is-sticky');
        $('.pxp-searchbar-header').removeClass('searchbar-without-header');
        $('.pxp-header').removeClass('pxp-is-hidden');

    }
    lastScrollTop = st;
}

window.onscroll = function() {
    onContentScrollHideHeader();
};


////GDPR
var cookieName = "terradalmatica_gdpr";
var cookieLifeTime = 999999;

$( document ).ready(function() {
    if(!getCookie(cookieName)){
        $("#cookie-msg").show();
    }
});


$(".btn-accept-gdpr").click(function(){
    $("#cookie-msg").hide();

    var cookieValues = $('#gdprForm input:checked').map(function(){
        return $(this).val();
    });
    setCookie(cookieName, JSON.stringify(cookieValues.get()), cookieLifeTime);
});

function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
// END GDPR

