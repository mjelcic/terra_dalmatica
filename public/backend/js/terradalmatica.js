$(document).ready(function () {

    // hide sidebar menu on mobile phones
    if ($(window).width() < 960) {
        $("#accordionSidebar").addClass("toggled");
    }

    $(".switchButton").click(function () {
        var button = $(this);
        $.ajax({
            url: $(this).data("path"),
            success: function (result) {
                button.find("i").removeClass("fa-toggle-off");
                button.find("i").removeClass("fa-toggle-on");
                console.log(result.value);
                if (result.value == "1") {
                    button.find("i").addClass("fa-toggle-on");
                }
                else {
                    button.find("i").addClass("fa-toggle-off");
                }
            },
            error: function(jqXHR, strStatus, strError){
                if(strStatus){
                    alert(jqXHR.responseText);
                }
            }
        });
    });

    //delete button logic
    $(".btn-delete").click(function(event){

        if(!confirm("Jeste li sigurni da želite izbrisati navedeni unos?")){
            event.preventDefault();
            event.stopPropagation();
        }
    });

});

