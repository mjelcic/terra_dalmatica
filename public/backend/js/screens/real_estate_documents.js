$(document).ready(function() {
    initializeDropzone();
    initializeDropzoneBos();
});
Dropzone.autoDiscover = false;


function initializeDropzone() {
    var formElement = document.querySelector('.js-reference-dropzone');
    if (!formElement) {
        return;
    }

    var dropzone = new Dropzone(formElement, {
        paramName: 'reference',
        autoProcessQueue: false,
        addRemoveLinks: false,
        acceptedFiles: "application/pdf,application/msword,application/vnd.ms-excel",
        parallelUploads: 5
    });

    dropzone.on("success", function (file, response) {
        document.querySelector("#documents_container")
            .insertAdjacentHTML('beforeend',
                response.html
            );

        dropzone.removeFile(file);
    });

    dropzone.on("sending", function(file, xhr, formData) {
        formData.append("docType", 1);
        formData.append("confidential", false);
        console.log(formData)
    });

    $('#btn_upload').on("click", function() {
        dropzone.processQueue(); // Tell Dropzone to process all queued files.
    });
}

function initializeDropzoneBos() {
    var formElement = document.querySelector('.js-reference-dropzone-bos');
    if (!formElement) {
        return;
    }

    var dropzone = new Dropzone(formElement, {
        paramName: 'reference',
        autoProcessQueue: false,
        addRemoveLinks: false,
        acceptedFiles: "application/pdf,application/msword,application/vnd.ms-excel",
        parallelUploads: 5
    });

    dropzone.on("success", function (file, response) {
        document.querySelector("#documents_bos_container")
            .insertAdjacentHTML('beforeend',
                response.html
            );

        dropzone.removeFile(file);
    });

    dropzone.on("sending", function(file, xhr, formData) {
        formData.append("docType", 0);
        formData.append("confidential", false);
    });

    $('#btn_upload_bos').on("click", function() {
        dropzone.processQueue(); // Tell Dropzone to process all queued files.
    });
}


function markAsExclusive(docId) {
    $.ajax(window.adminExclusiveUrl.replace("phDocId", docId),
        {
            type: 'POST',
            data: {},
            success: function(response) {
                window.location.reload();
            },
            failure: function() {
                alert("Akcija nije uspjela");
            }
        });

}
