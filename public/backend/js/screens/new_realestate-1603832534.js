$(document).ready(function() {

    initializeDropzone();
    initializeDropzoneForPlans();
    setupFormSubmitHandler();

    for(var i = 0; i < 3; i++) {

        var titleElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_title");
        var descElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_description");

        //this will be enough to say there is no language with index i
        if(!titleElement) {
            continue;
        }
        titleElement.addEventListener("blur", function (e) {
            let text = e.target.value;

            let suggestedSlug = Slugifier.slugify(text);
            let suggestedMetaWords = text.split(" ").map(word => word.trim());
            let suggestedMeta = suggestedMetaWords.slice(0, 10).join(" ");
            let slugId = e.target.id.replace("_title", "_slug");
            let titleMetaId = e.target.id.replace("EstateLocalizations", "EstateMetaLocalizations");

            if(document.querySelector("#" + slugId) && !document.querySelector("#" + slugId).value) {
                document.querySelector("#" + slugId).value = suggestedSlug;
            }
            if(document.querySelector("#" + titleMetaId) && !document.querySelector("#" + titleMetaId).value) {
                document.querySelector("#" + titleMetaId).value = suggestedMeta;
            }
        });

        descElement.addEventListener("blur", function (e) {
            let text = e.target.value;
            let words = text.split(" ").map(word => word.trim());

            let suggestedMeta = words.slice(0, 10).join(" ");
            let titleMetaId = e.target.id.replace("EstateLocalizations", "EstateMetaLocalizations");

            if(document.querySelector("#" + titleMetaId) && !document.querySelector("#" + titleMetaId).value) {
                document.querySelector("#" + titleMetaId).value = suggestedMeta;
            }
        })
    }

    var priceOnDemandCheckbox = document.querySelector("#" + userType + "_real_estate_" + formAction + "_priceOnRequest");
    var priceInput = document.querySelector("#" + userType + "_real_estate_" + formAction + "_fullPrice");
    var discountPriceCheckbox = document.querySelector("#discountPriceToggle");
    var discountPriceInput = document.querySelector("#" + userType + "_real_estate_" + formAction + "_discountPrice");
    var invalidPriceLabel = document.querySelector("#invalid_price_label");

    if(priceOnDemandCheckbox) {
        if(priceOnDemandCheckbox.checked) {
            priceInput.setAttribute("disabled", "");

        }
        priceOnDemandCheckbox.addEventListener("change", function() {
            if(this.checked) {
                priceInput.setAttribute("disabled", "");
                if(discountPriceCheckbox) {
                    discountPriceCheckbox.setAttribute("disabled", "");
                    discountPriceInput.setAttribute("disabled", "");
                    discountPriceInput.value = "";
                }
                priceInput.value = "";
                invalidPriceLabel.classList.remove("is-invalid");
            } else {
                priceInput.removeAttribute("disabled");
                discountPriceCheckbox.removeAttribute("disabled");
                if(!discountPriceCheckbox.checked) {
                    discountPriceInput.removeAttribute("disabled");
                }
            }
        })
    }

    priceInput.addEventListener("blur", function(e) {
        if(!discountPriceInput || discountPriceInput.value < priceInput.value) {
            invalidPriceLabel.classList.remove("is-invalid");
        }

        if(submitAttempted && discountPriceInput && discountPriceInput.value > priceInput.value) {
            invalidPriceLabel.classList.add("is-invalid");
        }
    });

    if(discountPriceInput) {
        discountPriceInput.addEventListener("blur", function(e) {
            if(discountPriceInput.value < priceInput.value) {
                invalidPriceLabel.classList.remove("is-invalid");
            }

            if(submitAttempted && discountPriceInput.value > priceInput.value) {
                invalidPriceLabel.classList.add("is-invalid");
            }
        });
    }

    if(discountPriceCheckbox) {
        if(discountPriceCheckbox.checked) {
            discountPriceInput.setAttribute("disabled", "");

        }
        discountPriceCheckbox.addEventListener("change", function() {
            if(this.checked) {
                discountPriceInput.setAttribute("disabled", "");
                discountPriceInput.value = 0;
            } else {
                discountPriceInput.removeAttribute("disabled");
            }
        })
    }

    const formContainer = document.querySelector("#new_realestate"),
        realEstateTypeSelect = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateType"),
        bedroomCountField = document.querySelector("#" + userType + "_real_estate_" + formAction + "_bedroomCount"),
        bathroomCountField = document.querySelector("#" + userType + "_real_estate_" + formAction + "_bathroomCount"),
        surfaceAreaField = document.querySelector("#" + userType + "_real_estate_" + formAction + "_surfaceArea");

    realEstateTypeSelect.addEventListener("change", function(event) {
        console.log(event);
        [1,2,3,4].forEach(id => formContainer.classList.remove("estate-type-" + id));
        formContainer.classList.add("estate-type-" + event.target.value);

        if(parseInt(event.target.value) !== 2) {
            if(surfaceAreaField.value === "") {
                surfaceAreaField.value = 0;
            }
        }

        if([3,4].indexOf(parseInt(event.target.value)) < 0) {
            if(bedroomCountField.value === "") {
                bedroomCountField.value = 0;
            }
            if(bathroomCountField.value === "") {
                bathroomCountField.value = 0;
            }
        }
    });
});


var lastFormSubmitAction = "none";
var submitAttempted = false;

function setupFormSubmitHandler()
{
    var formSubmitActions = [
        "add",
        "addAndPublish",
        "edit",
        "editAndPublish",
        "editPublished",
        "setInProgress",
        "setRejected",
        "setCompleted"
    ];

    formSubmitActions.forEach(submitAction => {
        if(document.querySelector("#" + userType + "_real_estate_" + formAction + "_" + submitAction)) {
            document.querySelector("#" + userType + "_real_estate_" + formAction + "_" + submitAction).addEventListener("click", () => lastFormSubmitAction = submitAction);
        }
    });

    var form = document.querySelector("#frmRealEstate");

    form.addEventListener("submit", function(event) {
        submitAttempted = true;

        let isLocalizationValid;
        let descElement;
        let titleElement;
        let i;
        let localeContainer = document.querySelector("#page_language_display");
        let localeLabel = document.querySelector("#locale_label");
        let prevented = false;

        switch(lastFormSubmitAction) {
            case "add":
            case "edit":
                isLocalizationValid = false;

                for(i = 0; i < 3; i++) {

                    titleElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_title");
                    descElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_description");

                    if(titleElement.value !== "" || descElement.value !== "") {
                        isLocalizationValid = true;
                        break;
                    }
                }

                if(!isLocalizationValid) {
                    localeContainer.classList.add("has-error");
                    localeLabel.textContent = "Za predaju oglasa potrebno je unijeti podatke na bar jednom jeziku!";
                    event.preventDefault();
                    prevented = true;

                }
                break;

            case "addAndPublish":
            case "editAndPublish":
            case "setInProgress":
            case "editPublished":
                isLocalizationValid = true;

                for(i = 0; i < 3; i++) {

                    titleElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_title");
                    descElement = document.querySelector("#" + userType + "_real_estate_" + formAction + "_realEstateLocalizations_" + i + "_description");

                    if(titleElement.value === "" || descElement.value === "") {
                        isLocalizationValid = false;
                        break;
                    }
                }
                if(!isLocalizationValid) {
                    localeContainer.classList.add("has-error");
                    localeLabel.textContent = "Za objavu i uređenje objavljenog oglasa potrebno je unijeti podatke na svim jezicima!";
                    event.preventDefault();
                    prevented = true;
                }
        }

        var priceOnDemandCheckbox = document.querySelector("#" + userType + "_real_estate_" + formAction + "_priceOnRequest");
        var priceInput = document.querySelector("#" + userType + "_real_estate_" + formAction + "_fullPrice");
        var discountPriceCheckbox = document.querySelector("#discountPriceToggle");
        var discountPriceInput = document.querySelector("#" + userType + "_real_estate_" + formAction + "_discountPrice");
        var invalidPriceLabel = document.querySelector("#invalid_price_label");

        if(!priceOnDemandCheckbox.checked) {
            var fullPrice = priceInput.value;

            if(discountPriceInput && !discountPriceCheckbox.checked) {
                var discountPrice = discountPriceInput.value;

                if(discountPrice > fullPrice) {
                    invalidPriceLabel.classList.add("is-invalid");
                    discountPriceInput.focus();
                    event.preventDefault();
                    prevented = true;
                }
            }
        }

        if(!prevented) {
            formSubmitActions.forEach(action => {
                let button = document.querySelector("#" + userType + "_real_estate_" + formAction + "_" + action);

                if(button) {
                    disableButton(button);
                }
            })
        }

    });

    function disableButton(button) {
        setTimeout(function() {
            button.setAttribute("disabled", "true");
        }, 0)
    }
}

function setValidationListeners() {
    var floatInputs = document.querySelectorAll(".float-input");

    for(let floatInput of floatInputs) {
        floatInput.addEventListener("keyup", function(event) {
            let value = event.target.value;
            if(value.startsWith("0") && value.charAt(1) != "."){
                event.target.value = "0";
            }
            if(["00", ",", "."].indexOf(value) >= 0) { event.preventDefault();}
        })
    }
}

Dropzone.autoDiscover = false;


function initializeDropzone() {
    var dzElement = document.querySelector('#dropzone_holder');
    if (!dzElement) {
        return;
    }

    var dropzone = new Dropzone(dzElement, {
        paramName: 'reference',
        autoProcessQueue: true,
        acceptedFiles: "image/png, image/jpeg",
        parallelUploads: 2,
        maxFiles: 20,
        url: '/common/real_estate_images'
    });

    dropzone.on("success", function (file, response) {
        var imageEl = document.querySelector(`#holder-${response.uuid}`);
        imageEl.insertAdjacentHTML('beforeend', response.html);
        imageEl.classList.remove('loading');

        dropzone.removeFile(file);
    });

    dropzone.on("error", function (file, error) {
        //logirati error
        dropzone.removeFile(file);
    });

    dropzone.on("sending", function (file, xhr, formData) {
        var uuid = file.upload.uuid;
        formData.append("imageUuid", uuid);

        document.querySelector("#gallery-thumbs-container")
            .insertAdjacentHTML('beforeend',
                `
                <div id="holder-${uuid}" class="gallery-thumb loading">
                <span class="loader">
                    <i class="fa fa-spinner fa-spin">
                    </i>
                </span>
                <p class="percentage"></p>
                </div>
                `
            );
    });

    dropzone.on("uploadprogress", function (progress) {

        var imageEl = document.querySelector(`#holder-${progress.upload.uuid}`);
        if(!imageEl) {
            return;
        }

        var percentageEl = imageEl.querySelector(".percentage");

        percentageEl.textContent = progress.upload.progress + "%";
    });
}

function initializeDropzoneForPlans() {
    var dzElement = document.querySelector('#dropzone_gplans_holder');
    if (!dzElement) {
        return;
    }

    var dropzone = new Dropzone(dzElement, {
        paramName: 'reference',
        autoProcessQueue: true,
        acceptedFiles: "image/png, image/jpeg, application/pdf",
        parallelUploads: 2,
        maxFiles: 20,
        url: '/common/ground_plans'
    });

    dropzone.on("success", function (file, response) {
        var imageEl = document.querySelector(`#holder-${response.uuid}`);
        imageEl.insertAdjacentHTML('beforeend', response.html);
        imageEl.classList.remove('loading');

        dropzone.removeFile(file);
    });

    dropzone.on("error", function (file, error) {
        //logirati error
        dropzone.removeFile(file);
    });

    dropzone.on("sending", function (file, xhr, formData) {
        var uuid = file.upload.uuid;
        formData.append("uuid", uuid);

        document.querySelector("#gallery-plan-thumbs-container")
            .insertAdjacentHTML('beforeend',
                `
                <div id="holder-${uuid}" class="gallery-thumb loading">
                <span class="loader">
                    <i class="fa fa-spinner fa-spin">
                    </i>
                </span>
                <p class="percentage"></p>
                </div>
                `
            );
    });

    dropzone.on("uploadprogress", function (progress) {

        var imageEl = document.querySelector(`#holder-${progress.upload.uuid}`);
        if(!imageEl) {
            return;
        }

        var percentageEl = imageEl.querySelector(".percentage");

        percentageEl.textContent = progress.upload.progress + "%";
    });
}

// Initialize and add the map
function initMap() {
    var latEl = document.getElementById(userType + "_real_estate_" + formAction + "_latlng_latitude"),
        lngEl = document.getElementById(userType + "_real_estate_" + formAction + "_latlng_longitude");

    var lat = parseFloat(latEl.value);
    var long = parseFloat(lngEl.value);
    //default. Sibenik
    var center = {lat: 43.7350, lng: 15.8952};

    // this should be called on edit
    if(lat && long) {
        center = {lat: lat, lng: long};
    }

    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 8, center: center});
    var marker = new google.maps.Marker({position: center, map: map, draggable: true});

    var infowindow = new google.maps.InfoWindow({
        content: `empty`
    });

    marker.addListener('click', function() {
        var latValue = formatGeoCoord(latEl.value),
            lngValue = formatGeoCoord(lngEl.value);

        infowindow.setContent(
            `<p>Lokacija: ${latValue}, ${lngValue}</p>
            <a href="https://www.google.com/maps/search/?api=1&query=${formatGeoCoord(latEl.value)},${formatGeoCoord(lngEl.value)}">Link na lokaciju</a>`);

        infowindow.open(map, marker);
    });

    marker.addListener('drag', handleMarkerDragEvent);

    latEl.value = center.lat;
    lngEl.value = center.lng;

    function handleMarkerDragEvent(event) {
        infowindow.close();
        latEl.value = event.latLng.lat();
        lngEl.value = event.latLng.lng();
    }

    function formatGeoCoord(geoCoord) {
        let parts = geoCoord.split(".");

        return parts[0] + "." + parts[1].substr(0,5);
    }
}

function removeImage(imageUuid, imageId) {
    var imageEl = document.querySelector(`#holder-${imageUuid}`);

    imageEl.insertAdjacentHTML('beforeend', `    
    <input type="hidden" class="gallery-image-id image-remove" name="images_remove[]" value="${imageId}">
    `);

    imageEl.classList.add('removing');
}

// haha
function unRemoveImage(imageUuid) {
    var imageEl = document.querySelector(`#holder-${imageUuid}`);

    var input = imageEl.querySelector('input.image-remove');

    imageEl.removeChild(input);
    imageEl.classList.remove('removing');
}

function setCoverImage(imageUuid, imageId) {
    var container = document.querySelector("#gallery-thumbs-container");

    var allThumbs = container.querySelectorAll(".gallery-thumb");

    for(var i = 0; i < allThumbs.length; i++) {
        allThumbs[i].classList.remove("featuring");
    }

    var imageEl = document.querySelector(`#holder-${imageUuid}`);

    imageEl.classList.add("featuring");

    var coverFormInput = document.querySelector("#cover-image");
    coverFormInput.value = imageId;
}

function setActiveLanguage(languageCode) {
    var languageLinks = document.querySelectorAll(".language-link");

    for(var i = 0; i < languageLinks.length; i++) {
        languageLinks[i].classList.remove("active");
    }

    var targetLink = document.querySelector("#nav_for_language_" + languageCode);

    targetLink.classList.add("active");

    var parentPage = document.querySelector("#page_language_display");

    parentPage.classList.remove("lan-active-en", "lan-active-hr", "lan-active-de");
    parentPage.classList.add("lan-active-" + languageCode);
}

function removeGroundPlan(uuid, id) {
    var imageEl = document.querySelector(`#holder-${uuid}`);

    imageEl.insertAdjacentHTML('beforeend', `    
    <input type="hidden" class="gallery-image-id image-remove" name="plans_remove[]" value="${id}">
    `);

    imageEl.classList.add('removing');
}

// haha
function unRemoveGroundPlan(uuid) {
    var imageEl = document.querySelector(`#holder-${uuid}`);

    var input = imageEl.querySelector('input.image-remove');

    imageEl.removeChild(input);
    imageEl.classList.remove('removing');
}



