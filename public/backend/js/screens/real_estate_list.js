$(document).ready(function () {
    //filter for each row
    $('#dataTable thead tr').clone(true).appendTo( '#dataTable thead' );
    $('#dataTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" class="form-control"/>' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );


    var table = $('#dataTable').DataTable({
        "order": [[0, "desc"]],
        responsive: true,
        fixedHeader: true,
        orderCellsTop: true
    });

    $.fn.dataTable.ext.search.push(
        function( settings, data ) {
            var from = $('#createdDateFrom').val();
            var to =  $('#createdDateTo').val();
            var createdAtIdx = settings.aoColumns.map(c => c.columnName).indexOf("created-at");
            var date = toIsoDate(data[createdAtIdx]);

            from = from ? toIsoDate(from) : "";
            to = to ? toIsoDate(to, true) : "";

            if(from && to) {
                return date >= from && date <= to;
            }

            if(from) {
                return date > from;
            }

            if(to) {
                return date < to;
            }

            return true;
        }
    );

    function toIsoDate(date, end) {
        let dateParts = date.split("."),
            yearAndTime = dateParts[2],
            month = dateParts[1],
            day = dateParts[0],
            yearAndTimeParts = yearAndTime.split(","),
            year = yearAndTimeParts[0],
            time = yearAndTimeParts.length > 1 ? yearAndTimeParts[1] : (end ? "23:59" : "00:00");

        return year + "-" + month + "-" + day + "T" + time;
    }


    $('#createdDateFrom, #createdDateTo').on("changeDate",  function(event) {
        console.log(event);
        table.draw();
    } );

    $('#createdDateFrom, #createdDateTo').keypress( function(event) {
        event.preventDefault();
    } );


    if(filter_by_status != ""){
        $("input[type=search]").val(filter_by_status);
        table.search(filter_by_status).draw();
    }


    $(".export-immo").click(function (e) {
        alert("Pokrenut izvoz nekretnine prema portalu ImmoScout24.");

        var $self = $(this);

        $.ajax($self.data("path"))
        .done(function( data, textStatus, jqXHR ) {
            alert(data);
            $td = $self.closest("td");
            $self.remove();
            $td.html("Izvezeno");
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            alert(jqXHR.responseText);
        });
    })


});