/**
 * Created by mijatra on 2/21/2020.
 */

$(document).ready(function(){
    $("input[data-slugify-from]").focusout(function (event) {
        var slugifiedValue = Slugifier.slugify($(this).val());
        $slugifyToElem = $("input[data-slugify-to='" + $(this).data("slugify-from") + "']");
        $slugifyToElem.val(slugifiedValue);
        console.log(slugifiedValue);
    });

    $('.summernote').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'clear']],
            ['insert', ['link']],
            ['misc', ['undo', 'redo']]
        ],
        styleWithSpan: false,
        height: 150,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                e.preventDefault();

                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            }
        }
    });

});
