$(document).ready(function(){

    function sendFile(file, el) {
        var form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
            data: form_data,
            type: "POST",
            url: "/summernote/image/upload",
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $(el).summernote('editor.insertImage', location.origin + data.url);
            }
        });
    }

    $('.summernote').summernote({
        styleWithSpan: false,
        height: 200,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                e.preventDefault();

                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            },
            onImageUpload: function (files, editor, welEditable) {
                for (var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

});