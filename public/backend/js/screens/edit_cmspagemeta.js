$(document).ready(function() {
    $('.summernote').summernote({
        styleWithSpan: false,
        height: 200,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                e.preventDefault();

                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            }
        }
    });
});

$( "#cms_page_all_meta_language_selector" ).change(function() {
    $(".lngCnt").addClass("d-none");
    if($("#cms_page_all_meta_language_selector").val()!="") {
        $(".lngCnt" + $("#cms_page_all_meta_language_selector").val()).removeClass("d-none");
    }
});
