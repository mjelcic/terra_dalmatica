$(document).ready(function () {
    $('.summernote').summernote({
        styleWithSpan: false,
        height: 200,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                e.preventDefault();

                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            },
            onImageUpload: function (files, editor, welEditable) {
                for (var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });
    hideShowFields();

    for(var i = 0; i < 3; i++) {
        document.querySelector("#cms_page_cmsPageLocalizations_" + i + "_title").addEventListener("blur", function (e) {
            let text = e.target.value;

            let suggestedSlug = Slugifier.slugify(text);
            let slugId = e.target.id.replace("_title", "_slug");

            if(!document.querySelector("#" + slugId).value) {
                document.querySelector("#" + slugId).value = suggestedSlug;
            }
        })
    }
});


$("#cms_page_page_type").change(function () {
    hideShowFields();
});

function hideShowFields() {
    if ($("#cms_page_page_type").val() == 0) {
        $(".hide_on_predefined").removeClass("d-none");
        $(".hide_on_blog").addClass("d-none");
    }
    else {
        $(".hide_on_predefined").addClass("d-none");
        $(".hide_on_blog").removeClass("d-none");
    }
}

function sendFile(file, el) {
    var form_data = new FormData();
    form_data.append('file', file);
    $.ajax({
        data: form_data,
        type: "POST",
        url: "/summernote/image/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $(el).summernote('editor.insertImage', data.url);
			$('img[src="' + data.url + '"]').prop("alt", prompt("Alt tag:"));
        }
    });
}
