$(document).ready(function () {

    $("#btn-start-deactivation").click(function () {
        $("#account-deactivation").removeClass("d-none");
        $(this).parent("div").addClass("d-none");
    });

    $("#btn-keep-account").click(function () {
        $("#account-deactivation").addClass("d-none");
        $("#btn-start-deactivation").parent("div").removeClass("d-none");
    });

});