$(document).ready(function () {
    $(".btn-remove-favorite").click(function(e){
        e.preventDefault();
        $self = $(this);

        $.ajax($self.data("action"))
            .done(function (data) {
                if(data.success === true){
                    $self.closest("div.user-favorite-container").remove();
                }

                if(data.message){
                    alert(data.message);
                }

            })
            .fail(function(jqXR, status, error){
                alert(status + ":" + error);
            });
    });
});