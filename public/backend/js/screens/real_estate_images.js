function sendRequest() {
    var imageNodes = document.querySelectorAll(".single-image-container"),
        imageObj,
        localizationNodes,
        localeObj;

    var reqImages = [];

    for(var i = 0; i < imageNodes.length; i++) {
        imageObj = {};

        imageObj.id = imageNodes[i].dataset.imageId;
        imageObj.locales = [];

        localizationNodes = imageNodes[i].querySelectorAll(".locale");

        for(var j = 0; j < localizationNodes.length; j++){
             localeObj = {};
             localeObj.locale = localizationNodes[j].dataset.locale;
             localeObj.altTag = localizationNodes[j].querySelector(".alt-tag").value;

             imageObj.locales.push(localeObj);
        };
        reqImages.push(imageObj);

    };

    $.ajax("/admin/real-estate/" + realEstateId + "/image-metadata",
        {
            type: 'POST',
            data: JSON.stringify({"images": reqImages}),
            success: function(response) {
                if(response.html) {
                    var messageContainer = document.querySelector("#flashMessages");
                    messageContainer.insertAdjacentHTML("beforeend", response.html);
                    window.scrollTo({top: 0});
                }
            }
        });
}

function nodeListToArray(nodelist) {
    return Array.prototype.slice(nodelist, 0);
}