$(document).ready(function () {
    /**$('.add-another-collection-widget').click(function (e) {
        var list = $($(this).attr('data-list-selector'));
        // Try to find the counter of the list or use the length of the list
        var counter = list.data('widget-counter') || list.children().length;

        // grab the prototype template
        var newWidget = list.attr('data-prototype');
        // replace the "__name__" used in the id and name of the prototype
        // with a number that's unique to your emails
        // end name attribute looks like name="contact[emails][2]"
        newWidget = newWidget.replace(/__name__/g, counter);
        // Increase the counter
        counter++;
        // And store it, the length cannot be used if deleting widgets is allowed
        list.data('widget-counter', counter);

        // create a new list element and add it to the list
        var newElem = $(list.attr('data-widget-tags')).html(newWidget);
        newElem.appendTo(list);
    });*/

    var $tblUserContacts = $("#tbl-user-contacts");
    var $btnAddContact = $("#btn-add-contact");
    var $btnRemoveContact = $(".btn-remove-contact");

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $tblUserContacts.data('index', $tblUserContacts.find('tbody > tr').length);

    $btnAddContact.on('click', function(e) {
        // add a new tag form (see next code block)
        addNewContactRow($tblUserContacts);
    });


    $btnRemoveContact.click(function () {
        $(this).closest("tr").remove();
    });

    function addNewContactRow($collectionHolder) {
        // Get the data-prototype explained earlier
        // get the new index
        var index = $collectionHolder.data('index');

        var $namePrototype = $($collectionHolder.data('prototype-name').replace(/__name__/g, index))
            .addClass('form-control');
        var $phoneNumberPrototype = $($collectionHolder.data('prototype-phone_number').replace(/__name__/g, index))
            .addClass('form-control');
        var $mobilePhoneNumberPrototype = $($collectionHolder.data('prototype-mobile_phone_number').replace(/__name__/g, index))
            .addClass('form-control');

        var $btnRemoveContactRow = $('<button type="button" class="btn btn-danger">Ukloni</button>');
        var $newContactRow = $("<tr></tr>");

        var $dataHolder = $("<div class='row'></div>");


        $dataHolder.append($("<div class='form-group col-12 col-md-4'><label for='name' class='required'>Ime: </label></div>").append($namePrototype));
        $dataHolder.append($("<div class='form-group col-12 col-md-4'><label for='phone_number' class='required'>Broj telefona: </label></div>").append($phoneNumberPrototype));
        $dataHolder.append($("<div class='form-group col-12 col-md-4'><label for='mobile_phone_number' class='required'>Broj mobitela </label></div>").append($mobilePhoneNumberPrototype));

        $newContactRow.append($("<td></td>").append($dataHolder));
        $newContactRow.append($('<td></td>').append($btnRemoveContactRow));

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        $collectionHolder.find("tbody").append($newContactRow);

        //Wire the remove btn
        $btnRemoveContactRow.click(function () {
            $newContactRow.remove();
        });
    }
});