$(document).ready(function () {
    $("input[data-slugify-from]").focusout(function (event) {
        var slugifiedValue = Slugifier.slugify($(this).val());
        $slugifyToElem = $("input[data-slugify-to='" + $(this).data("slugify-from") + "']");
        $slugifyToElem.val(slugifiedValue);
        console.log(slugifiedValue);
    });
});
