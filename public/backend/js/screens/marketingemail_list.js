$(document).ready(function () {

    $(".btn-resend").click(function (e) {
        e.preventDefault();

        $.ajax($(this).prop("href"))
            .done(function( data, textStatus, jqXHR ) {
                alert(jqXHR.responseText);
            })
            .fail(function( jqXHR, textStatus, errorThrown ) {
                alert(jqXHR.responseText);
            });
    });

});